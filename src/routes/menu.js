import React from 'react';
import {
  PlaylistAddCheck,
} from '@material-ui/icons';
import Func from '../functions/index';

const menu = [
  {
    title: 'Dashboard',
    link: '/home/dashboard',
    active: Func.checkPermission('home'),
    key: 'home',
    icon: 'home_icon',
    breadcrumb: {
      '/home': 'Home'
    },
    subMenu: [
      {
        title: 'Dashboard',
        key: 'home#dashboard',
        active: Func.checkPermission('home#dashboard'),
        link: '/home/dashboard',
        icon: 'lens',
        breadcrumb: {
          '/home/dashboard': 'Dashboard'
        },
      },
    ]
  },
  {
    title: 'Master Data',
    link: '/master-data/country?page=1&per_page=10',
    active: Func.checkPermission('master-data'),
    key: 'master-data',
    icon: 'storage_icon',
    breadcrumb: {
      '/master-data': 'Master Data'
    },
    subMenu: [
      {
        title: 'Negara',
        key: 'country',
        active: Func.checkPermission('master-data#country'),
        link: '/master-data/country?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/country': 'Negara'
        },
      },
      {
        title: 'Provinsi',
        key: 'province',
        active: Func.checkPermission('master-data#province'),
        link: '/master-data/province?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/province': 'Provinsi'
        },
      },
      {
        title: 'Kota',
        key: 'city',
        active: Func.checkPermission('master-data#city'),
        link: '/master-data/city?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/city': 'Kota'
        },
      },
      {
        title: 'Kantor Pusat',
        key: 'heaf-office',
        active: Func.checkPermission('master-data#head-office'),
        link: '/master-data/head-office?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/head-office': 'Kantor Pusat'
        },
      },
      {
        title: 'Kantor Wilayah',
        key: 'regional-office',
        active: Func.checkPermission('master-data#regional'),
        link: '/master-data/regional?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/regional': 'Kantor Wilayah'
        },
      },
      {
        title: 'Kantor Area',
        key: 'area',
        active: Func.checkPermission('master-data#area'),
        link: '/master-data/area?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/area': 'Area'
        },
      },
      {
        title: 'Kantor Cabang',
        key: 'branch-office',
        active: Func.checkPermission('master-data#branch-office'),
        link: '/master-data/branch-office?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/branch-office': 'Kantor Cabang'
        },
      },
      {
        title: 'Kantor Unit',
        key: 'unit-office',
        active: Func.checkPermission('master-data#unit-office'),
        link: '/master-data/unit-office?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/unit-office': 'Kantor Unit'
        },
      },
      {
        title: 'Bank',
        key: 'bank',
        active: Func.checkPermission('master-data#bank'),
        link: '/master-data/bank?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/bank': 'Bank'
        },
      },
      {
        title: 'Profesi',
        key: 'profession',
        active: Func.checkPermission('master-data#profession'),
        link: '/master-data/profession?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/profession': 'Profesi'
        },
      },
      {
        title: 'Pengguna',
        key: 'user',
        active: Func.checkPermission('master-data#user'),
        link: '/master-data/user?page=1&per_page=10',
        icon: 'lens',
        breadcrumb: {
          '/master-data/user': 'Pengguna'
        },
      },
      {
        title: 'Role',
        key: 'role',
        active: Func.checkPermission('master-data#role'),
        link: '/master-data/role',
        icon: 'lens',
        breadcrumb: {
          '/master-data/role': 'Role'
        },
      },
    ]
  },
  {
    title: 'Manajemen Parameter',
    link: '/parameters-management/transaction?page=1&per_page=10',
    active: Func.checkPermission('parameters-management'),
    key: 'parameters-management',
    icon: 'bar_chart_icon',
    breadcrumb: {
      '/parameters-management': 'Manajemen Parameter'
    },
    subMenu: [
      {
        title: 'Transaksi',
        link: '/parameters-management/transaction?page=1&per_page=10',
        key: 'transaction',
        active: Func.checkPermission('parameters-management#transaction'),
        icon: 'lens',
        breadcrumb: {
          '/parameters-management/transaction': 'Transaksi'
        },
      },
      {
        title: 'Nasabah',
        link: '/parameters-management/customer',
        active: Func.checkPermission('parameters-management#customer'),
        key: 'customer',
        icon: 'lens',
        breadcrumb: {
          '/parameters-management/customer': 'Nasabah'
        },
      },
      {
        title: 'Produk',
        link: '/parameters-management/product',
        active: Func.checkPermission('parameters-management#product'),
        key: 'product',
        icon: 'lens',
        breadcrumb: {
          '/parameters-management/product': 'Produk'
        },
      },
      {
        title: 'Jenis Barang Jaminan',
        link: '/parameters-management/type-of-collateral',
        key: 'type-of-collateral',
        active: Func.checkPermission('parameters-management#type-of-collateral'),
        icon: 'lens',
        breadcrumb: {
          '/parameters-management/type-of-collateral': 'Jenis Barang Jaminan'
        },
      },
      {
        title: 'Master Mata Uang',
        link: '/parameters-management/master-currency?page=1&per_page=10',
        active: Func.checkPermission('parameters-management#master-currency'),
        key: 'master-currency',
        icon: 'lens',
        breadcrumb: {
          '/parameters-management/master-currency': 'Master Mata Uang'
        },
      },
    ]
  },
  {
    title: 'Manajemen Keuangan',
    link: '/financial-management/home',
    active: Func.checkPermission('financial-management'),
    key: 'financial-management',
    icon: 'attach_money_icon',
    breadcrumb: {
      '/financial-management': 'Finansial Manajemen'
    },
    subMenu: [
      {
        key: 'home',
        title: 'Home',
        link: '/financial-management/home',
        active: Func.checkPermission('financial-management#home'),
        icon: 'lens',
        breadcrumb: {
          '/financial-management/home': 'Home'
        },
      },
      {
        key: 'central-account',
        title: 'Rekening Pusat',
        active: Func.checkPermission('financial-management#central-account'),
        link: '/financial-management/central-account',
        icon: 'lens',
        breadcrumb: {
          '/financial-management/central-account': 'Rekening Pusat'
        },
      },
      {
        key: 'branch-account',
        title: 'Rekening Cabang',
        active: Func.checkPermission('financial-management#branch-account'),
        link: '/financial-management/branch-account',
        icon: 'lens',
        breadcrumb: {
          '/financial-management/branch-account': 'Rekening Cabang'
        },
      },
      {
        key: 'request',
        title: 'Permintaan',
        active: Func.checkPermission('financial-management#request'),
        link: '/financial-management/request',
        icon: 'lens',
        breadcrumb: {
          '/financial-management/request': 'Permintaan'
        },
      }
    ]
  },
  {
    title: 'Manajemen Akuntansi',
    link: '/accounting-management/account',
    active: Func.checkPermission('accounting-management'),
    key: 'accounting-management',
    icon: 'account_box_icon',
    breadcrumb: {
      '/accounting-management': 'Manajemen Akuntansi'
    },
    subMenu: [
      {
        key: 'account',
        title: 'Akun',
        active: Func.checkPermission('accounting-management#account'),
        link: '/accounting-management/account',
        icon: 'lens',
        breadcrumb: {
          '/accounting-management/account': 'Akun'
        },
      },
      {
        key: 'coa',
        title: 'COA',
        active: Func.checkPermission('accounting-management#coa'),
        link: '/accounting-management/coa',
        icon: 'lens',
        breadcrumb: {
          '/accounting-management/coa': 'COA'
        },
      },
    ]
  },
  {
    title: 'Pengaturan',
    link: '/setting/assign-menu-to-role-sispus',
    active: Func.checkPermission('setting'),
    key: 'setting',
    icon: 'settings_icon',
    breadcrumb: {
      '/accounting-management': 'Manajemen Akuntansi'
    },
    subMenu: [
      {
        key: 'setting#assign-menu-to-role-sispus',
        title: 'Role S-Pusat',
        active: Func.checkPermission('setting#assign-menu-to-role-sispus'),
        link: '/setting/assign-menu-to-role-sispus',
        icon: 'lens',
        breadcrumb: {
          '/setting/assign-menu-to-role-sispus': 'Role S-Pusat'
        },
      },
      {
        key: 'setting#assign-menu-to-role-siscab',
        title: 'Role S-Cabang',
        active: Func.checkPermission('setting#assign-menu-to-role-siscab'),
        link: '/setting/assign-menu-to-role-siscab',
        icon: 'lens',
        breadcrumb: {
          '/setting/assign-menu-to-role-siscab': 'Role S-Cabang'
        },
      },
      {
        key: 'setting#menu-sispus',
        title: 'Menu S-Pusat',
        link: '/setting/menu-sispus',
        active: Func.checkPermission('setting#menu-sispus'),
        icon: 'lens',
        breadcrumb: {
          '/setting/menu-sispus': 'Menu S-Pusat'
        },
      },
      {
        key: 'setting#menu-siscab',
        title: 'Menu S-Cabang',
        active: Func.checkPermission('setting#menu-siscab'),
        link: '/setting/menu-siscab',
        icon: 'lens',
        breadcrumb: {
          '/setting/menu-siscab': 'Menu S-Cabang'
        },
      }
    ]
  },
  {
    title: 'Approval',
    link: '/approval/workcap',
    active: Func.checkPermission('approval'),
    key: 'approval',
    icon: <PlaylistAddCheck />,
    breadcrumb: {
      '/approval': 'Approval'
    },
    subMenu: [
      {
        title: 'Modal Kerja',
        key: 'approval#workcap',
        active: Func.checkPermission('approval#workcap'),
        link: '/approval/workcap',
        icon: 'lens',
        breadcrumb: {
          '/approval/workcap': 'Modal Kerja'
        },
      },
      {
        title: 'Deviasi',
        key: 'approval#deviation',
        active: Func.checkPermission('approval#deviation'),
        link: '/approval/deviation',
        icon: 'lens',
        breadcrumb: {
          '/approval/deviation': 'Deviasi'
        },
      },
    ]
  },
  {
    title: 'Pengaturan Profil',
    link: '/profile',
    active: false,
    key: 'profile',
    icon: 'lens',
    breadcrumb: {
      '/profile': 'Pengaturan Profil'
    }
  },
  {
    title: 'Ubah Password',
    link: '/change-password',
    active: false,
    key: 'change-password',
    icon: 'lens',
    breadcrumb: {
      '/change-password': 'Ubah Password'
    }
  },
  {
    title: 'Notifikasi',
    link: '/notification',
    active: false,
    key: 'notification',
    icon: 'lens',
    breadcrumb: {
      '/notification': 'Notifikasi'
    }
  },
];

export default menu;
