/* eslint-disable camelcase */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import Layout from '../container/Layout';
import PrivateRoute from '../components/PrivateRoute';
import {
  DashboardPage,
  BranchAccount,
  Country,
  Province,
  City,
  OfficeArea,
  HeadOffice,
  BranchOffice,
  UnitOffice,
  Bank,
  User,
  Role,
  Product,
  Customer,
  TypeOfColleteral,
  MasterCurrency,
  Transaction,
  FinanceHome,
  CentralAccount,
  Request,
  RegionalOffice,
  Profession,
  Account,
  Coa,
  Sispus,
  Siscab,
  AssignMenuToRole_SisPus,
  AssignMenuToRole_SisCab,
  AllNotification,
  WorkcapApproval,
  DeviationApproval,
  Profile,
  ChangePassword,
  accessDened
} from '../container/Pages/asyncpages';
import application from '../config/application';
import NotFound from './NotFound';

const SecureRoutes = ({ history }) => {
  const securePath = application.privatePath;

  return (
    <Layout history={history}>
      <Switch>

        {/* Dashboard  */}

        <PrivateRoute exact path={`${securePath}/home/dashboard`} component={DashboardPage} />

        {/* Master Data */}

        <PrivateRoute path={`${securePath}/master-data/country`} component={Country} />
        <PrivateRoute path={`${securePath}/master-data/province`} component={Province} />
        <PrivateRoute path={`${securePath}/master-data/city`} component={City} />
        <PrivateRoute path={`${securePath}/master-data/regional`} component={RegionalOffice} />
        <PrivateRoute path={`${securePath}/master-data/area`} component={OfficeArea} />
        <PrivateRoute path={`${securePath}/master-data/head-office`} component={HeadOffice} />
        <PrivateRoute path={`${securePath}/master-data/branch-office`} component={BranchOffice} />
        <PrivateRoute path={`${securePath}/master-data/unit-office`} component={UnitOffice} />
        <PrivateRoute path={`${securePath}/master-data/bank`} component={Bank} />
        <PrivateRoute path={`${securePath}/master-data/profession`} component={Profession} />
        <PrivateRoute path={`${securePath}/master-data/user`} component={User} />
        <PrivateRoute path={`${securePath}/master-data/role`} component={Role} />

        {/* Parameter Management */}

        <PrivateRoute path={`${securePath}/parameters-management/transaction`} component={Transaction} />
        <PrivateRoute path={`${securePath}/parameters-management/product`} component={Product} />
        <PrivateRoute path={`${securePath}/parameters-management/customer`} component={Customer} />
        <PrivateRoute path={`${securePath}/parameters-management/type-of-collateral`} component={TypeOfColleteral} />
        <PrivateRoute path={`${securePath}/parameters-management/master-currency`} component={MasterCurrency} />

        {/* Financial management */}

        <PrivateRoute path={`${securePath}/financial-management/home`} component={FinanceHome} />
        <PrivateRoute path={`${securePath}/financial-management/branch-account`} component={BranchAccount} />
        <PrivateRoute path={`${securePath}/financial-management/central-account`} component={CentralAccount} />
        <PrivateRoute path={`${securePath}/financial-management/request`} component={Request} />

        {/* Management Accounting */}

        <PrivateRoute path={`${securePath}/accounting-management/account`} component={Account} />
        <PrivateRoute path={`${securePath}/accounting-management/coa`} component={Coa} />

        {/* Settings   */}

        <PrivateRoute path={`${securePath}/setting/menu-sispus`} component={Sispus} />
        <PrivateRoute path={`${securePath}/setting/menu-siscab`} component={Siscab} />
        <PrivateRoute path={`${securePath}/setting/assign-menu-to-role-sispus`} component={AssignMenuToRole_SisPus} />
        <PrivateRoute path={`${securePath}/setting/assign-menu-to-role-siscab`} component={AssignMenuToRole_SisCab} />
        <Route path={`${securePath}/profile`} component={Profile} />
        <Route path={`${securePath}/change-password`} component={ChangePassword} />

        {/* Approval */}

        <PrivateRoute path={`${securePath}/approval/workcap`} component={WorkcapApproval} />
        <PrivateRoute path={`${securePath}/approval/deviation`} component={DeviationApproval} />

        {/* etc */}
        <Route path={`${securePath}/notification`} component={AllNotification} />
        <Route path={`${securePath}/page/access-dened`} component={accessDened} />
        <Route component={NotFound} />

      </Switch>
    </Layout>
  );
};

SecureRoutes.propTypes = ({
  history: PropTypes.object.isRequired
});

export default SecureRoutes;
