/* eslint-disable consistent-return */
/* eslint-disable padded-blocks */
import axios from 'axios';
import env from '../config/env';

const authApi = axios.create({
  baseURL: env.authApi
});

const refreshToken = async () => {
  try {
    const res = await authApi.post('/oauth/token', {
      client_id: env.clientId,
      client_secret: env.clientSecret,
      scope: 'read write',
      grant_type: 'refresh_token',
      refresh_token: localStorage.getItem('refresh_token')
    });

    if (res.status === 200) {

      localStorage.clear();
      // eslint-disable-next-line camelcase
      const { data: { access_token, refresh_token, user } } = res;
      localStorage.setItem('token', access_token);
      localStorage.setItem('refresh_token', refresh_token);
      localStorage.setItem('user', JSON.stringify(user));
    }
  } catch (error) {
    return error;
  }
};

export default refreshToken;
