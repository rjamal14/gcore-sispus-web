/* eslint-disable linebreak-style */
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

let theme = createMuiTheme();
theme = responsiveFontSizes(theme);

const styles = {
  // Styleing React Component
  mainContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E5E5E5'
  },
  container: {
    display: 'flex',
    alignSelf: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  // Styleing Material Component
  CoustomsStyles: {
    form: {
      width: '80%',
      [
      theme
        .breakpoints
        .only('xs')
      ]: {
        width: '95%',
      }
    },
    title: {
      fontWeight: 'normal',
      color: '#0E0E0E',
      fontSize: '34px',
      [
      theme
        .breakpoints
        .only('xs')
      ]: {
        fontSize: '20px'
      }
    },
    img: {
      marginBottom: 15,
      marginTop: 15,
      width: '350px',
      heigth: '250px',
      [
      theme
        .breakpoints
        .only('xs')
      ]: {
        width: '250px',
        heigth: '150px'
      }
    },
    Card: {
      width: 620,
      [
      theme
        .breakpoints
        .only('xs')
      ]: {
        width: '300px'
      }
    },
    input: {
      '&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
        borderColor: '#C4A643'
      },
      '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
        borderColor: '#C4A643'
      }
    },
    links: {
      color: '#C4A643',
      marginTop: 5,
      marginRight: -15
    }
  }
};
export default styles;
