/* eslint-disable linebreak-style */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable consistent-return */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */

import React from 'react';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import InputAdornment from '@material-ui/core/InputAdornment';
import { Redirect } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Func from '../../../functions';
import Icon from '../../../components/icon';
import Button from '../../../components/button';
import styles from './css';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      redirect: false,
      show: 'password',
      show2: 'password'
    };
    this.handleSubmit = this
      .handleSubmit
      .bind(this);
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleSubmit(event) {
    const validator = [
      {
        name: 'email',
        type: 'mail|required'
      }, {
        name: 'password',
        type: 'required'
      }, {
        name: 'confirm_password',
        type: 'same:password|required'
      }
    ];
    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      const str = this.props.location.search;
      fetch(process.env.REACT_APP_URL_AUTH + 'user/reset?token=' + str.split('=')[1], {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: this.state.value.email, password: this.state.value.password, confirm_password: this.state.value.confirm_password })
      }).then((response) => response.json()).then((json) => {
        if (json.message === 'berhasil reset password') {
          Func.Alert('Reset Kata Sandi Berhasil', 'Silahkan masuk untuk mencoba kata sandi baru.', Icon.error_lock);
          this.setState({ redirect: true });
        } else if (json.message === 'Link not valid or expired. Try generating a new link.') {
          Func.Alert('Reset Kata Sandi Gagal', 'Tautan tidak valid atau kedaluwarsa, Coba buat tautan baru.', Icon.error_lock);
        } else {
          Func.Alert('Reset Kata Sandi Gagal', json.message, Icon.error_lock);
        }
      }).catch(() => {})
        . finally(() => {});
    } else {
      this.setState({ validator: validate.error });
    }
    event.preventDefault();
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

    renderRedirect = () => {
      if (this.state.redirect) {
        return <Redirect to="/login" />;
      }
    };

    render() {
      const { classes } = this.props;
      return (
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justify="center"
          style={{
            minHeight: '100vh'
          }}
        >
          <div style={styles.mainContainer}>
            <Card className={classes.Card}>
              <CardContent>
                {this.renderRedirect()}
                <div style={styles.container}>
                  <img src={Icon.logo} className={classes.img} />
                  <Typography className={classes.title}>
                    Reset Kata Sandi
                  </Typography>
                  <form className={classes.form} onSubmit={this.handleSubmit}>
                    <TextField
                      className={classes.input}
                      variant="outlined"
                      margin="normal"
                      autoComplete="off"
                      fullWidth
                      onFocus={() => {
                        this.removeValidate('email');
                      }}
                      error={this.state.validator.email}
                      helperText={this.state.validator.email}
                      value={this.state.value.email}
                      onChange={(event) => {
                        this.handleChange(event, 'email');
                      }}
                      placeholder="Email atau NIK"
                      name="email"
                      autoFocus
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <img src={Icon.mail} />
                          </InputAdornment>
                        ),
                        endAdornment: this.state.validator.email
                          ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          )
                          : (<div />)
                      }}
                    />
                    <TextField
                      className={classes.input}
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      onFocus={() => {
                        this.removeValidate('password');
                      }}
                      value={this.state.value.password}
                      onChange={(event) => {
                        this.handleChange(event, 'password');
                      }}
                      error={this.state.validator.password}
                      helperText={this.state.validator.password}
                      name="password"
                      placeholder="Kata Sandi"
                      type={this.state.show}
                      id="password"
                      autoComplete="off"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <img src={Icon.lock} />
                          </InputAdornment>
                        ),
                        endAdornment: this.state.validator.password
                          ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          )
                          : this.state.show === 'password'
                            ? (
                              <InputAdornment position="start">
                                <img
                                  onClick={() => {
                                    this.setState({ show: 'text' });
                                  }}
                                  src={Icon.eye}
                                />
                              </InputAdornment>
                            )
                            : (
                              <InputAdornment position="start">
                                <img
                                  onClick={() => {
                                    this.setState({ show: 'password' });
                                  }}
                                  src={Icon.eye2}
                                />
                              </InputAdornment>
                            )
                      }}
                    />
                    <TextField
                      className={classes.input}
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      onFocus={() => {
                        this.removeValidate('confirm_password');
                      }}
                      value={this.state.value.confirm_password}
                      onChange={(event) => {
                        this.handleChange(event, 'confirm_password');
                      }}
                      error={this.state.validator.confirm_password}
                      helperText={this.state.validator.confirm_password}
                      name="confirm_password"
                      placeholder="Konformasi Kata Sandi"
                      type={this.state.show2}
                      id="confirm_password"
                      autoComplete="off"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <img src={Icon.lock} />
                          </InputAdornment>
                        ),
                        endAdornment: this.state.validator.confirm_password
                          ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          )
                          : this.state.show2 === 'password'
                            ? (
                              <InputAdornment position="start">
                                <img
                                  onClick={() => {
                                    this.setState({ show2: 'text' });
                                  }}
                                  src={Icon.eye}
                                />
                              </InputAdornment>
                            )
                            : (
                              <InputAdornment position="start">
                                <img
                                  onClick={() => {
                                    this.setState({ show2: 'password' });
                                  }}
                                  src={Icon.eye2}
                                />
                              </InputAdornment>
                            )
                      }}
                    />
                    <Button text="Reset" />
                  </form>
                </div>
              </CardContent>
            </Card>
          </div>
        </Grid>

      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
