/* eslint-disable linebreak-style */
/* eslint-disable no-useless-escape */
/* eslint-disable no-var */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable no-shadow */
/* eslint-disable func-names */
/* eslint-disable vars-on-top */
/* eslint-disable no-plusplus */
/* eslint-disable no-dupe-keys */
/* eslint-disable no-redeclare */
/* eslint-disable no-cond-assign */
/* eslint-disable no-unused-vars */
/* eslint-disable react/sort-comp */
/* eslint-disable block-scoped-var */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-extend-native */
/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable array-callback-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-unused-state */
import React from 'react';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Func from '../../../functions';
import Icon from '../../../components/icon';
import Button from '../../../components/button';
import styles from './css';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      mdlShow: true,
      mobile: false,
      redirect: false,
      show: 'password'
    };
    this.handleSubmit = this
      .handleSubmit
      .bind(this);
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
    this.setState({ verify: false });
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(event.target.value)) {
      this.setState({ verify: true });
    }
  }

  handleSubmit(event) {
    const validator = [
      {
        name: 'email',
        type: 'mail|required'
      }
    ];
    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      fetch(process.env.REACT_APP_URL_AUTH + 'user/forgot', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: this.state.value.email })
      }).then((response) => response.json()).then((json) => {
        if (json.message == 'No such email') {
          Func.Alert('Alamat email Anda tidak terdaftar', 'Mohon lakukan registrasi ulang', Icon.error_mail);
        } else if (json.message == 'link reset password telah dikirim ke email') {
          Func.Alert('Email berhasil dikirim', 'Silahkan periksa kotak masuk email Anda', Icon.succes_mail);
        } else {
          Func.Alert('Email tidak terkirim', 'Tunngu 5 menit dan coba kembali', Icon.error_mail);
        }
      }).catch((error) => {})
        . finally(() => {});
    } else {
      this.setState({ validator: validate.error });
    }
    event.preventDefault();
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  // renderRedirect = () => {
  //     if (this.state.redirect) {
  //         return <Redirect to="/master-data/country"/>;
  //     }
  // };

  render() {
    const { classes } = this.props;
    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{
          minHeight: '100vh'
        }}
      >
        <div style={styles.mainContainer}>
          <Card className={classes.Card}>
            <CardContent>
              <div style={styles.container}>
                <img src={Icon.logo} className={classes.img} />
                <Typography className={classes.title}>
                  Lupa Kata Sandi?
                </Typography>
                <br />
                <Typography className={classes.subTitle}>
                  Silahkan masukkan alamat email Anda. Kami akan
                </Typography>
                <Typography className={classes.subTitle}>
                  mengirimkan tautan untuk mengatur ulang kata sandi Anda.
                </Typography>
                <br />
                <form className={classes.form} onSubmit={this.handleSubmit}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    autoComplete="off"
                    fullWidth
                    onFocus={() => {
                      this.removeValidate('email');
                    }}
                    error={this.state.validator.email}
                    helperText={this.state.validator.email}
                    value={this.state.value.email}
                    onChange={(event) => {
                      this.handleChange(event, 'email');
                    }}
                    placeholder="Email"
                    name="email"
                    autoFocus
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <img src={Icon.mail} />
                        </InputAdornment>
                      ),
                      endAdornment: this.state.validator.email
                        ? (
                          <InputAdornment position="start">
                            <img src={Icon.warning} />
                          </InputAdornment>
                        )
                        : this.state.verify
                          ? (
                            <InputAdornment position="start">
                              <img src={Icon.check} />
                            </InputAdornment>
                          )
                          : (<div />)
                    }}
                  />
                  <Button text="Kirim" />
                </form>
                <Link className={classes.links} href="/">
                  Kembali
                </Link>
              </div>
            </CardContent>
          </Card>
        </div>
      </Grid>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
