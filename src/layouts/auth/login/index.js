/* eslint-disable linebreak-style */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable func-names */
/* eslint-disable no-cond-assign */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-extend-native */
/* eslint-disable camelcase */
/* eslint-disable no-dupe-keys */
/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable array-callback-return */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles';
import Icon from '../../../components/icon';
import Button from '../../../components/button';
import styles from './css';
import Func from '../../../functions';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      mdlShow: true,
      mobile: false,
      redirect: false,
      show: 'password'
    };
    this.handleSubmit = this
      .handleSubmit
      .bind(this);
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleSubmit(event) {
    const validator = [
      {
        name: 'email',
        type: 'required'
      }, {
        name: 'password',
        type: 'required'
      }
    ];
    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      fetch(process.env.REACT_APP_URL_TOKEN, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ username: this.state.value.email, password: this.state.value.password, grant_type: 'password' })
      }).then((response) => response.json()).then((json) => {
        if (json.message == 'login berhasil') {
          localStorage.setItem('token', json.access_token);
          localStorage.setItem('refresh_token', json.refresh_token);
          localStorage.setItem('name', json.user.first_name + ' ' + json.user.last_name);
          localStorage.setItem('id', json.user._id.$oid);
          localStorage.setItem('position', json.user.position);
          window.location = '/app/home/dashboard';
        } else {
          Func.Alert('Anda tidak dapat masuk', 'Pastikan Anda mengisi kotak email atau NIK dan kata sandi dengan benar', Icon.error_lock);
        }
      }).catch((error) => {})
        . finally(() => {});
    } else {
      this.setState({ validator: validate.error });
    }
    event.preventDefault();
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  // renderRedirect = () => {
  //   if (this.state.redirect) {
  //     return <Redirect to="/master-data/country" />;
  //   }
  // };

  componentDidMount() {
    if (localStorage.getItem('token')) {
      this.setState({ redirect: true });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{
          minHeight: '100vh'
        }}
      >
        <Card className={classes.Card}>
          <CardContent>
            <div style={styles.container}>
              <img src={Icon.logo} className={classes.img} />
              <Typography component="h1" variant="h4">
                Masuk
              </Typography>
              <form className={classes.form} onSubmit={this.handleSubmit}>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('email');
                  }}
                  error={this.state.validator.email}
                  helperText={this.state.validator.email}
                  value={this.state.value.email}
                  onChange={(event) => {
                    this.handleChange(event, 'email');
                  }}
                  placeholder="Email atau NIK"
                  name="email"
                  autoFocus
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <img src={Icon.mail} />
                      </InputAdornment>
                    ),
                    endAdornment: this.state.validator.email
                      ? (
                        <InputAdornment position="start">
                          <img src={Icon.warning} />
                        </InputAdornment>
                      )
                      : (<div />)
                  }}
                />
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('password');
                  }}
                  value={this.state.value.password}
                  onChange={(event) => {
                    this.handleChange(event, 'password');
                  }}
                  error={this.state.validator.password}
                  helperText={this.state.validator.password}
                  name="password"
                  placeholder="Kata Sandi"
                  type={this.state.show}
                  id="password"
                  autoComplete="off"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <img src={Icon.lock} />
                      </InputAdornment>
                    ),
                    endAdornment: this.state.validator.password
                      ? (
                        <InputAdornment position="start">
                          <img src={Icon.warning} />
                        </InputAdornment>
                      )
                      : this.state.show == 'password'
                        ? (
                          <InputAdornment position="start">
                            <img
                              onClick={() => {
                                this.setState({ show: 'text' });
                              }}
                              src={Icon.eye}
                            />
                          </InputAdornment>
                        )
                        : (
                          <InputAdornment position="start">
                            <img
                              onClick={() => {
                                this.setState({ show: 'password' });
                              }}
                              src={Icon.eye2}
                            />
                          </InputAdornment>
                        )
                  }}
                />
                <Grid container>
                  <Grid item xs={6} sm={6} lg={6} md={6}>
                    <Box display="flex" flexDirection="row" bgcolor="background.paper">
                      <Box>
                        <FormControlLabel
                          control={<Checkbox style={{ color: '#C4A643' }} value="remember" color="primary" />}
                          label="Ingat saya"
                        />
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={6} sm={6} lg={6} md={6}>
                    <Box display="flex" flexDirection="row-reverse" bgcolor="background.paper">
                      <Box>
                        <FormControlLabel
                          control={(
                            <Link
                              className={
                                classes.links
                              }
                              href="/lupa-kata-sandi"
                            >
                              {' '}
                              Lupa kata sandi
                              ?
                              {' '}
                            </Link>
                          )}
                        />
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
                <Button text="Masuk" />
              </form>
            </div>
          </CardContent>
        </Card>
      </Grid>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
