/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from './env';
import { checkTokenExpired, SetAuthTokenRequest } from './interceptor';

export const apiWithToken = axios.create({
  baseURL: `${env.masterApi}/api/v1`,
});

apiWithToken.interceptors.request.use(SetAuthTokenRequest, (err) => err);
apiWithToken.interceptors.response.use((res) => res, checkTokenExpired);
