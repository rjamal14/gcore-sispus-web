/* eslint-disable import/prefer-default-export */
import _ from 'lodash';
import React from 'react';
import env from '../config/env';

export function getField(formCollection, fieldName) {
  return _.find(formCollection, { name: fieldName });
}

export function showImage(formCollection, fieldName) {
  const path = env.masterApi + '/' + getField(formCollection, fieldName);
  return (
    <img alt="showimg" src={path} />
  );
}
