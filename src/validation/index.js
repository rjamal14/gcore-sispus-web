import Joi from 'joi';

const validationSchema = Joi.array().required().items(
  Joi.object().required().keys({
    regional_id: Joi.optional(),
    stle_value: Joi.number().required(),
    ltv_value: Joi.number().required(),
    id: Joi.optional().allow(''),
    regional_val: {
      id: Joi.optional(),
      name: Joi.string().required()
    },
    product_finance_id: Joi.optional(),
    regional_name: Joi.optional()
  })
);

export default validationSchema;

export const deviation = Joi.array().items(Joi.object({
  area: Joi.number().required(),
  branch: Joi.number().required(),
  head: Joi.number().required(),
  region: Joi.number().required(),
  insurance_item_id: Joi.optional(),
  regional_id: Joi.optional(),
  _destroy: Joi.optional(),
  id: Joi.optional()
}));

export const prolongationSchema = Joi.array().items(Joi.object({
  insurance_item_id: Joi.optional(),
  _destroy: Joi.valid(true, false),
  value: Joi.when('_destroy', { is: false, then: Joi.number().required() }),
  type: Joi.optional(),
  order: Joi.optional(),
  id: Joi.optional()
}));
