/* eslint-disable radix */
import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import FormComponent from './FormComponent';
import Func from '../../../../functions/index';

const UnitOffice = () => {
  const mapParams = (params) => {
    const newParams = params;

    newParams.id = params._id.$oid;
    newParams.city_id = { id: params.city_id, name: params.city_name };
    newParams.branch_office_id = { id: params.branch_office_id, name: params.branch_office_name };
    newParams.coordinate = { lat: params.latitude, lng: params.longitude };

    return newParams;
  };

  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Kantor Unit"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        mapParams={mapParams}
        parseResponse={parseResponse}
        parseId={parseId}
        formComponent={FormComponent}
        accessCreate={Func.checkPermission('master-data#unit-office#create')}
        accessUpdate={Func.checkPermission('master-data#unit-office#update')}
        accessDelete={Func.checkPermission('master-data#unit-office#delete')}
      />
    </Fragment>
  );
};
UnitOffice.propTypes = ({

});

export default UnitOffice;
