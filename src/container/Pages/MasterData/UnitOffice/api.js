/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1,
});

const path = '/unit_offices';
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

const mapParams = (params) => {
  const newParams = params;

  newParams.city_id = params.city_id.id;
  newParams.branch_office_id = params.branch_office_id.id;
  newParams.longitude = params.coordinate.lng;
  newParams.latitude = params.coordinate.lat;

  return newParams;
};

export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { unit_office: mapParams(params) });
export const editApi = (id, params) => api.put(`${path}/${id}`, { unit_office: mapParams(params) });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
