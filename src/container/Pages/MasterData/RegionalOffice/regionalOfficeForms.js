/* eslint-disable import/no-named-as-default-member */
import GoogleMapsField from '../../../../components/FormField/Fields/GoogleMapsField';
import SearchAutoComplete from '../../../../components/FormField/Fields/SearchAutoComplete';
import Text from '../../../../components/FormField/Fields/Text';
import { getApi as cityApi } from '../City/api';
import { getApi as headOfficeApi } from '../HeadOffice/api';
import FileUpload from '../../../../components/FormField/Fields/FileUpload';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const forms = [
  {
    name: 'code',
    label: 'Kode',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'company_name',
    label: 'Nama Perusahaan',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'logo',
    label: 'Logo',
    component: FileUpload,
  },
  {
    name: 'name',
    label: 'Nama',
    value: '',
    component: Text,
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'head_office_id',
    label: 'Kantor Pusat',
    value: '',
    component: SearchAutoComplete,
    api: headOfficeApi,
    fieldProps: {
      variant: 'outlined',
      style: { marginBottom: 8 },
      required: true,
      validate: required,
      placeholder: 'Ketikan sesuatu...'
    }
  },
  {
    name: 'city_id',
    label: 'Kota',
    value: '',
    component: SearchAutoComplete,
    api: cityApi,
    fieldProps: {
      variant: 'outlined',
      style: { marginBottom: 8 },
      required: true,
      validate: required,
      placeholder: 'Ketikan sesuatu...'
    }
  },
  {
    name: 'address',
    label: 'Alamat',
    value: '',
    component: Text,
    fieldProps: {
      required: true,
      validate: required,
      multiline: true,
      rows: 4
    }
  },
  {
    name: 'description',
    label: 'Deskripsi',
    value: '',
    component: Text,
    fieldProps: {
      multiline: true,
      rows: 4
    }
  },
  {
    name: 'coordinate',
    label: 'Koordinat',
    value: '',
    component: GoogleMapsField,
    fieldProps: {
      variant: 'outlined',
    }
  },
];

export default forms;
