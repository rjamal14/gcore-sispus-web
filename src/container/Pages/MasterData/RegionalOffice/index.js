import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './regionalOfficeColumns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import forms from './regionalOfficeForms';
import RegionalForm from './RegionalForm';
import Func from '../../../../functions/index';

const RegionalOffice = () => {
  const mapParams = (params) => {
    const newParams = params;

    newParams.id = params._id.$oid;
    newParams.city_id = { id: params.city_id, name: params.city_name };
    newParams.head_office_id = { _id: { $oid: params.head_office_id }, name: params.head_office_name };
    newParams.coordinate = { lat: params.latitude, lng: params.longitude };
    newParams.logo = params.logo;
    newParams.company_name = params.company_name;

    return newParams;
  };

  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={forms}
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        mapParams={mapParams}
        parseResponse={parseResponse}
        parseId={parseId}
        formComponent={RegionalForm}
        accessCreate={Func.checkPermission('master-data#regional#create')}
        accessUpdate={Func.checkPermission('master-data#regional#update')}
        accessDelete={Func.checkPermission('master-data#regional#delete')}
      />
    </Fragment>
  );
};
RegionalOffice.propTypes = ({

});

export default RegionalOffice;
