/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1,
});

const path = '/regionals';
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

const mapParams = (params) => {
  const newParams = params;

  newParams.city_id = params.city_id.id;
  newParams.head_office_id = params.head_office_id._id.$oid;
  newParams.company_name = params.company_name;
  newParams.longitude = params.coordinate.lng;
  newParams.latitude = params.coordinate.lat;

  return newParams;
};

export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { regional: mapParams(params) });
export const editApi = (id, params) => api.put(`${path}/${id}`, { regional: mapParams(params) });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
