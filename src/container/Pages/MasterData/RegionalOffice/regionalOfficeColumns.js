const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'code',
    label: 'Kode',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'name',
    label: 'Nama',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'address',
    label: 'Alamat',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'city',
    label: 'Kota',
    display: false,
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'description',
    label: 'Deskripsi',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'action',
    label: 'Aksi',
    options: {
      filter: false,
      sort: false
    }
  },
];

export default columns;
