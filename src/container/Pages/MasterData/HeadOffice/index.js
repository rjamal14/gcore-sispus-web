/* eslint-disable radix */
/* eslint-disable camelcase */
import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import FormComponent from './FormComponent';
import Func from '../../../../functions/index';

const HeadOffice = () => {
  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const mapParams = (params) => {
    const newParams = params;

    newParams.coordinate = { lat: parseFloat(params.latitude), lng: parseFloat(params.longitude) };
    newParams.city_id = { id: params.city_id, name: params.city_name };

    return newParams;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Kantor Pusat"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        mapParams={mapParams}
        parseId={parseId}
        formComponent={FormComponent}
        accessCreate={Func.checkPermission('master-data#head-office#create')}
        accessUpdate={Func.checkPermission('master-data#head-office#update')}
        accessDelete={Func.checkPermission('master-data#head-office#delete')}
      />
    </Fragment>
  );
};
HeadOffice.propTypes = ({

});

export default HeadOffice;
