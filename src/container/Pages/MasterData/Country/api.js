/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1,
});

const path = '/countries';
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { country: params });
export const editApi = (id, params) => api.put(`${path}/${id}`, { country: params });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
