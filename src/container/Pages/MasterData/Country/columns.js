const columns = [
  {
    name: 'id',
    label: 'ID',
    filterPredicate: '_cont',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'name',
    label: 'Nama',
    filterPredicate: '_cont',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'nationality',
    filterPredicate: '_cont',
    label: 'Kewarganegaraan',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'action',
    label: 'Aksi',
    options: {
      filter: false,
      sort: false
    }
  },
];

export default columns;
