import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import Func from '../../../../functions/index';

const Country = () => {
  const parseResponse = (response) => {
    const data = response.data.country;

    return data;
  };

  const mapParams = (params) => {
    const newParams = params;

    newParams.id = params.id.$oid;

    return newParams;
  };

  const parseId = (object) => {
    const { id: { $oid } } = object;

    return $oid;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Negara"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        parseId={parseId}
        mapParams={mapParams}
        accessCreate={Func.checkPermission('master-data#country#create')}
        accessUpdate={Func.checkPermission('master-data#country#update')}
        accessDelete={Func.checkPermission('master-data#country#delete')}
      />
    </Fragment>
  );
};

Country.propTypes = ({

});

export default Country;
