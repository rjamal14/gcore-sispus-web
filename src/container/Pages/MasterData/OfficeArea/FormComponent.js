import { Grid } from '@material-ui/core';
import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import forms from './formfields';
import Components from '../../../../components/FormField';
import { getField } from '../../../../helper/helper';

const FormComponent = (props) => {
  const { initialValues } = props;
  const regionOfficeObj = { value: initialValues.region_office_id };
  const cityObj = { value: initialValues.city_id };
  const coordinateObj = { value: initialValues.coordinate };

  const assignProps = (object, newObject) => _.assign(object, newObject);

  return (
    <Grid container c>
      <Grid item xs={12} md={6}>
        { Components({ ...getField(forms, 'code') }) }
        { Components({ ...getField(forms, 'name') }) }
        { Components({ ...assignProps(getField(forms, 'region_office_id'), regionOfficeObj) }) }
        { Components({ ...assignProps(getField(forms, 'city_id'), cityObj) }) }
        { Components({ ...getField(forms, 'address') }) }
        { Components({ ...getField(forms, 'description') }) }
      </Grid>
      <Grid item xs={12} md={6}>
        { Components({ ...assignProps(getField(forms, 'coordinate'), coordinateObj) }) }
      </Grid>
    </Grid>
  );
};

FormComponent.propTypes = ({
  initialValues: PropTypes.object.isRequired
});

export default FormComponent;
