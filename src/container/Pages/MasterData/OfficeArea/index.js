/* eslint-disable radix */
import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import FormComponent from './FormComponent';
import Func from '../../../../functions/index';

const OfficeArea = () => {
  const mapParams = (params) => {
    const newParams = params;

    newParams.city_id = { id: params.city_id, name: params.city_name };
    newParams.area_office_id = { id: params.region_office_id, name: params.region_office_name };
    newParams.coordinate = { lat: params.latitude, lng: params.longitude };

    return newParams;
  };

  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Kantor Area"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        mapParams={mapParams}
        parseResponse={parseResponse}
        parseId={parseId}
        formComponent={FormComponent}
        accessCreate={Func.checkPermission('master-data#area#create')}
        accessUpdate={Func.checkPermission('master-data#area#update')}
        accessDelete={Func.checkPermission('master-data#area#delete')}
      />
    </Fragment>
  );
};
OfficeArea.propTypes = ({

});

export default OfficeArea;
