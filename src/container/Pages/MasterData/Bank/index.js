import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import Func from '../../../../functions/index';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';

const Bank = () => {
  const mapParams = (params) => {
    const newParams = params;
    newParams.id = params._id.$oid;
    return newParams;
  };

  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const parseId = (object) => {
    const { _id: { $oid } } = object;

    return $oid;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Bank"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        parseId={parseId}
        mapParams={mapParams}
        accessCreate={Func.checkPermission('master-data#bank#create')}
        accessUpdate={Func.checkPermission('master-data#bank#update')}
        accessDelete={Func.checkPermission('master-data#bank#delete')}
      />
    </Fragment>
  );
};

Bank.propTypes = ({

});

export default Bank;
