/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1
});

api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

const path = '/banks';

const mapParams = (params) => {
  const newParams = {};

  newParams.bank_code = params.bank_code;
  newParams.name = params.name;
  newParams.status = params.status;
  newParams.description = params.description;

  return newParams;
};

// eslint-disable-next-line camelcase
export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { bank: mapParams(params) });
export const editApi = (id, params) => api.put(`${path}/${id}`, { bank: mapParams(params) });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
