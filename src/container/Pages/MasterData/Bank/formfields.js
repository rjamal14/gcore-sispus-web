/* eslint-disable import/no-named-as-default-member */
import Text from '../../../../components/FormField/Fields/Text';
import Switch from '../../../../components/FormField/Fields/Switch';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'status',
    label: 'Aktif',
    component: Switch,
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'bank_code',
    label: 'Kode Bank',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'name',
    label: 'Nama',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'description',
    label: 'Deskripsi',
    component: Text,
    value: '',
  },
];

export default formfields;
