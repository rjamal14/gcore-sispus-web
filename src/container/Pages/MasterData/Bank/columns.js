/* eslint-disable react/prop-types */

import React from 'react';
import RenderSelect from './RenderSelect';

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'bank_code',
    label: 'Kode Bank',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'name',
    label: 'Nama',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'status',
    label: 'Status',
    customBodyRender: (event, value, data) => <RenderSelect colValue={value} rowData={data} />,
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'action',
    label: 'Aksi',
    options: {
      filter: false,
      sort: false
    }
  },
];

export default columns;
