/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import swal from 'sweetalert';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import { CircularProgress, MenuItem, Select } from '@material-ui/core';
import { editApi } from './api';

const RenderSelect = ({ colValue, rowData }) => {
  const [valueSelect, setValueSelect] = useState(colValue);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const onChange = async (e) => {
    const { value } = e.target;
    setValueSelect(value);
    try {
      setLoading(true);
      const res = await editApi(rowData._id.$oid, { status: value });

      if (res.status === 200) {
        // eslint-disable-next-line no-unused-vars
        const { data: { code } } = res;
        if (code === 400) {
          setLoading(false);
          setSuccess(false);
        } else if (code === 200) {
          setLoading(false);
          setSuccess(true);
        }
      }
    } catch (error) {
      setLoading(false);
      setError(true);
      swal({
        title: 'Kesalahan',
        text: 'Maaf, terjadi kesalahan',
        icon: 'error',
        buttons: 'OK'
      });
    }
  };

  useEffect(() => {
    setTimeout(() => {
      setSuccess(false);
    }, 3000);
  }, [success]);

  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <Select
        name="status"
        value={valueSelect}
        style={{ backgroundColor: '#A8EAFF', marginRight: 8 }}
        onChange={onChange}
        variant="outlined"
        fullWidth
      >
        <MenuItem value>Aktif</MenuItem>
        <MenuItem value={false}>Tidak Aktif</MenuItem>
      </Select>
      {
        loading ? <CircularProgress size={24} /> : ''
      }
      {
        success ? <CheckIcon /> : ''
      }
      {
        error ? <ErrorIcon /> : ''
      }
    </div>
  );
};

export default RenderSelect;
