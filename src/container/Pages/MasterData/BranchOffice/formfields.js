/* eslint-disable import/no-named-as-default-member */
import GoogleMapsField from '../../../../components/FormField/Fields/GoogleMapsField';
import SearchAutoComplete from '../../../../components/FormField/Fields/SearchAutoComplete';
import Text from '../../../../components/FormField/Fields/Text';
import { getApi } from '../City/api';
import { getApi as areaOfficeApi } from '../OfficeArea/api';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'code',
    label: 'Kode',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'name',
    label: 'Nama',
    value: '',
    component: Text,
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'area_office_id',
    label: 'Kantor Area',
    value: '',
    component: SearchAutoComplete,
    api: areaOfficeApi,
    fieldProps: {
      variant: 'outlined',
      style: { marginBottom: 8 },
      required: true,
      validate: required,
      placeholder: 'Ketikan sesuatu...'
    }
  },
  {
    name: 'city_id',
    label: 'Kota',
    value: '',
    component: SearchAutoComplete,
    api: getApi,
    fieldProps: {
      variant: 'outlined',
      style: { marginBottom: 8 },
      required: true,
      validate: required,
      placeholder: 'Ketikan sesuatu...'
    }
  },
  {
    name: 'address',
    label: 'Alamat',
    value: '',
    component: Text,
    fieldProps: {
      required: true,
      validate: required,
      multiline: true,
      rows: 4
    }
  },
  {
    name: 'description',
    label: 'Deskripsi',
    value: '',
    component: Text,
    fieldProps: {
      multiline: true,
      rows: 4
    }
  },
  {
    name: 'coordinate',
    label: 'Koordinat',
    value: '',
    component: GoogleMapsField,
    fieldProps: {
      variant: 'outlined',
    }
  },
];

export default formfields;
