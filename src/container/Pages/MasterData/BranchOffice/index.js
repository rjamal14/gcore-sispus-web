/* eslint-disable radix */
/* eslint-disable camelcase */
import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import FormComponent from './FormComponent';
import Func from '../../../../functions/index';

const BranchOffice = () => {
  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const mapParams = (params) => {
    const newParams = params;

    newParams.city_id = { id: params.city_id, name: params.city_name };
    newParams.area_office_id = { id: params.area_office_id, name: params.area_office_name };
    newParams.coordinate = { lat: params.latitude, lng: params.longitude };

    return newParams;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Kantor Cabang"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        mapParams={mapParams}
        parseId={parseId}
        formComponent={FormComponent}
        accessCreate={Func.checkPermission('master-data#branch-office#create')}
        accessUpdate={Func.checkPermission('master-data#branch-office#update')}
        accessDelete={Func.checkPermission('master-data#branch-office#delete')}
      />
    </Fragment>
  );
};
BranchOffice.propTypes = ({

});

export default BranchOffice;
