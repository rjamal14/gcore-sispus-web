/* eslint-disable class-methods-use-this */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { CircularProgress, FormControlLabel, FormControl, RadioGroup, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import AsyncSelect from 'react-select/async';

import Radio from '@material-ui/core/Radio';
import swal from 'sweetalert';

import Typography from '@material-ui/core/Typography';
import DialogContentText from '@material-ui/core/DialogContentText';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: [],
      show: 'text',
      show2: 'text',
      type: '',
      activeTabs: 0,
      Proses: false,
      ImgBase124: '',
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      provin: [],
      dataRole: [],
      officeable_type: 'head_offices',
      officeName: 'Kantor Pusat',
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  validatePass(event, name) {
    const { value } = this.state;
    if (name === 'password' || name === 'password_confirmation') {
      let errorMsg = '';
      const rgx = new RegExp('^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%^&*]{8,}$');
      if (!(rgx.test(event))) {
        errorMsg = 'Min. 8 karakter kombinasi angka, simbol, huruf besar & kecil';
      } else if (value.password && name === 'password_confirmation' && value.password !== event) {
        errorMsg = 'Konfirmasi kata sandi baru tidak sama';
      }
      const validate = this.state.validator;
      validate[name] = errorMsg;
      this.setState({ validator: validate });
    }
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
    this.validatePass(event, name);
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    this.getRole();
    setTimeout(() => {
      this.setState({ show: 'password', show2: 'password' });
    }, 300);
    this.getBranch('', '', '');
    if (this.props.type === 'Ubah') {
      this.setState({ loader: true });
      fetch(process.env.REACT_APP_URL_AUTH + 'user/' + this.props.id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      })
        .then((response) => response.json())
        .then((json) => {
          if (json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }
          const val = [];
          val.first_name = json.data.first_name;
          val.last_name = json.data.last_name;
          val.nik = json.data.nik;
          val.email = json.data.email;
          val.role_id = json.data.user_role.role_id.$oid;
          val.img = json.data.photo.url;
          val.user_office_attributes = {
            value: json.data.user_office.officeable_id.$oid,
            label: json.data.user_office.officeable_name,
          };

          if (json.data.user_office.officeable_type === 'head') {
            var offType = 'head_offices';
          } else if (json.data.user_office.officeable_type === 'region') {
            var offType = 'region_offices';
          } else if (json.data.user_office.officeable_type === 'area') {
            var offType = 'area_offices';
          } else if (json.data.user_office.officeable_type === 'branch') {
            var offType = 'branch_offices';
          } else {
            var offType = 'unit_offices';
          }
          this.setState({
            value: val,
            officeable_type: offType,
            loader: false,
          });
        })
        .catch(() => {
          this.setState({
            loader: false,
          });
        })
        .finally(() => {
          this.setState({
            loader: false,
          });
        });
    }
  }

  getBranch = (val, id, name) => {
    this.setState({ isLoading: true });
    const { officeable_type } = this.state;
    const url = (officeable_type === 'region_offices'
      ? 'regionals/autocomplete'
      : officeable_type === 'area_offices'
        ? 'areas'
        : officeable_type);
    Func.getData(url, 10, 1, val).then((res) => {
      if (res.json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else {
        const datas = [];
        datas.push({
          value: '-',
          label: res.json.data?.length > 0 ? 'Pilih' : 'Tidak ditemukan',
          isDisabled: true,
        });

        res.json.data.map((value) => {
          datas.push({ value: value._id.$oid, label: value.name });
        });
        if (id !== '') {
          const search = res.json.data.find((o) => o._id.$oid === id);
          if (search === undefined) {
            datas.push({ value: id, label: name });
          }
        }

        this.setState({ data2: datas, data2_ori: res.json.data });
      }
    });
  };

  getRole() {
    fetch(
      process.env.REACT_APP_URL_AUTH
        + 'roles?category='
        + (this.state.officeable_type === 'head_offices' ? 'sispus' : 'siscab'),
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        json.data.map((value) => {
          datas.push({
            value: value.id.$oid,
            label: value.description,
            role_name: value.name,
          });
        });
        this.setState({ dataRole: datas });
      })
      .catch(() => {})
      .finally(() => {});
  }

  handleSubmit(type) {
    const validator = [
      {
        name: 'user_office_attributes',
        type: 'required',
      },
      {
        name: 'first_name',
        type: 'required',
      },
      {
        name: 'last_name',
        type: 'required',
      },
      {
        name: 'email',
        type: 'mail|required',
      },
      {
        name: 'nik',
        type: 'required',
      },
      {
        name: 'img',
        type: 'required',
      },
      {
        name: 'role_id',
        type: 'requiredCBX',
      },
      {
        name: 'password',
        type: 'required',
      },
      {
        name: 'password_confirmation',
        type: 'same:password|required',
      }
    ];

    var flag = true;
    if (this.props?.type === 'Tambah') {
      const { password, password_confirmation } = this.state.value;
      if (this.validatePass(password, 'password')) {
        flag = false;
      }
      if (this.validatePass(password_confirmation, 'password_confirmation')) {
        flag = false;
      }
      if (password !== password_confirmation) {
        flag = false;
      }
    }

    const validate = Func.Validator(this.state.value, validator);
    const state = this.state.validator;

    if (validate.success && flag) {
      this.setState({ loader_button: true });
      const userPayload1 = {
        position:
        this.state.officeable_type === 'head_offices' ? 'sispus' : 'siscab',
        email: this.state.value.email,
        first_name: this.state.value.first_name,
        last_name: this.state.value.last_name,
        nik: this.state.value.nik,
        password: this.state.value.password,
        password_confirmation: this.state.value.password_confirmation,
        user_office_attributes: {
          officeable_id: this.state.value.user_office_attributes.value,
          officeable_type:
          this.state.officeable_type === 'head_offices'
            ? 'head'
            : this.state.officeable_type === 'region_offices'
              ? 'region'
              : this.state.officeable_type === 'area_offices'
                ? 'area'
                : this.state.officeable_type === 'branch_offices'
                  ? 'branch'
                  : 'unit',
        },
        photo: this.state.ImgBase124,
        user_role_attributes: {
          role_id: this.state.value.role_id,
        },
      };

      const userPayload2 = {
        position:
        this.state.officeable_type === 'head_offices' ? 'sispus' : 'siscab',
        email: this.state.value.email,
        first_name: this.state.value.first_name,
        last_name: this.state.value.last_name,
        nik: this.state.value.nik,
        user_office_attributes: {
          officeable_id: this.state.value.user_office_attributes.value,
          officeable_type:
          this.state.officeable_type === 'head_offices'
            ? 'head'
            : this.state.officeable_type === 'region_offices'
              ? 'region'
              : this.state.officeable_type === 'area_offices'
                ? 'area'
                : this.state.officeable_type === 'branch_offices'
                  ? 'branch'
                  : 'unit',
        },
        photo: this.state.ImgBase124,
        user_role_attributes: {
          role_id: this.state.value.role_id,
        },
      };

      if (type === 'Tambah') {
        var payload = userPayload1;
      } else if (type !== 'Tambah' && this.state.value.password !== undefined) {
        var payload = userPayload1;
      } else {
        var payload = userPayload2;
      }

      fetch(
        process.env.REACT_APP_URL_AUTH
        + 'user/'
        + (type === 'Tambah' ? '' : this.props.id),
        {
          method: type === 'Tambah' ? 'POST' : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
          body: JSON.stringify({
            user: payload,
          }),
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          } else if (json.code === 400) {
            swal({
              title: 'Kesalahan',
              text: json?.message,
              icon: 'error',
              buttons: 'OK'
            });
          } else if (json.created || json.code === 200) {
            this.props.OnNext(json.message);
          }
          this.setState({ loader_button: false });
        })
        .catch(() => {
          this.setState({ loader_button: false });
        })
        .finally(() => {
          this.setState({ loader_button: false });
        });
    } else this.setState({ validator: validate.error });
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK',
      });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase124: reader.result });
    };
  }

  setOffice(office) {
    let name = 'Kepala Pusat';
    if (office === 'head_offices') name = 'Kepala Pusat';
    else if (office === 'region_offices') name = 'Kantor Region';
    else if (office === 'area_offices') name = 'Kantor Area';
    else if (office === 'branch_offices') name = 'Kantor Cabang';
    else name = 'Kantor Unit';
    return name;
  }

  render() {
    const Roles = [];
    const vals = this.state.value;
    this.state.dataRole.map((value, index) => {
      Roles.push(
        <FormControlLabel
          value={value.value}
          role_name={value.role_name}
          control={(
            <Radio
              checked={
                this.state.value.role_id === undefined
                  ? false
                  : this.state.value.role_id === value.value
              }
              style={{ color: '#C4A643' }}
            />
          )}
          label={value.label}
        />
      );
    });
    const { classes } = this.props;
    const loadOptions = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data2);
      }, 1000);
    };
    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#C4A643', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type + ' User'}
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              <div className={classes.root}>
                <Grid
                  container
                  direction="row"
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                >
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Nama Depan</text>
                        <text className={classes.starts1}>**</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('first_name');
                        }}
                        error={this.state.validator.first_name}
                        helperText={this.state.validator.first_name}
                        value={this.state.value.first_name}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'first_name');
                        }}
                        name="first_name"
                        InputProps={{
                          endAdornment: this.state.validator.first_name ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          ) : (
                            <div />
                          ),
                        }}
                      />
                    </Grid>
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Nama Belakang</text>
                        <text className={classes.starts1}>**</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('last_name');
                        }}
                        error={this.state.validator.last_name}
                        helperText={this.state.validator.last_name}
                        value={this.state.value.last_name}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'last_name');
                        }}
                        name="last_name"
                        InputProps={{
                          endAdornment: this.state.validator.last_name ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          ) : (
                            <div />
                          ),
                        }}
                      />
                    </Grid>
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Email</text>
                        <text className={classes.starts1}>**</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('email');
                        }}
                        error={this.state.validator.email}
                        helperText={this.state.validator.email}
                        value={this.state.value.email}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'email');
                        }}
                        name="email"
                        InputProps={{
                          endAdornment: this.state.validator.email ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          ) : (
                            <div />
                          ),
                        }}
                      />
                    </Grid>
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div
                        style={{
                          marginTop: '23px',
                        }}
                      >
                        <text className={classes.label1}>NIK</text>
                        <text className={classes.starts1}>**</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        type="number"
                        size="small"
                        autoComplete="new-password"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('nik');
                        }}
                        error={this.state.validator.nik}
                        helperText={this.state.validator.nik}
                        value={this.state.value.nik}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'nik');
                        }}
                        name="nik"
                        InputProps={{
                          endAdornment: this.state.validator.nik ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          ) : (
                            <div />
                          ),
                          form: {
                            autocomplete: 'off',
                          },
                        }}
                      />
                    </Grid>
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Kata Sandi</text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        type={this.state.show}
                        autoComplete="new-password"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('password');
                        }}
                        error={this.state.validator.password}
                        helperText={this.state.validator.password}
                        value={this.state.value.password}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'password');
                        }}
                        name="password"
                        InputProps={{
                          endAdornment: this.state.validator.password ? (
                            <InputAdornment position="start">
                              <img src={Icon.warning} />
                            </InputAdornment>
                          ) : this.state.show === 'password' ? (
                            <InputAdornment position="start">
                              <img
                                onClick={() => {
                                  this.setState({ show: 'text' });
                                }}
                                src={Icon.eye}
                              />
                            </InputAdornment>
                          ) : (
                            <InputAdornment position="start">
                              <img
                                onClick={() => {
                                  this.setState({ show: 'password' });
                                }}
                                src={Icon.eye2}
                              />
                            </InputAdornment>
                          ),
                          form: {
                            autocomplete: 'off',
                          },
                        }}
                      />
                    </Grid>
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>
                          Konfirmasi Kata Sandi
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        type={this.state.show2}
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('password_confirmation');
                        }}
                        error={this.state.validator.password_confirmation}
                        helperText={this.state.validator.password_confirmation}
                        value={this.state.value.password_confirmations}
                        onChange={(event) => {
                          this.handleChange(
                            event.target.value,
                            'password_confirmation'
                          );
                        }}
                        name="password_confirmation"
                        InputProps={{
                          endAdornment: this.state.validator
                            .password_confirmation ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            ) : this.state.show2 === 'password' ? (
                              <InputAdornment position="start">
                                <img
                                  onClick={() => {
                                    this.setState({ show2: 'text' });
                                  }}
                                  src={Icon.eye}
                                />
                              </InputAdornment>
                            ) : (
                              <InputAdornment position="start">
                                <img
                                  onClick={() => {
                                    this.setState({ show2: 'password' });
                                  }}
                                  src={Icon.eye2}
                                />
                              </InputAdornment>
                            ),
                          form: {
                            autocomplete: 'off',
                          },
                        }}
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div className={classes.label12}>
                        <text>Kantor</text>
                        <text className={classes.starts1}>*</text>
                        <FormControlLabel
                          style={{
                            marginLeft: 10,
                          }}
                          control={(
                            <Radio
                              style={{
                                color: '#C4A643',
                              }}
                              value="remember"
                              color="primary"
                              checked={
                                this.state.officeable_type === 'head_offices'
                              }
                              onChange={() => {
                                this.setState(
                                  {
                                    officeable_type: 'head_offices',
                                  },
                                  () => {
                                    this.getBranch('', '', '');
                                    this.getRole();
                                  }
                                );
                              }}
                            />
                          )}
                          label="Pusat"
                        />
                        <FormControlLabel
                          style={{
                            marginLeft: 10,
                          }}
                          control={(
                            <Radio
                              style={{
                                color: '#C4A643',
                              }}
                              value="remember"
                              color="primary"
                              checked={
                                this.state.officeable_type === 'region_offices'
                              }
                              onChange={() => {
                                this.setState(
                                  {
                                    officeable_type: 'region_offices',
                                  },
                                  () => {
                                    this.getBranch('', '', '');
                                    this.getRole();
                                  }
                                );
                              }}
                            />
                          )}
                          label="Wilayah"
                        />
                        <FormControlLabel
                          style={{
                            marginLeft: 10,
                          }}
                          control={(
                            <Radio
                              style={{
                                color: '#C4A643',
                              }}
                              value="remember"
                              color="primary"
                              checked={
                                this.state.officeable_type === 'area_offices'
                              }
                              onChange={() => {
                                this.setState(
                                  {
                                    officeable_type: 'area_offices',
                                  },
                                  () => {
                                    this.getBranch('', '', '');
                                    this.getRole();
                                  }
                                );
                              }}
                            />
                          )}
                          label="Area"
                        />
                        <FormControlLabel
                          control={(
                            <Radio
                              style={{
                                color: '#C4A643',
                              }}
                              value="remember"
                              color="primary"
                              checked={
                                this.state.officeable_type === 'branch_offices'
                              }
                              onChange={() => {
                                this.setState(
                                  {
                                    officeable_type: 'branch_offices',
                                  },
                                  () => {
                                    this.getBranch('', '', '');
                                    this.getRole();
                                  }
                                );
                              }}
                            />
                          )}
                          label="Cabang"
                        />
                        <FormControlLabel
                          control={(
                            <Radio
                              style={{
                                color: '#C4A643',
                              }}
                              value="remember"
                              color="primary"
                              checked={
                                this.state.officeable_type === 'unit_offices'
                              }
                              onChange={() => {
                                this.setState(
                                  {
                                    officeable_type: 'unit_offices',
                                  },
                                  () => {
                                    this.getBranch('', '', '');
                                    this.getRole();
                                  }
                                );
                              }}
                            />
                          )}
                          label="Unit"
                        />
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        value={this.state.value.user_office_attributes}
                        className={classes.input}
                        placeholder={`Cari ${this.setOffice(this.state.officeable_type || '')} (Masukan 1 karakter untuk mencari)`}
                        onFocus={() => {
                          this.removeValidate('user_office_attributes');
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator
                              .user_office_attributes
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem',
                          }),
                        }}
                        onInputChange={(val) => {
                          this.getBranch(val, '', '');
                        }}
                        loadOptions={loadOptions}
                        onChange={(val) => {
                          this.handleChange(val, 'user_office_attributes');
                        }}
                      />
                      <FormHelperText className={classes.error}>
                        {this.state.validator.user_office_attributes}
                      </FormHelperText>
                    </Grid>
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label1}>Role</text>
                        <text className={classes.starts1}>**</text>
                      </div>
                      <div
                        style={{
                          marginLeft: 15,
                          marginTop: 10,
                        }}
                      >
                        <FormControl component="fieldset">
                          <RadioGroup
                            aria-label="roles"
                            name="roles"
                            value={this.state.value.role_id}
                            onChange={(event) => {
                              this.handleChange(event.target.value, 'role_id');
                              this.removeValidate('role_id');
                            }}
                          >
                            {Roles}
                          </RadioGroup>
                        </FormControl>
                        <FormHelperText className={classes.error}>
                          {this.state.validator.role_id}
                        </FormHelperText>
                      </div>
                    </Grid>
                    <Grid
                      container
                      lg={12}
                      xl={12}
                      md={12}
                      sm={12}
                      xs={12}
                      className={classes.formPad}
                    >
                      <Grid
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        sm={12}
                        xs={12}
                        className={classes.formPad}
                      >
                        <div>
                          <text className={classes.label1}>Foto</text>
                          <text className={classes.starts1}>**</text>
                        </div>
                        <div
                          style={{
                            marginLeft: 15,
                            marginTop: 10,
                          }}
                        >
                          <Box
                            borderColor={
                              this.state.validator.img
                                ? 'error.main'
                                : 'grey.500'
                            }
                            border={1}
                            onClick={() => {
                              this.removeValidate('img');
                            }}
                            className={classes.imgScan}
                          >
                            {this.state.value.img ? (
                              <img
                                className={classes.imgScan2}
                                onClick={() => {
                                  this.removeValidate('img');
                                }}
                                src={this.state.value.img}
                              />
                            ) : null}
                          </Box>
                          <FormHelperText className={classes.error}>
                            {this.state.validator.img}
                          </FormHelperText>
                          <FormHelperText
                            style={{
                              marginLeft: '15px',
                            }}
                          >
                            Maximum File 2Mb
                          </FormHelperText>
                        </div>
                      </Grid>
                      <Grid
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        sm={12}
                        xs={12}
                        className={classes.formPad}
                      >
                        <div className={classes.BodytitleMdl22}>
                          <img
                            src={Icon.deleteImg}
                            onClick={() => {
                              const dataSet = this.state.value;
                              dataSet.img = null;
                              this.setState({ value: dataSet });
                            }}
                          />
                        </div>
                        <div className={classes.BodytitleMdl23}>
                          <input
                            type="file"
                            accept="image/*"
                            name="file"
                            title="Pilih Gambar"
                            onChange={(event) => {
                              this.handleChangeImg(event, 'img');
                            }}
                          />
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#c2a443',
              color: 'white',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography
              variant="button"
              style={{
                color: '#FFFFFF',
              }}
            >
              {this.state.loader_button ? (
                <CircularProgress
                  style={{
                    color: 'white',
                    marginLeft: '18px',
                    marginRight: '18px',
                    marginTop: 5,
                  }}
                  size={15}
                />
              ) : (
                'Simpan'
              )}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
