/* eslint-disable import/no-named-as-default-member */
import Text from '../../../../components/FormField/Fields/Text';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'name',
    label: 'Nama',
    value: '',
    component: Text,
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'description',
    label: 'Deskripsi',
    value: '',
    component: Text,
    fieldProps: {
      multiline: true,
      rows: 4,
      required: true,
      validate: required
    }
  },
];

export default formfields;
