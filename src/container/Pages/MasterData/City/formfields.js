import SearchAutoComplete from '../../../../components/FormField/Fields/SearchAutoComplete';
import Text from '../../../../components/FormField/Fields/Text';
import { getApi as provinceApi } from '../Province/api';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'name',
    label: 'Nama',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'province_id',
    label: 'Provinsi',
    value: '',
    api: provinceApi,
    component: SearchAutoComplete,
    fieldProps: {
      required: true,
      validate: required
    }
  },
];

export default formfields;
