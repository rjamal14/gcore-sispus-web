import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import forms from './formfields';
import Components from '../../../../components/FormField';
import { getField } from '../../../../helper/helper';

const FormComponent = (props) => {
  const { initialValues } = props;
  const provinceObj = { value: { id: initialValues.province_id, name: initialValues.province_name } };

  const assignProps = (object, newObject) => _.assign(object, newObject);

  return (
    <div>
      { Components({ ...getField(forms, 'name') }) }
      { Components({ ...assignProps(getField(forms, 'province_id'), provinceObj) }) }
    </div>
  );
};

FormComponent.propTypes = ({
  initialValues: PropTypes.object.isRequired
});

export default FormComponent;
