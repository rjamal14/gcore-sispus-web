import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import FormComponent from './FormComponent';
import Func from '../../../../functions/index';

const City = () => {
  const mapParams = (params) => {
    const newParams = params;

    newParams.id = params._id.$oid;
    newParams.province_id = { id: params.province_id.$oid, name: params.province_name };

    return newParams;
  };

  const parseResponse = (response) => {
    const { data: { data: { city } } } = response;
    return city;
  };

  const parseId = (object) => {
    const { _id: { $oid } } = object;

    return $oid;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Kota"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        parseId={parseId}
        mapParams={mapParams}
        formComponent={FormComponent}
        accessCreate={Func.checkPermission('master-data#city#create')}
        accessUpdate={Func.checkPermission('master-data#city#update')}
        accessDelete={Func.checkPermission('master-data#city#delete')}
      />
    </Fragment>
  );
};

City.propTypes = ({

});

export default City;
