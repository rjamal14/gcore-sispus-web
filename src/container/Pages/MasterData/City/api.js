/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1
});

api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

const path = '/cities';

const mapParams = (params) => {
  const newParams = params;
  newParams.province_id = params.province_id.id;

  return newParams;
};
// eslint-disable-next-line camelcase
export const cityAutoCompleteApi = (province_id) => api.get(`${path}/index_by_provinces`, { province_id });
export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { city: mapParams(params) });
export const editApi = (id, params) => api.put(`${path}/${id}`, { city: mapParams(params) });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
