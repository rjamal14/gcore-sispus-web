const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'province_code',
    label: 'Kode Provinsi',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'name',
    label: 'Nama',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'country_name',
    label: 'Negara',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'action',
    label: 'Aksi',
    options: {
      filter: false,
      sort: false
    }
  },
];

export default columns;
