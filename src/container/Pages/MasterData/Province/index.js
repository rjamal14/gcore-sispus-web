import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import FormComponent from './FormComponent';
import Func from '../../../../functions/index';

const Province = () => {
  const parseResponse = (response) => {
    const { data } = response.data;

    return data;
  };

  const mapParams = (params) => {
    const newParams = params;

    newParams.id = params._id.$oid;
    newParams.country_id = { id: params.country_id.$oid, name: params.country_name || '' };

    return newParams;
  };

  const parseId = (object) => {
    const { _id: { $oid } } = object;

    return $oid;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Provinsi"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        mapParams={mapParams}
        parseId={parseId}
        formComponent={FormComponent}
        accessCreate={Func.checkPermission('master-data#province#create')}
        accessUpdate={Func.checkPermission('master-data#province#update')}
        accessDelete={Func.checkPermission('master-data#province#delete')}
      />
    </Fragment>
  );
};

Province.propTypes = ({

});

export default Province;
