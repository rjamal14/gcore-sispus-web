/* eslint-disable import/no-named-as-default-member */
import Text from '../../../../components/FormField/Fields/Text';
import { getApi as countryApi } from '../Country/api';
import SearchAutoComplete from '../../../../components/FormField/Fields/SearchAutoComplete';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'name',
    label: 'Nama',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'country_id',
    label: 'Negara',
    value: '',
    api: countryApi,
    component: SearchAutoComplete,
    fieldProps: {
      required: true,
      validate: required
    }
  },
];

export default formfields;
