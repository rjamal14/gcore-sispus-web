/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1,
});

const path = '/provinces';
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

const mapParams = (params) => {
  const newParams = params;

  newParams.country_id = params.country_id.id.$oid;

  return newParams;
};

export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { province: mapParams(params) });
export const editApi = (id, params) => api.put(`${path}/${id}`, { province: mapParams(params) });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
