/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import { CircularProgress, FormControlLabel, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import swal from 'sweetalert';
import Typography from '@material-ui/core/Typography';
import DialogContentText from '@material-ui/core/DialogContentText';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      dataRole: [],
      category: 'sispus',
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    if (this.props.type === 'Ubah') {
      this.setState({ loader: true });
      fetch(process.env.REACT_APP_URL_AUTH + 'roles/' + this.props.id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      })
        .then((response) => response.json())
        .then((json) => {
          if (json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }
          const val = [];
          val.name = json.data.name;
          val.description = json.data.description;
          this.setState({
            value: val,
            category: json.data.category,
            loader: false
          });
        })
        .catch(() => {
          this.setState({ loader: false });
        })
        .finally(() => {
          this.setState({ loader: false });
        });
    }
  }

  handleSubmit(type) {
    const validator = [
      {
        name: 'name',
        type: 'required',
      },
      {
        name: 'description',
        type: 'required',
      }
    ];

    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      this.setState({ loader_button: true });
      fetch(
        process.env.REACT_APP_URL_AUTH
          + 'roles/'
          + (type === 'Tambah' ? '' : this.props.id),
        {
          method: type === 'Tambah' ? 'POST' : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
          body: JSON.stringify({
            role: {
              name: this.state.value.name,
              description: this.state.value.description,
              category: this.state.category,
            },
          }),
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          }
          if (type === 'Tambah') {
            if (json.code === 200) {
              this.props.OnNext('Role berhasil dibuat');
            } else {
              this.setState({ validator: json.status });
            }
          } else if (json.code === 200) {
            this.props.OnNext(json.message);
          } else {
            this.setState({ validator: json.status });
          }
          this.setState({ loader_button: false });
        })
        .catch(() => {
          this.setState({ loader_button: false });
        })
        .finally(() => {
          this.setState({ loader_button: false });
        });
    } else {
      this.setState({ validator: validate.error });
    }
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK',
      });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase124: reader.result });
    };
  }

  render() {
    const Roles = [];
    const vals = this.state.value;
    if (this.state.value.role_id !== undefined) {
      this.state.dataRole.map((value) => {
        Roles.push(
          <FormControlLabel
            value={value.value}
            role_name={value.role_name}
            control={(
              <Radio
                checked={this.state.value.role_id === value.value}
                style={{ color: '#C4A643' }}
              />
            )}
            label={value.label}
          />
        );
      });
    }
    const { classes } = this.props;
    const loadOptions = (inputValue, callback) => {
      setTimeout(() => {
        callback(this.state.data2);
      }, 1000);
    };
    if (this.state.loader) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress
                style={{ color: '#C4A643', margin: '18px' }}
                size={40}
              />
              Mohon Tunggu
              <div
                style={{
                  marginTop: '10px',
                }}
              />
            </div>
          </Dialog>
        </div>
      );
    }
    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type + ' Role'}
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div className={classes.scrool}>
              <div className={classes.root}>
                <Grid
                  container
                  direction="row"
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                >
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    sm={12}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Kode</text>
                      <text className={classes.starts1}>**</text>
                    </div>
                    <TextField
                      className={classes.input}
                      variant="outlined"
                      margin="normal"
                      size="small"
                      autoComplete="off"
                      fullWidth
                      onFocus={() => {
                        this.removeValidate('name');
                      }}
                      error={this.state.validator.name}
                      helperText={this.state.validator.name}
                      value={this.state.value.name}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'name');
                      }}
                      name="name"
                      InputProps={{
                        endAdornment: this.state.validator.name ? (
                          <InputAdornment position="start">
                            <img src={Icon.warning} />
                          </InputAdornment>
                        ) : (
                          <div />
                        ),
                      }}
                    />
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    sm={12}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Nama</text>
                      <text className={classes.starts1}>**</text>
                    </div>
                    <TextField
                      className={classes.input}
                      variant="outlined"
                      margin="normal"
                      size="small"
                      autoComplete="off"
                      fullWidth
                      onFocus={() => {
                        this.removeValidate('description');
                      }}
                      error={this.state.validator.description}
                      helperText={this.state.validator.description}
                      value={this.state.value.description}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'description');
                      }}
                      name="description"
                      InputProps={{
                        endAdornment: this.state.validator.description ? (
                          <InputAdornment position="start">
                            <img src={Icon.warning} />
                          </InputAdornment>
                        ) : (
                          <div />
                        ),
                      }}
                    />
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    sm={12}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div className={classes.label12}>
                      <text>Kategori</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <div>
                      <FormControlLabel
                        style={{ marginLeft: 10 }}
                        control={(
                          <Radio
                            style={{ color: '#C4A643' }}
                            value="remember"
                            color="primary"
                            checked={this.state.category === 'sispus'}
                            onChange={() => {
                              this.setState({ category: 'sispus' });
                            }}
                          />
                        )}
                        label="Pusat"
                      />
                      <FormControlLabel
                        control={(
                          <Radio
                            style={{ color: '#C4A643' }}
                            value="remember"
                            color="primary"
                            checked={this.state.category === 'siscab'}
                            onChange={() => {
                              this.setState({ category: 'siscab' });
                            }}
                          />
                        )}
                        label="Cabang"
                      />
                    </div>
                  </Grid>
                </Grid>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#c2a443',
              color: 'white',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography
              variant="button"
              style={{
                color: '#FFFFFF',
              }}
            >
              {this.state.loader_button ? (
                <CircularProgress
                  style={{
                    color: 'white',
                    marginLeft: '18px',
                    marginRight: '18px',
                    marginTop: 5,
                  }}
                  size={15}
                />
              ) : (
                'Simpan'
              )}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black',
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.handleModal();
            }}
            variant="outlined"
          >
            <Typography variant="button">Batal</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
