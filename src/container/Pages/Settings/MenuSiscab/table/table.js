/* eslint-disable func-names */
/* eslint-disable no-cond-assign */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-extend-native */
/* eslint-disable camelcase */
/* eslint-disable no-dupe-keys */
/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable array-callback-return */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Select2 from 'react-select';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Swal from 'sweetalert2';
import Pagination from '@material-ui/lab/Pagination';
import { Hidden, Button } from '@material-ui/core';
import BeatLoader from 'react-spinners/BeatLoader';
import FilterListIcon from '@material-ui/icons/FilterList';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import env from '../../../../../config/env';
import Func from '../../../../../functions/index';
import Icon from '../../../../../components/icon';
import styles from './css';

import Form from '../form/index';

function createData(data) {
  return { data };
}
const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset'
    }
  }
});

function Row3(props) {
  const { row } = props;
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow key={row.id.$oid}>
        <TableCell style={{ width: '3.5%' }} />
        <TableCell style={{ width: '25%' }}>{row.module_name}</TableCell>
        <TableCell style={{ width: '25%' }}>{row.module_prefix}</TableCell>
        <TableCell style={{ width: '25%' }}>
          <div className={classes.action} align="right">
            <IconButton
              onClick={() => {
                props.edit(row.id.$oid, 'Tambah Aksi');
              }}
              aria-label="Cari"
            >
              <EditIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                props.delete(row.id.$oid);
              }}
              aria-label="Cari"
            >
              <DeleteIcon />
            </IconButton>
          </div>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

function Row2(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow key={row.id.$oid}>
        <TableCell style={{ width: '0.5%' }}>
          <IconButton
            aria-label="expand row"
            disabled={row.module_action.length == 0}
            onClick={() => setOpen(!open)}
            size="small"
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell style={{ width: '25%' }}>{row.module_name}</TableCell>
        <TableCell style={{ width: '25%' }}>{row.module_prefix}</TableCell>
        <TableCell style={{ width: '25%' }} align="right">
          <div className={classes.action}>
            <IconButton
              onClick={() => {
                props.edit(row.id.$oid, 'Tambah Submenu');
              }}
              aria-label="Cari"
            >
              <EditIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                props.delete(row.id.$oid);
              }}
              aria-label="Cari"
            >
              <DeleteIcon />
            </IconButton>
          </div>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: '0.5%' }}>
                      <IconButton
                        aria-label="expand row"
                        size="small"
                      >
                        <img src={Icon.dash} style={{ width: 15 }} />
                      </IconButton>
                    </TableCell>
                    <TableCell style={{ backgroundColor: 'rgb(253 233 190)', }}>Action</TableCell>
                    <TableCell style={{ backgroundColor: 'rgb(253 233 190)', }}>Prefix</TableCell>
                    <TableCell style={{ backgroundColor: 'rgb(253 233 190)', }} align="right">Aksi</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.module_action.map((historyRow) => (
                    <Row3
                      key={historyRow.id.$oid}
                      row={historyRow}
                      delete={(id) => {
                        props.delete(id);
                      }}
                      edit={(id, type) => {
                        props.edit(id, type);
                      }}
                    />
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root} style={{ cursor: 'pointer' }}>
        <TableCell style={{ width: '0.5%' }}>
          <IconButton
            disabled={row.data.module_subs.length == 0}
            aria-label="expand row"
            onClick={() => {
              if (row.data.module_subs.length > 0) {
                setOpen(!open);
              }
            }}
            size="small"
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell style={{ width: '25%' }}>{row.data.module_name}</TableCell>
        <TableCell style={{ width: '25%' }}>{row.data.module_prefix}</TableCell>
        <TableCell style={{ width: '25%' }} align="right">
          <div className={classes.action}>
            <IconButton
              onClick={() => {
                props.edit(row.data.id.$oid, 'Tambah Menu');
              }}
              aria-label="Cari"
            >
              <EditIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                props.delete(row.data.id.$oid);
              }}
              aria-label="Cari"
            >
              <DeleteIcon />
            </IconButton>
          </div>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: '0.5%' }}>
                      <IconButton
                        aria-label="expand row"
                        size="small"
                      >
                        {open ? <img src={Icon.dash} style={{ width: 15 }} /> : <img src={Icon.dash} style={{ width: 15 }} />}
                      </IconButton>
                    </TableCell>
                    <TableCell style={{ backgroundColor: 'rgb(253 233 190)', }}>Sub Menu</TableCell>
                    <TableCell style={{ backgroundColor: 'rgb(253 233 190)', }}>Prefix</TableCell>
                    <TableCell style={{ backgroundColor: 'rgb(253 233 190)', }} align="right">Aksi</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.data.module_subs.map((historyRow) => (
                    <Row2
                      key={historyRow.id.$oid}
                      row={historyRow}
                      delete={(id) => {
                        props.delete(id);
                      }}
                      edit={(id, type) => {
                        props.edit(id, type);
                      }}
                    />
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

const headCells = [
  {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Menu'
  }, {
    id: 'type',
    numeric: false,
    disablePadding: false,
    label: 'Prefix'
  }, {
    id: 'date',
    numeric: false,
    disablePadding: false,
    label: 'Aksi'
  },
];

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3)
    }
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme
      .transitions
      .create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }
}))(InputBase);

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 1,
      rowsPerPage: 10,
      openSearch: false,
      rowFocus: '',
      page: 1,
      filter: false,
      type: '',
      count: 1,
      // eslint-disable-next-line no-dupe-keys
      data: [],
      filterValue: [],
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: [],
      bulk: false,
      redirect: false,
      failure: false
    };
    this.ChangePage = this
      .ChangePage
      .bind(this);
  }

  handleClick(id) {
    alert(id);
  }

  ChangePage(event, page) {
    this.setState({
      page
    }, () => {
      this.getData();
    });
  }

  setFocus(row) {
    this.setState({ rowFocus: row.id });
  }

  setUnfocus() {
    setTimeout(() => {
      this.setState({ rowFocus: '' });
    }, 5500);
  }

  handleModal(row, type) {
    this.setState({
      modal: true,
      row,
      type
    }, () => {
      this.setState({ modal: false, bulk: false });
    });
  }

  componentDidMount() {
    this.getData('first');
  }

  handleClickDelete(row) {
    Swal
      .fire({
        title: 'Apakah Anda yakin?',
        text: 'Akan menghapus data yang dipilih',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!',
        cancelButtonText: 'Batal'
      })
      .then((result) => {
        if (result.value) {
          fetch(env.authApi + env.apiPrefixV1 + '/' + this.props.path + '/' + row, {
            method: 'DELETE',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + localStorage.getItem('token')
            }
          }).then((response) => response.json()).then((json) => {
            if (json.success) {
              this.setState({ rowSelected: [] });
              Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
            } else {
              Swal.fire('Gagal!', json.message, 'warning');
            }
            this.getData('first');
          }).catch((error) => {})
            .finally(() => {});
        }
      });
  }

  getData = () => {
    this.setState({ loading: true });
    Func
      .getDataAuth(this.props.path + '?module_category=siscab&', this.state.rowsPerPage, this.state.page, this.state.filterValue)
      .then((res) => {
        if (res.json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        } else if (res.json.status === 500) {
          this.setState({ failure: true });
        } else {
          const datas = [];
          res.json.data.map((data) => {
            datas.push(createData(data.master_modules));
          });

          this.setState({
            con: res.json.data.length,
            data: datas,
            loading: false,
            total_data: res.json.total_data,
            page: res.json.current_page,
            next_page: res.json.next_page,
            prev_page: res.json.prev_page,
            current_page: res.json.current_page,
            total_page: res.json.total_page,
            filter: false
          });
        }
      });
  };

  Short(orderKey) {
    if (orderKey === this.state.order) {
      this.setState({ order: '' });
      var library = this.state.data;
      library.sort((a, b) => (a[orderKey] < b[orderKey]
        ? 1
        : b[orderKey] < a[orderKey]
          ? -1
          : 0));
    } else {
      this.setState({ order: orderKey });
      // eslint-disable-next-line no-redeclare
      var library = this.state.data;
      library.sort((a, b) => (a[orderKey] > b[orderKey]
        ? 1
        : b[orderKey] > a[orderKey]
          ? -1
          : 0));
    }
  }

  handleChange(event, name) {
    const dataSet = this.state.filterValue;
    dataSet[name] = event;
    this.setState({ filterValue: dataSet });
  }

  handleClickDeleteAll = () => {
    Swal
      .fire({
        title: 'Apakah Anda yakin?',
        text: 'Akan menghapus data yang dipilih',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!',
        cancelButtonText: 'Batal'
      })
      .then((result) => {
        if (result.value) {
          this
            .state
            .rowSelected
            .map((row) => {
              fetch(env.managementApi + env.apiPrefixV1 + '/' + this.props.path + '/' + row, {
                method: 'DELETE',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + localStorage.getItem('token')
                }
              }).then((response) => response.json()).then((json) => {
              }).catch((error) => {})
                .finally(() => {});
            });
          Swal.fire('Berhasil!', 'Data berhasil dihapus.', 'success');
          this.getData();
          this.setState({ rowSelected: [] });
        }
      });
  };

  DeleteBulk() {
    this.deleteAll();
  }

  render() {
    const { classes } = this.props;
    let con = 0;
    this.state.data.map((row) => {
      if (this.state.rowSelected.indexOf(row.id) > -1) {
        con += 1;
      }
    });
    return (
      <div>
        <Form type="Ubah" row={this.state.row} modal={this.state.modal} typeForm={this.state.type} />
        <TableContainer
          style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }}
          component={Paper}
        >
          <div style={{
            width: '100%'
          }}
          >
            <Box display="flex">
              <Box flexGrow={1}>
                <Typography variant="h6" className={classes.paginationTxt2}>
                  {this.props.title}
                </Typography>
              </Box>
            </Box>
          </div>
          <div style={{
            width: '100%'
          }}
          >
            <Box display="flex">
              <Box flexGrow={1}>
                <Typography className={classes.paginationTxt}>
                  {this.props.subtitle}
                </Typography>
              </Box>
            </Box>
          </div>
          <Hidden smUp>
            <div
              style={{
                width: '100%'
              }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h9" className={classes.textperdata}>
                    Tampilkan
                  </Typography>
                  <Select
                    size="small"
                    className={classes.selectperdata}
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    value={this.state.rowsPerPage}
                    onChange={(val) => {
                      this.setState({
                        rowsPerPage: val.target.value
                      }, () => {
                        this.getData();
                      });
                    }}
                    input={<BootstrapInput />}
                  >
                    <MenuItem value={10}>10 data</MenuItem>
                    <MenuItem value={50}>50 data</MenuItem>
                    <MenuItem value={100}>100 data</MenuItem>
                  </Select>
                  <Typography variant="h9" className={classes.textperdata}>
                    Total:
                    {' '}
                    {this.state.total_data}
                    {' '}
                    menu
                  </Typography>
                </Box>
              </Box>
            </div>
            <div
              style={{
                width: '100%'
              }}
            >
              <Box display="flex">
                <Box>
                  <Button
                    name="filterButton"
                    style={{ marginRight: 15, marginBottom: 15 }}
                    startIcon={<FilterListIcon />}
                    variant="outlined"
                    onClick={() => {
                      this.setState({
                        filter: !this.state.filter
                      });
                    }}
                    color="primary"
                  >
                    Filter
                  </Button>
                </Box>
              </Box>
            </div>
          </Hidden>
          <Dialog
            open={this.state.filter}
            onClose={() => {
              this.setState({
                filter: !this.state.filter
              });
            }}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">Filter</DialogTitle>
            <Divider style={{ marginBottom: 20 }} />
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <div>
                  <div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.filterValue.status_true}
                      placeholder="Pilih"
                      error
                      styles={{
                        control: (provided, state) => ({
                          ...provided,
                          borderColor: this.state.filterValue.status_true
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem'
                        })
                      }}
                      className={classes.search2}
                      options={[
                        {
                          value: '-',
                          label: 'Pilih Status',
                          isDisabled: true
                        }, {
                          value: 1,
                          label: 'Aktif'
                        }, {
                          value: 0,
                          label: 'Tidak Aktif'
                        }
                      ]}
                      onChange={(val) => {
                        this.handleChange(val, 'status_true');
                      }}
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      value={this.state.filterValue.name_cont}
                      placeholder="Nama Nasabah"
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'name_cont');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      placeholder="Email"
                      value={this.state.filterValue.email_cont}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'email_cont');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      value={this.state.filterValue.customer_contact_phone_number_cont}
                      placeholder="No. Telepon"
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'customer_contact_phone_number_cont');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      value={this.state.filterValue.identity_number}
                      placeholder="No. Indentitas"
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'identity_number');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                  <div>
                    <BootstrapInput
                      value={this.state.filterValue.cif_number_cont}
                      placeholder="No. CIF"
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'cif_number_cont');
                      }}
                      on
                      className={classes.search2}
                      id="demo-customized-textbox"
                    />
                  </div>
                </div>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => {
                  this.setState({
                    filterValue: []
                  }, () => {
                    this.getData();
                  });
                }}
                color="primary"
              >
                Hapus
              </Button>
              <Button
                onClick={() => {
                  this.getData();
                }}
                color="primary"
                autoFocus
              >
                Filter
              </Button>
            </DialogActions>
          </Dialog>
          <Hidden xsDown>
            <div
              style={{
                width: '100%'
              }}
            >
              <Box display="flex">
                <Box flexGrow={1}>
                  <Typography variant="h9" className={classes.textperdata}>
                    Tampilkan
                  </Typography>
                  <Select
                    size="small"
                    className={classes.selectperdata}
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    value={this.state.rowsPerPage}
                    onChange={(val) => {
                      this.setState({
                        rowsPerPage: val.target.value
                      }, () => {
                        this.getData();
                      });
                    }}
                    input={<BootstrapInput />}
                  >
                    <MenuItem value={10}>10 data</MenuItem>
                    <MenuItem value={50}>50 data</MenuItem>
                    <MenuItem value={100}>100 data</MenuItem>
                  </Select>
                  <Typography variant="h9" className={classes.textperdata}>
                    Total:
                    {' '}
                    {this.state.total_data}
                    {' '}
                    menu
                  </Typography>
                </Box>
                <Box>
                  <Button
                    name="filterButton"
                    style={{ marginRight: 15, marginBottom: 15 }}
                    startIcon={<FilterListIcon />}
                    variant="outlined"
                    color="primary"
                    onClick={() => {
                      this.setState({
                        filter: !this.state.filter
                      });
                    }}
                  >
                    Filter
                  </Button>
                </Box>
              </Box>
            </div>
          </Hidden>
        </TableContainer>
        <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115 }} component={Paper}>
          <Table size="small" aria-label="collapsible table">
            <TableHead className={classes.headTable}>
              <TableRow>
                <TableCell />
                <TableCell>Menu</TableCell>
                <TableCell>Prefix</TableCell>
                <TableCell align="right">Aksi</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              { !this.state.loading ? this.state.data.map((row) => (
                <Row
                  key={row.name}
                  row={row}
                  delete={(id) => {
                    this.handleClickDelete(id);
                  }}
                  edit={(id, type) => {
                    this.handleModal(id, type);
                  }}
                />
              ))

                : null}
            </TableBody>
          </Table>
          {this.state.loading
            ? (
              <div className={classes.loader}>
                <BeatLoader size={15} color="#3F3F3F" loading />
              </div>
            )
            : null}
        </TableContainer>
        <TableContainer style={{ width: this.props.open ? window.innerWidth - this.props.width : window.innerWidth - 115, marginBottom: 40 }} component={Paper}>
          <div className={classes.col}>
            <text>
              Halaman
              {' '}
              {' ' + this.state.current_page + ' '}
              dari
              {' '}
              {' ' + this.state.total_page + ' '}
              halaman
            </text>
          </div>
          <Pagination
            className={classes.row}
            count={this.state.total_page}
            defaultPage={this.state.page}
            onChange={this.ChangePage}
            siblingCount={0}
          />
        </TableContainer>
        {this.state.rowSelected.length > 0
          ? (
            <div className={classes.popupv2}>
              <Box display="flex" justifyContent="center" flexWrap="wrap">
                <Box flexWrap="wrap">
                  <div className={classes.popup}>
                    <Box display="flex" justifyContent="center" flexWrap="wrap">
                      <Box>
                        <Typography className={classes.popupTxt}>
                          Pilih aksi:
                        </Typography>
                      </Box>
                      <Box>
                        <IconButton
                          onClick={() => {
                            this.DeleteBulk();
                          }}
                          aria-label="Cari"
                        >
                          <img src={Icon.popupDelete} />
                        </IconButton>
                      </Box>
                      <Box>
                        <IconButton
                          className={classes.popupClose}
                          onClick={() => {
                            this.setState({ rowSelected: [] });
                          }}
                          aria-label="Cari"
                        >
                          <img src={Icon.close} />
                        </IconButton>
                      </Box>
                    </Box>
                  </div>
                </Box>
              </Box>
            </div>
          )
          : null}
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Tables' })(Tables);
