/* eslint-disable func-names */
/* eslint-disable no-cond-assign */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-extend-native */
/* eslint-disable camelcase */
/* eslint-disable no-dupe-keys */
/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable array-callback-return */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Form from '../form/index';
import Tables from './table';
import styles from './css';

const options = ['Tambah Menu', 'Tambah Submenu', 'Tambah Aksi'];

function SimpleDialog(props) {
  const { onClose, selectedValue, open, classes } = props;

  const handleClose = () => {
    onClose();
  };

  const handleListItemClick = (value) => {
    selectedValue(value);
    onClose();
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">Pilih Penambahan</DialogTitle>
      <List>
        {options.map((email) => (
          <ListItem button onClick={() => handleListItemClick(email)} key={email}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <AddIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={email} />
          </ListItem>
        ))}
      </List>
    </Dialog>
  );
}

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      master: false,
      validator: [],
      type: '',
      value: [],
      mdlShow: true,
      mobile: false,
      redirect: false,
      show: 'password',
      id_cust: null
    };
  }

  handleModal(value) {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false, type: value });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 8 }}>
          <Form type="Tambah" modal={this.state.modal} typeForm={this.state.type} />
          <Hidden only={['lg', 'xl']}>
            <Tables
              width={60}
              open={this.state.open}
              filter={false}
              title="Menu Sistem Cabang"
              subtitle=""
              path="master_modules"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Tables
              width={270}
              open={this.state.open}
              filter={false}
              title="Menu Sistem Cabang"
              subtitle=""
              path="master_modules"
            />
          </Hidden>
          <Fab
            color="secondary"
            className={classes.fab}
            aria-label="add"
            onClick={() => {
              this.setState({ dialog: !this.state.dialog });
            }}
          >
            <AddIcon />
          </Fab>
          <SimpleDialog
            classes={classes}
            selectedValue={(value) => {
              this.handleModal(value);
            }}
            open={this.state.dialog}
            onClose={() => {
              this.setState({ dialog: !this.state.dialog });
            }}
          />
        </div>
      </Fragment>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
