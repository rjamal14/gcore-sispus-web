/* eslint-disable func-names */
/* eslint-disable no-cond-assign */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-extend-native */
/* eslint-disable camelcase */
/* eslint-disable no-dupe-keys */
/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable array-callback-return */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import Grid from '@material-ui/core/Grid';
import MuiDialogContent from '@material-ui/core/DialogContent';
import FormHelperText from '@material-ui/core/FormHelperText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Select2 from 'react-select';
import Link from '@material-ui/core/Link';
import { BeatLoader } from 'react-spinners';
import Func from '../../../../../functions/index';
import styles from '../table/css';
import env from '../../../../../config/env';

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modal: false,
      validator: [],
      value: {},
      type: '',
      activeTabs: 0,
      Proses: false,
      ImgBase44: '',
      redirect: false,
      contactN0: 1,
      dataMenu: [],
      loader: false,
      contact: [
        {
          module_name: '',
          module_prefix: '',
        },
      ],
    };
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    if (name == 'menu_id') {
      this.getSubMenu(event.value);
    }
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  getMenu() {
    fetch(
      process.env.REACT_APP_URL_AUTH
        + 'master_modules/list/?module_category=siscab',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        var dataMenu = [];
        json.data.map((value) => {
          dataMenu.push({
            value: value._id.$oid,
            label: value.module_name,
          });
        });
        this.setState({ dataMenu });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  getSubMenu(id) {
    fetch(
      process.env.REACT_APP_URL_AUTH
        + 'master_modules/list?module_category=siscab&module_id=' + id,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.code == '403') {
          if (Func.Clear_Token() == true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        var dataSubMenu = [];
        json.data.map((value) => {
          dataSubMenu.push({
            value: value._id.$oid,
            label: value.module_name,
          });
        });
        this.setState({ dataSubMenu });
      })
      .catch((error) => {})
      .finally(() => {});
  }

  componentDidMount() {
    this.getMenu();
    if (this.props.type == 'Ubah') {
      fetch(
        process.env.REACT_APP_URL_AUTH
          + 'master_modules/'
          + this.props.id_cust,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            if (Func.Clear_Token() == true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          }
          var { data } = json;
          var { value } = this.state;
          value.module_name0 = data.module_name;
          value.module_prefix0 = data.module_prefix;
          this.setState({ value });
        })
        .catch((error) => {})
        .finally(() => {});
    }
  }

  handleSubmit(type) {
    this.setState({ loader: true });
    var validator = [];
    if (this.props.type == 'Tambah') {
      validator.push({
        name: 'parent_id',
        type: 'required',
      });
    }

    this.state.contact.map((value, index) => {
      if (!value._destroy) {
        validator.push({
          name: 'module_name' + index,
          type: 'required',
        });
        validator.push({
          name: 'module_prefix' + index,
          type: 'required',
        });
      }
    });
    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      if (type == 'Tambah') {
        var load = [];
        this.state.contact.map((value, index) => {
          load.push({
            module_name: this.state.value['module_name' + index],
            module_prefix: this.state.value['module_prefix' + index],
            module_type: 'sub',
            module_category: 'siscab',
            parent_id: this.state.value.parent_id.value,
          });
        });
        var payload = {
          master_modules: load,
        };
      } else {
        var payload = {
          master_module: {
            module_name: this.state.value.module_name0,
            module_prefix: this.state.value.module_prefix0,
          },
        };
      }

      fetch(
        env.authApi
          + env.apiPrefixV1
          + '/master_modules/'
          + (type == 'Tambah' ? '' : this.props.id_cust),
        {
          method: type == 'Tambah' ? 'POST' : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
          body: JSON.stringify(payload),
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.code == '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() == true) {
              this.handleSubmit();
            }
          }
          if (json.code == 200) {
            this.props.OnNext(json.message);
            this.setState({ loader: false });
          } else if (json.code == 400) {
            var { validator } = this.state;
            json.status.map((value) => {
              if (value.error.module_name != null) {
                validator['module_name' + value.index] = value.error.module_name;
              }
              if (value.error.module_prefix != null) {
                validator['module_prefix' + value.index] = value.error.module_prefix;
              }
              this.setState({ validator });
            });
            this.setState({ loader: false });
          } else {
            Func.AlertError('Maaf, Terjadi kesalahan pada sistem');
            this.setState({ loader: false });
          }
        })
        .catch((error) => {})
        .finally(() => {});
    } else {
      this.setState({ validator: validate.error });
      this.setState({ loader: false });
    }
  }

  addContact() {
    const data = this.state.contact;
    data.push({
      module_name: '',
      module_prefix: '',
    });
    this.setState({
      contact: data,
      contactN0: this.state.contactN0 + 1,
    });
  }

  renderListSubmenu() {
    const { classes } = this.props;
    const dataLIst = [];
    let key = 1;
    this.state.contact.map((value, index) => {
      dataLIst.push(
        <div style={{ marginTop: 0 }}>
          <div className={classes.BodytitleMdl2}>
            <text className={classes.titleMdl} />
          </div>
          {this.props.type == 'Tambah' ? (
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>
                Submenu
                {key}
              </text>
              <Link
                className={classes.deleteRec}
                onClick={() => {
                  if (key <= 2) {
                    swal({
                      title: 'Maaf, terjadi kesalahan',
                      text: 'Minimum 1',
                      icon: 'error',
                    });
                  } else if (index == 0) {
                    var datas = this.state.contact;
                    var datas2 = this.state.contact[index];
                    datas.shift();
                    this.setState({ contact: datas });
                  } else {
                    var datas = this.state.contact;
                    var datas2 = this.state.contact[index];
                    datas.splice(index, index);
                    this.setState({ contact: datas });
                  }
                }}
                color="inherit"
              >
                Hapus
              </Link>
            </div>
          ) : null}
          <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
            <Grid
              item
              lg={6}
              xl={6}
              md={6}
              sm={6}
              xs={12}
              className={classes.formPad}
            >
              <div>
                <text className={classes.label1}>Nama Submenu</text>
                <text className={classes.starts1}>*</text>
              </div>
              <TextField
                size="small"
                fullWidth
                className={classes.input2}
                variant="outlined"
                autoComplete="off"
                onFocus={() => {
                  this.removeValidate('module_name' + index);
                }}
                error={this.state.validator['module_name' + index]}
                helperText={this.state.validator['module_name' + index]}
                value={this.state.value['module_name' + index]}
                onChange={(event) => {
                  this.handleChange(event.target.value, 'module_name' + index);
                }}
                name={'module_name' + index}
              />
            </Grid>
            <Grid
              item
              lg={6}
              xl={6}
              md={6}
              sm={6}
              xs={12}
              className={classes.formPad}
            >
              <div>
                <text className={classes.label1}>Prefix</text>
                <text className={classes.starts1}>*</text>
              </div>
              <TextField
                size="small"
                fullWidth
                className={classes.input2}
                variant="outlined"
                autoComplete="off"
                onFocus={() => {
                  this.removeValidate('module_prefix' + index);
                }}
                error={this.state.validator['module_prefix' + index]}
                helperText={this.state.validator['module_prefix' + index]}
                value={this.state.value['module_prefix' + index]}
                onChange={(event) => {
                  this.handleChange(
                    event.target.value,
                    'module_prefix' + index
                  );
                }}
                name={'module_prefix' + index}
              />
            </Grid>
          </Grid>
        </div>
      );
      key++;
    });
    return dataLIst;
  }

  render() {
    const { classes } = this.props;
    const txtValidator = [];
    return (
      <div>
        <DialogContent>
          <div className={classes.scrool}>
            {Func.toLogin(this.state.redirect)}
            <div className={classes.root}>
              {this.props.type == 'Tambah' ? (
                <Grid
                  container
                  lg={12}
                  xl={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className={classes.formPad}
                >
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Menu</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value.menu_id}
                      placeholder="Pilih"
                      onFocus={() => {
                        this.removeValidate('menu_id');
                      }}
                      error
                      styles={{
                        control: (provided, state) => ({
                          ...provided,
                          borderColor: this.state.validator.menu_id
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={this.state.dataMenu}
                      onChange={(val) => {
                        this.handleChange(val, 'menu_id');
                      }}
                    />
                    <FormHelperText className={classes.error}>
                      {this.state.validator.menu_id}
                    </FormHelperText>
                  </Grid>
                  <Grid
                    item
                    lg={6}
                    xl={6}
                    md={6}
                    sm={6}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label1}>Submenu</text>
                      <text className={classes.starts1}>*</text>
                    </div>
                    <Select2
                      name="form-field-name-error"
                      value={this.state.value.parent_id}
                      placeholder="Pilih"
                      isDisabled={this.state.value.menu_id == undefined}
                      onFocus={() => {
                        this.removeValidate('parent_id');
                      }}
                      error
                      styles={{
                        control: (provided, state) => ({
                          ...provided,
                          borderColor: this.state.validator.parent_id
                            ? 'red'
                            : '#CACACA',
                          borderRadius: '0.25rem',
                        }),
                      }}
                      className={classes.input2}
                      options={this.state.dataSubMenu}
                      onChange={(val) => {
                        this.handleChange(val, 'parent_id');
                      }}
                    />
                    <FormHelperText className={classes.error}>
                      {this.state.validator.parent_id}
                    </FormHelperText>
                  </Grid>
                </Grid>
              ) : null}
              {this.renderListSubmenu()}
              {this.props.type == 'Tambah' ? (
                <button
                  style={{
                    backgroundColor: '#C4A443',
                    borderRadius: 50,
                    color: 'white',
                    width: 150,
                    height: 35,
                    marginLeft: 10,
                    marginTop: 25,
                    marginBottom: 15,
                    fontWeight: '500',
                    fontSize: 14,
                  }}
                  onClick={() => {
                    this.addContact();
                  }}
                >
                  Tambah Submenu
                </button>
              ) : null}
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div
            style={{
              position: 'absolute',
              left: 35,
              marginTop: 20,
              marginBottom: 20,
            }}
          >
            {txtValidator}
          </div>
          {this.state.loader ? (
            <div className={classes.loadingSubmit}>
              <Typography style={{ marginRight: 8 }}>Menyimpan Data</Typography>

              <BeatLoader />
            </div>
          ) : null}
          <button
            disabled={this.state.loader}
            style={{
              backgroundColor: '#C4A443',
              borderRadius: 50,
              color: 'white',
              width: 87,
              height: 35,
              marginRight: 25,
              marginBottom: 15,
              fontWeight: '500',
              fontSize: 14,
            }}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
          >
            Simpan
          </button>
        </DialogActions>
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
