/* eslint-disable func-names */
/* eslint-disable no-cond-assign */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-extend-native */
/* eslint-disable camelcase */
/* eslint-disable no-dupe-keys */
/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable array-callback-return */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Func from '../../../../../functions/index';
import styles from '../table/css';
import Menu from './menu';
import SubMenu from './submenu';
import Action from './action';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: [],
      type: '',
      activeTabs: 0,
      Proses: 0,
      count: 0,
      id_cust: null,
      state: 1,
    };
  }

  onPage(type) {
    var val = '';
    if (type === '-') {
      val = this.state.activeTabs - 1;
    } else {
      val = this.state.activeTabs + 1;
    }
    this.setState({ activeTabs: val, Proses: val });
  }

  componentWillReceiveProps() {
    if (this.props.row !== 'undefined' && this.props.modal) {
      this.setState({ modal: this.props.modal, type: this.props.type });
    }
    if (this.props.type === 'Ubah' && this.props.row !== undefined) {
      this.setState({ id_cust: this.props.row });
    }
  }

  removeValidate(name) {
    var data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleModal() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  handleChange(event, name) {
    var dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleSubmit() {
    if (this.state.activeTabs === 6) {
      this.setState({
        modal: !this.state.modal,
        activeTabs: 0,
      });
    } else {
      this.setState({ Proses: this.state.activeTabs });
    }
  }

  render() {
    const { classes } = this.props;
    if (this.props.typeForm == 'Tambah Menu') {
      var content = (
        <Menu
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id_cust={this.state.id_cust}
          count={this.state.count}
          OnNext={(res) => {
            this.setState({ modal: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    } else if (this.props.typeForm == 'Tambah Submenu') {
      var content = (
        <SubMenu
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id_cust={this.state.id_cust}
          count={this.state.count}
          OnNext={(res) => {
            this.setState({ modal: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    } else if (this.props.typeForm == 'Tambah Aksi') {
      var content = (
        <Action
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id_cust={this.state.id_cust}
          count={this.state.count}
          OnNext={(res) => {
            this.setState({ modal: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }

    if (this.state.modal) {
      return (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <MuiDialogTitle disableTypography>
              <Typography variant="h7" className={classes.tittleModal}>
                {this.props.type == 'Tambah'
                  ? this.props.typeForm
                  : this.props.typeForm.replace('Tambah', 'Ubah')}
              </Typography>
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={() => {
                  this.handleModal();
                }}
              >
                <CloseIcon />
              </IconButton>
            </MuiDialogTitle>
            {content}
          </Dialog>
        </div>
      );
    }
    return <div />;
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
