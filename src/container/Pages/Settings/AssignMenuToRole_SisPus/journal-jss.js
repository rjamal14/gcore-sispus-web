import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  tab: {
    flexGrow: 1,
    marginTop: 2,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default useStyles;
