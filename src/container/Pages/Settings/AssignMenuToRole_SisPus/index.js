/* eslint-disable func-names */
/* eslint-disable no-cond-assign */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-extend-native */
/* eslint-disable camelcase */
/* eslint-disable no-dupe-keys */
/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable no-redeclare */
/* eslint-disable array-callback-return */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable block-scoped-var */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React, { Fragment, useState } from 'react';
import { AppBar, Tab, Tabs } from '@material-ui/core';
import useStyles from './journal-jss';
import Tables from './table';

const JournalPage = () => {
  const classes = useStyles();
  const [tabValue, setTabValue] = useState(0);

  const handleChange = (e, newValue) => {
    setTabValue(newValue);
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  return (
    <Fragment>
      <AppBar position="sticky" className={classes.tab} />
      <Tables tabValue={tabValue} index={0} />
    </Fragment>
  );
};

JournalPage.propTypes = {
};

export default JournalPage;
