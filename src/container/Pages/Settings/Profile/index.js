/* eslint-disable camelcase */
/* eslint-disable consistent-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable semi */
import React, { useEffect, useState, Fragment } from 'react';
import { useFormikContext, useFormik } from 'formik';
import {
  Card,
  CardContent,
  Button,
  CircularProgress,
  Grid,
  Divider,
  TextField,
  Dialog
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import swal from 'sweetalert';
import useStyles from './profile-jss.js';
import { changePageTitle } from '../../../../redux/actions/changePageTitle';
import { getApi, editApi } from './api';

const Profile = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const formikContext = useFormikContext();
  const [loading, setLoading] = useState(false);
  const formik = useFormik({
    initialValues: {
      first_name: '',
      last_name: '',
      email: '',
      nik: '',
      role_name: '',
      role_id: '',
      photo: '',
      photoUrl: '',
    },
    initialErrors: {
      first_name: '',
      last_name: '',
      email: '',
      photo: '',
    },
    validate: (values) => {
      const errors = {};
      if (!values.first_name) errors.first_name = 'Tidak boleh kosong';
      if (!values.last_name) errors.last_name = 'Tidak boleh kosong';
      if (!values.email) errors.email = 'Tidak boleh kosong';
      if (!values.photoUrl && !values.photo) errors.photo = 'Tidak boleh kosong';
      return errors;
    },
    onSubmit: (values) => {
      const { first_name, last_name, email, photo, photoUrl } = values
      const payload = { email, first_name, last_name, ...(photo && { photo }) }
      if (!(!photoUrl && !photo)) {
        editApi(payload)
          .then((res) => {
            const response = res.data || res.response.data;
            if (res?.status === 200 && response) {
              swal({
                title: 'Sukses',
                text: response?.message,
                icon: 'success',
                buttons: 'OK'
              });
            }
          });
      }
    },
  });

  useEffect(() => {
    dispatch(changePageTitle('Pengaturan Profil'));
    setLoading(true)
    getApi()
      .then((res) => {
        const { data } = res.data || res.response.data;
        if (res?.status === 200 && data) {
          formik.setValues({
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            nik: data.nik,
            role: data?.user_role?.description
          });
          formik.setFieldValue('photoUrl', data?.photo?.url)
        }
        setLoading(false)
      });
  }, [])

  const selectFile = (evt) => {
    const file = evt.target.files[0];
    if (file?.size && file?.size > 2.9e6) {
      swal({ title: 'File Terlalu Besar', text: 'Maximal File 2Mb', icon: 'error', buttons: 'OK' });
      return false;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => formik.setFieldValue('photo', reader.result);
  };

  console.log(formik.errors)

  return (
    <Fragment>
      {loading && (
        <div className={classes.root2}>
          <Dialog
            disablePortal
            disableEnforceFocus
            disableAutoFocus
            open
            scroll="paper"
            maxWidth="md"
            aria-labelledby="server-modal-title"
            aria-describedby="server-modal-description"
            container={() => {}}
          >
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '20px',
                borderRadius: '50px',
              }}
            >
              <CircularProgress style={{ color: '#C4A643', margin: '18px' }} size={40} />
              Mohon Tunggu
              <div style={{
                marginTop: '10px',
              }}
              />
            </div>
          </Dialog>
        </div>
      )}
      <div className={classes.root}>
        <Card classes={{ root: classes.root }}>
          <CardContent>
            <div className={classes.summaryTitle}>
              Pengaturan Profil
            </div>
            <form onSubmit={formik.handleSubmit} noValidate>
              <Grid container>
                <Grid item xs={4} style={{ display: 'flex', justifyContent: 'center' }}>
                  <div className={classes.form}>
                    <div className={classes.label}>
                      Foto Profil
                    </div>
                    <div className={(formik.errors.photo) ? classes.photoContainerError : classes.photoContainer}>
                      {formik.values.photo && (
                        <img alt="" src={formik.values.photo} className={classes.photo} />
                      )}
                      {(!formik.values.photo && formik.values.photoUrl) && (
                        <img alt="" src={formik.values.photoUrl} className={classes.photo} />
                      )}
                    </div>
                    {(formik.errors.photo) && (
                      <div style={{ color: 'red' }}>{formik.errors.photo}</div>
                    )}
                    <div>
                      <Button
                        variant="contained"
                        component="label"
                      >
                        Pilih Foto
                        <input
                          name="photo"
                          type="file"
                          hidden
                          onChange={selectFile}
                        />
                      </Button>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={2} style={{ display: 'flex', justifyContent: 'center' }}>
                  <Divider orientation="vertical" style={{ width: 4 }} />
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.form}>
                    <div className={classes.label}>
                      Nama Depan
                    </div>
                    <TextField
                      margin="normal"
                      size="small"
                      variant="outlined"
                      name="first_name"
                      value={formik.values.first_name}
                      onChange={formik.handleChange}
                      error={formik.touched.first_name && Boolean(formik.errors.first_name)}
                      helperText={formik.touched.first_name && formik.errors.first_name}
                    />
                  </div>
                  <div className={classes.form}>
                    <div className={classes.label}>
                      Nama Belakang
                    </div>
                    <TextField
                      margin="normal"
                      size="small"
                      variant="outlined"
                      name="last_name"
                      value={formik.values.last_name}
                      onChange={formik.handleChange}
                      error={formik.touched.last_name && Boolean(formik.errors.last_name)}
                      helperText={formik.touched.last_name && formik.errors.last_name}
                    />
                  </div>
                  <div className={classes.form}>
                    <div className={classes.label}>
                      Email
                    </div>
                    <TextField
                      margin="normal"
                      size="small"
                      variant="outlined"
                      name="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      error={formik.touched.email && Boolean(formik.errors.email)}
                      helperText={formik.touched.email && formik.errors.email}
                    />
                  </div>
                  <div className={classes.form}>
                    <div className={classes.label}>
                      NIK
                    </div>
                    <TextField
                      disabled
                      margin="normal"
                      size="small"
                      variant="outlined"
                      name="nik"
                      value={formik.values.nik}
                      onChange={formik.handleChange}
                      error={formik.touched.nik && Boolean(formik.errors.nik)}
                      helperText={formik.touched.nik && formik.errors.nik}
                    />
                  </div>
                  <div className={classes.form}>
                    <div className={classes.label}>
                      Role
                    </div>
                    <TextField
                      disabled
                      margin="normal"
                      size="small"
                      variant="outlined"
                      name="role"
                      value={formik.values.role}
                      onChange={formik.handleChange}
                      error={formik.touched.role && Boolean(formik.errors.role)}
                      helperText={formik.touched.role && formik.errors.role}
                    />
                  </div>
                  <div>
                    <Button
                      type="submit"
                      color="primary"
                      disabled={formikContext?.isSubmitting}
                      onClick={formikContext?.submitForm}
                      variant="contained"
                      style={{ color: 'white', marginTop: 12 }}
                    >
                      {
                      formikContext?.isSubmitting ? <CircularProgress color="secondary" size={24} /> : 'Simpan'
                      }
                    </Button>
                  </div>
                </Grid>
              </Grid>
            </form>

          </CardContent>
        </Card>
      </div>
    </Fragment>
  );
};

export default Profile;
