/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-unused-vars */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable array-callback-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-useless-concat */
/* eslint-disable react/no-unused-state */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable camelcase */
import React from 'react';
import Hidden from '@material-ui/core/Hidden';
import { withStyles } from '@material-ui/core/styles';
import styles from '../css';
import Melting from './melting';
import Repayment from './repayment';
import Extension from './extension';
import Auction from './auction';

class Index extends React.Component {
  render() {
    const { classes } = this.props;

    if (this.props.active === 'Pencairan') {
      var ttl = 'Pencairan';
      var subtl = 'pencairan.';
      var tbl = (
        <div style={{ display: 'flex', justifyContent: 'center', }}>
          <Hidden only={['lg', 'xl']}>
            <Melting
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_disbursements?type=customer"
            />

          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>

            <Melting
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_disbursements?type=customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Pelunasan') {
      var ttl = 'Pelunasan';
      var subtl = 'pelunasan.';
      var tbl = (
        <div style={{ display: 'flex', justifyContent: 'center', }}>
          <Hidden only={['lg', 'xl']}>
            <Repayment
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_repayments?type=customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Repayment
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_repayments?type=customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Perpanjangan') {
      var ttl = 'Perpanjangan';
      var subtl = 'perpanjangan.';
      var tbl = (
        <div style={{ display: 'flex', justifyContent: 'center', }}>
          <Hidden only={['lg', 'xl']}>
            <Extension
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_time_extensions?type=customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Extension
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_time_extensions?type=customer"
            />
          </Hidden>
        </div>
      );
    } else if (this.props.active === 'Lelang') {
      var ttl = 'Lelang';
      var subtl = 'lelang.';
      var tbl = (
        <div style={{ display: 'flex', justifyContent: 'center', }}>
          <Hidden only={['lg', 'xl']}>
            <Auction
              width={60}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_auctions?type=customer"
            />
          </Hidden>
          <Hidden only={['sm', 'md', 'xs']}>
            <Auction
              width={285}
              open={this.props.open}
              filter={false}
              title={'Transaksi ' + ttl}
              subtitle={'Manajemen transaksi ' + subtl}
              path="transaction_auctions?type=customer"
            />
          </Hidden>
        </div>
      );
    }
    return (
      <div>
        {tbl}
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Index' })(Index);
