/* eslint-disable react/no-deprecated */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import Box from '@material-ui/core/Box';
import styles from './css';
import Tables from './tables/index';
import Form from './form/index';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      master: false,
      validator: [],
      value: [],
      mdlShow: true,
      mobile: false,
      redirect: false,
      active: 'Pencairan',
      show: 'password',
      id_cust: null
    };
  }

  handleModal() {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Hidden only={['lg', 'xl']}>
          <Box
            display="flex"
            justifyContent="flex-start"
            p={1}
            bgcolor="background.paper"
          >
            <Box
              onClick={() => {
                this.setState({ active: 'Pencairan' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Pencairan'
                ? 3
                : 0}
              color={this.state.active === 'Pencairan'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Pencairan
            </Box>
            <Box
              onClick={() => {
                this.setState({ active: 'Pelunasan' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Pelunasan'
                ? 3
                : 0}
              color={this.state.active === 'Pelunasan'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Pelunasan
            </Box>
            <Box
              onClick={() => {
                this.setState({ active: 'Perpanjangan' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Perpanjangan'
                ? 3
                : 0}
              color={this.state.active === 'Perpanjangan'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Perpanjangan
            </Box>
            <Box
              onClick={() => {
                this.setState({ active: 'Lelang' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Lelang'
                ? 3
                : 0}
              color={this.state.active === 'Lelang'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Lelang
            </Box>
          </Box>
        </Hidden>
        <Hidden only={['sm', 'md', 'xs']}>
          <Box
            display="flex"
            justifyContent="flex-start"
            p={1}
            bgcolor="background.paper"
          >
            <Box
              onClick={() => {
                this.setState({ active: 'Pencairan' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Pencairan'
                ? 3
                : 0}
              color={this.state.active === 'Pencairan'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Pencairan
            </Box>
            <Box
              onClick={() => {
                this.setState({ active: 'Pelunasan' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Pelunasan'
                ? 3
                : 0}
              color={this.state.active === 'Pelunasan'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Pelunasan
            </Box>
            <Box
              onClick={() => {
                this.setState({ active: 'Perpanjangan' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Perpanjangan'
                ? 3
                : 0}
              color={this.state.active === 'Perpanjangan'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Perpanjangan
            </Box>
            <Box
              onClick={() => {
                this.setState({ active: 'Lelang' });
              }}
              style={{
                cursor: 'default'
              }}
              ml={1}
              p={1}
              borderBottom={this.state.active === 'Lelang'
                ? 3
                : 0}
              color={this.state.active === 'Lelang'
                ? '#C4A643'
                : '#95A1A7'}
            >
              Lelang
            </Box>
          </Box>
        </Hidden>
        <Form type="Tambah" modal={this.state.modal} />

        <div style={{
          marginTop: 54
        }}
        />
        <Tables open={this.state.open} active={this.state.active} />

      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
