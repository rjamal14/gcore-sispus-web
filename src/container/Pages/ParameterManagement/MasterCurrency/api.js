/* eslint-disable camelcase */
/* eslint-disable semi */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1
});

api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

const path = '/money_bills';

const mapParams = (params) => {
  const newParams = {};
  const {
    nominal,
    description,
    money_type,
    status,
  } = params

  newParams.nominal = nominal;
  newParams.description = description;
  newParams.money_type = money_type;
  newParams.status = status;

  return newParams;
};

// eslint-disable-next-line camelcase
export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { money_bill: mapParams(params) });
export const editApi = (id, params) => api.put(`${path}/${id}`, { money_bill: mapParams(params) });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
