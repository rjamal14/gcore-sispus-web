/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import Func from '../../../../functions/index';

const MasterCurrency = () => {
  const mapParams = (params) => {
    const newParams = params;
    newParams.id = params.id;
    return newParams;
  };

  const parseResponse = (response) => {
    const { data } = response.data;
    
    return data;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Mata Uang"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        parseId={parseId}
        mapParams={mapParams}
        accessCreate={Func.checkPermission('parameters-management#master-currency#create')}
        accessUpdate={Func.checkPermission('parameters-management#master-currency#update')}
        accessDelete={Func.checkPermission('parameters-management#master-currency#delete')}
      />
    </Fragment>
  );
};

MasterCurrency.propTypes = ({

});

export default MasterCurrency;
