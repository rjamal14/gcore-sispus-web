/* eslint-disable import/no-named-as-default-member */
import Text from '../../../../components/FormField/Fields/Text';
import Select2 from '../../../../components/FormField/Fields/Select2';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'nominal',
    label: 'Pecahan Uang',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required,
      type: 'number'
    },
  },
  {
    name: 'money_type',
    label: 'Jenis Uang',
    component: Select2,
    items: [
      { label: 'koin', value: 'coin' },
      { label: 'kertas', value: 'paper' }
    ],
    value: '',
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'description',
    label: 'Keterangan',
    component: Text,
    value: '',
  },
];

export default formfields;
