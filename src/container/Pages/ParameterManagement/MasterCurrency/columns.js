/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/prop-types */

import React from 'react';
import RenderSelect from './RenderSelect';
import Func from '../../../../functions';

const setMoneyTypeLabel = (val) => {
  let label;
  if (val === 'coin') label = 'koin';
  else label = 'kertas';
  return label;
} 

const RpFormatting = (val) => Func.currencyFormatter(val);

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'nominal',
    label: 'Pecahan Uang',
    customBodyRender: (evt, val) => RpFormatting(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'money_type',
    label: 'Jenis Uang',
    customBodyRender: (evt, val) => setMoneyTypeLabel(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'description',
    label: 'Keterangan',
    options: {
      filter: true,
      sort: false
    }
  }, {
    name: 'status',
    label: 'Status',
    customBodyRender: (event, value, data) => <RenderSelect colValue={value} rowData={data} />,
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'action',
    label: 'Aksi',
    options: {
      filter: false,
      sort: false
    }
  },
];

export default columns;
