/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Form from './form/index';
import Tables from './table';
import styles from './css';
import Func from '../../../../functions';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    };
  }

  handleModal() {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {
    fetch(process.env.REACT_APP_URL_MASTER + 'insurance_item_graphs/', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }).then((response) => response.json()).then((json) => {
      const GraphName = [];
      const GraphData = [];
      const GraphColor = [];
      json
        .insurance_item
        .map((data) => {
          GraphName.push(data.name);
          GraphData.push(data.total_insurance_item.total);
          GraphColor.push(Func.Color());
        });

      this.setState({ GraphName, GraphData, GraphColor });
    }).catch(() => {})
      . finally(() => {});
  }

  render() {
    const { classes } = this.props;
    const data = {
      labels: this.state.GraphName,
      datasets: [
        {
          data: this.state.GraphData,
          backgroundColor: this.state.GraphColor,
          hoverBackgroundColor: this.state.GraphColor
        }
      ]
    };
    return (
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: 8 }}>
        <Form type="Tambah" modal={this.state.modal} />
        <Hidden only={['lg', 'xl']}>
          <Tables
            width={60}
            open={this.state.open}
            filter={false}
            title="Jenis Barang Jaminan"
            subtitle="Atur jenis barang jaminan."
            path="insurance_item"
          />
        </Hidden>
        <Hidden only={['sm', 'md', 'xs']}>
          <Tables
            width={285}
            open={this.state.open}
            filter={false}
            title="Jenis Barang Jaminan"
            subtitle="Atur jenis barang jaminan."
            path="insurance_item"
          />
        </Hidden>
        { Func.checkPermission('parameters-management#type-of-collateral#create')
          ? (
            <Fab
              className={classes.fab}
              color="#C4A643"
              aria-label="add"
              onClick={() => {
                this.handleModal();
              }}
            >
              <AddIcon />
            </Fab>
          ) : null}
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
