/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import { CircularProgress, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Select from '@material-ui/core/Select';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import MenuItem from '@material-ui/core/MenuItem';

import Typography from '@material-ui/core/Typography';
import DialogContentText from '@material-ui/core/DialogContentText';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    if (this.props.type === 'Ubah') {
      this.setState({ loader: true });
      fetch(process.env.REACT_APP_URL_MASTER + 'insurance_item/' + this.props.id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = [];
        val.name = json.data.name;
        val.status = json.data.status;
        val.description = json.data.description;
        this.setState({ value: val });
        this.setState({ loader: false });
      }).catch((error) => {})
        . finally(() => {
          this.setState({ loader: false });
        });
    }
    this.getCountry();
  }

    getCountry = () => {
      this.setState({ isLoading: true });
      Func
        .getData('countries', this.state.per_page, this.state.page, this.state.search)
        .then((res) => {
          if (res.json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else {
            const datas = [];
            res
              .json
              .data
              .map((data) => {
                datas.push({ id: data.id.$oid, name: data.name });
              });
            this.setState({ data2: datas });
          }
        });
    };

    handleSubmit(type) {
      const validator = [
        {
          name: 'name',
          type: 'required'
        }, {
          name: 'description',
          type: 'required'
        }
      ];

      const validate = Func.Validator(this.state.value, validator);
      if (validate.success) {
        this.setState({ loader_button: true });
        fetch(process.env.REACT_APP_URL_MASTER + 'insurance_item/' + (type === 'Tambah'
          ? ''
          : this.props.id), {
          method: type === 'Tambah'
            ? 'POST'
            : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token')
          },
          body: JSON.stringify({ name: this.state.value.name, status: this.state.value.status, description: this.state.value.description })
        }).then((response) => response.json()).then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          }
          if (type === 'Tambah') {
            if (json.created) {
              this
                .props
                .OnNext(json.message);
            } else {
              this.setState({ validator: json.status });
            }
          } else if (json.code === 200) {
            this
              .props
              .OnNext(json.message);
          } else {
            this.setState({ validator: json.status });
          }
          this.setState({ loader_button: false });
        }).catch((error) => {
          this.setState({ loader_button: false });
        })
          . finally(() => {
            this.setState({ loader_button: false });
          });
      } else {
        this.setState({ validator: validate.error });
      }
    }

    handleChangeImg(event) {
      this.removeValidate('img');
      const dataSet = this.state.value;
      dataSet.img = URL.createObjectURL(event.target.files[0]);
      this.setState({ value: dataSet });

      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.setState({ ImgBase124: reader.result });
      };
    }

    render() {
      const { classes } = this.props;
      const ExampleCustomInput = ({ value, onClick }) => (<img src={Icon.icon_date} onClick={onClick} />);
      if (this.state.loader) {
        return (
          <div className={classes.root2}>
            <Dialog
              disablePortal
              disableEnforceFocus
              disableAutoFocus
              open
              scroll="paper"
              maxWidth="md"
              aria-labelledby="server-modal-title"
              aria-describedby="server-modal-description"
              container={() => {}}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  padding: '20px',
                  borderRadius: '50px',
                }}
              >
                <CircularProgress
                  style={{ color: '#C4A643', margin: '18px' }}
                  size={40}
                />
                Mohon Tunggu
                <div
                  style={{
                    marginTop: '10px',
                  }}
                />
              </div>
            </Dialog>
          </div>
        );
      }
      return (
        <Dialog
          scroll="paper"
          open
          maxWidth="md"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title">
            <Typography variant="h7" className={classes.tittleModal}>
              {this.props.type + ' Jenis Barang Jaminan'}
            </Typography>
            <IconButton
              disabled={this.state.loader_button}
              aria-label="close"
              className={classes.closeButton}
              onClick={() => {
                this.props.handleModal();
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              <div className={classes.scrool}>
                <div className={classes.root}>
                  <Grid container direction="row" item lg={12} xl={12} md={12} xs={12} xs={12}>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad}>
                      <div>
                        <text className={classes.label1}>
                          Nama Barang Jaminan
                        </text>
                        <text className={classes.starts1}>**</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('name');
                        }}
                        error={this.state.validator.name}
                        helperText={this.state.validator.name}
                        value={this.state.value.name}
                        onChange={(event) => {
                          this.handleChange(event, 'name');
                        }}
                        name="name"
                        InputProps={{
                          endAdornment: this.state.validator.name
                            ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            )
                            : (<div />)
                        }}
                      />
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad2}>
                      <div className={classes.label12}>
                        <text>
                          Status
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <Select
                        size="small"
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('status');
                        }}
                        error={this.state.validator.status}
                        value={this.state.value.status === undefined
                          ? '-'
                          : this.state.value.status}
                        onChange={(event) => {
                          this.handleChange(event, 'status');
                        }}
                        name="status"
                        InputProps={{
                          endAdornment: this.state.validator.status
                            ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            )
                            : (<div />)
                        }}
                      >
                        <MenuItem value="-">
                          <em>Pilih</em>
                        </MenuItem>
                        <MenuItem value>Aktif</MenuItem>
                        <MenuItem value={false}>Tidak Aktif</MenuItem>
                      </Select>
                      <FormHelperText className={classes.error}>{this.state.validator.status}</FormHelperText>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad2}>
                      <div className={classes.label12}>
                        <text>
                          Deskripsi
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextareaAutosize
                        className={this.state.validator.description
                          ? classes.textArea2
                          : classes.textArea}
                        variant="outlined"
                        margin="normal"
                        rows={4}
                        autoComplete="off"
                        onFocus={() => {
                          this.removeValidate('description');
                        }}
                        value={this.state.value.description}
                        onChange={(event) => {
                          this.handleChange(event, 'description');
                        }}
                        name="description"
                        InputProps={{
                          endAdornment: this.state.validator.description
                            ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            )
                            : (<div />)
                        }}
                      />
                      <FormHelperText className={classes.error}>{this.state.validator.description}</FormHelperText>
                    </Grid>
                  </Grid>
                </div>
              </div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              style={{
                backgroundColor: '#c2a443',
                color: 'white',
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.handleSubmit(this.props.type);
              }}
              variant="contained"
            >
              <Typography
                variant="button"
                style={{
                  color: '#FFFFFF',
                }}
              >
                {this.state.loader_button ? (
                  <CircularProgress
                    style={{
                      color: 'white',
                      marginLeft: '18px',
                      marginRight: '18px',
                      marginTop: 5,
                    }}
                    size={15}
                  />
                ) : (
                  'Simpan'
                )}
              </Typography>
            </Button>
            <Button
              style={{
                color: 'black',
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.props.handleModal();
              }}
              variant="outlined"
            >
              <Typography variant="button">Batal</Typography>
            </Button>
          </DialogActions>
        </Dialog>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
