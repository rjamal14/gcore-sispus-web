/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
/* eslint-disable radix */
/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/button-has-type */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import FormHelperText from '@material-ui/core/FormHelperText';
import AsyncSelect from 'react-select/async';
import Autocomplete from '@material-ui/lab/Autocomplete';
import DialogContentText from '@material-ui/core/DialogContentText';
import { CircularProgress, DialogActions, DialogContent } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import swal from 'sweetalert';
import SewaPerpanjangan from './sewa/sewa_perpanjangan';

import styles from '../css';
import Func from '../../../../../functions/index';
import { prolongationSchema } from '../../../../../validation';

function createData(id, name) {
  return { id, name };
}

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      rowSelected: [],
      province: [],
      regionalselected: {},
      prolongAttb: {},
      prolongError: '',
    };
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

    onUnload = e => { // the method that will be used for both add and remove event
      e.preventDefault();
      e.returnValue = '';
    }

    componentWillUnmount() {
      window.removeEventListener('beforeunload', this.onUnload);
    }

    componentDidMount() {
      window.addEventListener('beforeunload', this.onUnload);
      this.setState({
        value: this.props.value,
        rowSelected: this.props.rowSelected,
        province: this.props.province,
        product_finances: this.props.product_finances
      }, () => {
        // Set regional default
        let regionalState
        if (this.state.value.regional_name0) {
          regionalState = {
            id: this.state.value.regional_id0.$oid,
            name: this.state.value.regional_name0
          }
        } else {
          regionalState = {
            id: this.state.province[0].id,
            name: this.state.province[0].name
          }
        }
        this.setState({ regionalselected: regionalState })
        // Set regional default
        this.getInsurance('', '', '');
      });
    }

    handleChangeWilayah(val) {
      const sewaVal = this.state.value.allValue;
      const { value, rowSelected } = this.state;
      const filterData = sewaVal.filter(data => data.regional_id?.$oid === val.id);
      if (filterData.length === 0) {
        rowSelected.map((data, index) => {
          value['rental_cost_percentage' + index] = '';
          value['regional_id' + index] = { $oid: val.id };
          value['regional_name' + index] = val.name;
          value['rental_cost_percentage_15_days' + index] = 0;
          delete value['id' + index];
        })
      } else {
        filterData.map((data, index) => {
          value['id' + index] = data.id.$oid;
          value['rental_cost_percentage' + index] = data.rental_cost_percentage;
          value['regional_id' + index] = data.regional_id;
          value['regional_name' + index] = data.regional_name;
          value['rental_cost_percentage_15_days' + index] = (data.rental_cost_percentage / 2);
        })
      }  
    }

    getInsurance() {
      const datas = [];
      const { rowSelected } = this.state
      rowSelected.map((data, index) => {
        if (!data._destroy) {
          if (this.state.value['day_period' + index] === undefined) {
            this.handleChange(30, 'day_period' + index);
          } else if (this.state.value['day_period' + index] === 15) {
            this.handleChange(parseInt(this.state.value['rental_cost_percentage' + index]) / 2, 'rental_cost_percentage2' + index);
          }
        
          datas[index] = createData(data.id, data.name);
          const set = { value: data.id, label: data.name };
          this.handleChange(set, 'insurance_item_id' + index);
        }
      });
      this.setState({ dataInsurance: datas });
    }

    handleSubmit() {
      this.setState({ loading: true });
      const validator = [];
      const prolongFormData = [];

      this.props.product_finances.map((data, index) => {
        validator.push({
          name: 'insurance_item_id' + index,
          type: 'required'
        }, {
          name: 'rental_cost_percentage' + index,
          type: 'required'
        });
      });

      // prolongation/gadai ulang
      this.props.rowSelected.map((data) => {
        const prolongData = this.state.prolongAttb[data.id]
        prolongData.sort((a, b) => (a._destroy - b._destroy))
          .map((val, idx) => {
            const { value, id, _destroy } = val;
            prolongFormData.push({
              insurance_item_id: data.id,
              value: parseFloat(value.toString().replace(/,/g, '.')),
              type: 'rental',
              order: idx + 1,
              _destroy,
              ...id !== undefined && { id }
            });
          });
      })

      const prolongValidate = prolongationSchema.validate(prolongFormData, { abortEarly: false });
      const validate = Func.Validator(this.state.value, validator);
      
      if (validate.success && !prolongValidate.error) {
        this.setState({ loader_button: true })
        const payload = [];
        this.props.rowSelected.map((data, index) => {
          // sewa
          if (this.state.value['id' + index] === undefined) {
            payload.push({
              regional_id: this.state.regionalselected.id,
              rental_cost_percentage: parseFloat(this.state.value['rental_cost_percentage' + index]),
              rental_cost_percentage_15_days: this.state.value['rental_cost_percentage_15_days' + index],
              insurance_item_id: this.state.value['insurance_item_id' + index].value
            });
          } else {
            payload.push({
              id: this.state.value['id' + index],
              regional_id: this.state.regionalselected.id,
              rental_cost_percentage: parseFloat(this.state.value['rental_cost_percentage' + index]),
              rental_cost_percentage_15_days: this.state.value['rental_cost_percentage_15_days' + index],
              insurance_item_id: this.state.value['insurance_item_id' + index].value
            });
          }
        });
        fetch(process.env.REACT_APP_URL_MASTER + 'product/' + this.props.id, {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token')
          },
          body: JSON.stringify({
            product: {
              product_rental_costs_attributes: payload,
              product_prolongations_attributes: prolongFormData,
            }
          })
        }).then((response) => response.json()).then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          } else if (json.code === 400) {
            swal({
              title: 'Kesalahan',
              text: json?.message,
              icon: 'error',
              buttons: 'OK'
            });
          }
          if (json.success) {
            const value = [];
            json.data.product_detail.product_rental_costs.map((data, index) => {
              value['id' + index] = data.id.$oid;
              value['insurance_item_id' + index] = { value: data.insurance_item.id.$oid, label: data.insurance_item.name };
              value['rental_cost_percentage' + index] = data.rental_cost_percentage;
              value['rental_cost_percentage_15_days' + index] = data.rental_cost_percentage_15_days;
              value['day_period' + index] = data.day_period;
              value['product_id' + index] = data.product_id;
              value['regional_id' + index] = data.regional_id;
              value['regional_name' + index] = data.regional_name;
            });
            value.allValue = json.data.product_detail.product_rental_costs;

            this.handleChange(json.data.product_detail.product_rental_costs[0].id.$oid, 'id');
            this.props.OnNext({
              success: true,
              id: json.data.product_detail.id.$oid,
              value: this.state.value,
              prolongation: json.data.product_detail.product_prolongations
            });
            this.setState({ value });
            this.handleChangeWilayah(this.state.regionalselected);
          } else {
            this.setState({ validator: json.status });
          }
          this.setState({ loader_button: false, prolongError: '' })
        }).catch(() => {
          this.setState({ loader_button: false })
        })
          . finally(() => {
            this.setState({ loader_button: false })
          });
      } else {
        this.setState({ 
          validator: validate.error,
          prolongError: 'Harus diisi',
        });
      }
    }

    renderInsurance() {
      const loadOptions = (inputValue, callback) => {
        setTimeout(() => {
          callback(this.state.data2);
        }, 1000);
      };
      const { classes } = this.props;
      const list = [];
      const { rowSelected } = this.state
      rowSelected.map((data, index) => {
        let flag = true;
        if (this.state.rowSelected.find(o => o.id === data.id) === undefined) {
          flag = false;
        } else if (this.state.rowSelected.find(o => o.id === data.id)._destroy) {
          flag = false;
        }
        list.push(
          <div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
              style={{
                marginBottom: 20
              }}
            >
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Jenis Barang Jaminan
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <div style={{ marginTop: 15 }}>
                  <AsyncSelect
                    value={this.state.value['insurance_item_id' + index]}
                    className={classes.input}
                    placeholder="Pilih"
                    // isDisabled
                    onFocus={() => {
                      this.removeValidate('insurance_item_id' + index);
                    }}
                    styles={{
                      control: (provided, state) => ({
                        ...provided,
                        borderColor: this.state.validator['insurance_item_id' + index]
                          ? 'red'
                          : '#CACACA',
                        borderRadius: '0.25rem'
                      })
                    }}
                    onInputChange={(val) => {
                      this.getInsurance(val, '', '');
                    }}
                    cacheOptions
                    loadOptions={loadOptions}
                    defaultOptions
                    options={this.state.data2}
                    onChange={(val) => {
                      this.handleChange(val, 'insurance_item_id' + index);
                    }}
                  />
                  <FormHelperText className={classes.error}>{this.state.validator['insurance_item_id' + index]}</FormHelperText>
                </div>
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Biaya Sewa/Bulan
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('rental_cost_percentage' + index);
                  }}
                  error={this.state.validator['rental_cost_percentage' + index]}
                  helperText={this.state.validator['rental_cost_percentage' + index]}
                  value={this.state.value['rental_cost_percentage' + index]}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'rental_cost_percentage' + index);
                    if (event.target.value) {
                      this.handleChange(parseFloat(event.target.value) / 2, 'rental_cost_percentage_15_days' + index);
                    } else {
                      this.handleChange(event.target.value, 'rental_cost_percentage_15_days' + index);
                    }
                  }}
                  name={'rental_cost_percentage' + index}
                  InputProps={{
                    endAdornment: <InputAdornment position="start"> % </InputAdornment>
                  }}
                />
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.labelValue}>Biaya Sewa/15 Hari</text>
                </div>
                <div
                  style={{
                    marginTop: 25,
                    marginLeft: '15px'
                  }}
                >
                  <text className={classes.value}>
                    {this.state.value['rental_cost_percentage_15_days' + index]}
                  </text>
                </div>
              </Grid>
            </Grid>
            <Divider className={classes.BodytitleMdl} />
          </div>
        )
      })
      return list;
    }

    render() {
      const { classes } = this.props;
      return (
        <div>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              <div className={classes.root}>
                <Grid
                  container
                  direction="row"
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  style={{
                    marginBottom: 20
                  }}
                >
                  <Grid item lg={8} xl={8} md={8} sm={8} xs={12}>
                    <div className={classes.BodytitleMdl}>
                      <text className={classes.titleMdl}>Gadai Baru</text>
                    </div>
                  </Grid>
                  <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                    <text className={classes.label1}>Wilayah:</text>
                    <Autocomplete
                      options={this
                        .state
                        .province
                        .map(({ id, name }) => ({ id, name }))}
                      onChange={(evt, val) => {
                        this.setState({ regionalselected: val })
                        this.handleChangeWilayah(val)
                      }}
                      value={this.state.regionalselected}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="outlined"
                          className={classes.input}
                          margin="normal"
                          size="small"
                        />
                      )}
                      noOptionsText="Data Kosong"
                    />
                  </Grid>
                </Grid>
                {this.renderInsurance()}
                <SewaPerpanjangan
                  rowSelected={this.state.rowSelected}
                  prolongation={this.props.prolongation}
                  prolongAttb={this.state.prolongAttb}
                  setProlongation={(value) => this.setState({ prolongAttb: value })}
                  error={this.state.prolongError}
                />
              </div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              style={{
                backgroundColor: '#c2a443',
                color: 'white'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.handleSubmit(this.props.type);
              }}
              variant="contained"
            >
              <Typography
                variant="button"
                style={{
                  color: '#FFFFFF'
                }}
              >
                {this.state.loader_button
                  ? (
                    <CircularProgress
                      style={{
                        color: 'white',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: 5
                      }}
                      size={15}
                    />
                  )
                  : ('Simpan')}
              </Typography>
            </Button>
            <Button
              style={{
                color: 'black'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this
                  .props
                  .finishSubmit()
              }}
              variant="outlined"
            >
              <Typography variant="button">Selesai</Typography>
            </Button>
          </DialogActions>
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
