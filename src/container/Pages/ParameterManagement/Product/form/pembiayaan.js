/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-state */
/* eslint-disable radix */
/* eslint-disable no-param-reassign */
/* eslint-disable camelcase */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/button-has-type */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import { DialogActions, DialogContent } from '@material-ui/core';
import DialogContentText from '@material-ui/core/DialogContentText';
import RenderInsurance from './RenderInsurance';
import validationSchema from '../../../../../validation';
import Func from '../../../../../functions/index';
import styles from '../css';

function createData(id, name) {
  return { id, name };
}

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      rowSelected: [],
      dataInsurance: [],
      activeButton: '',
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

  onUnload = (e) => {
    if (localStorage.getItem('delete_product') == null) {
      e.preventDefault();
      e.returnValue = '';
    }
  };

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.onUnload);
  }

  componentDidMount() {
    this.setState({
      value: this.props.value,
      rowSelected: this.props.rowSelected,
      province: this.props.province
    });
    window.addEventListener('beforeunload', this.onUnload);
    this.getInsurance();
  }

  handleSubmit() {
    this.setState({ loading: true });
    const validator = [];
    let validateResult = { error: '' };
    this.state.rowSelected.map((data) => {
      if (!data._destroy) {
        validator.push({
          name: 'min_weight' + data.id,
          type: 'required',
        });
        validator.push({
          name: 'min_karatase' + data.id,
          type: 'required',
        });
        validator.push({
          name: 'min_loan' + data.id,
          type: 'required',
        });
        validator.push({
          name: 'max_loan' + data.id,
          type: 'required',
        });
        // validation with Joi
        validateResult = validationSchema.validate(this.state.value['product_finance_items_attributes' + data.id], {
          abortEarly: false,
        });
      }
    });
    const validate = Func.Validator(this.state.value, validator);
    if (validate.success && !validateResult.error) {
      this.setState({ loader_button: true });
      const isurance = [];
      this.state.rowSelected.map(data => {
        const productFinanceState = this.state.value['product_finance_items_attributes' + data.id];
        const productFinanceData = [];
        productFinanceState.map((value, index) => {
          productFinanceData.push({
            ltv_value: value.ltv_value,
            regional_id: value.regional_id.$oid,
            stle_value: value.stle_value
          });
          if (value.id) productFinanceData[index].id = value.id.$oid;
        });
        if (data._id === '') {
          isurance.push({
            insurance_item_id: data.id,
            product_finance_items_attributes: productFinanceData,
            min_weight: this.state.value['min_weight' + data.id],
            min_karatase: this.state.value['min_karatase' + data.id],
            min_loan: this.state.value['min_loan' + data.id],
            max_loan: this.state.value['max_loan' + data.id],
            _destroy: data._destroy,
          });
        } else {
          isurance.push({
            id: this.state.value['id' + data.id].$oid,
            insurance_item_id: data.id,
            product_finance_items_attributes: productFinanceData,
            min_weight: this.state.value['min_weight' + data.id],
            min_karatase: this.state.value['min_karatase' + data.id],
            min_loan: this.state.value['min_loan' + data.id],
            max_loan: this.state.value['max_loan' + data.id],
            _destroy: data._destroy,
          });
        }
      });
      const productId = this.props.id;
      fetch(process.env.REACT_APP_URL_MASTER + 'product/' + productId, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
        body: JSON.stringify({
          product: {
            product_finances_attributes: isurance,
          },
        }),
      })
        .then((response) => response.json())
        .then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          }
          if (json.code === 200) {
            const { rowSelected, value } = this.state;
            json.data.product_detail.product_finances.map(data => {
              // update rowSelected sesuai yg baru disimpan
              rowSelected.map((val, inx) => {
                if (rowSelected[inx]._destroy) rowSelected.splice(inx, 1);
              });

              // update value pembiayan sesuai yg baru disimpan
              value['id' + data.insurance_item.id.$oid] = data.id;
              value['min_karatase' + data.insurance_item.id.$oid] = data.min_karatase;
              value['min_loan' + data.insurance_item.id.$oid] = data.min_loan;
              value['max_loan' + data.insurance_item.id.$oid] = data.max_loan;
              value['min_weight' + data.insurance_item.id.$oid] = data.min_weight;
              value['product_finance_items_attributes' + data.insurance_item.id.$oid] = data.product_finance_items;
            });

            this.props.OnNext({
              success: true,
              id: json.data.product_detail.id.$oid,
              product_finances: json.data.product_detail.product_finances,
              value: this.state.value,
              rowSelected
            });
          } else {
            this.setState({ validator: json.status });
          }
          this.setState({ loader_button: false });
        })
        .catch(() => {
          this.setState({ loader_button: false });
        })
        .finally(() => {
          this.setState({ loader_button: false });
        });
    } else {
      this.state.rowSelected.map(data => {
        if (validateResult.error) {
          validate.error['product_finance_items_attributes' + data.id] = {
            regional_id: 'Harap isi kotak ini',
            stle_value: 'Harap isi kotak ini',
            ltv_value: 'Harap isi kotak ini'
          };
        }
      });
      this.setState({ validator: validate.error });
    }
  }

  handleChangeImg(event) {
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
  }

  getInsurance() {
    Func.getData('insurance_item', 9999999, 1, '').then((res) => {
      if (res.json.code === '403') {
        if (Func.Clear_Token() === true) {
          if (!localStorage.getItem('token')) {
            this.setState({ redirect: true });
          }
        }
      } else if (res.json.status === 500) {
        this.setState({ failure: true });
      } else {
        const datas = [];
        res.json.data.map((data) => {
          if (data.status) {
            datas.push(createData(data._id.$oid, data.name));
          }
        });
        const sta = this.state.rowSelected;
        if (sta.length === 0) {
          sta.push({ id: datas[0].id, name: datas[0].name, _id: '', _destroy: false });
        }
        this.setState({ dataInsurance: datas, rowSelected: sta, activeButton: datas[0].name });
      }
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <div>
              {
                (this.state.dataInsurance.length < 1)
                  ? <LinearProgress color="primary" style={{ width: '500px' }} />
                  : (
                    <>
                      <ButtonGroup>
                        {this.state.dataInsurance.map((data) => (
                          <Button
                            onClick={() => this.setState({ activeButton: data.name })}
                            variant={
                              this.state.activeButton === data.name
                                ? 'outlined'
                                : 'contained'
                            }
                            color="primary"
                          >
                            {data.name}
                          </Button>
                        ))}
                      </ButtonGroup>
                    </>
                  )
              }
              <RenderInsurance
                activeButton={this.state.activeButton}
                dataInsurance={this.state.dataInsurance}
                rowSelected={this.state.rowSelected}
                removeValidate={this.removeValidate.bind(this)}
                setStateInsurance={this.setState.bind(this)}
                stateValidator={this.state.validator}
                stateValue={this.state.value}
                handleChange={this.handleChange.bind(this)}
                province={this.state.province}
              />
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#c2a443',
              color: 'white'
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography
              variant="button"
              style={{
                color: '#FFFFFF'
              }}
            >
              {this.state.loader_button
                ? (
                  <CircularProgress
                    style={{
                      color: 'white',
                      marginLeft: '18px',
                      marginRight: '18px',
                      marginTop: 5
                    }}
                    size={15}
                  />
                )
                : ('Simpan')}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black'
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.finishSubmit();
            }}
            variant="outlined"
          >
            <Typography variant="button">Selesai</Typography>
          </Button>
        </DialogActions>
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
