/* eslint-disable semi */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-plusplus */
/* eslint-disable radix */
/* eslint-disable react/no-unused-state */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import swal from 'sweetalert';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import { DialogActions, DialogContent } from '@material-ui/core';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';
import SewaPerpanjangan from './sewa/sewa_perpanjangan';
import { prolongationSchema } from '../../../../../validation';

function createData(id, name, cont) {
  return { id, name, cont };
}

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      rowSelected: [],
      cont: 0,
      dataInsurance: [],
      prolongAttb: {},
      prolongError: '',
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

    onUnload = e => {
      e.preventDefault();
      e.returnValue = '';
    }

    componentWillUnmount() {
      window.removeEventListener('beforeunload', this.onUnload);
    }

    componentDidMount() {
      let cont = 0;
      this.props.dataInsurance.map((data, index) => {
        cont += data.cont.length;
      });
      this.setState({
        cont,
        value: this.props.value,
        rowSelected: this.props.rowSelected,
      });
      window.addEventListener('beforeunload', this.onUnload);
      if (this.state.dataInsurance.length === 0) {
        this.getInsurance();
      } else {
        this.setState({
          value: this.props.value,
          dataInsurance: this.props.dataInsurance
        });
      }
    }

    handleSubmit() {
      this.setState({ loading: true });
      const validator = [];
      const prolongFormData = [];

      this.state.dataInsurance.map((data) => {
        data.cont.map((data2) => {
          validator.push({
            name: 'min_loan_range' + data2.id + data2.key,
            type: 'required'
          });
          validator.push({
            name: 'max_loan_range' + data2.id + data2.key,
            type: 'required'
          });
          validator.push({
            name: 'cost_nominal' + data2.id + data2.key,
            type: 'required'
          });
          validator.push({
            name: 'cost_percentage' + data2.id + data2.key,
            type: 'required'
          });
        });
      });

      // prolongation/gadai ulang
      this.props.rowSelected.map((data) => {
        const prolongData = this.state.prolongAttb[data.id]
        prolongData.sort((a, b) => (a._destroy - b._destroy))
          .map((val, idx) => {
            const { value, order, id, _destroy } = val;
            prolongFormData.push({
              insurance_item_id: data.id,
              value: parseFloat(value.toString().replace(/,/g, '.')),
              type: 'admin',
              order: idx + 1,
              _destroy,
              ...id !== undefined && { id }
            });
          });
      })

      const prolongValidate = prolongationSchema.validate(prolongFormData, { abortEarly: false });
      const validate = Func.Validator(this.state.value, validator);

      if (validate.success && !prolongValidate.error) {
        this.setState({ loader_button: true });
        const admin = [];
        this.state.dataInsurance.map((data) => {
          data.cont.map((data2) => {
            admin.push({
              insurance_item_id: data2.id,
              min_loan_range: parseInt(this.state.value['min_loan_range' + data2.id + data2.key]),
              max_loan_range: parseInt(this.state.value['max_loan_range' + data2.id + data2.key]),
              cost_nominal: parseInt(this.state.value['cost_nominal' + data2.id + data2.key]),
              cost_percentage: parseFloat(this.state.value['cost_percentage' + data2.id + data2.key]),
              _destroy: data2._destroy
            });
            if (data2.itemId) admin[data2.key].id = data2.itemId;
          });
        });

        fetch(process.env.REACT_APP_URL_MASTER + 'product/' + this.props.id, {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token')
          },
          body: JSON.stringify({
            product: {
              product_admin_costs_attributes: admin,
              product_prolongations_attributes: prolongFormData,
            }
          })
        }).then((response) => response.json()).then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          } else if (json.code === 400) {
            swal({
              title: 'Kesalahan',
              text: json?.message,
              icon: 'error',
              buttons: 'OK'
            });
          }
          if (json.success) {
            this.setState({ loading: false });
            // update value sewa
            const { value } = this.state;
            const loopAdmin = [];

            json.data.product_detail.product_finances.map((data) => {
              let key3 = 0;
              const arr = [];
              json.data.product_detail.product_admin_costs.map((data2) => {
                if (data.insurance_item.id.$oid === data2.insurance_item_id.$oid) {
                  arr.push({ itemId: data2.id.$oid, id: data.insurance_item.id.$oid, key: key3, _destroy: false });
                }
                key3 += 1;
              });
              if (arr.length === 0) {
                arr.push({ itemId: '', id: data.insurance_item.id.$oid, key: key3, _destroy: false });
              }
              loopAdmin.push({ id: data.insurance_item.id.$oid, name: data.insurance_item.name, cont: arr });
            });

            const z = json.data.product_detail.product_admin_costs;
            for (let index = 0; index < z.length; index++) {
              const data2 = z[index];
              const k = data2.insurance_item_id.$oid + index;
              value['id' + k] = data2.id.$oid;
              value['min_loan_range' + k] = data2.min_loan_range;
              value['max_loan_range' + k] = data2.max_loan_range;
              value['cost_nominal' + k] = data2.cost_nominal;
              value['cost_percentage' + k] = data2.cost_percentage;
              value['_destroy' + k] = false;
            }
            value.length_data = z.length;
            value.allValue = z;

            const datas = [];
            let count = 0;
            const { rowSelected } = this.state;
            rowSelected.map((data) => {
              const arr = [];
              z.map(data2 => {
                if (data2.insurance_item_id.$oid === data.id) {
                  arr.push({ itemId: data2.id.$oid, id: data.id, key: count, _destroy: false });
                  count += 1;
                }
              });
              const emptyArr = arr.filter(val => val.id === data.id);
              if (!emptyArr.length) arr.push({ itemId: '', id: data.id, key: count, _destroy: false });
              datas.push({ id: data.id, name: data.name, cont: arr });
            });

            this.setState({
              dataInsurance: datas,
              cont: count,
              value
            });

            this.props.OnNext({
              success: true,
              id: json.data.product_detail.id.$oid,
              value: this.state.value,
              dataInsurance: this.state.dataInsurance,
              prolongation: json.data.product_detail.product_prolongations
            });
          } else {
            this.setState({ validator: json.status });
          }
          this.setState({ loader_button: false, prolongError: '' });
        }).catch((error) => {
          this.setState({ loader_button: false });
        })
          . finally(() => {
            this.setState({ loader_button: false });
          });
      } else {
        this.setState({
          validator: validate.error,
          prolongError: 'Harus diisi',
        });
      }
    }

    handleChangeImg(event) {
      this.removeValidate('img');
      const dataSet = this.state.value;
      dataSet.img = URL.createObjectURL(event.target.files[0]);
      this.setState({ value: dataSet });

      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.setState({ ImgBase44: reader.result });
      };
    }

    getInsurance() {
      const datas = [];
      let count = 0;
      const dataLength = this.props.value?.length_data;
      this.props.rowSelected.map((data) => {
        const arr = [];
        if (dataLength) {
          this.props.value.allValue.map(data2 => {
            if (data2.insurance_item_id.$oid === data.id) {
              arr.push({ itemId: data2.id.$oid, id: data.id, key: count, _destroy: false });
              count += 1;
            }
          });
          const emptyArr = arr.filter(val => val.id === data.id);
          if (!emptyArr.length) arr.push({ itemId: '', id: data.id, key: count, _destroy: false });
        } else {
          arr.push({ itemId: '', id: data.id, key: count, _destroy: false });
          count += 1;
        }
        datas.push(createData(data.id, data.name, arr));
      });
      this.setState({
        dataInsurance: datas,
        cont: count
      });
    }

    renderInsurance2(index2, data) {
      const { classes, rowSelected } = this.props;
      const list = [];
      data.map((value, index) => {
        if (value._destroy === false) {
          list.push(
            <div>
              <Grid
                container
                direction="row"
                item
                lg={12}
                xl={12}
                md={12}
                xs={12}
              >
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    fullWidth
                    onFocus={() => {
                      this.removeValidate('min_loan_range' + value.id + value.key);
                    }}
                    error={this.state.validator['min_loan_range' + value.id + value.key]}
                    helperText={this.state.validator['min_loan_range' + value.id + value.key]}
                    value={this.state.value['min_loan_range' + value.id + value.key] !== undefined ? Func.FormatNumber(this.state.value['min_loan_range' + value.id + value.key]) : ''}
                    onChange={(event) => {
                      this.handleChange(Func.UnFormatRp(event.target.value), 'min_loan_range' + value.id + value.key);
                    }}
                    name={'min_loan_range' + value.id + value.key}
                    InputProps={{
                      startAdornment:
  <InputAdornment position="start">
    Rp
  </InputAdornment>
                    }}
                  />
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    fullWidth
                    onFocus={() => {
                      this.removeValidate('max_loan_range' + value.id + value.key);
                    }}
                    error={this.state.validator['max_loan_range' + value.id + value.key]}
                    helperText={this.state.validator['max_loan_range' + value.id + value.key]}
                    value={this.state.value['max_loan_range' + value.id + value.key] !== undefined ? Func.FormatNumber(this.state.value['max_loan_range' + value.id + value.key]) : ''}
                    onChange={(event) => {
                      this.handleChange(Func.UnFormatRp(event.target.value), 'max_loan_range' + value.id + value.key);
                    }}
                    name={'max_loan_range' + value.id + value.key}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                    }}
                  />
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    fullWidth
                    onFocus={() => {
                      this.removeValidate('cost_nominal' + value.id + value.key);
                    }}
                    error={this.state.validator['cost_nominal' + value.id + value.key]}
                    helperText={this.state.validator['cost_nominal' + value.id + value.key]}
                    value={this.state.value['cost_nominal' + value.id + value.key] !== undefined ? Func.FormatNumber(this.state.value['cost_nominal' + value.id + value.key]) : ''}
                    onChange={(event) => {
                      this.handleChange(Func.UnFormatRp(event.target.value), 'cost_nominal' + value.id + value.key);
                    }}
                    name={'cost_nominal' + value.id + value.key}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                    }}
                  />
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <div>

                    <TextField
                      className={classes.input}
                      style={{ marginRight: 30 }}
                      variant="outlined"
                      margin="normal"
                      size="small"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('cost_percentage' + value.id + value.key);
                      }}
                      error={this.state.validator['cost_percentage' + value.id + value.key]}
                      helperText={this.state.validator['cost_percentage' + value.id + value.key]}
                      value={this.state.value['cost_percentage' + value.id + value.key]}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'cost_percentage' + value.id + value.key);
                      }}
                      name={'cost_percentage' + value.id + value.key}
                      InputProps={{
                        endAdornment: <InputAdornment position="start"> % </InputAdornment>
                      }}
                    />
                    <img
                      src={Icon.delete}
                      style={{ marginLeft: -35, marginTop: 25, cursor: 'pointer' }}
                      onClick={() => {
                        const data = this.state.dataInsurance;
                        const i = data[index2].cont;
                        if (i.length <= 1) {
                          swal({
                            title: 'Maaf, terjadi kesalahan',
                            text: 'Minimum 1',
                            icon: 'error',
                          });
                        } else {
                          i[index]._destroy = true;
                        }
                        this.setState({ dataInsurance: data });
                      }}
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          );
        }
      });
      return list;
    }

    renderInsurance() {
      const { classes } = this.props;
      const list = [];
      const { cont } = this.state;
      this.state.dataInsurance.map((data, index) => {
        list.push(
          <div>
            <div className={classes.BodytitleMdl}>
              <text className={classes.titleMdl}>{data.name}</text>
              <button
                style={{
                  backgroundColor: '#C4A643',
                  borderRadius: 50,
                  color: 'white',
                  width: 80,
                  height: 35,
                  marginLeft: 10,
                  fontWeight: '500',
                  fontSize: 14
                }}
                onClick={() => {
                  const data = this.state.dataInsurance;
                  const i = data[index].cont;
                  i.push({ itemId: '', id: data[index].id, key: this.state.cont, _destroy: false });
                  this.setState({
                    dataInsurance: data,
                    cont: this.state.cont + 1
                  });
                }}
              >
                Tambah
              </button>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
            >
              <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Pinjaman Dari
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
              </Grid>
              <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Pinjaman Sampai
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
              </Grid>
              <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Nominal
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
              </Grid>
              <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Persentase
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
              </Grid>
            </Grid>
            {this.renderInsurance2(index, data.cont)}
            <Divider className={classes.BodytitleMdl} />
          </div>
        );
      });
      return list;
    }

    render() {
      const { classes } = this.props;
      return (
        <div>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {this.renderInsurance()}
              <SewaPerpanjangan
                rowSelected={this.state.rowSelected}
                prolongation={this.props.prolongation}
                prolongAttb={this.state.prolongAttb}
                setProlongation={(value) => this.setState({ prolongAttb: value })}
                error={this.state.prolongError}
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              style={{
                backgroundColor: '#c2a443',
                color: 'white'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.handleSubmit(this.props.type);
              }}
              variant="contained"
            >
              <Typography
                variant="button"
                style={{
                  color: '#FFFFFF'
                }}
              >
                {this.state.loader_button
                  ? (
                    <CircularProgress
                      style={{
                        color: 'white',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: 5
                      }}
                      size={15}
                    />
                  )
                  : ('Simpan')}
              </Typography>
            </Button>
            <Button
              style={{
                color: 'black'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.props.finishSubmit();
              }}
              variant="outlined"
            >
              <Typography variant="button">Selesai</Typography>
            </Button>
          </DialogActions>
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
