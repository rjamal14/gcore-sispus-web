/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable semi */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import Autocomplete from '@material-ui/lab/Autocomplete';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import Func from '../../../../../../functions';
import styles from '../../css';

function STLEfield(props) {
  const {
    classes,
    flag,
    removeValidate,
    data,
    stateValidator,
    stateValue,
    setStateInsurance,
    province
  } = props;

  const productFinanceProps = stateValue[`product_finance_items_attributes${data.id}`];
  const productFinanceValidator = stateValidator[`product_finance_items_attributes${data.id}`];
  const productFinanceInitialize = {
    id: '',
    ltv_value: '',
    regional_id: '',
    stle_value: '',
    regional_val: ''
  };

  const handleAddInput = () => {
    if (productFinanceProps) {
      productFinanceProps.push(productFinanceInitialize);
      setStateInsurance({ productFinanceProps });
    } else {
      stateValue[`product_finance_items_attributes${data.id}`] = [productFinanceInitialize];
      setStateInsurance({ value: stateValue });
    }
  };

  useEffect(() => {
    // jika productFinanceProps kosong, maka push 1 object kosong
    // jika ada, maka isi satu2 regional_val & id sesuai yg d dapat dr api, agar regionalnya bsa muncul d form input
    // isi validator dng objek kosong

    if (!productFinanceValidator) {
      stateValidator[`product_finance_items_attributes${data.id}`] = productFinanceInitialize;
      setStateInsurance({ validator: stateValidator });
    }

    if (!productFinanceProps || productFinanceProps.length < 1) handleAddInput();
    else {
      productFinanceProps.map(({ id, regional_id, regional_val }, idx) => {
        if (!regional_val && regional_id) {
          const filteredRegional = province.filter(val => val.id === regional_id.$oid)
          if (filteredRegional.length !== 0) {
            productFinanceProps[idx].regional_val = {
              id: filteredRegional[0].id,
              name: filteredRegional[0].name
            }
            productFinanceProps[idx].id = id;
            setStateInsurance({ productFinanceProps })
          }
        }
      })
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productFinanceProps]);

  const handleChangeInput = (val, name, index) => {
    if (name === 'regional_val') productFinanceProps[index].regional_id = { $oid: val.id };
    productFinanceProps[index][name] = val;
    setStateInsurance({ productFinanceProps });
  };

  const handleRemoveInput = (index) => {
    if (productFinanceProps.length > 1) productFinanceProps.splice(index, 1);
    else alert('Minimal 1 !');
    setStateInsurance({ productFinanceProps });
  };

  return (
    <>
      <Grid container direction="row" item lg={12} xl={12} md={12} xs={12} style={{ height: '20px' }}>
        <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
          <text className={classes.label1}>Wilayah</text>
          <text className={classes.starts1}>*</text>
        </Grid>
        <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
          <text className={classes.label1}>STLE (Rp.)</text>
          <text className={classes.starts1}>*</text>
        </Grid>
        <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
          <text className={classes.label1}>LTV (%)</text>
          <text className={classes.starts1}>*</text>
        </Grid>
        <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
          <IconButton aria-label="add" onClick={handleAddInput} size="small">
            <AddIcon />
          </IconButton>
        </Grid>
      </Grid>
      {/* ---------------------------- STLE & LTV -------------------------------------------- */}
      <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
        {
          productFinanceProps
          && productFinanceProps.map((value, idx) => {
            if (value.regional_val || value.regional_val === '') {
              return (
                <>
                  <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                    <Autocomplete
                      disabled={!flag}
                      options={province.map(({ id, name }) => ({ id, name }))}
                      onChange={(evt, val) => {
                        handleChangeInput(val, 'regional_val', idx, value.id);
                      }}
                      value={value.regional_val}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="outlined"
                          className={classes.input}
                          margin="normal"
                          size="small"
                          error={stateValidator['product_finance_items_attributes' + data.id]?.regional_id && !value.regional_val}
                          helperText={value.regional_val ? '' : stateValidator['product_finance_items_attributes' + data.id]?.regional_id}
                          onFocus={() => {
                            removeValidate('product_finance_items_attributes' + data.id);
                          }}
                        />
                      )}
                      noOptionsText="Data Kosong"
                    />
                  </Grid>
                  <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                    <TextField
                      disabled={!flag}
                      className={classes.input}
                      variant="outlined"
                      margin="normal"
                      size="small"
                      autoComplete="off"
                      fullWidth
                      onFocus={() => {
                        removeValidate('product_finance_items_attributes' + data.id);
                      }}
                      error={stateValidator['product_finance_items_attributes' + data.id]?.stle_value && !value.stle_value}
                      helperText={value.stle_value ? '' : stateValidator['product_finance_items_attributes' + data.id]?.stle_value}
                      value={value.stle_value}
                      onChange={(event) => {
                        handleChangeInput(Func.UnFormatRp(event.target.value), 'stle_value', idx, value.id);
                      }}
                      name={'stle_value' + data.id}
                      InputProps={{
                        startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                      }}
                    />
                  </Grid>
                  <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                    <TextField
                      disabled={!flag}
                      className={classes.input}
                      variant="outlined"
                      margin="normal"
                      size="small"
                      autoComplete="off"
                      fullWidth
                      onFocus={() => {
                        removeValidate('product_finance_items_attributes' + data.id);
                      }}
                      error={stateValidator['product_finance_items_attributes' + data.id]?.ltv_value && !value.ltv_value}
                      helperText={value.ltv_value ? '' : stateValidator['product_finance_items_attributes' + data.id]?.ltv_value}
                      value={value.ltv_value}
                      onChange={(event) => {
                        handleChangeInput(Func.UnFormatRp(event.target.value), 'ltv_value', idx, value.id);
                      }}
                      name={'stle_value' + data.id}
                      InputProps={{
                        endAdornment: <InputAdornment position="start"> % </InputAdornment>
                      }}
                    />
                  </Grid>
                  <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                    <div style={{ marginTop: '15%' }}>
                      <IconButton aria-label="delete" onClick={() => handleRemoveInput(idx)} size="small">
                        <DeleteIcon />
                      </IconButton>
                    </div>
                  </Grid>
                </>
              )
            }
          })
        }
      </Grid>
      {/* ---------------------------- STLE & LTV -------------------------------------------- */}
    </>
  );
}

export default withStyles(styles.CoustomsStyles, { name: 'STLEfield' })(STLEfield);
