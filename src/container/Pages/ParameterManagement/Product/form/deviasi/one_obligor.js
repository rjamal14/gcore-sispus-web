/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
/* eslint-disable array-callback-return */
import React, { useEffect, useState } from 'react';
import {
  LinearProgress,
  ButtonGroup,
  Button,
  Grid,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import TabPanel from '../TabPanel';
import useStyle from './style';

const OneObligor = (props) => {
  const { rowSelected, deviationAttb, setDeviation, deviationObligor, error } = props;
  const classes = useStyle();
  const [activeButton, setActiveButton] = useState(rowSelected[0].name);

  useEffect(() => {
    const insurance = {};
    rowSelected.map((item) => {
      insurance[item.id] = {};
    });

    (deviationObligor.length > 0)
    && deviationObligor.map((val) => {
      const insuranceId = val.insurance_item_id.$oid;
      insurance[insuranceId] = {
        id: val._id.$oid,
        branch: val.branch,
        area: val.area,
        region: val.region,
        head: val.head,
      };
    });

    setDeviation({ ...insurance });
  }, []);

  const handleChangeInput = (val, name, insuranceId) => {
    const newAttb = deviationAttb[insuranceId];
    newAttb[name] = val;
    const deviation = deviationAttb;
    deviation[insuranceId] = newAttb;
    setDeviation({ ...deviation });
  };

  return (
    <div style={{ marginTop: 10 }}>
      <div style={{ marginBottom: 20 }}>
        {(rowSelected.length < 1)
          ? <LinearProgress color="primary" style={{ width: '500px' }} />
          : (
            <ButtonGroup>
              {rowSelected.map((data) => (
                <Button
                  onClick={() => setActiveButton(data.name)}
                  variant={activeButton === data.name ? 'outlined' : 'contained'}
                  color="primary"
                >
                  {data.name}
                </Button>
              ))}
            </ButtonGroup>
          )}
      </div>
      {
        (rowSelected.length > 0) && rowSelected.map((data) => {
          const insuranceId = data.id;
          const value = deviationAttb[insuranceId] || {};
          return (
            <TabPanel value={activeButton} index={data.name}>
              <Grid fullWidth container direction="row" spacing={1} lg={12} xl={12} md={12} xs={12}>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <text className={classes.label1}>Cabang (Rp)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <text className={classes.label1}>Area (Rp)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <text className={classes.label1}>Wilayah (Rp)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <text className={classes.label1}>KP (Rp)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    error={error && !value.branch}
                    helperText={value.branch ? '' : error}
                    value={value?.branch}
                    onChange={(event) => {
                      handleChangeInput(event.target.value, 'branch', insuranceId);
                    }}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                    }}
                  />
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    error={error && !value.area}
                    helperText={value.area ? '' : error}
                    value={value?.area}
                    onChange={(event) => {
                      handleChangeInput(event.target.value, 'area', insuranceId);
                    }}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                    }}
                  />
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    error={error && !value.region}
                    helperText={value.region ? '' : error}
                    value={value?.region}
                    onChange={(event) => {
                      handleChangeInput(event.target.value, 'region', insuranceId);
                    }}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                    }}
                  />
                </Grid>
                <Grid item lg={3} xl={3} md={3} sm={3} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    error={error && !value.head}
                    helperText={value.head ? '' : error}
                    value={value?.head}
                    onChange={(event) => {
                      handleChangeInput(event.target.value, 'head', insuranceId);
                    }}
                    InputProps={{
                      startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                    }}
                  />
                </Grid>
              </Grid>
            </TabPanel>
          );
        })
      }
    </div>
  );
};

export default OneObligor;
