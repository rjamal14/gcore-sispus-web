import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  label1: {
    marginLeft: '15px'
  },
  starts1: {
    marginLeft: '5px',
    color: '#F78536'
  },
}));

export default useStyles;
