/* eslint-disable consistent-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
/* eslint-disable array-callback-return */
import React, { useEffect, useState, Fragment } from 'react';
import {
  LinearProgress,
  ButtonGroup,
  Button,
  Grid,
  IconButton,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AddIcon from '@material-ui/icons/Add';
import TabPanel from '../TabPanel';
import useStyle from './style';

const Sewa = (props) => {
  const { rowSelected, deviationAttb, setDeviation, deviationSewa, province, error } = props;
  const classes = useStyle();
  const [activeButton, setActiveButton] = useState(rowSelected[0].name);

  useEffect(() => {
    const insurance = {};
    rowSelected.map((item) => {
      insurance[item.id] = [];
    });

    if (deviationSewa?.length > 0) {
      deviationSewa.map((val) => {
        const insuranceId = val.insurance_item_id.$oid;
        insurance[insuranceId].push({
          id: val._id.$oid,
          branch: val.branch,
          area: val.area,
          region: val.region,
          head: val.head,
          regional_id: (val?.regional_id?.$oid) ? province.find(({ id }) => id === val.regional_id.$oid) : '',
          _destroy: false,
        });
      });
    }

    setDeviation({ ...insurance });
  }, []);

  const handleRemoveInput = (index, insuranceId) => {
    if (deviationAttb[insuranceId].length > 1) {
      const newAttb = deviationAttb[insuranceId];
      newAttb[index]._destroy = true;
      const deviation = deviationAttb;
      deviation[insuranceId] = newAttb;
      setDeviation({ ...deviation });
    } else alert('Minimal 1 !');
  };

  const handleAddInput = (insuranceId) => {
    const newAttb = deviationAttb[insuranceId];
    newAttb.push({
      branch: '',
      area: '',
      region: '',
      head: '',
      regional_id: '',
      _destroy: false,
    });
    const deviation = deviationAttb;
    deviation[insuranceId] = newAttb;
    setDeviation({ ...deviation });
  };

  const handleChangeInput = (val, name, index, insuranceId) => {
    const newAttb = deviationAttb[insuranceId];
    newAttb[index][name] = val;
    const deviation = deviationAttb;
    deviation[insuranceId] = newAttb;
    setDeviation({ ...deviation });
  };

  return (
    <div style={{ marginTop: 10 }}>
      <div style={{ marginBottom: 20 }}>
        {(rowSelected.length < 1)
          ? <LinearProgress color="primary" style={{ width: '500px' }} />
          : (
            <ButtonGroup>
              {rowSelected.map((data) => (
                <Button
                  onClick={() => setActiveButton(data.name)}
                  variant={activeButton === data.name ? 'outlined' : 'contained'}
                  color="primary"
                >
                  {data.name}
                </Button>
              ))}
            </ButtonGroup>
          )}
      </div>
      {
        (rowSelected.length > 0) && rowSelected.map((data) => {
          const insuranceId = data.id;
          return (
            <TabPanel value={activeButton} index={data.name}>
              <Grid fullWidth container direction="row" spacing={1} lg={12} xl={12} md={12} xs={12}>
                <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                  <text className={classes.label1}>Wilayah</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                  <text className={classes.label1}>Cabang (%)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                  <text className={classes.label1}>Area (%)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                  <text className={classes.label1}>Wilayah (%)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                  <text className={classes.label1}>KP (%)</text>
                  <text className={classes.starts1}>*</text>
                </Grid>
                <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                  <IconButton aria-label="add" onClick={() => handleAddInput(insuranceId)} size="small">
                    <AddIcon />
                  </IconButton>
                </Grid>
                {
                  (deviationAttb[insuranceId]?.length > 0)
                  && deviationAttb[insuranceId].map((value, idx) => {
                    if (!value._destroy) {
                      return (
                        <Fragment>
                          <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                            <Autocomplete
                              options={province.map(({ id, name }) => ({ id, name }))}
                              onChange={(evt, val) => {
                                handleChangeInput(val, 'regional_id', idx, insuranceId);
                              }}
                              value={value.regional_id}
                              getOptionLabel={(option) => option.name}
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  variant="outlined"
                                  className={classes.input}
                                  margin="normal"
                                  size="small"
                                  error={error && !value.regional_id?.id}
                                  helperText={value.regional_id?.id ? '' : error}
                                />
                              )}
                              noOptionsText="Data Kosong"
                            />
                          </Grid>
                          <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                            <TextField
                              className={classes.input}
                              variant="outlined"
                              margin="normal"
                              size="small"
                              autoComplete="off"
                              error={error && !value.branch}
                              helperText={value.branch ? '' : error}
                              value={value.branch}
                              onChange={(event) => {
                                handleChangeInput(event.target.value, 'branch', idx, insuranceId);
                              }}
                              InputProps={{
                                endAdornment: <InputAdornment position="start"> % </InputAdornment>
                              }}
                            />
                          </Grid>
                          <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                            <TextField
                              className={classes.input}
                              variant="outlined"
                              margin="normal"
                              size="small"
                              autoComplete="off"
                              error={error && !value.area}
                              helperText={value.area ? '' : error}
                              value={value.area}
                              onChange={(event) => {
                                handleChangeInput(event.target.value, 'area', idx, insuranceId);
                              }}
                              InputProps={{
                                endAdornment: <InputAdornment position="start"> % </InputAdornment>
                              }}
                            />
                          </Grid>
                          <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                            <TextField
                              className={classes.input}
                              variant="outlined"
                              margin="normal"
                              size="small"
                              autoComplete="off"
                              error={error && !value.region}
                              helperText={value.region ? '' : error}
                              value={value.region}
                              onChange={(event) => {
                                handleChangeInput(event.target.value, 'region', idx, insuranceId);
                              }}
                              InputProps={{
                                endAdornment: <InputAdornment position="start"> % </InputAdornment>
                              }}
                            />
                          </Grid>
                          <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                            <TextField
                              className={classes.input}
                              variant="outlined"
                              margin="normal"
                              size="small"
                              autoComplete="off"
                              error={error && !value.head}
                              helperText={value.head ? '' : error}
                              value={value.head}
                              onChange={(event) => {
                                handleChangeInput(event.target.value, 'head', idx, insuranceId);
                              }}
                              InputProps={{
                                endAdornment: <InputAdornment position="start"> % </InputAdornment>
                              }}
                            />
                          </Grid>
                          <Grid item lg={2} xl={2} md={2} sm={2} xs={12}>
                            <div style={{ marginTop: '15%' }}>
                              <IconButton aria-label="delete" onClick={() => handleRemoveInput(idx, insuranceId)} size="small">
                                <DeleteIcon />
                              </IconButton>
                            </div>
                          </Grid>
                        </Fragment>
                      );
                    }
                  })
                }
                {(deviationAttb[insuranceId]?.length === 0) && handleAddInput(insuranceId)}
              </Grid>
            </TabPanel>
          );
        })
      }
    </div>
  );
};

export default Sewa;
