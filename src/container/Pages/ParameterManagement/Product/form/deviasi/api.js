/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../../../config/interceptor';

const api = axios.create({
  baseURL: env.masterApi + env.apiPrefixV1
});

api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

// eslint-disable-next-line camelcase
export const getInsurance = () => api.get('/insurance_item?per_page=100');
export const updateApi = (id, payload) => api.put(`/product/${id}`, payload);
