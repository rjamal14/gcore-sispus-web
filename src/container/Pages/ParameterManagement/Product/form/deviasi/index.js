/* eslint-disable camelcase */
/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import {
  DialogActions,
  DialogContent,
  Box,
  Button,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import Ltv from './ltv';
import Sewa from './sewa';
import Admin from './admin';
// import BarangJaminan from './barang_jaminan';
import OneObligor from './one_obligor';
import { updateApi } from './api';
import { deviation } from '../../../../../../validation';

const Deviasi = (props) => {
  const { finishSubmit, rowSelected, id: idProduct, OnNext } = props;
  const [activeToolbar, setToolbar] = useState('LTV');
  const [loading, setLoading] = useState(false);
  const [ltvAttb, setLtv] = useState({});
  const [sewaAttb, setSewa] = useState({});
  const [adminAttb, setAdmin] = useState({});
  const [oneObligAttb, setObligor] = useState({});
  const [error, setError] = useState('');
  const deviasiToolbar = ['LTV', 'Sewa', 'Admin', 'One Obligor'];

  const handleSubmit = () => {
    setLoading(true);
    const formData = [];
    let payload = {};
    let validationResult = '';

    rowSelected.map((item) => {
      const attb = (activeToolbar === 'LTV') ? ltvAttb[item.id]
        : (activeToolbar === 'Sewa') ? sewaAttb[item.id]
          : (activeToolbar === 'Admin') ? adminAttb[item.id]
            : oneObligAttb[item.id];

      if (activeToolbar === 'LTV' || activeToolbar === 'Sewa') {
        attb.map((value) => {
          const { area, branch, head, region, regional_id, id, _destroy } = value;
          formData.push({
            area: parseFloat(area),
            branch: parseFloat(branch),
            head: parseFloat(head),
            insurance_item_id: item.id,
            region: parseFloat(region),
            regional_id: regional_id.id || '',
            _destroy,
            ...id !== undefined && { id }
          });
        });
        validationResult = deviation.validate(formData, { abortEarly: false });
      } else if (activeToolbar === 'Admin' || activeToolbar === 'One Obligor') {
        const { area, branch, head, region, id } = attb;
        formData.push({
          area: parseFloat(area),
          branch: parseFloat(branch),
          head: parseFloat(head),
          insurance_item_id: item.id,
          region: parseFloat(region),
          ...id !== undefined && { id }
        });
        validationResult = deviation.validate(formData, { abortEarly: false });
      }
    });

    payload = (activeToolbar === 'LTV') ? { product_deviation_ltv_attributes: formData }
      : (activeToolbar === 'Sewa') ? { product_deviation_rentals_attributes: formData }
        : (activeToolbar === 'Admin') ? { product_deviation_admins_attributes: formData }
          : { product_deviation_one_obligors_attributes: formData };

    if (!validationResult.error) {
      updateApi(idProduct, { product: payload })
        .then((res) => {
          if (res) {
            const { status, data: { data } } = res;
            if (status === 200) {
              OnNext({
                success: true,
                id: data.product_detail.id.$oid,
              });
            }
          }
          setLoading(false);
          setError('');
        });
    } else {
      setLoading(false);
      setError('Tidak boleh kosong');
      alert('Semua barang jaminan harus terisi!');
    }
  };

  return (
    <div>
      <DialogContent dividers>
        <div style={{ fontSize: '17px' }}>
          <Box display="flex" flexDirection="row" p={1} bgcolor="background.paper">
            {
              deviasiToolbar.map((item) => (
                <Box
                  p={1}
                  borderBottom={activeToolbar === item ? 2 : 0}
                  color={activeToolbar === item ? '#C4A643' : '#95A1A7'}
                  style={{ cursor: 'pointer' }}
                  onClick={() => { setToolbar(item); }}
                >
                  {item}
                </Box>
              ))
            }
          </Box>
        </div>
        {(activeToolbar === 'LTV') && (
          <Ltv
            deviationAttb={ltvAttb}
            setDeviation={setLtv}
            error={error}
            {...props}
          />
        )}
        {(activeToolbar === 'Sewa') && (
          <Sewa
            deviationAttb={sewaAttb}
            setDeviation={setSewa}
            error={error}
            {...props}
          />
        )}
        {(activeToolbar === 'Admin') && (
          <Admin
            deviationAttb={adminAttb}
            setDeviation={setAdmin}
            error={error}
            {...props}
          />
        )}
        {/* {(activeToolbar === 'Barang Jaminan') && (
          <BarangJaminan
            deviationAttb={deviationAttb}
            setDeviation={setDeviation}
            {...props}
          />
        )} */}
        {(activeToolbar === 'One Obligor') && (
          <OneObligor
            deviationAttb={oneObligAttb}
            setDeviation={setObligor}
            error={error}
            {...props}
          />
        )}
      </DialogContent>
      <DialogActions>
        <Button
          style={{
            backgroundColor: '#c2a443',
            color: 'white'
          }}
          disabled={loading}
          onClick={() => handleSubmit()}
          variant="contained"
        >
          <Typography
            variant="button"
            style={{ color: '#FFFFFF' }}
          >
            {loading ? (
              <CircularProgress
                style={{
                  color: 'white',
                  marginLeft: '18px',
                  marginRight: '18px',
                  marginTop: 5
                }}
                size={15}
              />
            ) : ('Simpan')}
          </Typography>
        </Button>
        <Button
          style={{ color: 'black' }}
          disabled={loading}
          onClick={() => finishSubmit()}
          variant="outlined"
        >
          <Typography variant="button">Selesai</Typography>
        </Button>
      </DialogActions>
    </div>
  );
};

export default Deviasi;
