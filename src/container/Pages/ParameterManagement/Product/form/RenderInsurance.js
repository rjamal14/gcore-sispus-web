/* eslint-disable react/jsx-no-comment-textnodes */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable spaced-comment */
/* eslint-disable react/prop-types */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Divider from '@material-ui/core/Divider';
import TabPanel from './TabPanel';
import STLEfield from './pembiayaan/STLEfield';
import Func from '../../../../../functions/index';
import styles from '../css';

class RenderInsurance extends React.Component {
  render() {
    const {
      classes,
      activeButton,
      dataInsurance,
      rowSelected,
      removeValidate,
      setStateInsurance,
      stateValidator,
      stateValue,
      handleChange,
      province
    } = this.props;
    const list = [];
    /************************** Function *********************************/
    dataInsurance.map((data, index) => {
      let flag = true;
      if (rowSelected.find((o) => o.id === data.id) === undefined) {
        flag = false;
      } else if (rowSelected.find((o) => o.id === data.id)._destroy) {
        flag = false;
      }
      list.push(
        <TabPanel value={activeButton} index={data.name}>
          {/* ------------------------------- MAIN FIELD -------------------------------- */}
          <div className={classes.BodytitleMdl}>
            <Checkbox
              style={{
                marginBottom: 5,
                marginRight: -15,
                color: '#C4A643',
              }}
              value="remember"
              onChange={() => {
                const sta = rowSelected;
                const pos = rowSelected.map((e) => e.id).indexOf(data.id);
                if (pos > -1) {
                  if (index > -1) {
                    let i = 0;
                    rowSelected.map((e) => {
                      if (!e._destroy) {
                        i += 1;
                      }
                    });
                    if (sta[pos]._destroy) {
                      sta[pos]._destroy = false;
                    } else if (i <= 1) {
                      alert('Minimum 1');
                    } else {
                      sta[pos]._destroy = true;
                      removeValidate('stle_value' + data.id);
                      removeValidate('min_karatase' + data.id);
                      removeValidate('min_loan' + data.id);
                      removeValidate('ltv_value' + data.id);
                      removeValidate('min_weight' + data.id);
                      removeValidate('max_loan' + data.id);

                      const value = stateValue;
                      value['stle_value' + data.id] = '';
                      value['min_karatase' + data.id] = '';
                      value['min_loan' + data.id] = '';
                      value['ltv_value' + data.id] = '';
                      value['min_weight' + data.id] = '';
                      value['max_loan' + data.id] = '';
                      value['product_finance_items_attributes' + data.id] = [{
                        id: '',
                        ltv_value: '',
                        regional_id: '',
                        stle_value: '',
                        regional_val: ''
                      }];

                      setStateInsurance({
                        value,
                      });
                    }
                  }
                } else {
                  sta.push({ id: data.id, _id: '', name: data.name, _destroy: false });
                }
                setStateInsurance({ rowSelected: sta });
              }}
              checked={flag}
            />
            <text className={classes.titleMdl}> {data.name} </text>
          </div>
          <Grid
            container
            direction="row"
            item
            lg={12}
            xl={12}
            md={12}
            xs={12}
            style={{
              marginBottom: 20,
            }}
          >
            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
              <div>
                <text className={classes.label1}>Min. Karatase</text>
                <text className={classes.starts1}>*</text>
              </div>
              <TextField
                disabled={!flag}
                className={classes.input}
                variant="outlined"
                margin="normal"
                size="small"
                autoComplete="off"
                fullWidth
                onFocus={() => {
                  removeValidate('min_karatase' + data.id);
                }}
                error={stateValidator['min_karatase' + data.id]}
                helperText={stateValidator['min_karatase' + data.id]}
                value={stateValue['min_karatase' + data.id]}
                onChange={(event) => {
                  handleChange(event.target.value, 'min_karatase' + data.id);
                }}
                name={'min_karatase' + data.id}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start"> karat </InputAdornment>
                  ),
                }}
              />
            </Grid>
            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
              <div>
                <text className={classes.label1}>Min. Pinjam</text>
                <text className={classes.starts1}>*</text>
              </div>
              <TextField
                disabled={!flag}
                className={classes.input}
                variant="outlined"
                margin="normal"
                size="small"
                autoComplete="off"
                fullWidth
                onFocus={() => {
                  removeValidate('min_loan' + data.id);
                }}
                error={stateValidator['min_loan' + data.id]}
                helperText={stateValidator['min_loan' + data.id]}
                value={
                  stateValue['min_loan' + data.id] !== undefined
                    ? Func.FormatNumber(stateValue['min_loan' + data.id])
                    : ''
                }
                onChange={(event) => {
                  handleChange(
                    Func.UnFormatRp(event.target.value),
                    'min_loan' + data.id
                  );
                }}
                name={'min_loan' + data.id}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start"> Rp </InputAdornment>
                  ),
                }}
              />
            </Grid>
          </Grid>
          <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
              <div>
                <text className={classes.label1}>Min. Berat</text>
                <text className={classes.starts1}>*</text>
              </div>
              <TextField
                disabled={!flag}
                className={classes.input}
                variant="outlined"
                margin="normal"
                size="small"
                autoComplete="off"
                fullWidth
                onFocus={() => {
                  removeValidate('min_weight' + data.id);
                }}
                error={stateValidator['min_weight' + data.id]}
                helperText={stateValidator['min_weight' + data.id]}
                value={stateValue['min_weight' + data.id]}
                onChange={(event) => {
                  handleChange(event.target.value, 'min_weight' + data.id);
                }}
                name={'min_weight' + data.id}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start"> gram </InputAdornment>
                  ),
                }}
              />
            </Grid>
            <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
              <div>
                <text className={classes.label1}>Max. Pinjam</text>
                <text className={classes.starts1}>*</text>
              </div>
              <TextField
                disabled={!flag}
                className={classes.input}
                variant="outlined"
                margin="normal"
                size="small"
                autoComplete="off"
                fullWidth
                onFocus={() => {
                  removeValidate('max_loan' + data.id);
                }}
                error={stateValidator['max_loan' + data.id]}
                helperText={stateValidator['max_loan' + data.id]}
                value={
                  stateValue['max_loan' + data.id] !== undefined
                    ? Func.FormatNumber(stateValue['max_loan' + data.id])
                    : ''
                }
                onChange={(event) => {
                  handleChange(
                    Func.UnFormatRp(event.target.value),
                    'max_loan' + data.id
                  );
                }}
                name={'max_loan' + data.id}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start"> Rp </InputAdornment>
                  ),
                }}
              />
            </Grid>
          </Grid>
          {/* ------------------------------- MAIN FIELD -------------------------------- */}
          {/* ------------------------------- STLE FIELD -------------------------------- */}
          <Divider className={classes.BodytitleMdl} />
          <div className={classes.BodytitleMdl}>
            <text className={classes.titleMdl}> STLE & LTV </text>
          </div>
          <STLEfield
            flag={flag}
            removeValidate={removeValidate}
            data={data}
            stateValidator={stateValidator}
            stateValue={stateValue}
            setStateInsurance={setStateInsurance}
            province={province}
          />
          {/* ------------------------------- STLE FIELD -------------------------------- */}
        </TabPanel>
      );
    });
    /************************** Function *********************************/
    /************************** Result *********************************/
    return list;
    /************************** Result *********************************/
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'RenderInsurance' })(
  RenderInsurance
);
