/* eslint-disable semi */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/jsx-pascal-case */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-plusplus */
/* eslint-disable radix */
/* eslint-disable react/no-unused-state */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { CircularProgress, Dialog } from '@material-ui/core';

import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import DialogTitle from '@material-ui/core/DialogTitle';
import Func from '../../../../../functions/index';
import Pembiayaan from './pembiayaan';
import Sewa from './sewa';
import Admin from './admin';
import Barang_Jaminan from './barang_jaminan';
import styles from '../css';
import env from '../../../../../config/env';
import service from '../../../../../functions/service';
import Produk from './product';
import Toolbar from './toolbar';
import Deviasi from './deviasi';

function createData(id, name, cont) {
  return { id, name, cont };
}

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      type: 'add',
      valueProduk: [],
      adminEdit: [],
      bjEdit: [],
      rowSelected: [],
      valuePembiayaan: [],
      valueSewa: [],
      valueAdmin: [],
      valueBarang_Jaminan: [],
      dataInsuranceAdmin: [],
      dataInsuranceBJ: [],
      typeActive: '',
      product_finances: [],
      redirect: false,
      province: [],
      prolongation: [],
    };
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  componentDidMount(types) {
    if (this.props.type === 'Ubah') {
      this.setState({ loader: true });
      fetch(process.env.REACT_APP_URL_MASTER + 'product/' + this.props.id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }

        // Section Produk
        const valueProduk = [];

        valueProduk.one_obligor = json.data.product_detail.one_obligor_value;
        valueProduk.name = json.data.product_detail.name;
        valueProduk.status = json.data.product_detail.status;
        valueProduk.tiering_type = json.data.product_detail.tiering_type;
        valueProduk.img = json.data.product_detail.product_image.url;
        valueProduk.period_of_time = json.data.product_detail.product_period.period_of_time;
        valueProduk.due_date = json.data.product_detail.product_period.due_date;
        valueProduk.auction_period = json.data.product_detail.product_period.auction_period;
        valueProduk.grace_period = json.data.product_detail.product_period.grace_period;
        valueProduk.fine_amount = json.data.product_detail.product_period.fine_amount;
        valueProduk.fine_percentage = json.data.product_detail.product_period.fine_percentage;

        // Section Pembiayaan

        const valuePembiayaan = [];
        json.data.product_detail.product_finances.map((data) => {
          valuePembiayaan['id' + data.insurance_item.id.$oid] = data.id;
          valuePembiayaan['min_karatase' + data.insurance_item.id.$oid] = data.min_karatase;
          valuePembiayaan['min_loan' + data.insurance_item.id.$oid] = data.min_loan;
          valuePembiayaan['max_loan' + data.insurance_item.id.$oid] = data.max_loan;
          valuePembiayaan['min_weight' + data.insurance_item.id.$oid] = data.min_weight;
          valuePembiayaan['product_finance_items_attributes' + data.insurance_item.id.$oid] = data.product_finance_items;
        });

        const rowSelected = [];
        json.data.product_detail.product_finances.map((data) => {
          rowSelected.push({
            _id: data.id.$oid, // id product finances
            id: data.insurance_item.id.$oid, // id insurance item
            name: data.insurance_item.name,
            _destroy: false
          });
        });

        // Section Sewa

        const valueSewa = [];
        json.data.product_detail.product_rental_costs.map((data, index) => {
          valueSewa['id' + index] = data.id.$oid;
          valueSewa['insurance_item_id' + index] = { value: data.insurance_item.id.$oid, label: data.insurance_item.name };
          valueSewa['rental_cost_percentage' + index] = data.rental_cost_percentage;
          valueSewa['rental_cost_percentage_15_days' + index] = data.rental_cost_percentage_15_days;
          valueSewa['day_period' + index] = data.day_period;
          valueSewa['product_id' + index] = data.product_id;
          valueSewa['regional_id' + index] = data.regional_id;
          valueSewa['regional_name' + index] = data.regional_name;
        });
        valueSewa.allValue = json.data.product_detail.product_rental_costs;

        // Section Admin

        const valueAdmin = [];
        const loopAdmin = [];

        json.data.product_detail.product_finances.map((data) => {
          let key3 = 0;
          const arr = [];
          json.data.product_detail.product_admin_costs.map((data2) => {
            if (data.insurance_item.id.$oid === data2.insurance_item_id.$oid) {
              arr.push({ id: data.insurance_item.id.$oid, key: key3, _destroy: false });
            }
            key3 += 1;
          });
          if (arr.length === 0) {
            arr.push({ id: data.insurance_item.id.$oid, key: key3, _destroy: false });
          }
          loopAdmin.push(createData(data.insurance_item.id.$oid, data.insurance_item.name, arr));
        });

        const z = json.data.product_detail.product_admin_costs;
        for (let index = 0; index < z.length; index++) {
          const data2 = z[index];
          const k = data2.insurance_item_id.$oid + index;
          valueAdmin['id' + k] = data2.id.$oid;
          valueAdmin['min_loan_range' + k] = data2.min_loan_range;
          valueAdmin['max_loan_range' + k] = data2.max_loan_range;
          valueAdmin['cost_nominal' + k] = data2.cost_nominal;
          valueAdmin['cost_percentage' + k] = data2.cost_percentage;
          valueAdmin['_destroy' + k] = false;
        }
        valueAdmin.length_data = z.length;
        valueAdmin.allValue = z;

        // Section Barang Jaminan

        const valueBJ = [];
        const loopBJ = [];

        json.data.product_detail.product_finances.map((data) => {
          let key3 = 0;
          const arr = [];
          json.data.product_detail.product_insurance_items.map((data2) => {
            if (data.insurance_item.id.$oid === data2.insurance_item.id.$oid) {
              arr.push({ id: data.insurance_item.id.$oid, key: key3, _destroy: false });
            }
            key3 += 1;
          });
          if (arr.length === 0) {
            arr.push({ id: data.insurance_item.id.$oid, key: key3, _destroy: false });
          }
          loopBJ.push(createData(data.insurance_item.id.$oid, data.insurance_item.name, arr));
        });

        const x = json.data.product_detail.product_insurance_items;
        for (let index = 0; index < x.length; index++) {
          const data22 = x[index];
          const v = data22.insurance_item.id.$oid + index;
          valueBJ['id' + v] = data22.id.$oid;
          valueBJ['name' + v] = data22.name;
          valueBJ['minimum_karatase' + v] = data22.minimum_karatase;
          valueBJ['certificate' + v] = data22.certificate;
          valueBJ['_destroy' + v] = false;
        }
        valueBJ.length_data = x.length;
        valueBJ.allValue = x;

        this.setState({
          id_prod: json.data.product_detail.id.$oid,
          valueProduk,
          valuePembiayaan,
          valueAdmin,
          dataInsuranceAdmin: loopAdmin,
          adminEdit: json.data.product_detail.product_admin_costs,
          bjEdit: json.data.product_detail.product_insurance_items,
          dataInsuranceBJ: loopBJ,
          valueBarang_Jaminan: valueBJ,
          valueSewa,
          typeActive: types === undefined ? 'Produk' : types,
          type: 'edit',
          rowSelected,
          product_finances: json.data.product_detail.product_finances,
          loader: false,
          deviationLtv: json.data.product_detail.product_deviation_ltv,
          deviationSewa: json.data.product_detail.product_deviation_rentals,
          deviationAdmin: json.data.product_detail.product_deviation_admins,
          deviationObligor: json.data.product_detail.product_deviation_one_obligors,
          prolongation: json.data.product_detail.product_prolongations,
        });
      }).catch((error) => {
        this.setState({ loader: false });
      })
        .finally(() => {
          this.setState({ loader: false });
        });
    } else {
      this.setState({
        typeActive: 'Produk'
      });
    }

    /* ------------------------- get Regional list for Pembiayaan & Sewa ------------------------------- */
    service.getProvince()
      .then(res => {
        if (res.status === 200) this.setState({ province: res.data.data });
      });
  /* ------------------------- get Regional list for Pembiayaan & Sewa ------------------------------- */
  }

  finishSubmit() {
    if (!this.state.id_prod) {
      alert('Simpan Produk terlebih dahulu');
    } else {
      localStorage.removeItem('delete_product');
      this.props.OnNext(this.state.type === 'add' ? 'Produk berhasil dibuat' : 'Produk berhasil diubah');
    }
  }

  render() {
    const {
      typeActive,
      id_prod,
      type,
      product_finances,
      rowSelected,
      prolongation
    } = this.state;
    const prolongRental = prolongation.filter(val => val.type === 'rental')
    const prolongAdmin = prolongation.filter(val => val.type === 'admin')
    if (typeActive === 'Produk') {
      var Field = (
        <Produk
          type={type}
          id={id_prod}
          value={this.state.valueProduk}
          handleModal={() => {
            this.props.handleModal();
          }}
          updateIdProduct={(res) => this.setState({ id_prod: res.id })}
          finishSubmit={() => this.finishSubmit()}
        />
      );
    } else if (id_prod && typeActive === 'Pembiayaan') {
      var Field = (
        <Pembiayaan
          value={this.state.valuePembiayaan}
          id={id_prod}
          rowSelected={rowSelected}
          province={this.state.province}
          handleModal={() => {
            this.props.handleModal();
          }}
          finishSubmit={() => this.finishSubmit()}
          OnNext={(res) => {
            if (res.success) {
              alert('Berhasil disimpan');
              this.setState({
                typeActive: 'Pembiayaan',
                id_prod: res.id,
                valueProduk: res.value,
                product_finances: res.product_finances,
                rowSelected: res.rowSelected
              });
            }
            if (type !== 'edit') {
              // localStorage.setItem('delete_product', res.id);
            }
          }}
        />
      );
    } else if (id_prod && product_finances.length !== 0 && typeActive === 'Sewa') {
      var Field = (
        <Sewa
          id={id_prod}
          value={this.state.valueSewa}
          product_finances={product_finances}
          rowSelected={rowSelected}
          handleModal={() => {
            this.props.handleModal();
          }}
          finishSubmit={() => this.finishSubmit()}
          OnNext={(res) => {
            // this.componentDidMount('Sewa');
            if (res.success) {
              alert('Berhasil disimpan');
              this.setState({
                typeActive: 'Sewa',
                valueSewa: res.value,
                prolongation: res.prolongation,
              });
            }
          }}
          province={this.state.province}
          prolongation={prolongRental}
        />
      );
    } else if (id_prod && product_finances.length !== 0 && typeActive === 'Admin') {
      var Field = (
        <Admin
          id={id_prod}
          adminEdit={this.state.adminEdit}
          dataInsurance={this.state.dataInsuranceAdmin}
          handleModal={() => {
            this.props.handleModal();
          }}
          value={this.state.valueAdmin}
          product_finances={product_finances}
          rowSelected={rowSelected}
          finishSubmit={() => this.finishSubmit()}
          OnNext={(res) => {
            // this.componentDidMount('Admin');
            if (res.success) {
              alert('Berhasil disimpan');
              this.setState({
                typeActive: 'Admin',
                valueAdmin: res.value,
                dataInsuranceAdmin: res.dataInsurance,
                prolongation: res.prolongation,
              });
            }
          }}
          prolongation={prolongAdmin}
        />
      );
    } else if (id_prod && product_finances.length !== 0 && typeActive === 'Barang Jaminan') {
      var Field = (
        <Barang_Jaminan
          id={id_prod}
          dataInsurance={this.state.dataInsuranceBJ}
          bjEdit={this.state.bjEdit}
          value={this.state.valueBarang_Jaminan}
          handleModal={() => {
            this.props.handleModal();
          }}
          product_finances={product_finances}
          rowSelected={rowSelected}
          finishSubmit={() => this.finishSubmit()}
          OnNext={(res) => {
            if (res.success) {
              alert('Berhasil disimpan');
            }
          }}
        />
      );
    } else if (id_prod && product_finances.length !== 0 && typeActive === 'Deviasi') {
      var Field = (
        <Deviasi
          id={id_prod}
          dataInsurance={this.state.dataInsuranceBJ}
          bjEdit={this.state.bjEdit}
          value={this.state.valueBarang_Jaminan}
          handleModal={() => { this.props.handleModal(); }}
          product_finances={product_finances}
          rowSelected={rowSelected}
          finishSubmit={() => this.finishSubmit()}
          OnNext={(res) => { if (res.success) { alert('Berhasil disimpan'); } }}
          deviationLtv={this.state.deviationLtv}
          deviationSewa={this.state.deviationSewa}
          deviationAdmin={this.state.deviationAdmin}
          deviationObligor={this.state.deviationObligor}
          province={this.state.province}
        />
      );
    }

    const { classes } = this.props;

    if (this.state.loader) {
      return (
        <Dialog
          disablePortal
          disableEnforceFocus
          disableAutoFocus
          open
          scroll="paper"
          maxWidth="xl"
          aria-labelledby="server-modal-title"
          aria-describedby="server-modal-description"
          container={() => {}}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              padding: '20px',
              borderRadius: '50px',
            }}
          >
            <CircularProgress
              style={{ color: '#C4A643', margin: '18px' }}
              size={40}
            />
            Mohon Tunggu
            <div
              style={{
                marginTop: '10px',
              }}
            />
          </div>
        </Dialog>
      );
    }

    return (
      <Dialog
        scroll="paper"
        open
        maxWidth="md"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">
          <Typography variant="h7" className={classes.tittleModal}>
            {this.props.type} Produk
          </Typography>
          <IconButton
            disabled={this.state.loader_button}
            aria-label="close"
            className={classes.closeButton}
            onClick={() => {
              this.props.handleModal();
            }}
          >
            <CloseIcon />
          </IconButton>
          <Toolbar
            typeActive={typeActive}
            setState={(obj) => this.setState(obj)}
            id_prod={id_prod}
            rowSelected={rowSelected}
          />
        </DialogTitle>
        {Field}
      </Dialog>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
