/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
import React from 'react';
import Box from '@material-ui/core/Box';

class Toolbar extends React.Component {
  render() {
    const { typeActive, setState, id_prod, rowSelected } = this.props;
    return (
      <div
        style={{
          fontSize: '17px',
          marginTop: 10,
        }}
      >
        <Box display="flex" flexDirection="row" p={1} bgcolor="background.paper">
          <Box
            p={1}
            borderBottom={typeActive === 'Produk'
              ? 2
              : 0}
            color={typeActive === 'Produk'
              ? '#C4A643'
              : '#95A1A7'}
            style={{ cursor: 'pointer' }}
            onClick={() => setState({ typeActive: 'Produk' })}
          >
            Produk
          </Box>
          <Box
            p={1}
            borderBottom={typeActive === 'Pembiayaan'
              ? 2
              : 0}
            color={typeActive === 'Pembiayaan'
              ? '#C4A643'
              : '#95A1A7'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              if (!id_prod) alert('Harap isi Produk terlebih dahulu');
              else setState({ typeActive: 'Pembiayaan' });
            }}
          >
            Pembiayaan
          </Box>
          <Box
            p={1}
            borderBottom={typeActive === 'Sewa'
              ? 2
              : 0}
            color={typeActive === 'Sewa'
              ? '#C4A643'
              : '#95A1A7'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              if (!id_prod || rowSelected.length === 0) {
                alert('Harap isi Produk & Pembiayaan terlebih dahulu');
              } else setState({ typeActive: 'Sewa' });
            }}
          >
            Sewa
          </Box>
          <Box
            p={1}
            borderBottom={typeActive === 'Admin'
              ? 2
              : 0}
            color={typeActive === 'Admin'
              ? '#C4A643'
              : '#95A1A7'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              if (!id_prod || rowSelected.length === 0) {
                alert('Harap isi Produk & Pembiayaan terlebih dahulu');
              } else setState({ typeActive: 'Admin' });
            }}
          >
            Admin
          </Box>
          <Box
            p={1}
            borderBottom={typeActive === 'Barang Jaminan'
              ? 2
              : 0}
            color={typeActive === 'Barang Jaminan'
              ? '#C4A643'
              : '#95A1A7'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              if (!id_prod || rowSelected.length === 0) {
                alert('Harap isi Produk & Pembiayaan terlebih dahulu');
              } else setState({ typeActive: 'Barang Jaminan' });
            }}
          >
            Barang Jaminan
          </Box>
          <Box
            p={1}
            borderBottom={typeActive === 'Deviasi'
              ? 2
              : 0}
            color={typeActive === 'Deviasi'
              ? '#C4A643'
              : '#95A1A7'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              if (!id_prod || rowSelected.length === 0) {
                alert('Harap isi Produk & Pembiayaan terlebih dahulu');
              } else setState({ typeActive: 'Deviasi' });
            }}
          >
            Deviasi
          </Box>
        </Box>
      </div>
    );
  }
}

export default Toolbar;
