/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable space-infix-ops */
/* eslint-disable react/prop-types */
import React, { useState, Fragment, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  LinearProgress,
  ButtonGroup,
  Button,
  Grid,
  IconButton,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import styles from '../../css';
import TabPanel from '../TabPanel';

function SewaPerpanjangan(props) {
  const {
    classes,
    rowSelected,
    prolongAttb,
    setProlongation,
    prolongation,
    error,
  } = props;

  const [activeButton, setActiveButton] = useState('');

  useEffect(() => {
    const insurance = {};
    rowSelected.map((item) => {
      insurance[item.id] = [];
    });

    if (prolongation?.length > 0) {
      prolongation.map((val) => {
        const insuranceId = val.insurance_item_id.$oid;
        insurance[insuranceId]
        && insurance[insuranceId].push({
          id: val._id.$oid,
          order: val.order,
          value: val.value,
          _destroy: false,
        });
      });
    }

    setProlongation({ ...insurance });
    setActiveButton(rowSelected[0]?.name);
  }, [rowSelected]);

  useEffect(() => {
    if (error) alert('Ada field yang kosong!');
  }, [error]);

  const handleChangeInput = (val, name, index, insuranceId) => {
    const newAttb = prolongAttb[insuranceId];
    newAttb[index][name] = val;
    const newProlong = prolongAttb;
    newProlong[insuranceId] = newAttb;
    setProlongation({ ...newProlong });
  };

  const handleAddInput = (insuranceId) => {
    const newAttb = prolongAttb[insuranceId];
    newAttb.push({
      value: '',
      _destroy: false,
    });
    const newProlong = prolongAttb;
    newProlong[insuranceId] = newAttb;
    setProlongation({ ...newProlong });
  };

  const handleRemoveInput = (index, insuranceId) => {
    const filterProlong = prolongAttb[insuranceId].filter(val => val._destroy === false);
    if (filterProlong.length > 1) {
      const newAttb = prolongAttb[insuranceId];
      newAttb[index]._destroy = true;
      const deviation = prolongAttb;
      deviation[insuranceId] = newAttb;
      setProlongation({ ...deviation });
    } else alert('Minimal 1 !');
  };

  return (
    <Fragment>
      <Grid fullWidth container direction="row" spacing={1} lg={12} xl={12} md={12} xs={12}>
        <Grid item lg={7} xl={7} md={7} sm={7} xs={12}>
          <text className={classes.titleMdl}> Biaya Sewa Perpanjangan </text>
        </Grid>
        <Grid item lg={5} xl={5} md={5} sm={5} xs={12} style={{ textAlign: 'end' }}>
          {(rowSelected.length < 1)
            ? <LinearProgress color="primary" style={{ width: '500px' }} />
            : (
              <ButtonGroup>
                {rowSelected.map((item) => (
                  <Button
                    onClick={() => setActiveButton(item.name)}
                    variant={activeButton === item.name ? 'outlined' : 'contained'}
                    color="primary"
                  >
                    {item.name}
                  </Button>
                ))}
              </ButtonGroup>
            )}
        </Grid>
      </Grid>
      {(rowSelected.length > 0) && rowSelected.map((item) => {
        const insuranceId = item.id;
        return (
          <TabPanel value={activeButton} index={item.name}>
            <Grid fullWidth container direction="row" spacing={1} lg={12} xl={12} md={12} xs={12}>
              <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                <Button
                  onClick={() => handleAddInput(insuranceId)}
                  variant="contained"
                  color="primary"
                  style={{ marginLeft: '2%', marginBottom: '2%' }}
                >
                  Tambah Baru
                </Button>
              </Grid>
              {(prolongAttb[insuranceId]?.length > 0)
              && prolongAttb[insuranceId].map((val, idx) => {
                if (!val._destroy) {
                  return (
                    <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                      <div>
                        <text className={classes.label1}>
                          GU {idx + 1}
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        autoComplete="off"
                        error={error && !val.value}
                        helperText={val.value ? '' : error}
                        value={val.value}
                        style={{ width: '75%' }}
                        onChange={(event) => {
                          handleChangeInput(event.target.value, 'value', idx, insuranceId);
                        }}
                        InputProps={{
                          endAdornment: <InputAdornment position="start"> % </InputAdornment>
                        }}
                      />
                      <IconButton style={{ marginTop: '4%' }} onClick={() => handleRemoveInput(idx, insuranceId)}>
                        <DeleteIcon />
                      </IconButton>
                    </Grid>
                  );
                }
              })}
              {(prolongAttb[insuranceId]?.length === 0) && handleAddInput(insuranceId)}
            </Grid>
          </TabPanel>
        );
      })}
    </Fragment>
  );
}

export default withStyles(styles.CoustomsStyles, { name: 'SewaPerpanjangan' })(SewaPerpanjangan);
