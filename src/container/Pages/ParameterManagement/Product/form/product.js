/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable consistent-return */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/button-has-type */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import FormHelperText from '@material-ui/core/FormHelperText';
import swal from 'sweetalert';
import DialogContentText from '@material-ui/core/DialogContentText';
import { CircularProgress, DialogActions, DialogContent } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    this.setState({ value: this.props.value });
  }

  handleSubmit(type) {
    const validator = [
      {
        name: 'img',
        type: 'required'
      }, {
        name: 'name',
        type: 'required'
      }, {
        name: 'tiering_type',
        type: 'required'
      }, {
        name: 'period_of_time',
        type: 'required'
      }, {
        name: 'due_date',
        type: 'required'
      }, {
        name: 'grace_period',
        type: 'required'
      }, {
        name: 'auction_period',
        type: 'required'
      }, {
        name: 'fine_amount',
        type: 'required'
      }, {
        name: 'fine_percentage',
        type: 'required'
      }, {
        name: 'one_obligor',
        type: 'required'
      }
    ];
    const validate = Func.Validator(this.state.value, validator);
    if (validate.success) {
      this.setState({ loader_button: true });
      fetch(process.env.REACT_APP_URL_MASTER + 'product/' + (type === 'add'
        ? ''
        : this.props.id), {
        method: type === 'add'
          ? 'POST'
          : 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
          product: {
            name: this.state.value.name,
            status: this.state.value.status,
            tiering_type: this.state.value.tiering_type,
            product_image: this.state.ImgBase44,
            one_obligor_value: this.state.value.one_obligor,
            product_period_attributes: {
              period_of_time: this.state.value.period_of_time,
              due_date: this.state.value.due_date,
              grace_period: this.state.value.grace_period,
              auction_period: this.state.value.auction_period,
              fine_amount: this.state.value.fine_amount,
              fine_percentage: this.state.value.fine_percentage
            }
          }

        })
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() === true) {
            this.handleSubmit(type);
          }
        }
        if (type === 'add') {
          if (json.created) {
            const productId = json.data.product_detail.id.$oid;
            localStorage.setItem('delete_product', productId);
            this
              .props
              .updateIdProduct({ id: json.data.product_detail.id.$oid });
            alert('Berhasil disimpan');
          } else {
            this.setState({ validator: json.status });
          }
        } else if (json.code === 200) {
          alert('Berhasil disimpan');
        } else {
          this.setState({ validator: json.status });
        }
        this.setState({ loader_button: false });
      }).catch(() => {
        this.setState({ loader_button: false });
      })
        . finally(() => {
          this.setState({ loader_button: false });
        });
    } else {
      this.setState({ validator: validate.error });
    }
  }

  handleChangeImg(event) {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({ title: 'File Terlalu Besar', text: 'Maximal File 2Mb', icon: 'error', buttons: 'OK' });
      return false;
    }
    this.removeValidate('img');
    const dataSet = this.state.value;
    dataSet.img = URL.createObjectURL(event.target.files[0]);
    this.setState({ value: dataSet });
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({ ImgBase44: reader.result });
    };
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
            <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Nama Produk
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('name');
                  }}
                  error={this.state.validator.name}
                  helperText={this.state.validator.name}
                  value={this.state.value.name}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'name');
                  }}
                  name="name"
                  InputProps={{
                    endAdornment: this.state.validator.name
                      ? (
                        <InputAdornment position="start">
                          <img src={Icon.warning} />
                        </InputAdornment>
                      )
                      : (<div />)
                  }}
                />
                <div
                  style={{
                    marginTop: '15px'
                  }}
                >
                  <text className={classes.label1}>
                    Status Produk
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <Select
                  size="small"
                  className={classes.input2}
                  variant="outlined"
                  margin="normal"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('status');
                  }}
                  error={this.state.validator.status}
                  helperText={this.state.validator.status}
                  value={this.state.value.status === undefined
                    ? true
                    : this.state.value.status}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'status');
                  }}
                  name="status"
                >
                  <MenuItem value="true">
                    <em>Aktif</em>
                  </MenuItem>
                  <MenuItem value="false">
                    <em>Tidak Aktif</em>
                  </MenuItem>
                </Select>
                <div
                  style={{
                    marginTop: '15px'
                  }}
                >
                  <text className={classes.label1}>
                    Tipe Biaya Admin
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <Select
                  size="small"
                  className={classes.input2}
                  variant="outlined"
                  margin="normal"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('tiering_type');
                  }}
                  error={this.state.validator.tiering_type}
                  helperText={this.state.validator.tiering_type}
                  value={this.state.value.tiering_type === undefined
                    ? '-'
                    : this.state.value.tiering_type}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'tiering_type');
                  }}
                  name="tiering_type"
                >
                  <MenuItem value="-">
                    <em>Pilih</em>
                  </MenuItem>
                  <MenuItem value="percentage">
                    <em>Persentase</em>
                  </MenuItem>
                  <MenuItem value="nominal">
                    <em>Nominal</em>
                  </MenuItem>
                </Select>
                <FormHelperText className={classes.error22}>
                  {this.state.validator.tiering_type}
                </FormHelperText>
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <Box
                  borderColor={this.state.validator.img
                    ? 'error.main'
                    : 'grey.500'}
                  border={1}
                  onClick={() => {
                    this.removeValidate('img');
                  }}
                  className={classes.imgScan}
                >
                  {this.state.value.img
                    ? (
                      <img
                        className={classes.imgScan2}
                        onClick={() => {
                          this.removeValidate('img');
                        }}
                        src={this.state.value.img}
                      />
                    )
                    : null}
                </Box>
                <FormHelperText className={classes.error22}>
                  {this.state.validator.img}
                </FormHelperText>
                <FormHelperText
                  style={{
                    marginLeft: '25px'
                  }}
                >
                  Maximum File 2Mb
                </FormHelperText>
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div className={classes.BodytitleMdl22}>
                  <img
                    src={Icon.deleteImg}
                    onClick={() => {
                      const dataSet = this.state.value;
                      dataSet.img = null;
                      this.setState({ value: dataSet, ImgBase44: '' });
                    }}
                  />
                </div>
                <div className={classes.BodytitleMdl23}>

                  <input
                    type="file"
                    accept="image/*"
                    name="file"
                    title="Pilih Gambar"
                    onChange={this.handleChangeImg}
                  />
                </div>
              </Grid>
            </Grid>
            <Divider className={classes.BodytitleMdl} />
            <div className={classes.BodytitleMdl}>
              <text className={classes.titleMdl}>Periode</text>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
              style={{
                marginBottom: 20
              }}
            >
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Jangka Waktu
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('period_of_time');
                  }}
                  error={this.state.validator.period_of_time}
                  helperText={this.state.validator.period_of_time}
                  value={this.state.value.period_of_time}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'period_of_time');
                  }}
                  name="period_of_time"
                  InputProps={{
                    endAdornment: <InputAdornment position="start"> hari </InputAdornment>
                  }}
                />
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Lelang
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('auction_period');
                  }}
                  error={this.state.validator.auction_period}
                  helperText={this.state.validator.auction_period}
                  value={this.state.value.auction_period}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'auction_period');
                  }}
                  name="auction_period"
                  InputProps={{
                    endAdornment: <InputAdornment position="start"> hari </InputAdornment>
                  }}
                />
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Denda Nominal
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('fine_amount');
                  }}
                  error={this.state.validator.fine_amount}
                  helperText={this.state.validator.fine_amount}
                  value={this.state.value.fine_amount !== undefined
                    ? Func.FormatNumber(this.state.value.fine_amount)
                    : ''}
                  onChange={(event) => {
                    this.handleChange(Func.UnFormatRp(event.target.value), 'fine_amount');
                  }}
                  name="fine_amount"
                  InputProps={{
                    startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                  }}
                />
              </Grid>
            </Grid>
            <Grid container direction="row" item lg={12} xl={12} md={12} xs={12} style={{ marginBottom: 20 }}>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Jatuh Tempo
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('due_date');
                  }}
                  error={this.state.validator.due_date}
                  helperText={this.state.validator.due_date}
                  value={this.state.value.due_date}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'due_date');
                  }}
                  name="due_date"
                  InputProps={{
                    endAdornment: <InputAdornment position="start"> hari </InputAdornment>
                  }}
                />
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Grace Period
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('grace_period');
                  }}
                  error={this.state.validator.grace_period}
                  helperText={this.state.validator.grace_period}
                  value={this.state.value.grace_period}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'grace_period');
                  }}
                  name="grace_period"
                  InputProps={{
                    endAdornment: <InputAdornment position="start"> hari </InputAdornment>
                  }}
                />
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Denda %
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('fine_percentage');
                  }}
                  error={this.state.validator.fine_percentage}
                  helperText={this.state.validator.fine_percentage}
                  value={this.state.value.fine_percentage}
                  onChange={(event) => {
                    this.handleChange(event.target.value, 'fine_percentage');
                  }}
                  name="fine_percentage"
                  InputProps={{
                    endAdornment: <InputAdornment position="start"> % </InputAdornment>
                  }}
                />
              </Grid>
            </Grid>
            <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    One Obligor
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
                <TextField
                  className={classes.input}
                  variant="outlined"
                  margin="normal"
                  size="small"
                  autoComplete="off"
                  fullWidth
                  onFocus={() => {
                    this.removeValidate('one_obligor');
                  }}
                  error={this.state.validator.one_obligor}
                  helperText={this.state.validator.one_obligor}
                  value={this.state.value.one_obligor !== undefined
                    ? Func.FormatNumber(this.state.value.one_obligor)
                    : ''}
                  onChange={(event) => {
                    this.handleChange(Func.UnFormatRp(event.target.value), 'one_obligor');
                  }}
                  name="one_obligor"
                  InputProps={{
                    startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                  }}
                />
              </Grid>
            </Grid>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: '#c2a443',
              color: 'white'
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.handleSubmit(this.props.type);
            }}
            variant="contained"
          >
            <Typography
              variant="button"
              style={{
                color: '#FFFFFF'
              }}
            >
              {this.state.loader_button
                ? (
                  <CircularProgress
                    style={{
                      color: 'white',
                      marginLeft: '18px',
                      marginRight: '18px',
                      marginTop: 5
                    }}
                    size={15}
                  />
                )
                : ('Simpan')}
            </Typography>
          </Button>
          <Button
            style={{
              color: 'black'
            }}
            disabled={this.state.loader_button}
            onClick={() => {
              this.props.finishSubmit();
            }}
            variant="outlined"
          >
            <Typography variant="button">Selesai</Typography>
          </Button>
        </DialogActions>
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
