/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-plusplus */
/* eslint-disable radix */
/* eslint-disable react/no-unused-state */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable camelcase */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import swal from 'sweetalert';
import MuiDialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Dialog, DialogActions, DialogContent } from '@material-ui/core';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

function createData(id, name, cont) {
  return { id, name, cont };
}

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: [],
      type: '',
      rowSelected: [],
      cont: 1,
      activeTabs: 0,
      Proses: false,
      ImgBase124: '',
      section: 0,
      id: null,
      redirect: false,
      date: new Date(),
      dataInsurance: [],
      loading: false
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

    onUnload = e => {
      e.preventDefault();
      e.returnValue = '';
    }

    componentWillUnmount() {
      window.removeEventListener('beforeunload', this.onUnload);
    }

    componentDidMount() {
      if (this.props.value.length !== 0) {
        this.setState({ cont: this.state.cont + 1 });
      }
      this.setState({
        value: this.props.value,
        rowSelected: this.props.rowSelected
      });
      window.addEventListener('beforeunload', this.onUnload);
      if (this.state.dataInsurance.length === 0) {
        this.getInsurance();
      } else {
        this.setState({
          value: this.props.value,
          dataInsurance: this.props.dataInsurance
        });
      }
    }

    handleSubmit() {
      this.setState({ loading: true });
      const validator = [];
      this.state.dataInsurance.map((data) => {
        data.cont.map((data2) => {
          validator.push({
            name: 'name' + data2.id + data2.key,
            type: 'required'
          });
          validator.push({
            name: 'minimum_karatase' + data2.id + data2.key,
            type: 'required'
          });
          validator.push({
            name: 'certificate' + data2.id + data2.key,
            type: 'required'
          });
        });
      });
      const validate = Func.Validator(this.state.value, validator);
      if (validate.success) {
        this.setState({ loader_button: true });
        const admin = [];
        this.state.dataInsurance.map((data) => {
          data.cont.map((data2) => {
            admin.push({
              insurance_item_id: data2.id,
              name: this.state.value['name' + data2.id + data2.key],
              minimum_karatase: parseInt(this.state.value['minimum_karatase' + data2.id + data2.key]),
              certificate: this.state.value['certificate' + data2.id + data2.key],
              _destroy: data2._destroy
            });
            if (data2.itemId) admin[data2.key].id = data2.itemId;
          });
        });

        fetch(process.env.REACT_APP_URL_MASTER + 'product/' + this.props.id, {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token')
          },
          body: JSON.stringify({
            product: {
              product_insurance_items_attributes: admin
            }
          })
        }).then((response) => response.json()).then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          }
          if (json.success) {
            const { value } = this.state;
            const loopBJ = [];

            json.data.product_detail.product_finances.map((data) => {
              let key3 = 0;
              const arr = [];
              json.data.product_detail.product_insurance_items.map((data2) => {
                if (data.insurance_item.id.$oid === data2.insurance_item.id.$oid) {
                  arr.push({ itemId: data2.id.$oid, id: data.insurance_item.id.$oid, key: key3, _destroy: false });
                }
                key3 += 1;
              });
              if (arr.length === 0) {
                arr.push({ itemId: '', id: data.insurance_item.id.$oid, key: key3, _destroy: false });
              }
              loopBJ.push({ id: data.insurance_item.id.$oid, name: data.insurance_item.name, cont: arr });
            });

            const x = json.data.product_detail.product_insurance_items;
            for (let index = 0; index < x.length; index++) {
              const data22 = x[index];
              const v = data22.insurance_item.id.$oid + index;
              value['id' + v] = data22.id.$oid;
              value['name' + v] = data22.name;
              value['minimum_karatase' + v] = data22.minimum_karatase;
              value['certificate' + v] = data22.certificate;
              value['_destroy' + v] = false;
            }
            value.length_data = x.length;
            value.allValue = x;

            const datas = [];
            let count = 0;
            const { rowSelected } = this.state;
            rowSelected.map((val) => {
              const arr = [];
              x.map(val2 => {
                if (val2.insurance_item.id.$oid === val.id) {
                  arr.push({ itemId: val2.id.$oid, id: val.id, key: count, _destroy: false });
                  count += 1;
                }
              });
              const emptyArr = arr.filter(vl => vl.id === val.id);
              if (!emptyArr.length) arr.push({ itemId: '', id: val.id, key: count, _destroy: false });
              datas.push({ id: val.id, name: val.name, cont: arr });
            });

            this.setState({
              dataInsurance: datas,
              cont: count,
              value
            });

            this.props.OnNext({
              success: true,
              id: json.data.product_detail.id.$oid,
              value,
              dataInsurance: datas
            });
          } else {
            this.setState({ validator: json.status });
          }
          this.setState({ loader_button: false });
        }).catch((error) => {
          this.setState({ loader_button: false });
        })
          . finally(() => {
            this.setState({ loader_button: false });
          });
      } else {
        this.setState({ validator: validate.error });
      }
    }

    handleChangeImg(event) {
      this.removeValidate('img');
      const dataSet = this.state.value;
      dataSet.img = URL.createObjectURL(event.target.files[0]);
      this.setState({ value: dataSet });

      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.setState({ ImgBase44: reader.result });
      };
    }

    getInsurance() {
      const datas = [];
      let count = 0;
      const dataLength = this.props.value?.length_data;
      this.props.rowSelected.map((data) => {
        const arr = [];
        if (dataLength) {
          this.props.value.allValue.map(data2 => {
            if (data2.insurance_item.id.$oid === data.id) {
              arr.push({ itemId: data2.id.$oid, id: data.id, key: count, _destroy: false });
              count += 1;
            }
          });
          const emptyArr = arr.filter(val => val.id === data.id);
          if (!emptyArr.length) arr.push({ itemId: '', id: data.id, key: count, _destroy: false });
        } else {
          arr.push({ itemId: '', id: data.id, key: count, _destroy: false });
          count += 1;
        }
        datas.push(createData(data.id, data.name, arr));
      });
      this.setState({
        dataInsurance: datas,
        cont: count
      });
    }

    renderInsurance2(index2, data) {
      const { classes } = this.props;
      const list = [];
      data.map((value, index) => {
        if (value._destroy === false) {
          list.push(
            <div>
              <Grid
                container
                direction="row"
                item
                lg={12}
                xl={12}
                md={12}
                xs={12}
              >
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    size="small"
                    autoComplete="off"
                    fullWidth
                    onFocus={() => {
                      this.removeValidate('name' + value.id + value.key);
                    }}
                    error={this.state.validator['name' + value.id + value.key]}
                    helperText={this.state.validator['name' + value.id + value.key]}
                    value={this.state.value['name' + value.id + value.key]}
                    onChange={(event) => {
                      this.handleChange(event.target.value, 'name' + value.id + value.key);
                    }}
                    name={'name' + value.id + value.key}
                  />
                </Grid>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                  <TextField
                    className={classes.input}
                    variant="outlined"
                    margin="normal"
                    type="number"
                    size="small"
                    autoComplete="off"
                    fullWidth
                    onFocus={() => {
                      this.removeValidate('minimum_karatase' + value.id + value.key);
                    }}
                    error={this.state.validator['minimum_karatase' + value.id + value.key]}
                    helperText={this.state.validator['minimum_karatase' + value.id + value.key]}
                    value={this.state.value['minimum_karatase' + value.id + value.key]}
                    onChange={(event) => {
                      this.handleChange(event.target.value, 'minimum_karatase' + value.id + value.key);
                    }}
                    name={'minimum_karatase' + value.id + value.key}
                  />
                </Grid>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                  <div>

                    <TextField
                      className={classes.input}
                      style={{ marginRight: 30 }}
                      variant="outlined"
                      margin="normal"
                      size="small"
                      autoComplete="off"
                      onFocus={() => {
                        this.removeValidate('certificate' + value.id + value.key);
                      }}
                      error={this.state.validator['certificate' + value.id + value.key]}
                      helperText={this.state.validator['certificate' + value.id + value.key]}
                      value={this.state.value['certificate' + value.id + value.key]}
                      onChange={(event) => {
                        this.handleChange(event.target.value, 'certificate' + value.id + value.key);
                      }}
                      name={'certificate' + value.id + value.key}
                    />
                    <img
                      src={Icon.delete}
                      style={{ marginLeft: -35, marginTop: 25, cursor: 'pointer' }}
                      onClick={() => {
                        const data = this.state.dataInsurance;
                        const i = data[index2].cont;
                        if (i.length <= 1) {
                          swal({
                            title: 'Maaf, terjadi kesalahan',
                            text: 'Minimum 1',
                            icon: 'error',
                          });
                        } else {
                          i[index]._destroy = true;
                        }
                        this.setState({ dataInsurance: data });
                      }}
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          );
        }
      });

      return list;
    }

    renderInsurance() {
      const { classes } = this.props;
      const list = [];
      this.state.dataInsurance.map((data, index) => {
        list.push(
          <div>
            <div className={classes.BodytitleMdl}>
              <button
                style={{
                  backgroundColor: '#C4A643',
                  borderRadius: 50,
                  color: 'white',
                  width: 80,
                  height: 35,
                  marginLeft: 10,
                  fontWeight: '500',
                  fontSize: 14
                }}
                onClick={() => {
                  const data = this.state.dataInsurance;
                  const i = data[index].cont;
                  i.push({ itemId: '', id: data[index].id, key: this.state.cont, _destroy: false });
                  this.setState({
                    dataInsurance: data,
                    cont: this.state.cont + 1
                  });
                }}
              >
                Tambah
              </button>
            </div>
            <Grid
              container
              direction="row"
              item
              lg={12}
              xl={12}
              md={12}
              xs={12}
            >
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Perhiasan
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Karatase
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
              </Grid>
              <Grid item lg={4} xl={4} md={4} sm={4} xs={12}>
                <div>
                  <text className={classes.label1}>
                    Sertifikat Logam Mulya
                  </text>
                  <text className={classes.starts1}>*</text>
                </div>
              </Grid>
            </Grid>
            {this.renderInsurance2(index, data.cont)}
          </div>
        );
      });
      return list;
    }

    render() {
      const { classes } = this.props;
      return (
        <div>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {this.renderInsurance()}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              style={{
                backgroundColor: '#c2a443',
                color: 'white'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.handleSubmit(this.props.type);
              }}
              variant="contained"
            >
              <Typography
                variant="button"
                style={{
                  color: '#FFFFFF'
                }}
              >
                {this.state.loader_button
                  ? (
                    <CircularProgress
                      style={{
                        color: 'white',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: 5
                      }}
                      size={15}
                    />
                  )
                  : ('Simpan')}
              </Typography>
            </Button>
            <Button
              style={{
                color: 'black'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.props.finishSubmit();
              }}
              variant="outlined"
            >
              <Typography variant="button">Selesai</Typography>
            </Button>
          </DialogActions>
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
