/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable no-unused-vars */
/* eslint-disable no-empty */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { CircularProgress, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import DialogContentText from '@material-ui/core/DialogContentText';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import swal from 'sweetalert';
import Active from './active';
import History from './history';
import Loyalti from './loyalty';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';
import Nasabah from './nasabah';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
      status: false,
      typeActive: 'Nasabah',
      rekening: [],
      contact: [],
      cif_number: '',
      active_transaction: [],
      transaction_history: [],
      transaction_points: [],
      loader: false,
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    if (this.props.type === 'Ubah') {
      this.setState({ loader: true });
      fetch(process.env.REACT_APP_URL_MANAJEMENT + 'customer/' + this.props.id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        this.setState({ loader: false });
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = {};
        try {
          val.name = json.customer_detail.name;
          val.birth_place = json.customer_detail.birth_place;
          val.birth_date2 = new Date(json.customer_detail.birth_date);
          val.birth_date = json.customer_detail.birth_date;
          val.identity_type = json.customer_detail.identity_type;
          val.identity_number = json.customer_detail.identity_number;
          val.degree = json.customer_detail.degree;
          val.gender = json.customer_detail.gender;
          val.expired_date = json.customer_detail.expired_date;
          val.cif_number = json.customer_detail.cif_number;
          val.status = json.customer_detail.status;
          val.reason = json.customer_detail.reason;

          val.img = json.customer_detail.id_scan_image.url;

          val.residence_province = json.customer_detail.customer_contact_data.residence_province_name;
          val.residence_city = json.customer_detail.customer_contact_data.residence_city_name;
          val.residence_region = json.customer_detail.customer_contact_data.residence_region_name;
          val.residence_postal_code = json.customer_detail.customer_contact_data.residence_postal_code;
          val.residence_address = json.customer_detail.customer_contact_data.residence_address;
          val.identity_province = json.customer_detail.customer_contact_data.identity_province_name;
          val.identity_city = json.customer_detail.customer_contact_data.identity_city_name;
          val.identity_region = json.customer_detail.customer_contact_data.identity_region_name;
          val.identity_postal_code = json.customer_detail.customer_contact_data.identity_postal_code;
          val.identity_address = json.customer_detail.customer_contact_data.identity_address;
          val.phone_number = json.customer_detail.customer_contact_data.phone_number;
          val.telephone_number = json.customer_detail.customer_contact_data.telephone_number;

          val.job_type = json.customer_detail.customer_job.job_type;
          val.income_source = json.customer_detail.customer_job.income_source;
          val.customer_type = json.customer_detail.customer_job.customer_type;
          val.company_name = json.customer_detail.customer_job.company_name;
          val.disbursement_type = json.customer_detail.customer_job.disbursement_type;
          val.financing_purpose = json.customer_detail.customer_job.financing_purpose;
          val.tax_number = json.customer_detail.customer_job.tax_number;
          val.mother_name = json.customer_detail.mother_name;
          val.salary_amount = json.customer_detail.customer_job.salary_amount;

          json
            .customer_detail
            .customer_bank_accounts
            .map((value, index) => {
              this.addRekening();
              val['id_Rek' + index] = value.id;
              val['account_number' + index] = value.account_number;
              val['account_name' + index] = value.account_name;
              val['bank_name' + index] = value.bank_name;
              val['branch_name' + index] = value.branch_name;
              if (value.main_account) {
                this.setState({ rekeningActive: index });
              }
            });

          json
            .customer_detail
            .customer_emergency_contacts
            .map((value, index) => {
              this.addContact();
              val['emergency_id' + index] = value.id;
              val['emergency_name' + index] = value.name;
              val['emergency_relation' + index] = value.relation;
              val['emergency_job_name' + index] = value.job_name;
              val['emergency_nationality' + index] = value.nationality;
              val['emergency_address' + index] = value.address;
            });
        } catch (err) {
        }
        this.setState({
          value: val,
          status: json.customer_detail.status,
          reason: json.customer_detail.reason,
          cif_number: json.customer_detail.cif_number,
          active_transaction: json.customer_detail.active_transaction,
          transaction_history: json.customer_detail.transaction_history,
          transaction_points: json.customer_detail.transaction_points
        });
      }).catch((error) => { this.setState({ loader: false }); })
        . finally(() => {});
    }
    this.getCountry();
  }

  addContact() {
    const data = this.state.contact;
    data.push({
      emergency_id: this.state.contactN0,
      emergency_name: '',
      emergency_relation: '',
      emergency_job_name: '',
      emergency_nationality: '',
      emergency_address: '',
      _destroy: false
    });
    this.setState({
      contact: data,
      contactN0: this.state.contactN0 + 1
    });
  }

  addRekening() {
    const data = this.state.rekening;
    data.push({
      id_Rek: this.state.rekeningN0,
      norek: '',
      rek_name: '',
      bank_name: '',
      bank_branch: '',
      _destroy: false
    });
    this.setState({
      rekening: data,
      rekeningN0: this.state.rekeningN0 + 1
    });
  }

    getCountry = () => {
      this.setState({ isloader: true });
      Func
        .getData('countries', this.state.per_page, this.state.page, this.state.search)
        .then((res) => {
          if (res.json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else {
            const datas = [];
            res
              .json
              .data
              .map((data) => {
                datas.push({ id: data.id.$oid, name: data.name });
              });
            this.setState({ data2: datas });
          }
        });
    };

    handleSubmit(type) {
      this.setState({ loader_button: true });
      fetch(process.env.REACT_APP_URL_MANAJEMENT + 'customer/' + this.props.id, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
          status: !this.state.status,
        })
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          Func.Refresh_Token();
          if (Func.Refresh_Token() === true) {
            this.handleSubmit(type);
          }
        }

        if (json.code === 200) {
          this
            .props
            .OnNext(json.message);
        } else {
          this.setState({ validator: json.status });
        }
        this.setState({ loader_button: false });
      }).catch((error) => {
        this.setState({ loader_button: false });
      })
        . finally(() => {
          this.setState({ loader_button: false });
        });
    }

    handleChangeImg(event) {
      this.removeValidate('img');
      const dataSet = this.state.value;
      dataSet.img = URL.createObjectURL(event.target.files[0]);
      this.setState({ value: dataSet });

      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.setState({ ImgBase124: reader.result });
      };
    }

    render() {
      if (this.state.typeActive === 'Nasabah') {
        var Field = (
          <Nasabah
            value={this.state.value}
            rekening={this.state.rekening}
            rekeningActive={this.state.rekeningActive}
            contact={this.state.contact}
            loader={this.state.loader}
          />
        );
      } else if (this.state.typeActive === 'Aktif') {
        var Field = (
          <Active
            active_transaction={this.state.active_transaction}
            cif_number={this.state.cif_number}
          />
        );
      } else if (this.state.typeActive === 'History') {
        var Field = (
          <History
            transaction_history={this.state.transaction_history}
            cif_number={this.state.cif_number}
          />
        );
      } else if (this.state.typeActive === 'Loyalti') {
        var Field = (
          <Loyalti
            transaction_points={this.state.transaction_points}
            cif_number={this.state.cif_number}
          />
        );
      }

      const { classes } = this.props;
      const ExampleCustomInput = ({ value, onClick }) => (<img src={Icon.icon_date} onClick={onClick} />);

      if (this.state.loader) {
        return (
          <div className={classes.root2}>
            <Dialog
              disablePortal
              disableEnforceFocus
              disableAutoFocus
              open
              scroll="paper"
              maxWidth="md"
              aria-labelledby="server-modal-title"
              aria-describedby="server-modal-description"
              container={() => {}}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  padding: '20px',
                  borderRadius: '50px',
                }}
              >
                <CircularProgress
                  style={{ color: '#C4A643', margin: '18px' }}
                  size={40}
                />
                Mohon Tunggu
                <div
                  style={{
                    marginTop: '10px',
                  }}
                />
              </div>
            </Dialog>
          </div>
        );
      }
      return (
        <Dialog
          scroll="paper"
          open
          maxWidth="md"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title">
            <Typography variant="h7" className={classes.tittleModal}>
              Persetujuan Nasabah
            </Typography>
            <IconButton
              disabled={this.state.loader_button}
              aria-label="close"
              className={classes.closeButton}
              onClick={() => {
                this.props.handleModal();
              }}
            >
              <CloseIcon />
            </IconButton>
            <div style={{ fontSize: '17px' }}>
              <Box display="flex" flexDirection="row" p={1} bgcolor="background.paper">
                <Box
                  style={{ cursor: 'default' }}
                  p={1}
                  onClick={() => {
                    this.setState({ typeActive: 'Nasabah' });
                  }}
                  borderBottom={this.state.typeActive === 'Nasabah'
                    ? 2
                    : 0}
                  color={this.state.typeActive === 'Nasabah'
                    ? '#C4A643'
                    : '#95A1A7'}
                >
                  Nasabah
                </Box>
                <Box
                  style={{ cursor: 'default' }}
                  p={1}
                  onClick={() => {
                    this.setState({ typeActive: 'Aktif' });
                  }}
                  borderBottom={this.state.typeActive === 'Aktif'
                    ? 2
                    : 0}
                  color={this.state.typeActive === 'Aktif'
                    ? '#C4A643'
                    : '#95A1A7'}
                >
                  Pembiayaan Aktif
                </Box>
                <Box
                  style={{ cursor: 'default' }}
                  p={1}
                  onClick={() => {
                    this.setState({ typeActive: 'History' });
                  }}
                  borderBottom={this.state.typeActive === 'History'
                    ? 2
                    : 0}
                  color={this.state.typeActive === 'History'
                    ? '#C4A643'
                    : '#95A1A7'}
                >
                  History Pembiayaan
                </Box>
                <Box
                  style={{ cursor: 'default' }}
                  p={1}
                  onClick={() => {
                    this.setState({ typeActive: 'Loyalti' });
                  }}
                  borderBottom={this.state.typeActive === 'Loyalti'
                    ? 2
                    : 0}
                  color={this.state.typeActive === 'Loyalti'
                    ? '#C4A643'
                    : '#95A1A7'}
                >
                  Loyalti
                </Box>
              </Box>
            </div>
          </DialogTitle>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              {Field}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            {Func.checkPermission('parameters-management#customer#approval')
              ? (
                <Button
                  style={{
                    backgroundColor: '#c2a443',
                    color: 'white',
                  }}
                  disabled={this.state.loader_button}
                  onClick={() => {
                    swal({
                      title: 'Apakah Anda Yakin?',
                      text: 'Akan ' + (this.state.status ? 'Membatalkan Persetujuan' : 'Menyetujui') + ' Nasabah',
                      icon: 'info',
                      buttons: true,
                      dangerMode: true,
                    }).then((willDelete) => {
                      if (willDelete) {
                        this.handleSubmit(this.props.type);
                      }
                    });
                  }}
                  variant="contained"
                >
                  <Typography
                    variant="button"
                    style={{
                      color: '#FFFFFF',
                    }}
                  >
                    {this.state.loader_button ? (
                      <CircularProgress
                        style={{
                          color: 'white',
                          marginLeft: '18px',
                          marginRight: '18px',
                          marginTop: 5,
                        }}
                        size={15}
                      />
                    ) : (
                      this.state.status ? 'Batalkan Persetujuan' : 'Setujui'
                    )}
                  </Typography>
                </Button>
              ) : null }
            <Button
              style={{
                color: 'black',
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.props.handleModal();
              }}
              variant="outlined"
            >
              <Typography variant="button">Batal</Typography>
            </Button>
          </DialogActions>
        </Dialog>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
