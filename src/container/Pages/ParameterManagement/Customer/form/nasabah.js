/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable consistent-return */
/* eslint-disable react/prop-types */
/* eslint-disable array-callback-return */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable no-dupe-keys */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { BeatLoader } from 'react-spinners';
import { Typography } from '@material-ui/core';
import styles from '../css';
import Func from '../../../../../functions';

class Form extends React.Component {
  renderListRekening() {
    if (this.props.rekeningActive !== undefined) {
      const { classes } = this.props;
      const dataLIst = [];
      this
        .props
        .rekening
        .map((value, index) => {
          if (value._destroy === false) {
            dataLIst.push(
              <Grid
                className={index === this.props.rekeningActive
                  ? classes.rekBook
                  : classes.rekBook2}
                item
                lg={4}
                xl={4}
                md={4}
                xs={12}
              >
                <div className={classes.label111}>
                  <text className={classes.titleMdl2}>
                    REKENING
                    {' '}
                    {index + 1}
                  </text>
                  {index === this.props.rekeningActive
                    ? (
                      <text className={classes.makePrio1}>Utama</text>
                    )
                    : null}
                </div>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  md={12}
                  xs={12}
                  style={{
                    marginTop: 10
                  }}
                >
                  <div>
                    <text className={classes.label1121}>Nomor Rekening</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginBottom: 10
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['account_number' + index]}
                    </text>
                  </div>
                </Grid>
                <Grid item lg={12} xl={12} md={12} xs={12}>
                  <div>
                    <text className={classes.label1121}>Nama Pemilik</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginBottom: 10
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['account_name' + index]}
                    </text>
                  </div>
                </Grid>
                <Grid item lg={12} xl={12} md={12} xs={12}>
                  <div>
                    <text className={classes.label1121}>Nama Bank</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginBottom: 10
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['bank_name' + index]}
                    </text>
                  </div>
                </Grid>
                <Grid item lg={12} xl={12} md={12} xs={12}>
                  <div>
                    <text className={classes.label1121}>Cabang</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginBottom: 10
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['branch_name' + index]}
                    </text>
                  </div>
                </Grid>
              </Grid>
            );
          }
        });
      return dataLIst;
    }
  }

  renderListContact() {
    const { classes } = this.props;
    const dataLIst = [];
    this
      .props
      .contact
      .map((value, index) => {
        if (value._destroy === false) {
          dataLIst.push(
            <div>
              <Grid container lg={12} xl={12} md={12} xs={12}>
                <div>
                  <div className={classes.BodytitleMdl2}>
                    <text className={classes.titleMdl} />
                  </div>
                  <div className={classes.BodytitleMdl2}>
                    <text className={classes.titleMdl}>
                      KONTAK
                      {' '}
                      {index + 1}
                    </text>
                  </div>
                </div>
              </Grid>
              <Grid container lg={12} xl={12} md={12} xs={12}>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                  <div>
                    <text className={classes.label1}>Nama Lengkap</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginLeft: '15px'
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['emergency_name' + index]}
                    </text>
                  </div>
                </Grid>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                  <div>
                    <text className={classes.label1}>Pekerjaan</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginLeft: '15px'
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['emergency_job_name' + index]}
                    </text>
                  </div>
                </Grid>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                  <div>
                    <text className={classes.label1}>Alamat</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginLeft: '15px'
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['emergency_address' + index]}
                    </text>
                  </div>
                </Grid>
              </Grid>
              <Grid container lg={12} xl={12} md={12} xs={12}>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                  <div>
                    <text className={classes.label1}>Hubungan</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginLeft: '15px'
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['emergency_relation' + index]}
                    </text>
                  </div>
                </Grid>
                <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                  <div>
                    <text className={classes.label1}>Kewarganegaraan</text>
                  </div>
                  <div
                    style={{
                      marginTop: 10,
                      marginLeft: '15px'
                    }}
                  >
                    <text className={classes.value}>
                      {this.props.value['emergency_nationality' + index]}
                    </text>
                  </div>
                </Grid>
              </Grid>
            </div>
          );
        }
      });
    return dataLIst;
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.scrool}>
        {
          this.props.loading
            ? (
              <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center' }}>
                <Typography>Mohon Tunggu</Typography>
                <BeatLoader />
              </div>
            )
            : (
              <div className={classes.root}>
                <Box
                  display="flex"
                  style={{
                    marginBottom: 20
                  }}
                >
                  <Box flexGrow={1}>
                    <div>
                      <text className={classes.titleMdl}>Data Perorangan</text>
                    </div>
                  </Box>
                  <Box
                    ml={2}
                    style={{
                      borderRadius: 5,
                      padding: 5,
                      backgroundColor: this.props.value.status
                        ? '#46C1E8'
                        : '#dc3545',
                      color: '#FFFFFF',
                      paddingLeft: 10,
                      paddingRight: 10
                    }}
                  >
                    Status -
                    {' '}
                    {this.props.value.status
                      ? 'Aktif'
                      : 'Tidak Aktif'}
                  </Box>
                  <Box
                    ml={2}
                    style={{
                      borderRadius: 5,
                      padding: 5,
                      backgroundColor: '#D8D8D8',
                      paddingLeft: 10,
                      paddingRight: 10
                    }}
                  >
                    No. CIF -
                    {' '}
                    {this.props.value.cif_number}
                  </Box>
                </Box>
                <Grid container lg={12} xl={12} md={12} xs={12}>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>
                            Nama Lengkap Sesuai ID
                          </text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.name}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>
                            Tempat, Tanggal Lahir
                          </text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.birth_place + ', ' + Func.FormatDate(this.props.value.birth_date)}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>ID</text>
                          <text className={classes.label1} />
                          <text className={classes.label1} />
                          <text className={classes.label1} />
                          <text className={classes.label1}>Nomor ID</text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.identity_type !== undefined
                              ? this
                                .props
                                .value.identity_type
                                .toUpperCase()
                              : this.props.value.identity_type}
                          </text>
                          <text
                            className={classes.label1}
                            style={{
                              marginLeft: '47px'
                            }}
                          />
                          <text className={classes.value}>
                            {this.props.value.identity_number}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                    <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid
                        container
                        item
                        lg={4}
                        xl={4}
                        md={4}
                        sm={4}
                        xs={12}
                        className={classes.formPad}
                      >
                        <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                          <div>
                            <text className={classes.label1}>Gelar</text>
                          </div>
                          <div
                            style={{
                              marginTop: 10,
                              marginLeft: '15px'
                            }}
                          >
                            <text className={classes.value}>
                              {this.props.value.degree}
                            </text>
                          </div>
                        </Grid>
                        <Grid item lg={6} xl={6} md={6} sm={6} xs={12} className={classes.formPad}>
                          <div>
                            <text className={classes.label1}>Jenis Kelamin</text>
                          </div>
                          <div
                            style={{
                              marginTop: 10,
                              marginLeft: '15px'
                            }}
                          >
                            <text className={classes.value}>
                              {this.props.value.gender === 'l'
                                ? 'Laki-Laki'
                                : this.props.value.gender === 'p'
                                  ? 'Perempuan'
                                  : '-'}
                            </text>
                          </div>
                        </Grid>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>Tanggal Berlaku ID</text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.expired_date ? Func.FormatDate(this.props.value.expired_date) : 'Seumur Hidup'}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Divider className={classes.divider} />
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>Kontak</text>
                </div>
                <Grid container lg={12} xl={12} md={12} xs={12}>
                  <Grid item lg={6} xl={6} md={6} xs={12}>
                    <div className={classes.label111}>
                      <text className={classes.label1}>Alamat Sesuai ID</text>
                    </div>
                    <Grid item lg={12} xl={12} md={12} xs={12}>
                      <div
                        style={{
                          marginTop: 10,
                          marginLeft: '15px'
                        }}
                      >
                        <text className={classes.value}>
                          {this.props.value.identity_address + ', ' + this.props.value.identity_region + ', ' + this.props.value.identity_city + ', ' + this.props.value.residence_province + ', \n' + this.props.value.identity_postal_code}
                        </text>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid item lg={6} xl={6} md={6} xs={12}>
                    <div className={classes.label111}>
                      <text className={classes.label1}>Alamat Saat Ini</text>
                    </div>
                    <Grid item lg={12} xl={12} md={12} xs={12}>
                      <div
                        style={{
                          marginTop: 10,
                          marginLeft: '15px'
                        }}
                      >
                        <text className={classes.value}>
                          {this.props.value.residence_address + ', ' + this.props.value.residence_region + ', ' + this.props.value.residence_city + ', ' + this.props.value.residence_province + ', \n' + this.props.value.residence_postal_code}
                        </text>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    style={{
                      marginTop: 10
                    }}
                    lg={12}
                    xl={12}
                    md={12}
                    xs={12}
                  >
                    <Grid item lg={6} xl={6} md={6} xs={12}>
                      <div>
                        <text className={classes.label1}>Nomor HP</text>
                      </div>
                      <div
                        style={{
                          marginTop: 10,
                          marginLeft: '15px'
                        }}
                      >
                        <text className={classes.value}>
                          {this.props.value.phone_number}
                        </text>
                      </div>
                    </Grid>
                    <Grid item lg={6} xl={6} md={6} xs={12}>
                      <div>
                        <text className={classes.label1}>Nomor Telepon</text>
                      </div>
                      <div
                        style={{
                          marginTop: 10,
                          marginLeft: '15px'
                        }}
                      >
                        <text className={classes.value}>
                          {this.props.value.telephone_number}
                        </text>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                <Divider className={classes.divider} />
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>
                    Pekerjaan Dan Penghasilan
                  </text>
                </div>
                <Grid container lg={12} xl={12} md={12} xs={12}>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>
                            Jenis Pekerjaan
                          </text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.job_type}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>
                            Sumber Penghasilan
                          </text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.income_source}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>Customer Type</text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.customer_type}
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                    <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>Perusahaan</text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.company_name}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>Pencairan</text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.disbursement_type}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>Tujuan Gadai</text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.financing_purpose}
                          </text>
                        </div>
                      </Grid>
                    </Grid>

                    <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>NPWP</text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.tax_number}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>
                            Nama Gadis Ibu Kandung
                          </text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.mother_name}
                          </text>
                        </div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} xs={12} className={classes.formPad}>
                        <div>
                          <text className={classes.label1}>
                            Penghasilan per Bulan
                          </text>
                        </div>
                        <div
                          style={{
                            marginTop: 10,
                            marginLeft: '15px'
                          }}
                        >
                          <text className={classes.value}>
                            {this.props.value.salary_amount === 1 ? 'Kurang dari Rp 3.000.000' : this.props.value.salary_amount === 2 ? 'Diatas dari Rp 3.000.000' : this.props.value.salary_amount === 3 ? 'Diatas dari Rp 6.000.000' : '-' }
                          </text>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Divider className={classes.divider} />
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>
                    Nomor Rekening Nasabah
                  </text>
                </div>
                <div
                  style={{
                    paddingRight: 15,
                    paddingLeft: 15
                  }}
                >
                  <Grid spacing={2} container lg={12} xl={12} md={12} xs={12}>
                    {this.renderListRekening()}
                  </Grid>
                </div>
                <Divider className={classes.divider} />
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>
                    Kontak Darurat
                  </text>
                </div>
                <div
                  style={{
                    paddingRight: 15,
                    paddingLeft: 15
                  }}
                >
                  {this.renderListContact()}
                </div>
                <Divider className={classes.divider} />
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>Scan ID</text>
                </div>
                <div className={classes.BodytitleMdl}>
                  <Box
                    borderColor="grey.500"
                    border={1}
                    onClick={() => {
                    }}
                    className={classes.imgScan}
                  >
                    {this.props.value.img
                      ? (
                        <img
                          className={classes.imgScan2}
                          onClick={() => {
                            this.removeValidate('img');
                          }}
                          src={this.props.value.img}
                        />
                      )
                      : null}
                  </Box>
                </div>
                <Divider className={classes.divider} />
                <div className={classes.BodytitleMdl2}>
                  <text className={classes.titleMdl}>Catatan</text>
                </div>
                <Grid container lg={12} xl={12} md={12} xs={12}>
                  <div
                    style={{
                      marginLeft: '15px'
                    }}
                  >
                    <text className={classes.value}>Tidak ada catatan</text>
                  </div>
                </Grid>
              </div>
            )
        }
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
