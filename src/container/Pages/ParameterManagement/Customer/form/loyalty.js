/* eslint-disable react/prop-types */
/* eslint-disable array-callback-return */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable no-dupe-keys */
/* eslint-disable react/no-unused-state */
/* eslint-disable camelcase */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import BeatLoader from 'react-spinners/BeatLoader';
import Pagination from '@material-ui/lab/Pagination';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Typography from '@material-ui/core/Typography';
import styles from '../css';

function createData(sge, cif_number, transaction_type, contract_date, due_date, auction_date, estimate_value, loan_amount, status) {
  return {
    sge,
    cif_number,
    transaction_type,
    contract_date,
    due_date,
    auction_date,
    estimate_value,
    loan_amount,
    status,
  };
}

const headCells = [
  {
    id: 'sge',
    numeric: false,
    disablePadding: false,
    label: 'No. CIF'
  }, {
    id: 'cif_number',
    numeric: false,
    disablePadding: false,
    label: 'Aktivitas'
  }, {
    id: 'transaction_type',
    numeric: false,
    disablePadding: false,
    label: 'Tgl. Poin'
  }, {
    id: 'contract_date',
    numeric: false,
    disablePadding: false,
    label: 'Jumlah Poin'
  }, {
    id: 'due_date',
    numeric: false,
    disablePadding: false,
    label: 'Penukaran'
  }, {
    id: 'auction_date',
    numeric: false,
    disablePadding: false,
    label: 'Tgl. Penukaran'
  }, {
    id: 'estimated_value',
    numeric: false,
    disablePadding: false,
    label: 'Item Penukaran'
  }, {
    id: 'loan_amount',
    numeric: false,
    disablePadding: false,
    label: 'Pinjaman'
  }, {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Saldo Poin'
  }
];

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total_data: 0,
      total_page: 1,
      current_page: 1,
      prev_page: null,
      data: [],
      next_page: 2,
      rowsPerPage: 10,
      openSearch: false,
      rowFocus: '',
      page: 1,
      count: 1,
      data: [],
      loading: true,
      modal: false,
      selectAll: false,
      order: 'asc',
      rowSelected: '',
      bulk: false,
      redirect: false
    };
    this.ChangePage = this
      .ChangePage
      .bind(this);
  }

  ChangePage(event, page) {
    this.setState({
      page
    }, () => {
      this.getData();
    });
  }

  componentDidMount() {
    this.getData();
  }

  Short(orderKey) {
    if (orderKey === this.state.order) {
      this.setState({ order: '' });
      var library = this.state.data;
      library.sort((a, b) => (a[orderKey] < b[orderKey]
        ? 1
        : b[orderKey] < a[orderKey]
          ? -1
          : 0));
    } else {
      this.setState({ order: orderKey });
      var library = this.state.data;
      library.sort((a, b) => (a[orderKey] > b[orderKey]
        ? 1
        : b[orderKey] > a[orderKey]
          ? -1
          : 0));
    }
  }

    getData = () => {
      const datas = [];
      this
        .props
        .transaction_points
        .map((data) => {
          if (data.transaction_type === 'disbursement') {
            var type = 'Pembayaran';
          } else if (data.transaction_type === 'repayment') {
            var type = 'Pembayaran Kembali';
          } else if (data.transaction_type === 'auction') {
            var type = 'Lelang';
          } else if (data.transaction_type === 'time_extension') {
            var type = 'Perpanjangan Waktu';
          } else if (data.transaction_type === 'cancelled') {
            var type = 'Dibatalkan';
          }
          datas.push(createData(data.sge, this.props.cif_number, type, data.contract_date, data.due_date, data.auction_date, data.estimate_value, data.loan_amount, data.status,));
        });
      this.setState({
        data: datas,
        loading: false,
        total_data: this.props.transaction_points.length,
        page: 1,
        next_page: 1,
        prev_page: 1,
        current_page: 1,
        total_page: 1
      });
    };

    render() {
      const { classes } = this.props;
      return (
        <div className={classes.scrool}>
          <div className={classes.root}>
            <Box
              display="flex"
              style={{
                marginBottom: 15
              }}
            >
              <Box flexGrow={1}>
                <div>
                  <text className={classes.titleMdl}>Data Loyalti</text>
                </div>
              </Box>
              <Box
                ml={2}
                style={{
                  borderRadius: 5,
                  padding: 5,
                  backgroundColor: '#46C1E8',
                  color: '#FFFFFF',
                  paddingLeft: 10,
                  paddingRight: 10
                }}
              >
                Status - Aktif
              </Box>
            </Box>
            <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
              <Grid item lg={4} xl={4} md={4} sm={12} xs={12} className={classes.formPad}>
                <div>
                  <text className={classes.label1}>
                    Total Poin
                  </text>
                </div>
                <div
                  style={{
                    marginTop: 10,
                    marginLeft: '15px'
                  }}
                >
                  <text className={classes.value}>
                    0
                  </text>
                </div>
              </Grid>
            </Grid>
            <Divider className={classes.divider} />

            <TableContainer component={Paper}>
              <Table
                size="small"
                onMouseOut={() => {}}
                className={classes.table}
                aria-label="simple table"
              >
                <TableHead className={classes.headTable}>
                  <TableRow>
                    {headCells.map((headCell) => (
                      <TableCell
                        key={headCell.id}
                        align={headCell.numeric
                          ? 'right'
                          : 'left'}
                        sortDirection={this.state.order === headCell.id
                          ? this.state.order
                          : false}
                      >
                        <TableSortLabel
                          active={this.state.order === headCell.id}
                          direction={this.state.order === headCell.id
                            ? this.state.order
                            : 'asc'}
                          onClick={() => {
                            this.Short(headCell.id);
                          }}
                        >
                          {headCell.label}
                          {this.state.order === headCell.id
                            ? (
                              <span className={classes.visuallyHidden} />
                            )
                            : null}
                        </TableSortLabel>
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                {this.state.data.length === 0 && !this.state.loading
                  ? (
                    <TableBody>
                      <TableRow>
                        <TableCell colSpan={9} align="center">
                          <Typography>
                            Tidak Ada Data
                          </Typography>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  )
                  : null}
                <TableBody>
                  {!this.state.loading
                    ? this
                      .state
                      .data
                      .map((row) => (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.id}
                        >
                          <TableCell align="left">{row.sge}</TableCell>
                          <TableCell align="left">{row.cif_number}</TableCell>
                          <TableCell align="left">{row.transaction_type}</TableCell>
                          <TableCell align="left">{row.contract_date}</TableCell>
                          <TableCell align="left">{row.due_date}</TableCell>
                          <TableCell align="left">{row.auction_date}</TableCell>
                          <TableCell align="left">{row.estimate_value}</TableCell>
                          <TableCell align="left">{row.loan_amount}</TableCell>
                          <TableCell align="left">{row.status}</TableCell>
                        </TableRow>
                      ))
                    : null}
                </TableBody>
              </Table>
              {this.state.loading
                ? (
                  <div className={classes.loader}>
                    <BeatLoader size={15} color="#3F3F3F" loading />
                  </div>
                )
                : null}
            </TableContainer>
            <TableContainer component={Paper}>
              <Pagination
                className={classes.row}
                count={this.state.total_page}
                defaultPage={this.state.page}
                onChange={this.ChangePage}
                siblingCount={0}
              />
            </TableContainer>

            <Divider className={classes.divider} />
            <div className={classes.BodytitleMdl2}>
              <text className={classes.titleMdl}>Catatan</text>
            </div>
            <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
              <div
                style={{
                  marginLeft: '15px'
                }}
              >
                <text className={classes.value}>
                  Tidak ada catatan
                </text>
              </div>
            </Grid>
          </div>
        </div>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
