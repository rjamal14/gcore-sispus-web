/* eslint-disable react/prop-types */
/* eslint-disable react/button-has-type */
/* eslint-disable react/no-this-in-sfc */
/* eslint-disable eqeqeq */
import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';

function NonActiveDialog({ handleNonActiveStatus, status, reason }) {
  const [dialogBox, setDialogBox] = useState(false);
  const [NonActiveDesc, setNonActiveDesc] = useState('');
  const [err, setErr] = useState(false);

  const checkStatus = () => {
    if (status) setDialogBox(true);
    else handleNonActiveStatus(true, reason);
  };

  const handleClick = () => {
    if (NonActiveDesc) {
      handleNonActiveStatus(false, NonActiveDesc);
      setNonActiveDesc('');
      setDialogBox(false);
    } else setErr(true);
  };

  return (
    <>
      <button
        style={{
          backgroundColor: status ? '#dc3545' : '#28a745',
          borderRadius: 50,
          color: 'white',
          width: status ? 150 : 87,
          height: 35,
          fontWeight: '500',
          fontSize: 14
        }}
        onClick={() => checkStatus(status)}
      >
        {status ? 'Batalkan Persetujuan' : 'Setujui' }
      </button>
      <Dialog open={dialogBox}>
        <DialogTitle>Non Aktifkan Nasabah</DialogTitle>
        <DialogContent>
          <TextField
            variant="outlined"
            helperText={err ? '*Harus diisi' : ''}
            multiline
            rows={5}
            cols
            style={{ width: 500 }}
            placeholder="Masukkan alasan menon-aktifkan nasabah"
            required
            error={err}
            onChange={(evt) => setNonActiveDesc(evt.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleClick}>
            Non Aktifkan
          </Button>
          <Button color="primary" onClick={() => setDialogBox(false)}>
            Tutup
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default NonActiveDialog;
