/* eslint-disable react/prop-types */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-deprecated */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Func from '../../../../../functions/index';
import styles from '../css';
import Field from './field';
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      validator: [],
      value: [],
      type: '',
      activeTabs: 0,
      Proses: 0,
      count: 0,
      id: null,
    };
    this.handleChangeImg = this.handleChangeImg.bind(this);
  }

  onPage(type) {
    let val = '';
    if (type === '-') {
      val = this.state.activeTabs - 1;
    } else {
      val = this.state.activeTabs + 1;
    }
    this.setState({
      activeTabs: val,
      Proses: val
    });
  }

  componentWillReceiveProps() {
    if (this.props.row !== 'undefined' && this.props.modal) {
      this.setState({ modal: this.props.modal, type: this.props.type });
    }
    if (this.props.type === 'Ubah' && this.props.row !== undefined) {
      this.setState({ id: this.props.row.id });
    }
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleModal() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event.target.value;
    this.setState({ value: dataSet });
  }

  handleSubmit() {
    if (this.state.activeTabs === 6) {
      this.setState({
        modal: !this.state.modal,
        activeTabs: 0,
      });
    } else {
      this.setState({
        Proses: this.state.activeTabs,
      });
    }
  }

  handleChangeImg(event) {
    this.setState({
      imgPath: URL.createObjectURL(event.target.files[0]),
    });
  }

  render() {
    if (this.state.modal) {
      return (
        <Field
          Proses={this.state.Proses}
          value={this.state.value}
          type={this.state.type}
          id={this.state.id}
          count={this.state.count}
          handleModal={() => {
            this.handleModal();
          }}
          OnNext={(res) => {
            this.setState({ modal: false });
            Func.AlertForm('Berhasil', res, 'success');
          }}
        />
      );
    }
    return <div />;
  }
}

export default withStyles(styles.CoustomsStyles, {
  name: 'Form',
})(Form);
