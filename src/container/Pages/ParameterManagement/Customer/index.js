/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/sort-comp */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import styles from './css';
import Tables from './table';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true
    };
  }

  handleModal() {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: 8 }}>
        <Hidden only={['lg', 'xl']}>
          <Tables
            width={60}
            open={this.state.open}
            filter={false}
            title="Manajemen Nasabah"
            subtitle="Atur data nasabah."
            path="customer"
          />
        </Hidden>
        <Hidden only={['sm', 'md', 'xs']}>
          <Tables
            width={285}
            open={this.state.open}
            filter={false}
            title="Manajemen Nasabah"
            subtitle="Atur data nasabah."
            path="customer"
          />
        </Hidden>

      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
