/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/prop-types */
import React from 'react'

const customUnreadColor = (val, rowItem) => {
  if (!rowItem.is_read) {
    return (
      <div style={{
        backgroundColor: '#b53f2f',
        width: 'auto',
        color: 'white',
        height: '40px',
        borderRadius: 5,
        padding: '10px 10px',
      }}
      >
        {val}
      </div>
    )
  } 
  return (
    <div style={{ padding: '10px 10px' }}>
      {val}
    </div>
  )
}

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'body',
    label: '',
    customBodyRender: (evt, val, rowItem) => customUnreadColor(val, rowItem),
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'pointerCursor'
  },
  
];

export default columns;
