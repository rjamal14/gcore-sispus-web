/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Crud from '../../../components/Crud';
import { getApiList } from './api';
import columns from './columns';
import { changePageTitle } from '../../../redux/actions/changePageTitle';
import { changeNotifStatus } from '../../../redux/actions/notification';

const Notification = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(changePageTitle('Notifikasi'));
  }, []);

  const handleRowClick = (event, row) => {
    history.replace(`${row.url}`);
    dispatch(changeNotifStatus(row.id));
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  const customCollection = (data) => {
    return data.filter(val => val.type === 'sispus');
  };

  return (
    <Crud
      columns={columns}
      getApi={getApiList}
      disableAdd
      disableFilter
      disableExport
      disableSearch
      onRowClick={handleRowClick}
      parseId={parseId}
      customCollection={customCollection}
    />
  );
};

export default Notification;
