/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, Fragment } from 'react';
import { Dropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import swal from 'sweetalert';
import { approval } from './api';
import PublishStatement from './publishStatement';

const ApproveAction = ({ row }) => {
  const [option, setOption] = useState([
    'Terima',
    'Tolak',
  ]);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const { branch_approval, head_approval, region_approval } = row;
    if (branch_approval === 'approved' && head_approval === 'approved' && region_approval === 'approved') {
      setOption(['Publish Statement']);
    }
  }, []);

  const handleApprove = (status) => {
    swal({
      title: 'Apa Anda yakin?',
      text: `${status === 'approve' ? 'Menerima' : 'Menolak'} pengajuan`,
      icon: 'info',
      buttons: ['Batal', 'Yakin'],
    })
      .then((willDelete) => {
        if (willDelete) {
          approval({ type: status, id: row.id })
            .then((res) => {
              const response = res?.data || res?.response?.data;
              if (res?.status === 200) {
                swal('Sukses!', response.message, 'success')
                  .then(() => { window.location.reload(); });
              }
            });
        }
      });
  };

  return (
    <Fragment>
      <Dropdown>
        <Dropdown.Toggle variant="info" id="dropdown-basic">
          PILIH
        </Dropdown.Toggle>

        <Dropdown.Menu>
          {option.map((opt) => (
            <Dropdown.Item
              eventKey={opt}
              key={opt}
              onClick={() => {
                if (opt === 'Terima') handleApprove('approve');
                if (opt === 'Tolak') handleApprove('reject');
                if (opt === 'Publish Statement') setOpen(true);
              }}
            >
              {opt}
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
      <PublishStatement open={open} setOpen={setOpen} id={row.id} amountval={row.amount} />
    </Fragment>

  );
};

export default ApproveAction;
