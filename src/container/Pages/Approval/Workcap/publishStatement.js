/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  CircularProgress,
  InputAdornment,
  Table,
  TableBody,
  TableRow,
  withStyles,
} from '@material-ui/core';
import MuiTableCell from '@material-ui/core/TableCell';
import swal from 'sweetalert';
import useStyle from './css';
import { publishStatement } from './api';

const TableCell = withStyles({
  root: {
    borderBottom: 'none',
  },
})(MuiTableCell);

const PublishStatement = ({ open, setOpen, id, amountval }) => {
  const style = useStyle();
  const [amount, setAmount] = useState(amountval);
  const [error, setError] = useState('');
  const [selectedFile, setSelected] = useState(null);
  const [attachment, setAttachment] = useState('');
  const [loading, setLoading] = useState(false);

  const handleSubmit = () => {
    if (amount && attachment) {
      setLoading(true);
      const payload = {
        bank_statement: {
          amount,
          attachment
        }
      };
      swal({
        title: 'Apa Anda yakin?',
        text: 'Melakukan publish statement',
        icon: 'info',
        buttons: ['Batal', 'Yakin'],
      })
        .then((willDelete) => {
          if (willDelete) {
            publishStatement(id, payload)
              .then((res) => {
                const response = res?.data || res?.response?.data;
                if (res?.status === 200) {
                  swal('Sukses!', response.message, 'success')
                    .then(() => { window.location.reload(); });
                }
              });
          } else setLoading(false);
        });
    } else setError('Tidak boleh kosong');
  };

  const buttonProps = [
    ['contained', 'primary', style.whiteTextButton, 'Simpan', () => handleSubmit()],
    ['outlined', '', style.blackTextButton, 'Batal', () => {
      setOpen(false);
      setSelected(null);
    }]
  ];

  const onChange = (evt) => {
    const { value } = evt.target;
    setAmount(value);
  };

  const selectFile = (evt) => {
    setSelected(evt.target.files);
    const file = evt.target.files[0];
    if (file.size > 2.9e6) {
      swal({ title: 'File Terlalu Besar', text: 'Maximal File 2Mb', icon: 'error', buttons: 'OK' });
      return false;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => setAttachment(reader.result);
  };

  return (
    <Dialog scroll="paper" open={open}>
      <DialogTitle>Publish Statement</DialogTitle>
      <DialogContent dividers>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>Amount</TableCell>
              <TableCell>
                <TextField
                  value={amount}
                  defaultValue={amountval}
                  fullWidth
                  size="small"
                  variant="outlined"
                  type="number"
                  onChange={onChange}
                  error={(error && !amount)}
                  helperText={amount ? '' : error}
                  onKeyDown={() => setError('')}
                  InputProps={{
                    startAdornment: <InputAdornment position="start"> Rp </InputAdornment>
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Lampiran</TableCell>
              <TableCell>
                <Button
                  variant="contained"
                  component="label"
                >
                  Pilih File
                  <input
                    type="file"
                    hidden
                    onChange={selectFile}
                  />
                </Button>
                {selectedFile && selectedFile.length > 0 ? selectedFile[0].name : null}
                {!attachment && error}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </DialogContent>
      <DialogActions>
        {buttonProps.map(([variant, color, classStyle, title, onclick]) => (
          <Button
            variant={variant}
            color={color}
            className={classStyle}
            onClick={onclick}
          >
            {(title === 'Simpan' && loading)
              ? <CircularProgress size={22} />
              : title}
          </Button>
        ))}
      </DialogActions>
    </Dialog>
  );
};

export default PublishStatement;
