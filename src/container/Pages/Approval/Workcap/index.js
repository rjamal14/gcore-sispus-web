/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-trailing-spaces */
import React from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { getApi } from './api';
// import { changePageTitle } from '../../../../redux/actions/changePageTitle';

const parseId = (object) => {
  const { id } = object;

  return id;
};

const Cashflow = () => (
  <Crud
    columns={columns}
    description="Draft Pengajuan Modal Kerja"
    getApi={getApi}
    disableAdd
    disableFilter
    disableExport
    disableDelete
    disableEdit
    parseId={parseId}
  />
);

export default Cashflow;
