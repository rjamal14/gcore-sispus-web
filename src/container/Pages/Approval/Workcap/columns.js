/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable react/prop-types */
import React from 'react';
import Func from '../../../../functions';
import ApproveAction from './approveAction';

const setColor = (val) => {
  let color = '#000000'
  let backgroundColor = '#F1F1F1'
  let status = 'Draft'

  if (val === 'waiting') {
    color = '#FFFFFF'
    backgroundColor = '#2196F3'
    status = 'Menunggu Persetujuan'
  } else if (val === 'in_progress') {
    color = '#FFFFFF'
    backgroundColor = '#FF9800'
    status = 'Dalam Proses'
  } else if (val === 'approved') {
    color = '#FFFFFF'
    backgroundColor = '#4CAF50'
    status = 'Diterima'
  } else if (val === 'rejected') {
    color = '#FFFFFF'
    backgroundColor = '#F44336'
    status = 'Ditolak'
  }
  return (
    <div style={{
      backgroundColor,
      width: 'auto',
      color,
      borderRadius: 5,
      padding: 10,
      textAlign: 'center',
      fontSize: 10,
    }}
    >
      {status.toUpperCase()}
    </div>
  )
}

const columns = [
  {
    name: 'id',
    label: 'ID',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'requested_office_name',
    label: 'Nama Kantor',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'amount',
    label: 'Jumlah',
    customBodyRender: (evt, val) => Func.currencyFormatter(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'branch_approval',
    label: 'Acc Cabang',
    customBodyRender: (evt, val) => setColor(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'region_approval',
    label: 'Acc Wilayah',
    customBodyRender: (evt, val) => setColor(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'head_approval',
    label: 'Acc Pusat',
    customBodyRender: (evt, val) => setColor(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: '',
    label: 'Aksi',
    customBodyRender: (evt, val, rowItem) => <ApproveAction row={rowItem} />,
    options: {
      filter: true,
      sort: false
    }
  },
];

export default columns;
