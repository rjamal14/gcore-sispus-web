import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  blackTextButton: {
    color: '#000000'
  },
  whiteTextButton: {
    color: '#FFFFFF'
  },
}));

export default useStyles;
