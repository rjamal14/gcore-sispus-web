/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-restricted-globals */
/* eslint-disable radix */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable padded-blocks */
/* eslint-disable no-param-reassign */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-use-before-define */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect, Fragment } from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Button,
  Divider,
  IconButton,
  Typography,
  DialogContentText,
  makeStyles,
  CircularProgress,
  Box,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Func from '../../../../functions';
import '../../../../styles/date_field.css';
import styles from './cssModal';
import env from '../../../../config/env';

const useStyles = makeStyles((theme) => styles.CoustomsStyles);

function DeviationDetail(props) {
  const { open, setOpen, data } = props;
  const classes = useStyles();
  const [redirect, setRedirect] = useState(false);
  const [value, setValue] = useState({});
  const [loader, setLoader] = useState(false);
  const [cashItem, setCashItem] = useState([]);

  useEffect(() => {
    setValue(data);
    setCashItem(data?.cash_report_items);
  }, [data]);

  const setColor = (val) => {
    let status = '-';
  
    if (val === 'waiting') status = 'Menunggu Persetujuan';
    if (val === 'approved') status = 'Diterima';
    if (val === 'rejected') status = 'Ditolak';

    return status;
  };

  const setDeviasi = (val) => {
    let type = '-';

    if (val === 'one_obligor') type = 'One Obligor';
    else if (val === 'ltv') type = 'LTV';
    else if (val === 'admin') type = 'Admin';
    else type = 'Rental';

    return type;
  };

  const renderBJ = () => {
    const bjData = value?.transaction_insurance_items || [];
    return (
      <Fragment>
        {
          (bjData.length > 0) && bjData.map((val, idx) => (
            <div>
              <div
                className={classes.BodytitleMdl2}
                style={{ marginTop: 50 }}
              >
                <text className={classes.titleMdl}>
                  BARANG JAMINAN {idx + 1}
                </text>
              </div>
              <Grid
                container
                direction="row"
                item
                lg={12}
                xl={12}
                md={12}
                xs={12}
              >
                <Grid container item lg={8} xl={8} md={8} sm={8} xs={12}>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>
                          Kepemilikan Barang Jaminan
                        </text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {val.ownership}
                          </text>
                        </div>
                      </div>
                    </Grid>
                    <Grid
                      container
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <Grid
                        item
                        lg={6}
                        xl={6}
                        md={6}
                        sm={6}
                        xs={12}
                        className={classes.formPad}
                      >
                        <div>
                          <text className={classes.label121}>Jumlah</text>
                        </div>
                        <div>
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {val.amount}
                            </text>
                          </div>
                        </div>
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        xl={6}
                        md={6}
                        sm={6}
                        xs={12}
                        className={classes.formPad}
                      >
                        <div>
                          <text className={classes.label121}>Karat</text>
                        </div>
                        <div>
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {val.carats}
                            </text>
                          </div>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid container item lg={12} xl={12} md={12} sm={12} xs={12}>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <div>
                        <text className={classes.label121}>
                          Kategori Barang Jaminan
                        </text>
                      </div>
                      <div>
                        <div className={classes.label1112}>
                          <text className={classes.label1}>
                            {val.product_insurance_item_name}
                          </text>
                        </div>
                      </div>
                    </Grid>
                    <Grid
                      container
                      lg={6}
                      xl={6}
                      md={6}
                      sm={6}
                      xs={12}
                      className={classes.formPad}
                    >
                      <Grid
                        item
                        lg={6}
                        xl={6}
                        md={6}
                        sm={6}
                        xs={12}
                        className={classes.formPad}
                      >
                        <div>
                          <text className={classes.label121}>Berat Kotor</text>
                        </div>
                        <div>
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {val.gross_weight}
                            </text>
                          </div>
                        </div>
                      </Grid>
                      <Grid
                        item
                        lg={6}
                        xl={6}
                        md={6}
                        sm={6}
                        xs={12}
                        className={classes.formPad}
                      >
                        <div>
                          <text className={classes.label121}>Berat Bersih</text>
                        </div>
                        <div>
                          <div className={classes.label1112}>
                            <text className={classes.label1}>
                              {val.net_weight}
                            </text>
                          </div>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  container
                  item
                  lg={4}
                  xl={4}
                  md={4}
                  sm={4}
                  xs={12}
                  spacing={3}
                >
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    md={12}
                    sm={12}
                    xs={12}
                    className={classes.formPad}
                  >
                    <div>
                      <text className={classes.label121}>Deskripsi</text>
                    </div>
                    <div>
                      <div className={classes.label1112}>
                        <text className={classes.label1}>
                          {val.description}
                        </text>
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          ))
        }
      </Fragment>
    );
  };

  const renderDeviation = () => {
    const deviationData = value?.transaction_deviations || [];
    return (
      <Fragment>
        {
          (deviationData.length > 0) && deviationData.map((val, idx) => (
            <Fragment>
              <Grid
                container
                item
                lg={2}
                xl={2}
                md={2}
                sm={2}
                xs={12}
              >
                <div className={classes.label111}>
                  <text className={classes.label1}>
                    {idx + 1}
                  </text>
                </div>
              </Grid>
              <Grid
                container
                item
                lg={2}
                xl={2}
                md={2}
                sm={2}
                xs={12}
              >
                <div className={classes.label111}>
                  <text className={classes.label1}>
                    {Func.FormatDate(val?.created_at)}
                  </text>
                </div>
              </Grid>
              <Grid 
                container
                item
                lg={2}
                xl={2}
                md={2}
                sm={2}
                xs={12}
              >
                <div className={classes.label111}>
                  <text className={classes.label1}>
                    {setDeviasi(val?.deviation_type)}
                  </text>
                </div>
              </Grid>
              <Grid
                container
                item
                lg={2}
                xl={2}
                md={2}
                sm={2}
                xs={12}
              >
                <div className={classes.label111}>
                  <text className={classes.label1}>
                    {Func.currencyFormatter(val?.value || 0)}
                  </text>
                </div>
              </Grid>
              <Grid
                container
                item
                lg={2}
                xl={2}
                md={2}
                sm={2}
                xs={12}
              >
                <div className={classes.label111}>
                  <text className={classes.label1}>
                    {setColor(val?.status)}
                  </text>
                </div>
              </Grid>
              <Grid
                container
                item
                lg={2}
                xl={2}
                md={2}
                sm={2}
                xs={12}
              >
                <div className={classes.label111}>
                  <text className={classes.label1}>
                    {val?.rejected_reason || '-'}
                  </text>
                </div>
              </Grid>
            </Fragment>
          ))
        }
      </Fragment>
    );
    
  };

  return (
    <Fragment>
      {
        (!value && open) && (
          <div className={classes.root2}>
            <Dialog
              disablePortal
              disableEnforceFocus
              disableAutoFocus
              open
              scroll="paper"
              maxWidth="md"
              aria-labelledby="server-modal-title"
              aria-describedby="server-modal-description"
              container={() => {}}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  padding: '20px',
                  borderRadius: '50px',
                }}
              >
                <CircularProgress
                  style={{ color: '#85203B', margin: '18px' }}
                  size={40}
                />
                Mohon Tunggu
                <div
                  style={{
                    marginTop: '10px',
                  }}
                />
              </div>
            </Dialog>
          </div>
        )
      }
      {
        (value) && (
          <Dialog
            scroll="paper"
            open={open}
            maxWidth="md"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
          >
            <DialogTitle id="scroll-dialog-title">
              <Typography variant="h7" className={classes.tittleModal}>
                Detail Deviasi
              </Typography>
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={() => setOpen(false)}
              >
                <CloseIcon />
              </IconButton>
            </DialogTitle>
            <DialogContent dividers>
              <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
                <div className={classes.scrool}>
                  <div>
                    {Func.toLogin(redirect)}
                    <div className={classes.root}>
                      <div className={classes.BodytitleMdl2}>
                        <text className={classes.titleMdl}>Data Nasabah</text>
                      </div>
                      <Grid
                        container
                        direction="row"
                        item
                        lg={12}
                        xl={12}
                        md={12}
                        xs={12}
                      >
                        <Grid item lg={12} xl={12} md={12} sm={12} xs={12}>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={8}
                              xl={8}
                              md={8}
                              sm={8}
                              xs={12}
                            >
                              <Grid
                                container
                                item
                                lg={12}
                                xl={12}
                                md={12}
                                sm={12}
                                xs={12}
                              >
                                <Grid
                                  item
                                  lg={6}
                                  xl={6}
                                  md={6}
                                  sm={6}
                                  xs={12}
                                  className={classes.formPad}
                                >
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>
                                      Nama Lengkap
                                    </text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>
                                      {value?.customer?.name}
                                    </text>
                                  </div>
                                </Grid>
                                <Grid
                                  item
                                  lg={6}
                                  xl={6}
                                  md={6}
                                  sm={6}
                                  xs={12}
                                  className={classes.formPad}
                                >
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>
                                      Nomor HP
                                    </text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>
                                      {value?.customer?.phone_number}
                                    </text>
                                  </div>
                                </Grid>
                              </Grid>
                              <Grid
                                container
                                item
                                lg={12}
                                xl={12}
                                md={12}
                                sm={12}
                                xs={12}
                              >
                                <Grid
                                  item
                                  lg={6}
                                  xl={6}
                                  md={6}
                                  sm={6}
                                  xs={12}
                                  className={classes.formPad}
                                >
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>
                                      Jenis Kelamin
                                    </text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>
                                      {value?.customer?.gender === 'p' ? 'Perempuan' : 'Laki-laki'}
                                    </text>
                                  </div>
                                </Grid>
                                <Grid
                                  item
                                  lg={6}
                                  xl={6}
                                  md={6}
                                  sm={6}
                                  xs={12}
                                  className={classes.formPad}
                                >
                                  <div className={classes.label111}>
                                    <text className={classes.label121}>
                                      Nomor ID
                                    </text>
                                  </div>
                                  <div className={classes.label1112}>
                                    <text className={classes.label1}>
                                      {value?.customer?.identity_number}
                                    </text>
                                  </div>
                                </Grid>
                              </Grid>
                              <Grid
                                container
                                item
                                lg={12}
                                xl={12}
                                md={12}
                                sm={12}
                                xs={12}
                              />
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                              spacing={3}
                            >
                              <Grid
                                item
                                lg={12}
                                xl={12}
                                md={12}
                                sm={12}
                                xs={12}
                                className={classes.formPad}
                              >
                                <div className={classes.label111}>
                                  <text className={classes.label121}>Alamat</text>
                                </div>
                                <div className={classes.label1112}>
                                  <text className={classes.label1}>
                                    {value?.customer?.address}
                                  </text>
                                </div>
                              </Grid>
                            </Grid>
                          </Grid>
                          <Divider className={classes.divider} />
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Produk Gadai
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Kategori Barang Jaminan
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {value?.product_name}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {value?.insurance_item_name}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <div
                            style={{
                              marginBottom: 50,
                            }}
                          >
                            {renderBJ()}
                          </div>
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>
                              Perhitungan Barang Jaminan
                            </text>
                          </div>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Nilai Taksiran
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Pinjaman yang Diajukan
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Biaya Admin
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(value?.estimated_value || 0)}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(value?.loan_amount || 0)}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(value?.admin_fee || 0)}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Biaya Sewa Perbulan
                                </text>
                              </div>
                            </Grid>
                          </Grid>

                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label1}>
                                  {Func.currencyFormatter(value?.monthly_fee || 0)}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Divider className={classes.divider} />
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>
                              Tanggal-Tanggal Penting
                            </text>
                          </div>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Tanggal Akad
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Tanggal Jatuh Tempo
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Tanggal Lelang
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.FormatDate(value?.contract_date)}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.FormatDate(value?.due_date)}
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={4}
                              xl={4}
                              md={4}
                              sm={4}
                              xs={12}
                            >
                              <div className={classes.label1112}>
                                <text className={classes.label1}>
                                  {Func.FormatDate(value?.auction_date)}
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          
                          <Divider className={classes.divider} />
                          <div className={classes.BodytitleMdl2}>
                            <text className={classes.titleMdl}>
                              Detail Deviasi
                            </text>
                          </div>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  No.
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Tanggal
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Tipe Deviasi
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Jumlah
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Status
                                </text>
                              </div>
                            </Grid>
                            <Grid
                              container
                              item
                              lg={2}
                              xl={2}
                              md={2}
                              sm={2}
                              xs={12}
                            >
                              <div className={classes.label111}>
                                <text className={classes.label121}>
                                  Ket
                                </text>
                              </div>
                            </Grid>
                          </Grid>
                          <Grid
                            container
                            direction="row"
                            item
                            lg={12}
                            xl={12}
                            md={12}
                            xs={12}
                          >
                            {renderDeviation()}
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  </div>
                </div>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                style={{ color: 'black' }}
                disabled={loader}
                onClick={() => setOpen(false)}
                variant="outlined"
              >
                <Typography variant="button">Tutup</Typography>
              </Button>
            </DialogActions>
          </Dialog>
        )
      }
    </Fragment>
  );
}

export default DeviationDetail;
