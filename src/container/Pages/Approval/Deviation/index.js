/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-trailing-spaces */
import React, { useState, Fragment } from 'react';
import Crud from '../../../../components/Crud';
import columns from './columns';
import { getApi, getApiDetail } from './api';
import DeviationDetail from './deviationDetail';

const Cashflow = () => {
  const [open, setOpen] = useState(false);
  const [detailData, setDetail] = useState();

  const onColsClick = (event, row) => {
    setOpen(true);
    getApiDetail(row.id)
      .then((res) => {
        const response = res?.data || res?.response?.data;
        if (res?.status === 200) setDetail(response.data);
      });
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        description="Approval Deviasi"
        getApi={getApi}
        disableAdd
        disableFilter
        disableExport
        disableDelete
        disableEdit
        parseId={parseId}
        onColsClick={onColsClick}
        onClickDetail={onColsClick}
        accessDetail
      />
      <DeviationDetail
        data={detailData}
        setOpen={setOpen}
        open={open}
      />
    </Fragment>
    
  ); 
};

export default Cashflow;
