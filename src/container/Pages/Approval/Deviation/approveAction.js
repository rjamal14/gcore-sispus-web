/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, Fragment } from 'react';
import { Dropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import swal from 'sweetalert';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  CircularProgress,
} from '@material-ui/core';
import { approval, rejection } from './api';
import useStyle from './css';

const ApproveAction = ({ row }) => {
  const style = useStyle();
  const [open, setOpen] = useState(false);
  const [reason, setReason] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const option = [
    'Terima',
    'Tolak',
  ];

  const handleReject = () => {
    if (reason) {
      const payload = {
        transaction_deviation: {
          rejected_reason: reason
        }
      };
      swal({
        title: 'Apa Anda yakin?',
        text: 'Menolak pengajuan',
        icon: 'info',
        buttons: ['Batal', 'Yakin'],
      })
        .then((willDelete) => {
          if (willDelete) {
            rejection(row.id, payload)
              .then((res) => {
                const response = res?.data || res?.response?.data;
                if (res?.status === 200) {
                  swal('Sukses!', response.message, 'success')
                    .then(() => { window.location.reload(); });
                }
              });
            setLoading(false);
          }
        });
    } else setError('Tidak boleh kosong');
  };

  const handleApprove = () => {
    swal({
      title: 'Apa Anda yakin?',
      text: 'Menerima pengajuan',
      icon: 'info',
      buttons: ['Batal', 'Yakin'],
    })
      .then((willDelete) => {
        if (willDelete) {
          approval(row.id)
            .then((res) => {
              const response = res?.data || res?.response?.data;
              if (res?.status === 200) {
                swal('Sukses!', response.message, 'success')
                  .then(() => { window.location.reload(); });
              }
            });
          setLoading(false);
        }
      });
  };

  const buttonProps = [
    ['contained', 'primary', style.whiteTextButton, 'Simpan', () => handleReject()],
    ['outlined', '', style.blackTextButton, 'Batal', () => setOpen(false)]
  ];

  const reasonForm = () => (
    <Dialog scroll="paper" open={open}>
      <DialogTitle>Alasan Penolakan</DialogTitle>
      <DialogContent dividers>
        <TextField
          value={reason}
          fullWidth
          size="small"
          variant="outlined"
          onChange={(evt) => setReason(evt.target.value)}
          error={(error && !reason)}
          helperText={reason ? '' : error}
          onKeyDown={() => setError('')}
          multiline
          rows={4}
        />
      </DialogContent>
      <DialogActions>
        {buttonProps.map(([variant, color, classStyle, title, onclick]) => (
          <Button
            variant={variant}
            color={color}
            className={classStyle}
            onClick={onclick}
          >
            {(title === 'Simpan' && loading)
              ? <CircularProgress size={22} />
              : title}
          </Button>
        ))}
      </DialogActions>
    </Dialog>
  );

  return (
    <Fragment>
      <Dropdown>
        <Dropdown.Toggle variant="info" id="dropdown-basic">
          PILIH
        </Dropdown.Toggle>

        <Dropdown.Menu>
          {option.map((opt) => (
            <Dropdown.Item
              eventKey={opt}
              key={opt}
              onClick={() => {
                if (opt === 'Terima') handleApprove();
                if (opt === 'Tolak') setOpen(true);
              }}
            >
              {opt}
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
      {reasonForm()}
    </Fragment>
  );
};

export default ApproveAction;
