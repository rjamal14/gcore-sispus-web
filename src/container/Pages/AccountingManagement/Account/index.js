import React, { Fragment } from 'react';
import Crud from '../../../../components/Crud';
import Func from '../../../../functions/index';
import columns from './columns';
import { deleteApi, editApi, getApi, postApi, showApi } from './api';
import formfields from './formfields';
import FormComponent from './FormComponent';

const Account = () => {
  const parseResponse = (response) => {
    const { data } = response;

    return data;
  };

  const mapParams = (params) => {
    const newParams = {};

    newParams.item = params.data.item;
    newParams.sub_classification = {
      id: params.data.sub_classification_id,
      name: params.data.sub_classification_name
    };

    return newParams;
  };

  const parseId = (object) => {
    const { id } = object;

    return id;
  };

  return (
    <Fragment>
      <Crud
        columns={columns}
        formField={formfields}
        description="Master Data Akun"
        getApi={getApi}
        postApi={postApi}
        deleteApi={deleteApi}
        showApi={showApi}
        editApi={editApi}
        parseResponse={parseResponse}
        parseId={parseId}
        formComponent={FormComponent}
        mapParams={mapParams}
        accessCreate={Func.checkPermission('master-data#account#create')}
        accessDelete={Func.checkPermission('master-data#account#delete')}
      />
    </Fragment>
  );
};

Account.propTypes = ({

});

export default Account;
