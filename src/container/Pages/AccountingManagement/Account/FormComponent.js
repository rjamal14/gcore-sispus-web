import React, { Fragment } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import forms from './formfields';
import Components from '../../../../components/FormField';
import { getField } from '../../../../helper/helper';

const FormComponent = (props) => {
  const { initialValues } = props;
  const subClassObj = { value: initialValues.sub_classification };

  const assignProps = (object, newObject) => _.assign(object, newObject);

  return (
    <Fragment>
      { Components({ ...assignProps(getField(forms, 'sub_classification_id'), subClassObj) }) }
      { Components({ ...getField(forms, 'item') }) }
    </Fragment>
  );
};

FormComponent.propTypes = ({
  initialValues: PropTypes.object.isRequired
});

export default FormComponent;
