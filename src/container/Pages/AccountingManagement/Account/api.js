/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.financialApi + env.apiPrefixV1,
});

const path = 'accounts';
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);

const mapParam = (params) => {
  const newParam = {};

  newParam.item = params.item;
  newParam.sub_classification_id = params?.sub_classification_id?.id;

  return newParam;
};

export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, mapParam(params));
export const editApi = (id, params) => api.put(`${path}/${id}`, { account: params });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);
export const getSubclass = (params) => api.get('sub_classifications/autocomplete/account', { params });
