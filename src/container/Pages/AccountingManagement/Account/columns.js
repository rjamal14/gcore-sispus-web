const columns = [
  {
    name: 'id',
    label: 'ID',
    filterPredicate: '_cont',
    display: false,
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'account_number',
    label: 'No. Akun',
    filterPredicate: '_cont',
    options: {
      filter: false,
      sort: false
    }
  },
  {
    name: 'item',
    label: 'Nama Akun',
    filterPredicate: '_cont',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'action',
    label: 'Aksi',
    options: {
      filter: false,
      sort: false
    }
  },
];

export default columns;
