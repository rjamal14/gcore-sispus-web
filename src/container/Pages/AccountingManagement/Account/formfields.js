/* eslint-disable import/no-named-as-default-member */
import Text from '../../../../components/FormField/Fields/Text';
import SearchAutoComplete from '../../../../components/FormField/Fields/SearchAutoComplete';
import { getSubclass } from './api';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'sub_classification_id',
    label: 'Sub Klasifikasi',
    value: '',
    api: getSubclass,
    component: SearchAutoComplete,
    fieldProps: {
      required: true,
      validate: required
    }
  },
  {
    name: 'item',
    label: 'Nama Akun',
    value: '',
    component: Text,
    fieldProps: {
      required: true,
      validate: required
    }
  },
];

export default formfields;
