/* eslint-disable radix */
import React, { Fragment, useState, useEffect } from 'react';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import { Fab, Menu, MenuItem, Divider } from '@material-ui/core';
import swal from 'sweetalert';
import _ from 'lodash';
import qs from 'qs';
import { useLocation } from 'react-router-dom';
import columns from './columns';
import { getApi, postClasification, postSubclass } from './api';
import formfieldsClasification from './formfieldsClasification';
import formfieldsClassSub from './formfieldsClassSub';
import AddDialogForm from '../../../../components/Crud/DialogForm/AddDIalogForm';
import useStyles from './coa-jss';
import MainTable from '../../../../components/MainTable';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const Coa = () => {
  const [formtype, setFormtype] = useState('clasification'); // clasification dan subclass
  const formField = formtype === 'clasification' ? formfieldsClasification : formfieldsClassSub;
  const postApi = formtype === 'clasification' ? postClasification : postSubclass;

  const mapInitValues = () => {
    const mappedValue = {};

    _.map(formField, (item) => _.assign(mappedValue, { [item.name]: item.value })
    );

    return mappedValue;
  };

  const [newForm, setNewForm] = useState(false);
  const [crudLoading, setCrudLoading] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [errors, setErrors] = useState();
  const [initialValues] = useState(mapInitValues());
  const classes = useStyles();
  const location = useLocation();
  const [loading, setLoading] = useState(false);
  const [collection, setCollection] = useState([]);
  const [totalData, setTotalData] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const query = useQuery();
  const scrHeight = window.screen.height;

  /* const mapParams = (params) => {
    const newParams = params;

    newParams.id = params.id.$oid;

    return newParams;
  }; */

  const parseId = (object) => {
    const { id: { $oid } } = object;

    return $oid;
  };

  const onCloseForm = () => {
    newForm && setNewForm(false);
    // setErrors([]);
    // history.push(location.pathname + location.search);
  };

  const getData = async (params) => {
    try {
      setLoading(true);
      const res = await getApi(params);

      if (res.status === 200) {
        setLoading(false);
        // eslint-disable-next-line camelcase
        const { data: { data, total_data, total_page } } = res;
        setCollection(data);
        setTotalData(total_data);
        setTotalPage(total_page);
      }
    } catch (error) {
      setLoading(false);
      swal({
        title: 'Kesalahan',
        text: 'Maaf, Terjadi kesalahan pada sistem :(',
        icon: 'error',
      });
    }
  };

  const createData = async (params) => {
    try {
      setCrudLoading(true);
      const res = await postApi(params);
      if (res.status === 201) {
        // eslint-disable-next-line no-unused-vars
        const { data: { message, code, status } } = res;
        if (code === 400) {
          setCrudLoading(false);
          setErrors(status);
          swal({
            title: 'Terjadi Kesalahan',
            text: message,
            icon: 'warning',
            buttons: 'OK'
          });
        } else if (code === 201) {
          setCrudLoading(false);
          setNewForm(false);
          getData(qs.parse(location.search, { ignoreQueryPrefix: true }));
          swal({
            title: 'Sukses',
            text: message,
            icon: 'success',
            buttons: 'OK'
          });
        }
      }
    } catch (error) {
      setCrudLoading(false);
      onCloseForm();
      swal({
        title: 'Kesalahan',
        text: 'Maaf, terjadi kesalahan',
        icon: 'error',
        buttons: 'OK'
      });
    }
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    getData(qs.parse(location.search, { ignoreQueryPrefix: true }));
  }, [location.search]);

  return (
    <Fragment>
      <div className={classes.root}>
        <MainTable
          title="Manajemen Akuntansi"
          description="Atur Data Manajemen Akunting"
          data={collection}
          columns={columns}
          loading={loading}
          totalPage={totalPage}
          totalData={totalData}
          page={parseInt(query.get('page')) || 1}
          perPage={parseInt(query.get('per_page')) || 10}
          parseId={parseId}
          disableFilter
          disableSearch
          onDelete={() => {}}
        />
      </div>
      <AddDialogForm
        open={newForm}
        onClose={onCloseForm}
        formField={formField}
        createData={createData}
        loading={crudLoading}
        initialValues={initialValues}
        errors={errors}
      />
      <div className={classes.floatingButton}>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          PaperProps={{ style: { marginTop: (scrHeight >= 1080) ? '-6%' : '-8%', } }}
        >
          <MenuItem onClick={() => {
            setNewForm(true);
            setFormtype('clasification');
          }}
          >
            Klasifikasi
          </MenuItem>
          <Divider />
          <MenuItem onClick={() => {
            setNewForm(true);
            setFormtype('subclass');
          }}
          >
            Subklasifikasi
          </MenuItem>
        </Menu>
        <Fab
          color="secondary"
          aria-label="add"
          onClick={handleClick}
        >
          <UpIcon />
        </Fab>
      </div>
    </Fragment>
  );
};

Coa.propTypes = ({

});

export default Coa;
