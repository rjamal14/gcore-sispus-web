/* eslint-disable radix */
/* eslint-disable import/no-named-as-default-member */
import Text from '../../../../components/FormField/Fields/Text';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'name',
    label: 'Nama',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required,
    }
  },
  {
    name: 'prefix',
    label: 'Nomor Akun',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required,
    }
  }
];

export default formfields;
