/* eslint-disable radix */
/* eslint-disable import/no-named-as-default-member */
import Text from '../../../../components/FormField/Fields/Text';
import SearchAutoComplete2 from '../../../../components/FormField/Fields/SearchAutoComplete2';
import { getClasification, getSubclass } from './api';

const required = (value) => { let error; if (!value) { error = 'tidak boleh kosong'; } return error; };

const formfields = [
  {
    name: 'name',
    label: 'Nama',
    component: Text,
    value: '',
    fieldProps: {
      required: true,
      validate: required,
    }
  },
  {
    name: 'classification_id',
    label: 'Klasifikasi',
    value: '',
    api: getClasification,
    api2: getSubclass,
    component: SearchAutoComplete2,
    fieldProps: {
      required: true,
      validate: required
    }
  },
];

export default formfields;
