/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import env from '../../../../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../../../../config/interceptor';

const api = axios.create({
  baseURL: env.financialApi + env.apiPrefixV1,
});

const path = '/classifications';
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(res => res, checkTokenExpired);

export const getApi = (params) => api.get(path, { params });
export const postApi = (params) => api.post(path, { coa: params });
export const editApi = (id, params) => api.put(`${path}/${id}`, { coa: params });
export const deleteApi = (id) => api.delete(`${path}/${id}`);
export const showApi = (id) => api.get(`${path}/${id}`);

const subclass = (params) => {
  const newParam = {};

  newParam.name = params.name;
  newParam.classification_id = params?.classification_id?.id;
  newParam.parent_id = params?.parent_id?.id;

  if (newParam.classification_id) delete newParam.parent_id;
  else if (newParam.parent_id) delete newParam.classification_id;

  return newParam;
};

// subclasification api
export const getApiSubs = (params) => api.get(path, { params });
export const getClasification = (params) => api.get(`${path}/autocomplete`, { params });
export const getSubclass = (params) => api.get('sub_classifications/autocomplete', { params });
export const postClasification = (params) => api.post(path, params);
export const postSubclass = (params) => api.post('/sub_classifications', subclass(params));
