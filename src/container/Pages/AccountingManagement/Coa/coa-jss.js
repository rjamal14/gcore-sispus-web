import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    padding: theme.spacing(2),
  },
  floatingButton: {
    position: 'fixed',
    bottom: '10%',
    right: theme.spacing(4),
    zIndex: 1
  },
  paper: {
    top: 0,
  }
}));

export default useStyles;
