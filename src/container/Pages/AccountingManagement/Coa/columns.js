import React from 'react';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';

const setIcon = (val) => {
  if (val) return (<LockIcon />);
  return (<LockOpenIcon />);
};

const columns = [
  {
    name: 'id',
    label: 'ID',
    filterPredicate: '_cont',
    display: false,
  },
  {
    name: 'name',
    filterPredicate: '_cont',
    label: 'Nama',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'prefix',
    filterPredicate: '_cont',
    label: 'Nomor Akun',
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'locked',
    label: 'Status',
    filterPredicate: '_cont',
    customBodyRender: (evt, val) => setIcon(val),
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: 'sub_classifications',
    display: false,
  },
];

export default columns;
