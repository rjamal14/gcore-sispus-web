/* eslint-disable camelcase */
/* eslint-disable import/prefer-default-export */
import Loadable from 'react-loadable';
import { Loader as Loading } from '../../components/Loader';

export const DashboardPage = Loadable({
  loader: () => import('./Dashboard'),
  loading: Loading
});

// FINANCIAL MANAGEMENT
export const BranchAccount = Loadable({
  loader: () => import('./FinancialManagement/BranchAccount'),
  loading: Loading
});

export const CentralAccount = Loadable({
  loader: () => import('./FinancialManagement/CentralAccount'),
  loading: Loading
});

export const FinanceHome = Loadable({
  loader: () => import('./FinancialManagement/FinanceHome'),
  loading: Loading
});

export const Request = Loadable({
  loader: () => import('./FinancialManagement/Request'),
  loading: Loading
});

// MASTER DATA
export const OfficeArea = Loadable({
  loader: () => import('./MasterData/OfficeArea'),
  loading: Loading
});

export const Bank = Loadable({
  loader: () => import('./MasterData/Bank'),
  loading: Loading
});

export const BranchOffice = Loadable({
  loader: () => import('./MasterData/BranchOffice'),
  loading: Loading
});

export const City = Loadable({
  loader: () => import('./MasterData/City'),
  loading: Loading
});

export const Country = Loadable({
  loader: () => import('./MasterData/Country'),
  loading: Loading
});

export const HeadOffice = Loadable({
  loader: () => import('./MasterData/HeadOffice'),
  loading: Loading
});

export const Province = Loadable({
  loader: () => import('./MasterData/Province'),
  loading: Loading
});

export const RegionalOffice = Loadable({
  loader: () => import('./MasterData/RegionalOffice'),
  loading: Loading
});

export const UnitOffice = Loadable({
  loader: () => import('./MasterData/UnitOffice'),
  loading: Loading
});

export const Profession = Loadable({
  loader: () => import('./MasterData/Profession'),
  loading: Loading
});

export const User = Loadable({
  loader: () => import('./MasterData/User'),
  loading: Loading
});

export const Role = Loadable({
  loader: () => import('./MasterData/Role'),
  loading: Loading
});

// PARAMETER MENAGEMENT

export const Customer = Loadable({
  loader: () => import('./ParameterManagement/Customer'),
  loading: Loading
});

export const Product = Loadable({
  loader: () => import('./ParameterManagement/Product'),
  loading: Loading
});

export const Transaction = Loadable({
  loader: () => import('./ParameterManagement/Transaction'),
  loading: Loading
});

export const TypeOfColleteral = Loadable({
  loader: () => import('./ParameterManagement/TypeOfColleteral'),
  loading: Loading
});

export const MasterCurrency = Loadable({
  loader: () => import('./ParameterManagement/MasterCurrency'),
  loading: Loading
});

// ACCOUNTING MANAGEMENT
export const Account = Loadable({
  loader: () => import('./AccountingManagement/Account'),
  loading: Loading
});

export const Coa = Loadable({
  loader: () => import('./AccountingManagement/Coa'),
  loading: Loading
});

// Settings Menu
export const Sispus = Loadable({
  loader: () => import('./Settings/MenuSispus'),
  loading: Loading
});

export const Siscab = Loadable({
  loader: () => import('./Settings/MenuSiscab'),
  loading: Loading
});

export const AssignMenuToRole_SisPus = Loadable({
  loader: () => import('./Settings/AssignMenuToRole_SisPus'),
  loading: Loading
});

export const AssignMenuToRole_SisCab = Loadable({
  loader: () => import('./Settings/AssignMenuToRole_SisCab'),
  loading: Loading
});

// Approval

export const WorkcapApproval = Loadable({
  loader: () => import('./Approval/Workcap'),
  loading: Loading
});

export const DeviationApproval = Loadable({
  loader: () => import('./Approval/Deviation'),
  loading: Loading
});

// Setting

export const Profile = Loadable({
  loader: () => import('./Settings/Profile'),
  loading: Loading
});

export const ChangePassword = Loadable({
  loader: () => import('./Settings/ChangePassword'),
  loading: Loading
});

// etc

export const accessDened = Loadable({
  loader: () => import('../../routes/accessDened'),
  loading: Loading
});

export const AllNotification = Loadable({
  loader: () => import('./Notification'),
  loading: Loading
});
