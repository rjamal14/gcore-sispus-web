/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import { CircularProgress, Dialog, DialogActions, DialogContent } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import DialogContentText from '@material-ui/core/DialogContentText';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import AsyncSelect from 'react-select/async';
import DialogTitle from '@material-ui/core/DialogTitle';
import styles from '../css';
import Icon from '../../../../../components/icon';
import Func from '../../../../../functions/index';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validator: [],
      value: [],
    };
    this.handleChangeImg = this
      .handleChangeImg
      .bind(this);
  }

  removeValidate(name) {
    const data = this.state.validator;
    delete data[name];
    this.setState({ validator: data });
  }

  handleChange(event, name) {
    const dataSet = this.state.value;
    dataSet[name] = event;
    this.setState({ value: dataSet });
  }

  handleChangeDate(date, name, name2) {
    const dt = new Date(date);
    const dataSet = this.state.value;
    dataSet[name2] = dt;
    dataSet[name] = dt.getDate() + '/' + dt.getMonth() + '/' + dt.getFullYear();
    this.setState({ value: dataSet });
  }

  componentDidMount() {
    this.getBranch('', '', '');
    this.getBank('', '', '');
    if (this.props.type === 'Ubah') {
      this.setState({ loader: true });
      fetch(process.env.REACT_APP_URL_FINANCIAL + 'branch_office_accounts/' + this.props.id, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const val = [];
        val.bank = { value: json.data.branch_office_account.bank_code, label: json.data.branch_office_account.bank_name };
        val.account_number = json.data.branch_office_account.account_number;
        val.account_name = json.data.branch_office_account.account_name;
        val.branch_office = { value: json.data.branch_office_account.branch_office_id, label: json.data.branch_office_account.branch_office_name };
        this.getBranch('', json.data.branch_office_account.branch_office_id, json.data.branch_office_account.branch_office_name);
        this.getBank('', json.data.branch_office_account.bank_code, json.data.branch_office_account.bank_name);
        this.setState({ value: val });
        this.setState({ loader: false });
      }).catch(() => {
        this.setState({ loader: false });
      })
        .finally(() => {
          this.setState({ loader: false });
        });
    }
  }

    getBank= (val, id, name) => {
      this.setState({ isLoading: true });
      fetch(process.env.REACT_APP_URL_MASTER + 'banks/autocomplete?query=' + val, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then((response) => response.json()).then((json) => {
        if (json.code === '403') {
          if (Func.Clear_Token() === true) {
            if (!localStorage.getItem('token')) {
              this.setState({ redirect: true });
            }
          }
        }
        const datas = [];
        datas.push({
          value: '-',
          label: json.data.length > 0
            ? 'Pilih'
            : 'Tidak ditemukan',
          isDisabled: true
        });

        json
          .data
          .map((value) => {
            datas.push({ value: value._id.$oid, label: value.name });
          });
        if (id !== '') {
          const search = json
            .data
            .find(o => o._id.$oid === id);
          if (search === undefined) {
            datas.push({ value: id, label: name });
          }
        }

        this.setState({ data4: datas, data4_ori: json.data });
      }).catch(() => { })
        .finally(() => { });
    };

    getBranch = (val, id, name) => {
      this.setState({ isLoading: true });
      Func
        .getData('branch_offices', 10, 1, val)
        .then((res) => {
          if (res.json.code === '403') {
            if (Func.Clear_Token() === true) {
              if (!localStorage.getItem('token')) {
                this.setState({ redirect: true });
              }
            }
          } else {
            const datas = [];
            datas.push({
              value: '-',
              label: res.json.data.length > 0
                ? 'Pilih'
                : 'Tidak ditemukan',
              isDisabled: true
            });

            res
              .json
              .data
              .map((value) => {
                datas.push({ value: value.id.$oid, label: value.name });
              });
            if (id !== '') {
              const search = res
                .json
                .data
                .find(o => o.id.$oid === id);
              if (search === undefined) {
                datas.push({ value: id, label: name });
              }
            }

            this.setState({ data3: datas, data3_ori: res.json.data });
          }
        });
    };

    handleSubmit(type) {
      const validator = [
        {
          name: 'bank',
          type: 'required'
        }, {
          name: 'account_number',
          type: 'required'
        }, {
          name: 'account_name',
          type: 'required'
        }, {
          name: 'branch_office',
          type: 'required'
        }
      ];

      const validate = Func.Validator(this.state.value, validator);
      if (validate.success) {
        this.setState({ loader_button: true });
        fetch(process.env.REACT_APP_URL_FINANCIAL + 'branch_office_accounts/' + (type === 'Tambah'
          ? ''
          : this.props.id), {
          method: type === 'Tambah'
            ? 'POST'
            : 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token')
          },
          body: JSON.stringify({
            bank_name: this.state.value.bank.label,
            bank_code: this.state.value.bank.value,
            account_name: this.state.value.account_name,
            account_number: this.state.value.account_number,
            branch_office_id: this.state.value.branch_office.value,
            branch_office_name: this.state.value.branch_office.label,
          })
        }).then((response) => response.json()).then((json) => {
          if (json.code === '403') {
            Func.Refresh_Token();
            if (Func.Refresh_Token() === true) {
              this.handleSubmit();
            }
          }
          if (type === 'Tambah') {
            if (json.created) {
              this
                .props
                .OnNext(json.message);
            } else {
              this.setState({ validator: json.status });
            }
          } else if (json.code === 200) {
            this
              .props
              .OnNext(json.message);
          } else {
            this.setState({ validator: json.status });
          }
          this.setState({ loader_button: false });
        }).catch(() => {
          this.setState({ loader_button: false });
        })
          .finally(() => {
            this.setState({ loader_button: false });
          });
      } else {
        this.setState({ validator: validate.error });
      }
    }

    handleChangeImg(event) {
      this.removeValidate('img');
      const dataSet = this.state.value;
      dataSet.img = URL.createObjectURL(event.target.files[0]);
      this.setState({ value: dataSet });

      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.setState({ ImgBase124: reader.result });
      };
    }

    render() {
      const { classes } = this.props;
      const loadOptions = (inputValue, callback) => {
        setTimeout(() => {
          callback(this.state.data3);
        }, 1000);
      };
      const loadOptions2 = (inputValue, callback) => {
        setTimeout(() => {
          callback(this.state.data4);
        }, 1000);
      };

      if (this.state.loader) {
        return (
          <div className={classes.root2}>
            <Dialog
              disablePortal
              disableEnforceFocus
              disableAutoFocus
              open
              scroll="paper"
              maxWidth="md"
              aria-labelledby="server-modal-title"
              aria-describedby="server-modal-description"
              container={() => {}}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  padding: '20px',
                  borderRadius: '50px',
                }}
              >
                <CircularProgress
                  style={{ color: '#C4A643', margin: '18px' }}
                  size={40}
                />
                Mohon Tunggu
                <div
                  style={{
                    marginTop: '10px',
                  }}
                />
              </div>
            </Dialog>
          </div>
        );
      }

      return (
        <Dialog
          scroll="paper"
          open
          maxWidth="md"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title">
            <Typography variant="h7" className={classes.tittleModal}>
              {this.props.type + ' Rekening Cabang'}
            </Typography>
            <IconButton
              disabled={this.state.loader_button}
              aria-label="close"
              className={classes.closeButton}
              onClick={() => {
                this
                  .props
                  .handleModal();
              }}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
              <div className={classes.scrool}>

                <div className={classes.root}>
                  <Grid container direction="row" item lg={12} xl={12} md={12} xs={12}>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad2}>
                      <div className={classes.label12}>
                        <text>
                          Kantor Pusat
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        value={this.state.value.head_office}
                        placeholder="Pilih"
                        onFocus={() => {
                          this.removeValidate('head_office');
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator.head_office
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        onInputChange={(val) => {
                          this.getHead(val, '', '');
                        }}
                        cacheOptions
                        loadOptions={loadOptions}
                        defaultOptions
                        className={classes.input21}
                        options={this.state.data3}
                        onChange={(val) => {
                          this.handleChange(val, 'head_office');
                        }}
                      />
                      <FormHelperText className={classes.error}>{this.state.validator.head_office}</FormHelperText>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad2}>
                      <div className={classes.label12}>
                        <text>
                          Nama Bank
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <AsyncSelect
                        name="form-field-name-error"
                        value={this.state.value.bank}
                        placeholder="Pilih"
                        onFocus={() => {
                          this.removeValidate('bank');
                        }}
                        styles={{
                          control: (provided, state) => ({
                            ...provided,
                            borderColor: this.state.validator.bank
                              ? 'red'
                              : '#CACACA',
                            borderRadius: '0.25rem'
                          })
                        }}
                        onInputChange={(val) => {
                          this.getBank(val, '', '');
                        }}
                        cacheOptions
                        loadOptions={loadOptions2}
                        defaultOptions
                        className={classes.input21}
                        options={this.state.data4}
                        onChange={(val) => {
                          this.handleChange(val, 'bank');
                        }}
                      />
                      <FormHelperText className={classes.error}>{this.state.validator.bank}</FormHelperText>
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad}>
                      <div>
                        <text className={classes.label1}>
                          Nomor Rekening
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        type="number"
                        size="small"
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('account_number');
                        }}
                        error={this.state.validator.account_number}
                        helperText={this.state.validator.account_number}
                        value={this.state.value.account_number}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'account_number');
                        }}
                        name="account_number"
                        InputProps={{
                          endAdornment: this.state.validator.account_number
                            ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            )
                            : (<div />)
                        }}
                      />
                    </Grid>
                    <Grid item lg={12} xl={12} md={12} sm={12} xs={12} className={classes.formPad}>
                      <div>
                        <text className={classes.label1}>
                          Nama Akun Rekening
                        </text>
                        <text className={classes.starts1}>*</text>
                      </div>
                      <TextField
                        className={classes.input}
                        variant="outlined"
                        margin="normal"
                        size="small"
                        autoComplete="off"
                        fullWidth
                        onFocus={() => {
                          this.removeValidate('account_name');
                        }}
                        error={this.state.validator.account_name}
                        helperText={this.state.validator.account_name}
                        value={this.state.value.account_name}
                        onChange={(event) => {
                          this.handleChange(event.target.value, 'account_name');
                        }}
                        name="account_name"
                        InputProps={{
                          endAdornment: this.state.validator.account_name
                            ? (
                              <InputAdornment position="start">
                                <img src={Icon.warning} />
                              </InputAdornment>
                            )
                            : (<div />)
                        }}
                      />
                    </Grid>
                  </Grid>
                </div>
              </div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              style={{
                backgroundColor: '#c2a443',
                color: 'white'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this.handleSubmit(this.props.type);
              }}
              variant="contained"
            >
              <Typography
                variant="button"
                style={{
                  color: '#FFFFFF'
                }}
              >
                {this.state.loader_button
                  ? (
                    <CircularProgress
                      style={{
                        color: 'white',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: 5
                      }}
                      size={15}
                    />
                  )
                  : ('Simpan')}
              </Typography>
            </Button>
            <Button
              style={{
                color: 'black'
              }}
              disabled={this.state.loader_button}
              onClick={() => {
                this
                  .props
                  .handleModal();
              }}
              variant="outlined"
            >
              <Typography variant="button">Batal</Typography>
            </Button>
          </DialogActions>
        </Dialog>
      );
    }
}

export default withStyles(styles.CoustomsStyles, { name: 'Form' })(Form);
