/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './css';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    };
  }

  handleModal() {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root2}>
        <main className={classes.content}>

          <div
            style={{
              display: 'flex',
              marginLeft: '5%',
              alignItems: 'center',
              width: window.innerWidth,
              minHeight: window.innerHeight - 70,
              justifyContent: 'center'
            }}
          >
            <text
              style={{
                fontSize: 30
              }}
            >
              Coming Soon
            </text>
          </div>
        </main>
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
