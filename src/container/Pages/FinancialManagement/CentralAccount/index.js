/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import styles from './css';
import Tables from './table';
import Form from './form/index';
import Func from '../../../../functions/index';
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    };
  }

  handleModal() {
    this.setState({
      modal: true
    }, () => {
      this.setState({ modal: false });
    });
  }

  handleDrawer() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {}

  render() {
    const { classes } = this.props;
    return (
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: 8 }}>
        <Form type="Tambah" modal={this.state.modal} />
        <Hidden only={['lg', 'xl']}>
          <Tables
            width={60}
            open={this.state.open}
            filter={false}
            title="Rekening Pusat"
            subtitle="Master data rekening pusat."
            path="head_office_accounts"
          />
        </Hidden>
        <Hidden only={['sm', 'md', 'xs']}>
          <Tables
            width={285}
            open={this.state.open}
            filter={false}
            title="Rekening Pusat"
            subtitle="Master data rekening pusat."
            path="head_office_accounts"
          />
        </Hidden>
        { Func.checkPermission('financial-management#central-account#create')
          ? (
            <Fab
              className={classes.fab}
              color="#C4A643"
              aria-label="add"
              onClick={() => {
                this.handleModal();
              }}
            >
              <AddIcon />
            </Fab>
          ) : null}
      </div>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'Login' })(Login);
