import React, { Fragment, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import Sidebar from '../../components/Sidebar';
import menu from '../../routes/menu';
import application from '../../config/application';
import { changePageTitle } from '../../redux/actions/changePageTitle';
import refreshToken from '../../services/getRefreshToken';

const mapBreadcrumbs = () => {
  const breadcrumb = {};

  _.map(menu, (item) => {
    if (item.breadcrumb) {
      _.assign(breadcrumb, item.breadcrumb);
    }

    if (item.subMenu) {
      _.map(item.subMenu, (subItem) => _.assign(breadcrumb, subItem.breadcrumb)
      );
    }
  });

  return breadcrumb;
};

const Layout = ({ children }) => {
  const [breadrumbs] = useState(mapBreadcrumbs());
  const [title, setTitle] = useState('');
  const location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    const { pathname } = location;
    const to = pathname.replace('/app', '');

    setTitle(`${application.name} - ${breadrumbs[to]}`);
    dispatch(changePageTitle(breadrumbs[to]));

    setInterval(() => {
      refreshToken();
    }, 1800000);
  }, [breadrumbs, dispatch, location, title]);

  return (
    <Fragment>
      <Sidebar>
        <Helmet>
          <meta charSet="utf-8" />
          <title>{title}</title>
        </Helmet>
        { children }
      </Sidebar>
    </Fragment>
  );
};

Layout.propTypes = ({
  children: PropTypes.node.isRequired
});

export default Layout;
