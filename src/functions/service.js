/* eslint-disable semi */
import Axios from 'axios';
import env from '../config/env';
import { SetAuthTokenRequest, checkTokenExpired } from '../config/interceptor';

const api = Axios.create({
  baseURL: env.masterApi + env.apiPrefixV1,
});
api.interceptors.request.use(SetAuthTokenRequest, null);
api.interceptors.response.use(null, checkTokenExpired);
const service = {};

service.getProvince = () => api.get('regionals');
service.getRegionalByID = (params) => api.get(`regionals/${params}`)

export default service;
