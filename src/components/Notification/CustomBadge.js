/* eslint-disable react/prop-types */
import React from 'react';
import { IconButton, Badge } from '@material-ui/core';
import NotificationsIcon from '@material-ui/icons/Notifications';

const CustomBadge = ({
  notifCount,
  handleClickNotif,
  classes,
}) => (
  <IconButton onClick={handleClickNotif}>
    <Badge badgeContent={notifCount} color="secondary">
      <NotificationsIcon
        color="primary"
        fontSize="large"
        className={classes.notificationIconMobile}
      />
    </Badge>
  </IconButton>
);

export default CustomBadge;
