/* eslint-disable react/prop-types */
import React from 'react';
import {
  Button,
  Divider,
  List,
  ListItem,
  ListItemText,
  Menu,
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import useStyles from './notification-jss';
import { changeNotifStatus } from '../../redux/actions/notification';

const Notification = ({
  anchorEl2,
  handleClose,
}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const history = useHistory();
  const { notifList } = useSelector(state => state.notification);

  const handleClickNotif = (url, id) => {
    handleClose();
    history.replace(`${url}`);
    dispatch(changeNotifStatus(id));
  };

  return (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl2}
      keepMounted
      open={Boolean(anchorEl2)}
      onClose={handleClose}
      classes={{ paper: classes.notification, }}
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      transformOrigin={{ vertical: 'top', horizontal: 'center' }}
    >
      <List>
        {
          (notifList.length >= 1) && notifList.map((item) => (
            <div key={item.id}>
              <ListItem
                button
                onClick={handleClose}
                className={classes.listItem}
              >
                <ListItemText
                  primary={item.subject}
                  secondary={item.body}
                  variant="caption"
                  className={classes.secondaryText}
                  onClick={() => handleClickNotif(item.url, item.id)}
                />
              </ListItem>
              <Divider />
            </div>
          ))
        }
        {
          (notifList.length >= 1) && (
            <Button
              fullWidth
              onClick={() => {
                handleClose();
                history.replace('/app/notification');
              }}
              style={{ color: '#000000' }}
            >
              Lihat Semua
            </Button>
          )
        }
        {
          (notifList.length < 1) && (
            <Button
              disabled
              fullWidth
            >
              Tidak ada notifikasi baru
            </Button>
          )
        }
      </List>
    </Menu>
  );
};

export default Notification;
