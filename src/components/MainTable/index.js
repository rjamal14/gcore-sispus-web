/* eslint-disable react/prop-types */
/* eslint-disable no-prototype-builtins */
/* eslint-disable consistent-return */
/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Paper,
  Table,
  TableContainer
} from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import { Pagination } from '@material-ui/lab';
import qs from 'qs';
import useStyles from './mainTable-jss';
import TableBody from './TableBody';
import TableToolbar from './TableToolbar';

const MainTable = (props) => {
  const {
    data,
    columns,
    loading,
    editForm,
    setEditForm,
    showData,
    deleteData,
    totalPage,
    page,
    perPage,
    title,
    description,
    totalData,
    onRowClick,
    onEdit,
    onDelete,
    parseId,
    disableFilter,
    accessUpdate,
    accessDelete,
  } = props;

  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();

  const onChangePage = (event, pageNumber) => {
    const params = qs.parse(location.search, { ignoreQueryPrefix: true });
    const newParams = params;
    newParams.page = pageNumber;

    const toQueryString = qs.stringify(newParams, { encode: false });

    history.push(`${location.pathname}?${toQueryString}`);
  };

  return (
    <TableContainer component={Paper}>
      <TableToolbar
        page={page}
        perPage={perPage}
        title={title}
        description={description}
        totalData={totalData}
        columns={columns}
        disableFilter={disableFilter}
        {...props}
      />
      <Table size="small">
        <TableBody
          data={data}
          columns={columns}
          loading={loading}
          editForm={editForm}
          setEditForm={setEditForm}
          showData={showData}
          deleteData={deleteData}
          onRowClick={onRowClick}
          onEdit={onEdit}
          onDelete={onDelete}
          parseId={parseId}
          accessUpdate={accessUpdate}
          accessDelete={accessDelete}
          {...props}
        />
      </Table>
      <div className={classes.pageInfo}>
        Halaman {page} dari {totalPage} Halaman
      </div>
      <Paper className={classes.tableFooter}>
        <Pagination
          count={totalPage}
          defaultPage={1}
          // eslint-disable-next-line radix
          page={parseInt(page)}
          onChange={onChangePage}
          siblingCount={0}
          color="secondary"
        />
      </Paper>
    </TableContainer>
  );
};

MainTable.defaultProps = ({
  description: '',
  total: 0,
  editForm: null,
  setEditForm: null,
  showData: null,
  deleteData: null,
  totalPage: 0,
  page: 1,
  perPage: 10,
  title: 'Data',
  totalData: 0,
  onRowClick: null,
  onEdit: null,
  onDelete: null,
  disableFilter: false
});

MainTable.propTypes = ({
  data: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  columns: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  loading: PropTypes.bool.isRequired,
  editForm: PropTypes.bool,
  setEditForm: PropTypes.func,
  showData: PropTypes.func,
  deleteData: PropTypes.func,
  totalPage: PropTypes.number,
  page: PropTypes.number,
  perPage: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  totalData: PropTypes.number,
  onRowClick: PropTypes.func,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  parseId: PropTypes.func.isRequired,
  disableFilter: PropTypes.bool
});

export default MainTable;
