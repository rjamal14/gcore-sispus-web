/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import {
  TableRow,
  TableCell,
} from '@material-ui/core';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';

const setIcon = (val) => {
  if (val) return (<LockIcon />);
  return (<LockOpenIcon />);
};

const FourthChildren = ({ child, accounts }) => (
  <Fragment>
    {(child.length > 0) && child.map((chd) => (
      <Fragment>
        <TableRow key={chd.name}>
          <TableCell padding="checkbox" />
          <TableCell style={{ paddingLeft: 66 }}>
            <SubdirectoryArrowRightIcon />
            {chd.name}
          </TableCell>
          <TableCell>{chd.prefix}</TableCell>
          <TableCell>{setIcon(chd.locked)}</TableCell>
        </TableRow>
      </Fragment>
    ))}
    {(accounts.length > 0) && accounts.map((acc) => (
      <TableRow key={acc.item}>
        <TableCell padding="checkbox" />
        <TableCell style={{ paddingLeft: 66 }}>
          <SubdirectoryArrowRightIcon />
          {acc.item}
        </TableCell>
        <TableCell>{acc.account_number}</TableCell>
        <TableCell>{setIcon(acc.locked)}</TableCell>
      </TableRow>
    )
    )}
  </Fragment>
);

const ThirdChildren = ({ child, accounts }) => (
  <Fragment>
    {(child.length > 0) && child.map((chd) => {
      const fourthChild = chd?.children;
      const fourthAcc = chd?.accounts;
      return (
        <Fragment>
          <TableRow key={chd.name} style={{ backgroundColor: '#DCDCDC' }}>
            <TableCell padding="checkbox" />
            <TableCell style={{ paddingLeft: 56 }}>
              <SubdirectoryArrowRightIcon />
              {chd.name}
            </TableCell>
            <TableCell>{chd.prefix}</TableCell>
            <TableCell>{setIcon(chd.locked)}</TableCell>
          </TableRow>
          <FourthChildren child={fourthChild} accounts={fourthAcc} />
        </Fragment>
      );
    })}
    {(accounts.length > 0) && accounts.map((acc) => (
      <TableRow key={acc.item} style={{ backgroundColor: '#DCDCDC' }}>
        <TableCell padding="checkbox" />
        <TableCell style={{ paddingLeft: 56 }}>
          <SubdirectoryArrowRightIcon />
          {acc.item}
        </TableCell>
        <TableCell>{acc.account_number}</TableCell>
        <TableCell>{setIcon(acc.locked)}</TableCell>
      </TableRow>
    )
    )}
  </Fragment>
);

const SecondChildren = ({ child, accounts }) => (
  <Fragment>
    {(child.length > 0) && child.map((chd) => {
      const thirdChildren = chd?.children;
      const thirdAcc = chd?.accounts;
      return (
        <Fragment>
          <TableRow key={chd.name} style={{ backgroundColor: '#E8E8E8' }}>
            <TableCell padding="checkbox" />
            <TableCell style={{ paddingLeft: 46 }}>
              <SubdirectoryArrowRightIcon />
              {chd.name}
            </TableCell>
            <TableCell>{chd.prefix}</TableCell>
            <TableCell>{setIcon(chd.locked)}</TableCell>
          </TableRow>
          <ThirdChildren child={thirdChildren} accounts={thirdAcc} />
        </Fragment>
      );
    })}
    {(accounts.length > 0) && accounts.map((acc) => (
      <TableRow key={acc.item} style={{ backgroundColor: '#E8E8E8' }}>
        <TableCell padding="checkbox" />
        <TableCell style={{ paddingLeft: 46 }}>
          <SubdirectoryArrowRightIcon />
          {acc.item}
        </TableCell>
        <TableCell>{acc.account_number}</TableCell>
        <TableCell>{setIcon(acc.locked)}</TableCell>
      </TableRow>
    )
    )}
  </Fragment>
);

const FirstChildren = ({ child, accounts }) => (
  <Fragment>
    {(child.length > 0) && child.map((chd) => {
      const secondChild = chd?.children;
      const secondAcc = chd?.accounts;
      return (
        <Fragment>
          <TableRow key={chd.name} style={{ backgroundColor: '#F5F5F5' }}>
            <TableCell padding="checkbox" />
            <TableCell style={{ paddingLeft: 36 }}>
              <SubdirectoryArrowRightIcon />
              {chd.name}
            </TableCell>
            <TableCell>{chd.prefix}</TableCell>
            <TableCell>{setIcon(chd.locked)}</TableCell>
          </TableRow>
          <SecondChildren child={secondChild} accounts={secondAcc} />
        </Fragment>
      );
    })}
    {(accounts.length > 0) && accounts.map((acc) => (
      <TableRow key={acc.item} style={{ backgroundColor: '#F5F5F5' }}>
        <TableCell padding="checkbox" />
        <TableCell style={{ paddingLeft: 36 }}>
          <SubdirectoryArrowRightIcon />
          {acc.item}
        </TableCell>
        <TableCell>{acc.account_number}</TableCell>
        <TableCell>{setIcon(acc.locked)}</TableCell>
      </TableRow>
    )
    )}
  </Fragment>
);

const SubClassCoa = ({ data }) => {
  const subClassList = data;

  return (
    <Fragment>
      {subClassList?.sub_classifications.map((subclass) => {
        const child = subclass?.children;
        const accounts = subclass?.accounts;
        return (
          <Fragment>
            <TableRow key={subclass.name}>
              <TableCell padding="checkbox" />
              <TableCell style={{ paddingLeft: 26 }}>
                <SubdirectoryArrowRightIcon />
                {subclass.name}
              </TableCell>
              <TableCell>{subclass.prefix}</TableCell>
              <TableCell>{setIcon(subclass.locked)}</TableCell>
            </TableRow>
            <FirstChildren child={child} accounts={accounts} />
          </Fragment>
        );
      })}
    </Fragment>
  );
};

export default SubClassCoa;
