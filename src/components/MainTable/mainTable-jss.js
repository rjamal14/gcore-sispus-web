import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    padding: theme.spacing(2),
  },
  tableToolbar: {
    display: 'flex',
    alignItems: 'flex-end',
    padding: theme.spacing(2),
    borderRadius: '12px 12px 0px 0px',
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    }
  },
  tableLeftContent: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    [theme.breakpoints.down('sm')]: {
      alignItems: 'center'
    }
  },
  title: {
    marginBottom: theme.spacing(2)
  },
  leftToolbar: {
    display: 'flex',
    alignItems: 'center'
  },
  rightToolbar: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      display: 'flex',
      marginTop: theme.spacing(1),
      justifyContent: 'flex-end'
    }
  },
  toolbarPage: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    height: 40,
    borderRadius: 50,
    '&.MuiInputBase-root': {
      fontSize: 14
    }
  },
  searchField: {
    display: 'flex',
    marginRight: theme.spacing(1),
    height: 40
  },
  searchFieldClose: {
    display: 'none'
  },
  searchIcon: {
    marginRight: theme.spacing(1)
  },
  tableHead: {
    backgroundColor: '#FCE8BE',
  },
  tableFooter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(2),
    width: '100%'
  },
  emptyRow: {
    textAlign: 'center',
    height: 50
  },
  pageInfo: {
    display: 'flex',
    padding: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      justifyContent: 'center'
    }
  },
  floatingButton: {
    position: 'fixed',
    bottom: '10%',
    right: theme.spacing(4),
    zIndex: 1
  },
  actionButton: {
    display: 'flex'
  },
  filterMenu: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(1),
  },
  filterField: {
    margin: theme.spacing(1)
  },
  floatingToolbar: {
    display: 'flex',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    bottom: '10%',
    left: '50%',
    position: 'fixed',
    zIndex: 1,
    paddingLeft: theme.spacing(3),
    backgroundColor: theme.palette.secondary.main
  },
  hideFloatingToolbar: {
    display: 'none'
  },
  toolWrapper: {
    display: 'inline-block',
    margin: theme.spacing(1)
  }
}));

export default useStyles;
