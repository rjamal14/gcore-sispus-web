/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable no-prototype-builtins */
import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Checkbox,
  Grow,
  Icon,
  IconButton,
  Paper,
  TableBody,
  TableCell,
  TableRow,
  Tooltip,
  Typography,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import { BeatLoader } from 'react-spinners';
import AppsIcon from '@material-ui/icons/Apps';
import _ from 'lodash';
import clsx from 'clsx';
import { useDispatch } from 'react-redux';
import useStyles from './mainTable-jss';
import { ACTIVE, DISABLED } from '../../redux/constants';
import TableHead from './TableHead';
import SubClassCoa from './SubClassCoa';

const Tablebody = ({
  data,
  columns,
  loading,
  onRowClick,
  onEdit,
  onDelete,
  parseId,
  accessUpdate,
  accessDelete,
  onClickDetail,
  onColsClick,
  accessDetail,
}) => {
  const classes = useStyles();
  const [idCollection, setIdCollection] = useState([]);
  const dispatch = useDispatch();
  const tableColumns = columns || [];
  const tableData = data || [];
  const filterCol = columns.filter(col => col.name === 'pointerCursor');
  const cursorStyle = filterCol ? 'pointer' : 'auto';

  const handleRowClick = (event, row, rowData) => {
    onRowClick && onRowClick(event, row, rowData);
  };

  const onClickEdit = (event, row, rowData) => {
    onEdit && onEdit(event, row, rowData);
  };

  const onClickDelete = (event, dataId, action) => {
    onDelete && onDelete(event, dataId, action);
  };

  const triggerIdCollection = (value) => {
    setIdCollection(value);
  };

  const handleClickDetail = (event, row, rowData) => {
    onClickDetail && onClickDetail(event, row, rowData);
  };

  const handleColsClick = (event, row, rowData) => {
    onColsClick && onColsClick(event, row, rowData);
  };

  const onCheckedItem = (event) => {
    const { value, checked } = event.target;
    const newCollection = [...idCollection];

    if (checked) {
      newCollection.push(value);
      dispatch({ type: DISABLED });
    } else {
      const index = newCollection.indexOf(value);
      if (index > -1) {
        newCollection.splice(index, 1);
      }

      newCollection.length === 0 && dispatch({ type: ACTIVE });
    }

    setIdCollection(newCollection);
  };

  const setFloatingToolbar = (value) => {
    setIdCollection(value);
  };

  const renderFloatingToolbar = () => (
    <Grow in={idCollection.length > 0}>
      <div>
        <Paper
          className={clsx({
            [classes.floatingToolbar]: idCollection.length > 0,
            [classes.hideFloatingToolbar]: idCollection.length === 0
          })}
        >
          <div className={classes.toolWrapper}>
            <Typography
              variant="button"
              style={{ color: '#FFFFFF' }}
            >
              data terpilih : {idCollection.length}
            </Typography>
          </div>
          <div className={classes.toolWrapper}>
            <Tooltip title="Hapus" placement="top">
              <IconButton
                size="small"
                color="primary"
                onClick={onDelete.bind(null, null, idCollection, { setFloatingToolbar })}
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </div>
          <div className={classes.toolWrapper}>
            <Tooltip title="Batal" placement="top">
              <IconButton
                size="small"
                style={{ color: '#FFFFFF' }}
                onClick={() => {
                  setIdCollection([]);
                  dispatch({ type: ACTIVE });
                }}
              >
                <CloseIcon />
              </IconButton>
            </Tooltip>
          </div>
        </Paper>
      </div>
    </Grow>
  );

  return (
    <>
      <TableHead
        columns={columns}
        triggerIdCollection={triggerIdCollection}
        data={tableData}
        parseId={parseId}
        idCollection={idCollection}
      />
      <TableBody>
        {
          loading
            ? (
              <TableRow hover>
                <TableCell
                  className={classes.emptyRow}
                  colSpan={tableColumns.length + 2}
                >
                  <div className={{
                    marginTop: 20,
                    marginBottom: 20,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                  >
                    <BeatLoader size={15} color="#3F3F3F" loading />
                  </div>
                </TableCell>
              </TableRow>
            )
            : tableData.length === 0
              ? (
                <TableRow hover>
                  <TableCell
                    className={classes.emptyRow}
                    colSpan={tableColumns.length + 2}
                  >
                    <Button
                      color="inherit"
                      startIcon={<AppsIcon />}
                    >
                      Tidak Ada Data
                    </Button>

                  </TableCell>
                </TableRow>
              )
              : _.map(tableData, (rowItem, row) => (
                <Fragment>
                  <TableRow hover key={row} onClick={handleRowClick.bind(null, row, rowItem)}>
                    <TableCell padding="checkbox">
                      <Checkbox
                        value={parseId(rowItem)}
                        onChange={onCheckedItem}
                        checked={idCollection.indexOf(parseId(rowItem)) > -1}
                      />
                    </TableCell>
                    {
                      _.map(tableColumns, (colItem, col) => {
                        const show = colItem.hasOwnProperty('display') ? colItem.display : true;
                        const render = [];
                        if (show) {
                          if (colItem.name === 'action' || colItem.name === 'locked') {
                            render.push(
                              <TableCell key={col}>
                                <div className={classes.actionButton}>
                                  <div style={{ marginRight: 20 }}>
                                    {(colItem.customBodyRender) && colItem.customBodyRender(null, rowItem[colItem.name], rowItem)}
                                  </div>
                                  { accessUpdate
                                    ? (
                                      <IconButton
                                        onClick={onClickEdit.bind(null, row, rowItem)}
                                        disabled={idCollection.length > 0}
                                      >
                                        <Icon fontSize="small">edit</Icon>
                                      </IconButton>
                                    ) : null}
                                  { accessDelete
                                    ? (
                                      <IconButton
                                        onClick={onClickDelete.bind(null, row, rowItem)}
                                        disabled={idCollection.length > 0}
                                      >
                                        <Icon fontSize="small">delete</Icon>
                                      </IconButton>
                                    ) : null}
                                  { accessDetail
                                    && (
                                      <IconButton
                                        onClick={handleClickDetail.bind(null, row, rowItem)}
                                        disabled={idCollection.length > 0}
                                      >
                                        <Icon fontSize="small">visibility</Icon>
                                      </IconButton>
                                    ) }
                                  {(colItem.name === 'locked') && colItem.customBodyRender(null, rowItem?.locked)}
                                </div>
                              </TableCell>
                            );
                          } else if (colItem.name === 'id') {
                            render.push(
                              <TableCell key={col}>
                                {parseId(rowItem) }
                              </TableCell>);
                          } else if (colItem.hasOwnProperty('customBodyRender')) {
                            return (
                              <TableCell onClick={handleColsClick.bind(null, row, rowItem)} key={col} style={{ cursor: cursorStyle }}>
                                {colItem.customBodyRender(null, rowItem[colItem.name], rowItem)}
                              </TableCell>
                            );
                          } else {
                            render.push(
                              <TableCell onClick={handleColsClick.bind(null, row, rowItem)} key={col} style={{ cursor: cursorStyle }}>
                                {rowItem[colItem.name]}
                              </TableCell>);
                          }
                        }
                        return render;
                      }
                      )
                    }
                  </TableRow>
                  {(rowItem?.sub_classifications?.length > 0) && <SubClassCoa data={rowItem} />}
                </Fragment>
              )
              )
        }
      </TableBody>
      {renderFloatingToolbar()}
    </>
  );
};

Tablebody.defaultProps = ({
  onRowClick: null,
  onEdit: null,
  onDelete: null,
});

Tablebody.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  onRowClick: PropTypes.func,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  parseId: PropTypes.func.isRequired
};

export default Tablebody;
