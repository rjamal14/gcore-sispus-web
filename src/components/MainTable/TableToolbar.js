/* eslint-disable consistent-return */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  MenuItem,
  OutlinedInput,
  Popover,
  Select,
  TextField,
  Typography
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import FilterListIcon from '@material-ui/icons/FilterList';
import _ from 'lodash';
import { useHistory, useLocation } from 'react-router-dom';
import qs from 'qs';
import useStyles from './mainTable-jss';

const Tabletoolbar = ({ title, description, totalData, columns, page, perPage, disableFilter, disableSearch }) => {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const [filterParams, setFilterParams] = useState({ q: {} });
  const [searchValue, setSearchValue] = useState('');
  const [anchorEl, setAnchorEl] = React.useState(null);
  const openFilterMenu = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onFilterChange = (e) => {
    const newFilterParams = filterParams;
    const currentParams = qs.parse(location.search);
    newFilterParams.q[e.target.name] = e.target.value;

    const finalParams = _.assign(currentParams, newFilterParams);

    setFilterParams(finalParams);
  };

  const onChangePerPage = (e) => {
    const params = qs.parse(location.search, { ignoreQueryPrefix: true });
    const newParams = params;
    newParams.page = 1;
    newParams.per_page = e.target.value;

    const toQueryString = qs.stringify(newParams, { encode: false });

    history.push(`${location.pathname}?${toQueryString}`);
  };

  const renderPerPage = () => (
    <Select
      name="perPage"
      value={perPage}
      onChange={onChangePerPage}
      variant="outlined"
      className={classes.toolbarPage}
    >
      <MenuItem value={10}>10 Data</MenuItem>
      <MenuItem value={15}>15 Data</MenuItem>
      <MenuItem value={25}>25 Data</MenuItem>
    </Select>
  );

  const renderFilterMenu = () => (
    <Popover
      open={openFilterMenu}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
    >
      <div className={classes.filterMenu}>
        <Typography
          variant="button"
          color="primary"
        >
          Filter Data
        </Typography>
        {
          _.map(columns, (col, key) => {
            if (col.options.filter) {
              return (
                <FormControl key={key} className={classes.filterField}>
                  <TextField
                    name={col.name + col.filterPredicate}
                    label={col.label}
                    variant="outlined"
                    onChange={onFilterChange}
                  />
                </FormControl>
              );
            }
          }
          )
        }
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={() => history.push(`${location.pathname}${qs.stringify(filterParams).replace('%3F', '?')}`)}
        >
          Filter
        </Button>
      </div>
    </Popover>
  );

  const onSearchChange = (e) => {
    setSearchValue(e.target.value);
  };

  const onKeyPress = (e) => {
    if (e.key === 'Enter') {
      history.push(`${location.pathname}?page=1&per_page=10&search=${searchValue}`);
    }
  };

  return (
    <div className={classes.tableToolbar}>
      <div className={classes.tableLeftContent}>
        <Typography variant="h6" className={classes.title} style={{ fontWeight: 'bold' }}>
          {title}
        </Typography>
        <Typography variant="body2" className={classes.title}>
          {description}
        </Typography>

        <div className={classes.leftToolbar}>
          Tampilkan: {renderPerPage()} Total: {totalData} {title}
        </div>
      </div>
      <div className={classes.rightToolbar}>
        {
          !disableSearch && (
            <>
              <OutlinedInput
                name="searchField"
                placeholder="Cari data..."
                onChange={onSearchChange}
                variant="outlined"
                value={searchValue}
                className={classes.searchField}
                onKeyPress={onKeyPress}
                endAdornment={(
                  <InputAdornment>
                    <IconButton
                      edge="end"
                      size="small"
                      onClick={() => history.push(`${location.pathname}?page=$1&per_page=10&search=${searchValue}`)}
                    >
                      <SearchIcon color="primary" />
                    </IconButton>
                  </InputAdornment>
                )}
              />
              <Button
                color="primary"
                variant="outlined"
                onClick={() => {
                  setSearchValue('');
                  history.push(`${location.pathname}?&page=${page}&per_page=${perPage}`);
                }}
              >
                Reset
              </Button>
            </>
          )
        }

        {
          !disableFilter
          && (
            <div>
              <Button
                name="filterButton"
                startIcon={<FilterListIcon />}
                variant="outlined"
                color="primary"
                onClick={handleClick}
              >
                Filter
              </Button>
              {renderFilterMenu()}
            </div>
          )
        }

      </div>

    </div>
  );
};

Tabletoolbar.defaultProps = ({
  title: 'Table',
  description: '',
  totalData: 0,
  page: 1,
  perPage: 10,
  disableFilter: false
});

Tabletoolbar.propTypes = {
  columns: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  totalData: PropTypes.number,
  page: PropTypes.number,
  perPage: PropTypes.number,
  disableFilter: PropTypes.bool
};

export default Tabletoolbar;
