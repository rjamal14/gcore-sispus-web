/* eslint-disable consistent-return */
/* eslint-disable no-prototype-builtins */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Checkbox,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';
import _ from 'lodash';
import { useDispatch } from 'react-redux';
import useStyles from './mainTable-jss';
import { ACTIVE, DISABLED } from '../../redux/constants';

const Tablehead = ({ columns, triggerIdCollection, data, parseId, idCollection }) => {
  const classes = useStyles();
  const [checkedAll, setCheckedAll] = useState(false);
  const dispatch = useDispatch();

  const selectAll = () => {
    const array = [];
    _.map(data, (item) => array.push(parseId(item))
    );

    triggerIdCollection(array);
  };

  const onChange = (e) => {
    const { checked } = e.target;

    if (checked) {
      setCheckedAll(checked);
      dispatch({ type: DISABLED });
      selectAll();
    } else {
      setCheckedAll(false);
      dispatch({ type: ACTIVE });
      triggerIdCollection([]);
    }
  };

  useEffect(() => {
    idCollection.length === 0 && setCheckedAll(false);
  }, [idCollection]);

  return (
    <TableHead className={classes.tableHead}>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            onChange={onChange}
            checked={checkedAll}
          />
        </TableCell>
        {
          _.map(columns, (item, col) => {
            const show = item.hasOwnProperty('display') ? item.display : true;
            if (show) {
              return (
                <TableCell key={col}>
                  {item.label}
                </TableCell>
              );
            }
          }
          )
        }
      </TableRow>
    </TableHead>
  );
};
Tablehead.propTypes = {
  columns: PropTypes.array.isRequired,
  triggerIdCollection: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
  parseId: PropTypes.func.isRequired,
  idCollection: PropTypes.array.isRequired
};

export default Tablehead;
