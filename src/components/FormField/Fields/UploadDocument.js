/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable react/no-deprecated */

import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import { Document, Page } from 'react-pdf';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const UploadDocument = (props) => {
  const classes = useStyles();
  const { field, label, form } = props;
  const { values } = form;
  const { name } = field;
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [open, setOpen] = useState(false);

  if (typeof values[name] === 'string') {
    fetch(values[name])
      .then(res => res.blob())
      .then(blob => {
        const urlSplit = values[name].split('/');
        const filename = urlSplit[urlSplit.length - 1];
        const file = new File([blob], filename, { type: blob.type });
        values[name] = file;
      });
  }

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function onChange(e) {
    form.setFieldValue(field.name, e.currentTarget.files[0], false);
    form.setFieldTouched(field.name, true);
  }

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  return (
    <div style={{ marginTop: '20px' }}>
      <FormControl fullWidth>
        <Button variant="contained" color="primary" component="label">
          {`Upload ${label}`}
          <input
            style={{ display: 'none' }}
            id={name}
            type="file"
            onChange={onChange}
          />
        </Button>
        {values[name] && (
          <div>
            <br />
            <button type="button" onClick={handleOpen}>
              {`Lihat ${label}`}
            </button>
            <Modal
              className={classes.modal}
              open={open}
              onClose={handleClose}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
            >
              <Fade in={open}>
                <div className={classes.paper}>
                  <Document
                    file={values[name]}
                    onLoadSuccess={onDocumentLoadSuccess}
                  >
                    <Page pageNumber={pageNumber} />
                  </Document>
                  <br />
                  <p>{`Halaman ${pageNumber} dari ${numPages}`}</p>
                  <Pagination
                    onChange={(e, page) => setPageNumber(page)}
                    count={numPages}
                  />
                </div>
              </Fade>
            </Modal>
          </div>
        )}
      </FormControl>
    </div>
  );
};

export default function Component(props) {
  return (
    <Field
      {...props}
      component={UploadDocument}
    />
  );
}
