/* eslint-disable object-curly-newline */
/* eslint-disable react/jsx-props-no-spreading */

import { Select as BaseSelect } from 'formik-material-ui';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import React, { useState } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';
import map from 'lodash.map';
import find from 'lodash.find';
import filter from 'lodash.filter';
import includes from 'lodash.includes';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import Chip from '@material-ui/core/Chip';

function Select(props) {
  const { field, form, inputProps, ...restProps } = props;
  const { setFieldValue, setFieldTouched } = form;
  const { multiple, mapped, chips, items, onChange: setSelected, ...restInputProps } = inputProps;
  const { onChange, onBlur, name, ...restField } = field;
  const selectChange = ({ target }) => {
    setSelected(target.value);
    setFieldValue(name, target.value, false);
    setFieldTouched(name, true);
  };
  const selectBlur = () => onBlur(name);
  const newProps = {
    ...restProps,
    disabled: false,
    inputProps: restInputProps,
    form,
    field: {
      name,
      ...restField,
      onChange: selectChange,
      onBlur: selectBlur
    }
  };
  return (
    <BaseSelect
      {...newProps}
      renderValue={
        (selection) => {
          let rendered;
          if (multiple) {
            let values = selection;
            if (mapped) {
              const filtered = filter(items, (item) => includes(selection, item.value));
              values = map(filtered, 'label');
            }
            if (chips) {
              rendered = map(values, (value) => (
                <span key={value} style={{ marginRight: '10px' }}>
                  <Chip
                    size="small"
                    label={value}
                    color="primary"
                  />
                </span>
              ));
            } else {
              rendered = values.join(', ');
            }
          } else {
            rendered = mapped ? (find(items, (o) => o.value === selection)).label : selection;
          }
          return rendered;
        }
      }
      input={<Input />}
    />
  );
}

Select.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  inputProps: PropTypes.object.isRequired
};

const SelectMultiple = ({ label, name, id, items, chips, checkbox, initialValue, ...props }) => {
  const [selected, setSelected] = useState(initialValue);
  const multiple = initialValue instanceof Array;
  const mapped = typeof items[0] === 'object';
  return (
    <FormControl fullWidth variant="outlined">
      <InputLabel htmlFor={`${name}-${id}`}>{label}</InputLabel>
      <Field
        {...props}
        component={Select}
        multiple={multiple}
        defaultValue={selected}
        name={name}
        inputProps={{
          id: `${name}-${id}`,
          onChange: setSelected,
          items,
          mapped,
          chips,
          multiple,
        }}
      >
        {
          map(
            items,
            (item, index) => (
              (multiple)
                ? (
                  <MenuItem key={index} value={mapped ? item.value : item}>
                    {(checkbox) ? (<Checkbox checked={selected.indexOf((mapped ? item.value : item)) > -1} />) : ''}
                    <ListItemText primary={mapped ? item.label : item} />
                  </MenuItem>
                )
                : (<MenuItem key={index} value={mapped ? item.value : item}>{mapped ? item.label : item}</MenuItem>)
            )
          )
        }
      </Field>
    </FormControl>
  );
};

SelectMultiple.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  items: PropTypes
    .arrayOf(PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.oneOfType([
          PropTypes.number,
          PropTypes.string
        ])
      })
    ]))
    .isRequired,
  initialValue: PropTypes
    .oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.arrayOf(PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
      ]))
    ])
    .isRequired,
  chips: PropTypes.bool,
  checkbox: PropTypes.bool
};

SelectMultiple.defaultProps = {
  chips: false,
  checkbox: false
};

export default SelectMultiple;
