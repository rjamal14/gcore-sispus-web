import React from 'react';
import { Switch } from 'formik-material-ui';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';

function Component({ name, label }) {
  return (
    <FormControl>
      <FormControlLabel
        label={label}
        control={(
          <Field
            component={Switch}
            name={name}
            label={label}
            type="checkbox"
          />
        )}
      />
    </FormControl>
  );
}

Component.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.any.isRequired
};

export default Component;
