/* eslint-disable no-unused-vars */
/* eslint-disable object-curly-newline */
/* eslint-disable react/jsx-props-no-spreading */

import { Select as BaseSelect } from 'formik-material-ui';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import React, { createElement } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import map from 'lodash.map';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  select: {
    '&.MuiInputLabel-root': {
      marginLeft: theme.spacing(2)
    }
  },
  field: {
    marginBottom: theme.spacing(2)
  }
}));

const Select = ({ label, name, id, items }) => {
  const classes = useStyles();
  const Items = item => createElement(MenuItem, { key: item.idx, children: item.label, ...item });
  return (
    <FormControl fullWidth>
      <InputLabel className={classes.select} htmlFor={`${name}-${id}`}>{label}</InputLabel>
      <Field
        className={classes.field}
        component={BaseSelect}
        variant="outlined"
        name={name}
        InputProps={{
          id: `${name}-${id}`,
        }}
      >
        {map(items, (item, idx) => <MenuItem key={idx} value={item.value}>{item.label}</MenuItem>
        )}
      </Field>
    </FormControl>
  );
};

Select.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Select;
