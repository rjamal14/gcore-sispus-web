/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */

import React, { useState } from 'react';
import MUISelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import _ from 'lodash';

function BaseSelect(props) {
  const { label, field, form, items, parent, parentvalue } = props;
  const { name } = field;
  const { values } = form;
  const selectedItem = values[name] && _.find(items, { label: values[name] });
  const [selected, setSelected] = useState(selectedItem ? selectedItem.value : '');
  const [emptyOption, setEmptyOption] = useState('\u00A0');
  let checkCondition = !parent && !parentvalue;

  if (selectedItem && checkCondition) {
    form.setFieldValue(field.name, selectedItem.value, false);
    form.setFieldTouched(field.name, true);
  }

  if (!checkCondition) {
    checkCondition = values[parent] === parentvalue;
  }

  function onChange(e) {
    setSelected(e.target.value);
    form.setFieldValue(field.name, e.target.value, false);
    form.setFieldTouched(field.name, true);
  }

  function onOpen() {
    setEmptyOption(label);
  }

  function onClose() {
    setEmptyOption('\u00A0');
  }

  if (checkCondition) {
    return (
      <div>
        <FormControl fullWidth>
          <InputLabel>{label}</InputLabel>
          <MUISelect displayEmpty value={selected} onChange={onChange} onOpen={onOpen} onClose={onClose}>
            <MenuItem value="" style={{ color: 'grey' }}>{emptyOption}</MenuItem>
            {items.map(
              (item) => (
                <MenuItem key={item.value} value={item.value}>
                  {item.label}
                </MenuItem>
              )
            )}
          </MUISelect>
        </FormControl>
      </div>
    );
  }

  return false;
}

export default function Component(props) {
  return (
    <Field
      {...props}
      component={BaseSelect}
    />
  );
}
