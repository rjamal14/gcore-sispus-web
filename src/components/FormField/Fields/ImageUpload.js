/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
import React, { useEffect, useState } from 'react';
import { Field } from 'formik';
import { makeStyles } from '@material-ui/core';
import swal from 'sweetalert';
import FormControl from '@material-ui/core/FormControl';

const useStyles = makeStyles((theme) => ({
  map: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  autocomplete: {
    zIndex: 1,
    backgroundColor: 'white'
  },
  autocompleteContainer: {
    padding: theme.spacing(1),
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  helperText: {
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: 'rgba(0,0,0,0.8)',
    padding: 4
  }
}));

const Fileupload = ({ label, name, fieldProps, value, ...props }) => {
  const [Img, setImg] = useState(value);
  const [Base64, setBase64] = useState(value);
  const classes = useStyles();

  const onFieldClicked = (event) => {
    const file = event.target.files[0];
    if (file.size > 2.9e6) {
      swal({
        title: 'File Terlalu Besar',
        text: 'Maximal File 2Mb',
        icon: 'error',
        buttons: 'OK',
      });
      return false;
    }
    setImg(URL.createObjectURL(event.target.files[0]));
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setBase64(reader.result);
    };
  };

  useEffect(() => {
  }, [value]);

  return (
    <div>
      {Img ? (
        <img
          className={classes.imgScan2}
          src={Img}
        />
      ) : null}
      <input
        onChange={onFieldClicked}
        type="file"
        accept="image/*"
        name="logo"
        title="Pilih Gambar"
        {...props}
      />
    </div>
  );
};

const FieldText = ({ label, name, fieldProps }) => (
  <FormControl fullWidth style={{ marginBottom: 8 }}>
    <Field
      as={Fileupload}
      name="logo"
      label="logo"
      variant="outlined"
      {...fieldProps}
    />
  </FormControl>
);

export default FieldText;
