/* eslint-disable react/no-unused-prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react';
import { SimpleFileUpload } from 'formik-material-ui';
import { Field, useField } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';

const Fileupload = ({ name, label }) => {
  const [field, meta, helpers] = useField(name);
  const { setValue } = helpers;
  const { value } = field;
  return (
    <FormControl style={{ marginBottom: '10vh' }}>
      <Field
        component={SimpleFileUpload}
        name={name}
        label={label}
        InputProps={{
          onChange: (event) => {
            const { files } = event.target;
            if (files[0]?.name) {
              const reader = new FileReader();
              reader.readAsDataURL(event.target.files[0]);
              reader.onload = () => {
                const base64data = reader.result;
                setValue(base64data);
              };
            }
          }
        }}
      />
      {
        field?.name && (
          <img
            src={value}
            alt={value}
            width={400}
            style={{ marginTop: '3vh', overflow: 'hidden' }}
          />
        )
      }
    </FormControl>

  );
};

Fileupload.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};

export default Fileupload;
