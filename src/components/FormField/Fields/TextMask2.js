/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable object-curly-newline */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */

import { TextField as BaseTextField } from 'formik-material-ui';
import { FastField } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import React from 'react';
import ReactNumberFormat from 'react-number-format';

function FormatedInput(props) {
  const { inputRef, onChange, onBlur, name, ...other } = props;
  const fiOnBlur = () => onBlur();
  const fiOnChange = ({ value }) => onChange(value);
  return (
    <ReactNumberFormat
      name={name}
      {...other}
      getInputRef={inputRef}
      onValueChange={fiOnChange}
      onBlur={fiOnBlur}
      thousandSeparator
      isNumericString
      prefix="Rp. "
    />
  );
}

const TextField = (props) => {
  const { field, form } = props;
  const { setFieldValue, setFieldTouched } = form;
  const { onChange, onBlur, name, ...restField } = field;
  const fieldChange = (e) => {
    setFieldValue(name, e, false);
    setFieldTouched();
  };
  const fieldBlur = () => onBlur(name);
  const newProps = { ...props, field: { name, ...restField, onChange: fieldChange, onBlur: fieldBlur } };
  return (
    <BaseTextField
      {...newProps}
    />
  );
};

const Component = ({ label, name, maxLength, prefix }) => (
  <FormControl fullWidth>
    <FastField
      component={TextField}
      name={name}
      spellCheck={false}
      label={label}
      InputProps={{
        inputComponent: FormatedInput
      }}
      inputProps={{ maxLength, prefix }}
    />
  </FormControl>
);

Component.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  maxLength: PropTypes.number,
  prefix: PropTypes.string
};

Component.defaultProps = {
  maxLength: 20,
  prefix: 'Rp. '
};

export default Component;
