/* eslint-disable object-curly-newline */
/* eslint-disable react/jsx-props-no-spreading */

import { Select as BaseSelect } from 'formik-material-ui';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import React, { createElement } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import map from 'lodash.map';

function BaseSelect2() {
  return (<BaseSelect />);
}

const Select = ({ label, name, id, items }) => {
  const Items = item => createElement(MenuItem, { key: item.idx, children: item.label, ...item });
  return (
    <FormControl fullWidth>
      <InputLabel htmlFor={`${name}-${id}`}>{label}</InputLabel>
      <Field
        component={BaseSelect2}
        name={name}
        inputProps={{
          id: `${name}-${id}`,
        }}
      >
        {map(items, (item, idx) => Items({ idx, ...item }))}
      </Field>
    </FormControl>
  );
};

Select.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Select;
