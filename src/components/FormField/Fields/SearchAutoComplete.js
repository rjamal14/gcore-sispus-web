/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { Field, useField } from 'formik';
import PropTypes from 'prop-types';
import { Autocomplete } from 'formik-material-ui-lab';
import { TextField } from '@material-ui/core';

const SearchAutoComplete = ({ name, label, api, fieldProps, value }) => {
  const [collection, setCollection] = useState([]);
  const [loading, setLoading] = useState(false);

  const query = async (params) => {
    try {
      setLoading(true);

      const res = await api(params);

      if (res.status === 200) {
        setLoading(false);
        const { data: { data } } = res;
        setCollection(data);
      }
    } catch (error) {
      setLoading(false);
      return error;
    }
  };

  useEffect(() => {
    query();
  }, []);

  return (
    <div>
      <Field
        name={name}
        component={Autocomplete}
        options={collection}
        getOptionLabel={(option) => option.name || ''}
        defaultValue={value}
        {...fieldProps}
        renderInput={(params) => (
          <TextField
            {...params}
            label={loading ? 'sedang mencari data ... ' : label}
            variant="outlined"
            onChange={e => query({ search: e.target.value })}
            style={{ marginBottom: 16 }}
          />
        )}
      />
    </div>
  );
};

SearchAutoComplete.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default SearchAutoComplete;
