/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */

import React, { useState } from 'react';
import { useAsync } from 'react-async-hook';
import MUISelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

function BaseSelect(props) {
  const { label, field, form, apiCall, column, itemDisplay } = props;
  const { name, onBlur: fieldOnBlur } = field;
  const { values } = form;
  const items = useAsync(apiCall, []);
  const [selected, setSelected] = useState(values[name] || '');
  const [emptyOption, setEmptyOption] = useState('\u00A0');

  function onChange(e) {
    setSelected(e.target.value);
    form.setFieldValue(field.name, e.target.value, false);
    form.setFieldTouched(field.name, true);
  }

  function onOpen() {
    setEmptyOption(label);
  }

  function onClose() {
    setEmptyOption('\u00A0');
  }

  function onBlur() {
    fieldOnBlur(name);
  }

  return (
    <MUISelect onBlur={onBlur} variant="outlined" displayEmpty value={items.result ? selected : ''} onChange={onChange} onOpen={onOpen} onClose={onClose}>
      <MenuItem value="" style={{ color: 'grey' }}>{emptyOption}</MenuItem>
      {items.result && items.result.data.data.map(
        (item) => (
          <MenuItem key={item.id} value={item.id}>
            {(column && item[column]) || (itemDisplay && itemDisplay(item)) || item.name}
          </MenuItem>
        )
      )}
    </MUISelect>
  );
}

export default function Component(props) {
  const { label } = props;
  return (
    <div>
      <FormControl fullWidth>
        <InputLabel>{label}</InputLabel>
        <Field
          {...props}
          component={BaseSelect}
        />
      </FormControl>
    </div>
  );
}
