/* eslint-disable object-curly-newline */
/* eslint-disable react/prop-types */

import React from 'react';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';

const FormikDatePicker = ({
  views,
  format,
  label,
  form: { setFieldValue, setFieldTouched },
  field: { onBlur, name, value }
}) => {
  const fieldChange = (e) => {
    const newValue = e; // dayjs(e).isValid ? dayjs(e).format(format) : null;
    setFieldValue(name, newValue, false);
    setFieldTouched(name, true);
  };
  const fieldBlur = () => onBlur(name);
  return (
    <KeyboardDatePicker
      name={name}
      clearable
      label={label}
      format={format}
      placeholder={label}
      mask={[]}
      onChange={fieldChange}
      value={value}
      onBlur={fieldBlur}
      animateYearScrolling={false}
      views={views}
    />
  );
};

function Component({ label, name, format, views }) {
  return (
    <FormControl fullWidth>
      <Field
        component={FormikDatePicker}
        name={name}
        label={label}
        format={format}
        views={views}
      />
    </FormControl>
  );
}
Component.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  format: PropTypes.string
};

Component.defaultProps = {
  format: 'YYYY-MM-DD'
};
export default Component;
