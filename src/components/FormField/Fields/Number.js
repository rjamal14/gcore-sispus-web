/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable object-curly-newline */
/* eslint-disable react/prop-types */

import { TextField as BaseTextField } from 'formik-material-ui';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import React from 'react';

const TextField = (props) => {
  const { field, maxDigit } = props;
  const { value } = field;
  let length = 0;
  if (value && value.length) {
    length = value.length;
  }
  const maxLength = maxDigit || 50;
  return (
    <BaseTextField
      inputProps={{
        maxLength
      }}
      helperText={`${length}/${maxLength}`}
      {...props}
    />
  );
};

const FieldText = ({ label, name, fieldProps }) => (
  <FormControl fullWidth style={{ marginBottom: 8 }}>
    <Field
      component={TextField}
      name={name}
      spellCheck={false}
      label={label}
      variant="outlined"
      {...fieldProps}
    />
  </FormControl>
);

FieldText.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default FieldText;
