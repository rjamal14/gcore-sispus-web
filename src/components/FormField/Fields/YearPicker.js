/* eslint-disable object-curly-newline */
/* eslint-disable react/prop-types */

import React, { useState } from 'react';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import dayjs from 'dayjs';

const FormikDatePicker = ({
  format,
  label,
  form: { setFieldValue, setFieldTouched },
  field: { onBlur, name, value }
}) => {
  const parsedValue = dayjs().set('year', `${value}`);
  const validated = (value && parsedValue.isValid()) ? parsedValue : null;
  const [selected, setSelected] = useState(validated);

  const fieldChange = (e) => {
    const val = e ? e.format(format) : null;
    setSelected(val);
    setFieldValue(name, val, false);
    setFieldTouched(name, true);
  };
  const fieldBlur = () => onBlur(name);
  return (
    <KeyboardDatePicker
      views={['year']}
      name={name}
      clearable
      label={label}
      format={format}
      placeholder={label}
      mask={[]}
      onChange={fieldChange}
      value={selected}
      onBlur={fieldBlur}
      animateYearScrolling={false}
    />
  );
};

function Component({ label, name, format }) {
  return (
    <FormControl fullWidth>
      <Field
        component={FormikDatePicker}
        name={name}
        label={label}
        format={format}
      />
    </FormControl>
  );
}
Component.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  format: PropTypes.string
};

Component.defaultProps = {
  format: 'YYYY'
};
export default Component;
