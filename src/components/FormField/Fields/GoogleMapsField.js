/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import { BeatLoader } from 'react-spinners';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import { FormHelperText, makeStyles, Paper, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { TextField as BaseTextField } from 'formik-material-ui';
import env from '../../../config/env';

const useStyles = makeStyles((theme) => ({
  map: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  autocomplete: {
    zIndex: 1,
    backgroundColor: 'white'
  },
  autocompleteContainer: {
    padding: theme.spacing(1),
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  helperText: {
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: 'rgba(0,0,0,0.8)',
    padding: 4
  }
}));

const TextFieldBase = ({ longlat, ...props }) => {
  const { form, field } = props;
  const { setFieldValue, setFieldTouched } = form;

  useEffect(() => {
    setFieldValue(field.name, longlat);
    setFieldTouched(field.name, true);
  }, [longlat]);

  return (
    <div>
      <BaseTextField
        {...props}
        type="hidden"
      />
    </div>
  );
};

const GoogleMapsField = ({ label, name, fieldProps, value, ...props }) => {
  const { google } = props;
  const [longlat, setLonglat] = useState(value);
  const [loadingQuery, setLoadingQuery] = useState(false);
  const [map, setMap] = useState();
  const [placeCollection, setPlaceCollection] = useState([]);
  const classes = useStyles();

  const getLocation = () => {
    if (navigator.geolocation) {
      // eslint-disable-next-line no-use-before-define
      navigator.geolocation.getCurrentPosition(setPosition);
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  };

  const setPosition = (position) => {
    const lat = position.coords.latitude;
    const lng = position.coords.longitude;
    setLonglat({ lat, lng });
  };

  const mapGeoCoder = (address) => {
    const geocoder = new props.google.maps.Geocoder();

    geocoder.geocode({ address }, (results, status) => {
      if (status === 'OK') {
        const lat = results[0].geometry.location.lat();
        const lng = results[0].geometry.location.lng();
        setLonglat({ lat, lng });
      }
    });
  };

  // eslint-disable-next-line no-unused-vars
  const onMapClicked = (mapProps, map, e) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();

    setLonglat({ lat, lng });
  };

  const getPlaces = (event) => {
    const request = {
      input: event.target.value,
      types: 'geocode',
    };

    const service = new google.maps.places.AutocompleteService(map);
    setLoadingQuery(true);
    service.getQueryPredictions(request, (results, status) => {
      if (status === 'OK') {
        setPlaceCollection(results);
        setLoadingQuery(false);
      }
    });
  };

  useEffect(() => {
    value === '' && getLocation();
  }, [value]);

  return (
    <Paper
      className={classes.map}
    >
      <FormControl fullWidth style={{ marginBottom: 8 }}>
        {
          longlat
            ? (
              <Map
                google={google}
                zoom={14}
                style={{ height: 500 }}
                initialCenter={longlat}
                center={longlat}
                onDblclick={onMapClicked}
                onReady={(mapProps, map) => setMap(map)}
                mapTypeControl={false}
                fullscreenControl={false}
                disableDoubleClickZoom
              >
                <Marker
                  title="Lokasi anda saat ini"
                  name="lokasi"
                  position={longlat}
                />
              </Map>
            )
            : <BeatLoader />
        }
      </FormControl>

      <div className={classes.autocompleteContainer}>
        <FormControl fullWidth>
          <Autocomplete
            spellCheck={false}
            label={label}
            variant="outlined"
            loading={loadingQuery}
            onChange={(e, value, reason) => reason === 'select-option' && mapGeoCoder(value.description)}
            loadingText="Sedang mengambil data..."
            id="place"
            {...fieldProps}
            options={placeCollection}
            getOptionLabel={(option) => option.description}
            className={classes.autocomplete}
            renderInput={(params) => (
              <div>
                <TextField
                  {...params}
                  label="Lokasi"
                  variant="outlined"
                  onChange={getPlaces}
                  placeholder="Ketik Sesuatu"
                />
              </div>
            )}
          />
          <FormHelperText className={classes.helperText}>
            Cari lokasi atau klik dua kali pada peta untuk mendapatkan lokasi
          </FormHelperText>
          <FormHelperText className={classes.helperText}>
            Lat: {longlat.lat}, Lng: {longlat.lng}
          </FormHelperText>
          <Field
            component={TextFieldBase}
            name={name}
            longlat={longlat}
            {...fieldProps}
          />
        </FormControl>
      </div>
    </Paper>
  );
};
GoogleMapsField.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default GoogleApiWrapper({
  apiKey: env.googleMapsApiKey,
})(GoogleMapsField);
