/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { Field, useField } from 'formik';
import PropTypes from 'prop-types';
import { Autocomplete } from 'formik-material-ui-lab';
import { TextField } from '@material-ui/core';

const SearchAutoComplete = ({ label, api, api2, fieldProps, value }) => {
  const [collection, setCollection] = useState([]);
  const [collection2, setCollection2] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loading2, setLoading2] = useState(false);
  const [field2, meta2, helpers2] = useField('classification_id');
  const [field, meta, helpers] = useField('parent_id');

  const query = async (params) => {
    try {
      setLoading(true);

      const res = await api(params);

      if (res.status === 200) {
        setLoading(false);
        const { data: { data } } = res;
        setCollection(data);
      }
    } catch (error) {
      setLoading(false);
      return error;
    }
  };

  const query2 = async (params) => {
    try {
      setLoading2(true);

      const res = await api2(params);

      if (res.status === 200) {
        setLoading2(false);
        const { data: { data } } = res;
        setCollection2(data);
      }
    } catch (error) {
      setLoading2(false);
      return error;
    }
  };

  useEffect(() => {
    query();
    query2();
  }, []);

  return (
    <div>
      <Field
        disabled={field?.value?.id}
        name="classification_id"
        component={Autocomplete}
        options={collection}
        getOptionLabel={(option) => option.name || ''}
        defaultValue={value}
        {...fieldProps}
        renderInput={(params) => (
          <TextField
            {...params}
            label={
              loading ? 'sedang mencari data ... '
                : (field?.value?.id) ? 'Sudah memilih sub-klasifikasi, tidak bisa memilih ini'
                  : 'Klasifikasi'
            }
            variant="outlined"
            onChange={e => query({ search: e.target.value })}
            style={{ marginBottom: 16 }}
          />
        )}
      />
      <Field
        disabled={field2?.value?.id}
        name="parent_id"
        component={Autocomplete}
        options={collection2}
        getOptionLabel={(option) => option.name || ''}
        defaultValue={value}
        {...fieldProps}
        renderInput={(params) => (
          <TextField
            {...params}
            label={
              loading2 ? 'sedang mencari data ... '
                : (field2?.value?.id) ? 'Sudah memilih klasifikasi, tidak bisa memilih ini'
                  : 'Sub Klasifikasi'
            }
            variant="outlined"
            onChange={e => query2({ search: e.target.value })}
            style={{ marginBottom: 16 }}
          />
        )}
      />
    </div>
  );
};

SearchAutoComplete.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default SearchAutoComplete;
