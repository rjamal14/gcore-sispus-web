/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-fragments */
/* eslint-disable react/jsx-props-no-spreading */

import React, { useState, useEffect, useRef } from 'react';
import { useAsyncCallback } from 'react-async-hook';
import MUISelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { Field } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import omitBy from 'lodash.omitby';
import map from 'lodash.map';
import zipObject from 'lodash.zipobject';
import mapValues from 'lodash.mapvalues';
import lvalues from 'lodash.values';
import isEmpty from 'lodash.isempty';

const useIsMounted = () => {
  const isMounted = useRef(false);
  useEffect(() => {
    isMounted.current = true;
    return () => { isMounted.current = false; };
  }, []);
  return isMounted;
};

function Select(props) {
  const isMounted = useIsMounted();
  const { label, field, form, apiCall, dependencyKeys } = props;
  const { name } = field;
  const { values } = form;
  const [items, setItems] = useState([]);
  const [selected, setSelected] = useState('');
  const [emptyOption, setEmptyOption] = useState('\u00A0');

  const q = omitBy(mapValues(zipObject(dependencyKeys, map(dependencyKeys, key => [values[key]])), v => lvalues(omitBy(v, x => x === ''))), x => isEmpty(x));

  function onChange(e) {
    setSelected(e.target.value);
    form.setFieldValue(name, e.target.value, false);
    form.setFieldTouched(name, true);
  }

  function onOpen() {
    setEmptyOption(label);
  }

  function onClose() {
    setEmptyOption('\u00A0');
  }

  const getCol = useAsyncCallback(async () => {
    const result = await apiCall({ q });
    return result;
  });

  async function populate() {
    const result = await getCol.execute();
    if (isMounted.current) {
      setItems(result.data.data);
      setSelected(values[name] || '');
    }
  }

  useEffect(
    () => {
      if (isEmpty(q)) return;
      populate();
    },
    [values]
  );

  return (
    <MUISelect disabled={!(items.length > 0)} displayEmpty value={selected} onChange={onChange} onOpen={onOpen} onClose={onClose}>
      <MenuItem value="" style={{ color: 'grey' }}>{emptyOption}</MenuItem>
      {items.length > 0 && items.map(
        (item) => (
          <MenuItem key={item.id} value={item.id}>
            {item.name}
          </MenuItem>
        )
      )}
    </MUISelect>
  );
}

export default function SelectFromApiWithDependency(props) {
  const { label } = props;
  return (
    <FormControl fullWidth>
      <InputLabel>{label}</InputLabel>
      <Field
        {...props}
        component={Select}
      />
    </FormControl>
  );
}
