/* eslint-disable react/prop-types */
/* eslint-disable flowtype/no-types-missing-file-annotation */
/* eslint-disable no-undef */
/* eslint-disable react/react-in-jsx-scope */
import NumberFormat from 'react-number-format';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import React from 'react';
import { TextField } from '@material-ui/core';

const RenderNumberFormat = (props) => {
  const {
    form: { setFieldValue },
    field: { name, value },
    label,
  } = props;

  const onChange = React.useCallback(
    values => {
      // eslint-disable-next-line no-shadow
      const { value } = values;
      setFieldValue(name, value || '');
    },
    [setFieldValue, name]
  );

  return (
    <NumberFormat
      value={value}
      name={name}
      label={label}
      customInput={TextField}
      displayType="input"
      thousandSeparator="."
      prefix="Rp. "
      decimalSeparator={false}
      onValueChange={onChange}
    />
  );
};

const RupiasFormat = ({ label, name }) => (
  <FormControl fullWidth>
    <Field
      component={RenderNumberFormat}
      name={name}
      spellCheck={false}
      label={label}
    />
  </FormControl>
);

RupiasFormat.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default RupiasFormat;
