import React from 'react';

import KeyboardDatePicker from './Fields/KeyboardDatePicker';
import Text from './Fields/Text';
import TextMask from './Fields/TextMask';
import Select from './Fields/Select';
import SelectGet from './Fields/SelectGet';
import SelectGet2 from './Fields/SelectGet2';
import Switch from './Fields/Switch';

export function NoFieldDefinition() {
  return (<span style={{ color: 'yellow' }}>No Fields Definition, please define the Fields.</span>);
}

const definitions = {
  keyboardDatePicker: KeyboardDatePicker,
  text: Text,
  textMask: TextMask,
  select: Select,
  switch: Switch,
  phone: Switch,
  selectGet: SelectGet,
  selectGet2: SelectGet2,
};
export default definitions;
