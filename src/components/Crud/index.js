/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
/* eslint-disable radix */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import swal from 'sweetalert';
import { useDispatch, useSelector } from 'react-redux';
import { Fab } from '@material-ui/core';
import _ from 'lodash';
import AddIcon from '@material-ui/icons/Add';
import { useHistory, useLocation } from 'react-router-dom';
import qs from 'qs';
import useStyles from './crud-jss';
import MainTable from '../MainTable';
import AddDialogForm from './DialogForm/AddDIalogForm';
import EditDialogForm from './DialogForm/EditFormDialog';
import { ACTIVE } from '../../redux/constants';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const Crud = (props) => {
  const {
    // eslint-disable-next-line react/prop-types
    formComponent,
    columns,
    formField,
    description,
    getApi,
    postApi,
    editApi,
    deleteApi,
    showApi,
    mapParams,
    parseResponse,
    parseId,
    tableTitle,
    accessCreate,
    accessUpdate,
    accessDelete,
    onRowClick,
    customCollection,
  } = props;

  const mapInitValues = () => {
    const mappedValue = {};

    _.map(formField, (item) => _.assign(mappedValue, { [item.name]: item.value })
    );

    return mappedValue;
  };

  const history = useHistory();
  const location = useLocation();
  const query = useQuery();
  const classes = useStyles();
  const [collection, setCollection] = useState([]);
  const { title } = useSelector(state => state.pageTitle);
  const { disabled } = useSelector(state => state.addButton);
  const dispatch = useDispatch();
  const [totalPage, setTotalPage] = useState(0);
  const [loading, setLoading] = useState(false);
  const [crudLoading, setCrudLoading] = useState(false);
  const [showLoading, setShowLoading] = useState(false);
  const [totalData, setTotalData] = useState(0);
  const [newForm, setNewForm] = useState(false);
  const [editForm, setEditForm] = useState(false);
  const [initialValues] = useState(mapInitValues());
  const [editValue, setEditValue] = useState({});
  const [errors, setErrors] = useState();

  const triggerEditValue = (value) => {
    if (mapParams) {
      setEditValue(mapParams(value));
    } else {
      setEditValue(value);
    }
  };

  const onCloseForm = () => {
    editForm && setEditForm(false);
    newForm && setNewForm(false);
    setErrors([]);
    history.push(location.pathname + location.search);
  };

  const triggerEditForm = (value) => setEditForm(value);

  // eslint-disable-next-line no-shadow
  const getData = async (params) => {
    try {
      setLoading(true);
      const res = await getApi(params);

      if (res.status === 200) {
        setLoading(false);
        // eslint-disable-next-line camelcase
        const { data: { data, total_data, total_page } } = res;
        const customData = (customCollection) ? customCollection(data) : data;
        setCollection(customData);
        setTotalData(total_data);
        setTotalPage(total_page);
      }
    } catch (error) {
      setLoading(false);
    }
  };

  const createData = async (params) => {
    try {
      setCrudLoading(true);
      const res = await postApi(params);
      if (res.status === 201) {
        // eslint-disable-next-line no-unused-vars
        const { data: { message, code, status } } = res;
        if (code === 400) {
          setCrudLoading(false);
          setErrors(status);
          swal({
            title: 'Terjadi Kesalahan',
            text: message,
            icon: 'warning',
            buttons: 'OK'
          });
        } else if (code === 201) {
          setCrudLoading(false);
          setNewForm(false);
          getData(qs.parse(location.search, { ignoreQueryPrefix: true }));
          swal({
            title: 'Sukses',
            text: message,
            icon: 'success',
            buttons: 'OK'
          });
        }
      }
    } catch (error) {
      setCrudLoading(false);
      onCloseForm();
      swal({
        title: 'Kesalahan',
        text: 'Maaf, terjadi kesalahan',
        icon: 'error',
        buttons: 'OK'
      });
    }
  };

  const editData = async (id, params) => {
    try {
      setCrudLoading(true);
      const res = await editApi(id, params);

      if (res.status === 200) {
        // eslint-disable-next-line no-unused-vars
        const { data: { message, code, status } } = res;
        if (code === 400) {
          setCrudLoading(false);
          setErrors(status);
          swal({
            title: 'Terjadi Kesalahan',
            text: message,
            icon: 'warning',
            buttons: 'OK'
          });
        } else if (code === 200) {
          setCrudLoading(false);
          onCloseForm();

          getData(qs.parse(location.search, { ignoreQueryPrefix: true }));
          swal({
            title: 'Sukses',
            text: message,
            icon: 'success',
            buttons: 'OK'
          });
        }
      }
    } catch (error) {
      setCrudLoading(false);
      setNewForm(false);
      swal({
        title: 'Kesalahan',
        text: 'Maaf, terjadi kesalahan',
        icon: 'error',
        buttons: 'OK'
      });
    }
  };

  const showData = async (id) => {
    try {
      setShowLoading(true);
      const res = await showApi(id);
      if (res.status === 200) {
        setShowLoading(false);
        const data = parseResponse(res);
        triggerEditValue(data);
      }
    } catch (error) {
      setShowLoading(false);
      swal({
        title: 'Terjadi Kesalahan',
        text: 'Maaf, Terjadi Kesalahan pada sistem',
        icon: 'error'
      });
    }
  };

  const deleteData = async (id) => {
    try {
      const res = await deleteApi(id);

      if (res.status === 200) {
        // eslint-disable-next-line no-unused-vars
        const { message } = res;
        swal({
          title: 'Sukses',
          text: message,
          icon: 'success'
        });
        getData(qs.parse(location.search, { ignoreQueryPrefix: true }));
      }
    } catch (error) {
      swal({
        title: 'Terjadi Kesalahan',
        text: 'Maaf terjadi kesalahan pada sistem',
        icon: 'error'
      });
    }
  };

  const onClickEdit = (row, rowData) => {
    const id = parseId(rowData);
    showData(id);

    if (!editForm) {
      // history.push(`${location.pathname}?id=${rowData.id.$oid || rowData.id}`);
      setEditForm(true);
    }
  };

  const onClickDelete = (row, rowData, action) => {
    const { setFloatingToolbar } = action;
    let id;
    if (Array.isArray(rowData)) {
      id = rowData.join(',');
    } else {
      id = parseId(rowData);
    }

    swal({
      title: 'Hapus Data',
      text: 'Anda yakin akan menghapus data ini ?',
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(async (willDelete) => {
      if (willDelete) {
        history.push(location.pathname + location.search);
        await deleteData(id);
        setFloatingToolbar && setFloatingToolbar([]);
        dispatch({ type: ACTIVE });
      } else {
        history.push(location.pathname + location.search);
        setFloatingToolbar && setFloatingToolbar([]);
        dispatch({ type: ACTIVE });
      }
    });
  };

  useEffect(() => {
    getData(qs.parse(location.search, { ignoreQueryPrefix: true }));
  }, [location.search]);

  return (
    <Fragment>
      <div className={classes.root}>
        <MainTable
          title={tableTitle || title}
          description={description}
          data={collection}
          columns={columns}
          loading={loading}
          editForm={editForm}
          setEditForm={triggerEditForm}
          showData={showData}
          deleteData={deleteData}
          totalPage={totalPage}
          totalData={totalData}
          page={parseInt(query.get('page')) || 1}
          perPage={parseInt(query.get('per_page')) || 10}
          onEdit={onClickEdit}
          onDelete={onClickDelete}
          parseId={parseId}
          accessUpdate={accessUpdate}
          accessDelete={accessDelete}
          disableFilter
          onRowClick={onRowClick}
          {...props}
        />
      </div>
      {accessCreate ? (
        <div className={classes.floatingButton}>
          <Fab
            color="secondary"
            aria-label="add"
            onClick={() => setNewForm(true)}
            disabled={disabled}
          >
            <AddIcon />
          </Fab>
        </div>
      )
        : null}
      <AddDialogForm
        open={newForm}
        onClose={onCloseForm}
        formField={formField}
        createData={createData}
        loading={crudLoading}
        initialValues={initialValues}
        component={formComponent}
        errors={errors}
      />

      <EditDialogForm
        open={editForm}
        onClose={onCloseForm}
        formField={formField}
        editData={editData}
        loading={crudLoading}
        showLoading={showLoading}
        initialValues={editValue}
        component={formComponent}
        errors={errors}
      />
    </Fragment>
  );
};

Crud.defaultProps = ({
  description: '',
  mapParams: null,
  tableTitle: '',
});

Crud.propTypes = ({
  columns: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  formField: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  mapParams: PropTypes.func,
  description: PropTypes.string,
  getApi: PropTypes.func.isRequired,
  editApi: PropTypes.func.isRequired,
  deleteApi: PropTypes.func.isRequired,
  showApi: PropTypes.func.isRequired,
  postApi: PropTypes.func.isRequired,
  parseResponse: PropTypes.func.isRequired,
  parseId: PropTypes.func.isRequired,
  tableTitle: PropTypes.string
});

export default Crud;
