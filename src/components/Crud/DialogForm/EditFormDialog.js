/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Icon,
  IconButton,
  Typography,
  CircularProgress } from '@material-ui/core';
import { Form, Formik } from 'formik';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { Alert } from '@material-ui/lab';
import Components from '../../FormField';
import useStyles from './dialogForm-jss';

const EditDialogForm = (props) => {
  const {
    open,
    onClose,
    editData,
    formField,
    component,
    loading,
    showLoading,
    errors,
    initialValues
  } = props;

  const classes = useStyles();
  const { title } = useSelector(state => state.pageTitle);

  const onSubmit = async (value, action) => {
    await editData(initialValues.id, value);
    action.setSubmitting(false);
  };

  if (showLoading) {
    return (
      <div className={classes.root2}>
        <Dialog
          scroll="paper"
          open
          maxWidth="md"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              padding: '20px',
              borderRadius: '50px',
            }}
          >
            <CircularProgress
              style={{ color: '#C4A643', margin: '18px' }}
              size={40}
            />
            Mohon Tunggu
            <div
              style={{
                marginTop: '10px',
              }}
            />
          </div>
        </Dialog>
      </div>
    );
  }
  return (
    <Dialog
      open={open}
      onClose={onClose}
      maxWidth="md"
      fullWidth
      aria-labelledby="dialog-form"
    >
      <Formik
        initialValues={initialValues}
        enableReinitialize
        onSubmit={onSubmit}
        component={({ submitForm, isSubmitting }) => (
          <Form autoComplete="off" className={classes.container}>
            <div className={classes.dialogTitle}>
              <div className={classes.title}>
                <Typography variant="h5">
                  {`Edit ${title}`}
                </Typography>
              </div>
              <IconButton disabled={(!!loading)} onClick={onClose}>
                <Icon>close</Icon>
              </IconButton>
            </div>
            <DialogContent>
              {
                errors
                  && _.map(Object.keys(errors), (item) => (
                    <div style={{ marginBottom: 8 }}>
                      <Alert severity="error">
                        {`${item} ${errors[item]}`}
                      </Alert>
                    </div>
                  )
                  )
              }
              {
                component ? Components({ component, ...props }) : _.map(formField, (value, id) => Components({ id, ...value }))
              }
            </DialogContent>
            <DialogActions>
              <Button
                color="primary"
                disabled={isSubmitting || showLoading}
                onClick={submitForm}
                variant="contained"
              >
                { loading
                  ? (
                    <CircularProgress
                      style={{
                        color: '#C4A643',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: '5px',
                        marginBottom: '5px',
                      }}
                      size={15}
                    />
                  )
                  : (
                    <Typography variant="button" style={{ color: '#FFFFFF' }}>
                      Simpan
                    </Typography>
                  )}
              </Button>
              <Button
                color="secondary"
                disabled={isSubmitting}
                onClick={onClose}
                variant="outlined"
              >
                <Typography variant="button">
                  Batal
                </Typography>
              </Button>
            </DialogActions>
          </Form>
        )}
      />
    </Dialog>
  );
};

EditDialogForm.defaultProps = ({
  errors: [],
});

EditDialogForm.propTypes = ({
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  initialValues: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  errors: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  editData: PropTypes.func.isRequired,
  formField: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  showLoading: PropTypes.bool.isRequired
});

export default EditDialogForm;
