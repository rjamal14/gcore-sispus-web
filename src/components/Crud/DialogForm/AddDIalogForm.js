/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Icon,
  IconButton,
  Typography,
  CircularProgress } from '@material-ui/core';
import { Form, Formik } from 'formik';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { Alert } from '@material-ui/lab';
import Components from '../../FormField';
import useStyles from './dialogForm-jss';

const AddDialogForm = (props) => {
  const {
    open,
    onClose,
    initialValues,
    createData,
    formField,
    component,
    loading,
    errors
  } = props;

  const classes = useStyles();
  const { title } = useSelector(state => state.pageTitle);

  const onSubmit = async (value, action) => {
    await createData(value);
    action.setSubmitting(false);
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      maxWidth="md"
      fullWidth
      aria-labelledby="dialog-form"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        component={({ submitForm, isSubmitting }) => (
          <Form autoComplete="off" className={classes.container}>

            <div className={classes.dialogTitle}>
              <div className={classes.title}>
                <Typography variant="h5">
                  {`Tambah ${title}`}
                </Typography>
              </div>
              <IconButton disabled={!!loading} onClick={onClose}>
                <Icon>close</Icon>
              </IconButton>
            </div>
            <DialogContent style={{ overflowY: 'hidden' }}>
              {
                errors
                  && _.map(Object.keys(errors), (item) => (
                    <div style={{ marginBottom: 8 }}>
                      <Alert severity="error">
                        {`${item} ${errors[item]}`}
                      </Alert>
                    </div>
                  )
                  )
              }
              {
                component ? Components({ component, ...props }) : _.map(formField, (value, id) => Components({ id, ...value }))
              }
            </DialogContent>
            <DialogActions>
              <Button
                color="primary"
                disabled={isSubmitting}
                onClick={submitForm}
                variant="contained"
              >
                { loading
                  ? (
                    <CircularProgress
                      style={{
                        color: '#C4A643',
                        marginLeft: '18px',
                        marginRight: '18px',
                        marginTop: '5px',
                        marginBottom: '5px',
                      }}
                      size={15}
                    />
                  )
                  : (
                    <Typography variant="button" style={{ color: '#FFFFFF' }}>
                      Simpan
                    </Typography>
                  )}
              </Button>
              <Button
                color="secondary"
                disabled={isSubmitting}
                onClick={onClose}
                variant="outlined"
              >
                <Typography variant="button">
                  Batal
                </Typography>
              </Button>
            </DialogActions>
          </Form>
        )}
      />
    </Dialog>
  );
};

AddDialogForm.defaultProps = ({
  errors: [],
});

AddDialogForm.propTypes = ({
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  initialValues: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  errors: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]),
  createData: PropTypes.func.isRequired,
  formField: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,

});

export default AddDialogForm;
