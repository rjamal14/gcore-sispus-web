/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Icon from '../icon';
import styles from './css_mini';

class SliderBar extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid item xs={9}>
        <ListItem
          component="a"
          href="/customer/individual"
          button
          className={
            this.props.selected === 'customer'
              ? classes.itemAreaSelected
              : classes.itemArea
          }
        >
          <ListItemIcon>
            <img
              src={
                this.props.selected === 'customer'
                  ? Icon.circle_active
                  : Icon.circle
              }
            />
          </ListItemIcon>
          <ListItemText
            className={classes.itemTxt}
            primary={(
              <Typography
                type="body2"
                style={{ color: '#FFFFFF', fontSize: '11px' }}
              >
                Perorangan
              </Typography>
            )}
          />
        </ListItem>
        <ListItem
          component="a"
          href="/customer/companies"
          button
          className={
            this.props.selected === 'companies'
              ? classes.itemAreaSelected
              : classes.itemArea
          }
        >
          <ListItemIcon>
            <img
              src={
                this.props.selected === 'companies'
                  ? Icon.circle_active
                  : Icon.circle
              }
            />
          </ListItemIcon>
          <ListItemText
            className={classes.itemTxt}
            primary={(
              <Typography
                type="body2"
                style={{ color: '#FFFFFF', fontSize: '11px' }}
              >
                Perusahaan
              </Typography>
            )}
          />
        </ListItem>
      </Grid>
    );
  }
}

export default withStyles(styles.CoustomsStyles, { name: 'SliderBar' })(
  SliderBar
);
