import { Typography } from '@material-ui/core';
import React from 'react';
import { BeatLoader } from 'react-spinners';

export const LoaderData = () => (
  <div style={{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  }}
  >
    <Typography variant="button">
      Mohon Tunggu
    </Typography>
    <BeatLoader />
  </div>
);

export default LoaderData;
