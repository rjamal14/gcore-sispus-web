import { combineReducers } from 'redux';
import example from './exampleReducer';
import pageTitle from './pageTitleReducer';
import addButton from './disableAddButtonReducers';
import notification from './notification';

const rootReducer = combineReducers({
  // ADD YOUR REDUCER HERE !!!
  example,
  pageTitle,
  addButton,
  notification
});

export default rootReducer;
