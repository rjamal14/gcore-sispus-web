/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-fallthrough */
const { default: produce } = require('immer');
const { ADD_NEW_NOTIFICATION, REDUCE_NOTIFICATION } = require('../constants');

const initState = {
  notifList: [],
  notifCount: 0,
};

const reducer = (state = initState, action) => produce(
  state, draft => {
    switch (action.type) {
      case ADD_NEW_NOTIFICATION:
        draft.notifList = action.notifList;
        draft.notifCount = action.notifCount;
        break;

      case REDUCE_NOTIFICATION:
        draft.notifCount -= 1;
        draft.notifList = draft.notifList.filter(val => val.id !== action.id);
        break;

      default:
        return state;
    }
  }
);

export default reducer;
