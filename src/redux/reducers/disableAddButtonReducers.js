/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-fallthrough */
import produce from 'immer';
import { ACTIVE, DISABLED } from '../constants';

const initState = {
  disabled: false
};

const reducer = (state = initState, action = {}) => produce(
  state, draft => {
    switch (action.type) {
      case ACTIVE:
        draft.disabled = false;
        break;
      case DISABLED:
        draft.disabled = true;
        break;
      default:
        return state;
    }
  }
);

export default reducer;
