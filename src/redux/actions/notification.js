/* eslint-disable no-unused-vars */
/* eslint-disable import/prefer-default-export */
import swal from 'sweetalert';
import { ADD_NEW_NOTIFICATION, REDUCE_NOTIFICATION } from '../constants';
import { apiWithToken } from '../../config/apiWithToken';

export const addNewNotification = (dispatch, notifList) => {
  dispatch({
    type: ADD_NEW_NOTIFICATION,
    notifList,
    notifCount: notifList.length,
  });
};

export const getNotification = () => async (dispatch) => {
  const url = '/notifications/unread?&per_page=100';

  try {
    const response = await apiWithToken.get(url);
    const { data } = response.data;
    const filteredNotif = data.filter(val => val.type === 'sispus');
    dispatch({
      type: ADD_NEW_NOTIFICATION,
      notifList: filteredNotif,
      notifCount: filteredNotif.length,
    });
  } catch (error) {
    swal(
      '',
      'Terjadi kesalahan pada server',
      'error'
    );
  }
};

export const changeNotifStatus = (id) => async (dispatch) => {
  const url = `/notifications/${id}`;

  try {
    const response = await apiWithToken.get(url);
    dispatch({
      type: REDUCE_NOTIFICATION, id,
    });
  } catch (error) {
    swal(
      '',
      'Terjadi kesalahan pada server',
      'error'
    );
  }
};
